package services;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.Carriers;
import beans.CarriersBean;

@Path("/carriers")

public class CarriersResource 
{
	
	// Get all records:
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })

	public ArrayList<CarriersBean> getAllCarriers() 
	{
		return Carriers.createInstance().getRecords();
	}

	
	// Create new record:
	@POST
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response createPlayer(final CarriersBean carriersBean)
	{
		// TODO: In future, need a DAO-like class to execute SQL
		
		/*
		crud.beans.PlayerBean crudBean = new crud.beans.PlayerBean(playerBean);
		crud.beans.PlayerBean existingPlayerBean = PlayerDAO.getPlayerBeanById(playerBean.getId());
		
		if (existingPlayerBean == null)
		{
			PlayerDAO.insertNewPlayerBean(crudBean);
		}
		else
		{
			PlayerDAO.updatePlayerBean(crudBean);
		}
		*/
		
		return Response.ok(carriersBean).build();
	}

	
	// Get one record:
	@GET @Path("/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })

	public CarriersBean getCarrier(@PathParam("id") int id) 
	{
		return Carriers.createInstance(id).getRecords().get(0);
	}

	
	// Update existing record:
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response updateCarrier(final CarriersBean carrierBean)
	{
		// TODO: In future, need a DAO-like class to execute SQL
		
		/*
		crud.beans.PlayerBean crudBean = new crud.beans.PlayerBean(playerBean);
		crud.beans.PlayerBean existingPlayerBean = PlayerDAO.getPlayerBeanById(playerBean.getId());
		
		if (existingPlayerBean == null)
		{
			PlayerDAO.insertNewPlayerBean(crudBean);
		}
		else
		{
			PlayerDAO.updatePlayerBean(crudBean);
		}
		*/
		
		return Response.ok(carrierBean).build();
	}
	
	
	// Delete existing record:
	@DELETE 
	@Path("/{id}")

	public Response deleteCarrier(@PathParam("id") int id) 
	{
		// TODO: In future, need a DAO-like class to execute SQL
		
		/*
		crud.beans.PlayerBean existingPlayerBean = PlayerDAO.getPlayerBeanById(id);
		
		if (existingPlayerBean != null)
		{
			PlayerDAO.deletePlayerBean(existingPlayerBean);
		}
		*/
		
		return Response.ok().build();
	}

}
