package ui;

import db.DBBroker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;

public class TestGetPlayers
{

    public static ArrayList<String> getPlayers()
    {
        ArrayList<String> players = new ArrayList<String>();

        // load the sqlite-JDBC driver using the current class loader
        // Class.forName("org.sqlite.JDBC");

        Connection connection = null;
        try
        {
            // create a database connection
            // connection = DriverManager.getConnection("jdbc:sqlite:/TennisDb/tennis.db");
            connection = DBBroker.getConnection();
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.


            /*
            statement.executeUpdate("drop table if exists person");
            statement.executeUpdate("create table person (id integer, name string)");
            statement.executeUpdate("insert into person values(1, 'leo')");
            statement.executeUpdate("insert into person values(2, 'yui')");
            */

            ResultSet rs = statement.executeQuery("select name from dadapp_person order by name");

            while(rs.next())
            {
                // read the result set
                players.add(rs.getString("name"));
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBBroker.freeConnection(connection);
            /*
            try
            {
                if(connection != null)
                    connection.close();

            }
            catch(SQLException e)
            {
                // connection close failed.
                System.err.println(e);
            }
            */
        }

        // Collections.sort(players);
        return players;
    }
}
