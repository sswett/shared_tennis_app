package ui;

import beans.EventResponseKeyBean;
import beans.EventResponseNewDataBean;
import dao.EventResponses;
import dao.Events;
import dao.Players;
import utils.DateUtils;
import other.EventResponseIdType;
import other.PlayerResponse;
import other.SessionHelper;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class FullSpreadsheetComponent
{
    private Events events;
    private Players players;
    private boolean isOddRow = true;
    private int[] yesCount;
    private Calendar startDate;
    private int groupId;


    public FullSpreadsheetComponent(HttpSession session)
    {
        startDate = DateUtils.getCalendarAtBeginningOfToday();
        groupId = SessionHelper.getDefaultGroupId(session);
        events = Events.createInstance(startDate, groupId);
        players = Players.createInstance(false, groupId, true);
        yesCount = new int[events.getRecords().size()];

        for (int x = 0; x < yesCount.length; x++)
        {
            yesCount[x] = 0;
        }
    }


    /**
     *
     * @param parameterMap - note the "value" side of this map is a String[] -- even though it often only has one
     * value in it.  Handle with care.
     */
    public static void doSave(Map<String,String[]> parameterMap)
    {
        TreeMap<String,String[]> treeMap = new TreeMap<String,String[]>(parameterMap);
        SortedMap<String,String[]> sortedMap = treeMap.subMap("old", "old~");

        // NOTE: Using a delete-then-add approach because of the way the pre-existing database had its keys
        // structured.

        ArrayList<EventResponseKeyBean> keysToDelete = new ArrayList<EventResponseKeyBean>();
        ArrayList<EventResponseNewDataBean> dataToAdd = new ArrayList<EventResponseNewDataBean>();

        Calendar now = Calendar.getInstance();

        for (String oldKey : sortedMap.keySet())
        {
            String newKey = oldKey.replaceFirst("old", "new");
            String oldValue = parameterMap.get(oldKey)[0];
            String newValue = parameterMap.get(newKey)[0];

            if (!oldValue.equals(newValue))
            {
                // Gather up old responses to delete:

                String playerIdAndEventId = oldKey.replaceFirst("old_", "");
                String[] parts = playerIdAndEventId.split("_");
                int playerId = Integer.parseInt(parts[0]);
                int eventId = Integer.parseInt(parts[1]);
                keysToDelete.add(new EventResponseKeyBean(playerId, eventId));

                // Gather up new responses to add:

                PlayerResponse playerResponse;

                try
                {
                    playerResponse = PlayerResponse.valueOf(newValue);
                }
                catch (IllegalArgumentException e)
                {
                    playerResponse = PlayerResponse.unknown;
                }

                dataToAdd.add(new EventResponseNewDataBean(playerId, eventId, playerResponse, now));
            }
        }

        if (keysToDelete.size() > 0)
        {
            EventResponses.delete(keysToDelete);
        }

        if (dataToAdd.size() > 0)
        {
            EventResponses.add(dataToAdd);
        }
    }


    public Events getEvents()
    {
        return events;
    }


    public int getGroupId()
    {
        return groupId;
    }


    public Players getPlayers()
    {
        return players;
    }


    public EventResponses getEventResponses(int playerId)
    {
        return EventResponses.createInstance(EventResponseIdType.PLAYER, playerId, startDate, groupId);
    }


    public boolean isOddRowNumber()
    {
        boolean isOdd = isOddRow;
        isOddRow = !isOddRow;   // flip value
        return isOdd;
    }


    public String getLeftOrRightEdgeCellClass(int relativeEventNo, int lastRelativeEventNo)
    {
        if (relativeEventNo == 0 && lastRelativeEventNo == 0)
        {
            return "left_edge_cell right_edge_cell";
        }
        else if (relativeEventNo == 0)
        {
            return "left_edge_cell";
        }
        else if (relativeEventNo == lastRelativeEventNo)
        {
            return "right_edge_cell";
        }
        else
        {
            return "";
        }
    }


    public void potentiallyIncrementYesCount(int relativeEventCol,
                                             PlayerResponse response,
                                             boolean isSelected)
    {
        if (isSelected && response == PlayerResponse.yes)
        {
            yesCount[relativeEventCol] = yesCount[relativeEventCol] + 1;
        }
    }


    public int getYesCount(int relativeEventCol)
    {
        return yesCount[relativeEventCol];
    }


}
