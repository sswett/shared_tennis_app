package ui;

import db.DBBroker;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class TestGetEvents
{

    private static DateFormat df = new SimpleDateFormat("yyyy.MM.dd");

    public static ArrayList<String> getEvents()
    {
        ArrayList<String> events = new ArrayList<String>();


        Connection connection = null;
        try
        {
            connection = DBBroker.getConnection();
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            ResultSet rs = statement.executeQuery("select date,description from dadapp_event order by date");

            while(rs.next())
            {
                // read the result set
                /*
                Date date = rs.getDate("date");   // doesn't work with sqlite
                String formattedDate = df.format(date);
                Calendar cal = Calendar.getInstance();
                Date date2 = rs.getDate("date", cal);   // doesn't work either
                */

                String dateStr = rs.getString("date");
                String desc = rs.getString("description");
                events.add(dateStr + " - " + desc);
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBBroker.freeConnection(connection);
        }

        return events;
    }
}
