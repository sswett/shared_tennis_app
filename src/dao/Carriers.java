package dao;

import beans.CarriersBean;
import beans.GroupsBean;
import db.DBBroker;
import db.DBUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Carriers
{
    private ArrayList<CarriersBean> records;


    private Carriers()
    {

    }


    public static Carriers createInstance()
    {
        String qryStmt = "select id, name, domain_name from carriers order by name";
        return loadCarriers(qryStmt);
    }


    public static Carriers createInstance(int id)
    {
        String qryStmt = String.format("select id, name, domain_name from carriers where id = %s order by name", id);
        return loadCarriers(qryStmt);
    }


    private static Carriers loadCarriers(String qryStmt)
    {
        Carriers carriers = new Carriers();
        carriers.records = new ArrayList<CarriersBean>();

        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            rs = statement.executeQuery(qryStmt);

            while(rs.next())
            {
                // read the result set

                int id = rs.getInt("id");
                String name = rs.getString("name");
                String domain_name = rs.getString("domain_name");

                CarriersBean carrier = new CarriersBean(id, name, domain_name);
                carriers.records.add(carrier);
            }
        }
        catch(SQLException e)
        {
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }

        return carriers;
    }


    public ArrayList<CarriersBean> getRecords()
    {
        return records;
    }

}
