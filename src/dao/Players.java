package dao;

import beans.PlayerBean;
import db.DBBroker;
import db.DBUtils;
import other.RatingLevelType;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

public class Players
{
    private ArrayList<PlayerBean> records;


    private Players()
    {

    }


    public static Players createInstance(boolean includeDisabled)
    {
        final int disabledFactor = includeDisabled ? 2 : 1;

        String qryStmt = String.format("select id, name, disabled, phone, email, is_admin, carrier_id, " +
                "rating_level, mbr_type "+
                "from dadapp_person " +
                "where disabled < %s " +
                "order by name ",
                String.valueOf(disabledFactor));

        return loadPlayers(qryStmt, includeDisabled);
    }


    public static Players createInstance(int playerId)
    {
        String qryStmt = String.format("select id, name, disabled, phone, email, is_admin, carrier_id, " +
                "rating_level, mbr_type " +
                "from dadapp_person " +
                "where id = %s " +
                "order by name ",
                String.valueOf(playerId));

        return loadPlayers(qryStmt, false);
    }


    public static Players createInstance(String phoneNumber)
    {
        String qryStmt = String.format("select id, name, disabled, phone, email, is_admin, carrier_id, " +
                "rating_level, mbr_type " +
                "from dadapp_person " +
                "where phone = '%s' " +
                "order by name ",
                phoneNumber);

        return loadPlayers(qryStmt, true);
    }


    public static Players createInstanceForAdmins()
    {
        String qryStmt = "select id, name, disabled, phone, email, is_admin, carrier_id, rating_level, mbr_type " +
                "from dadapp_person " +
                "where is_admin = 1 " +
                "order by name ";

        return loadPlayers(qryStmt, false);
    }


    public static Players createInstance(boolean includeDisabled, int groupId, boolean memberOfGroup)
    {
        final int disabledFactor = includeDisabled ? 2 : 1;

        String qryStmt = String.format("select id, name, disabled, phone, email, is_admin, carrier_id, rating_level, mbr_type " +
                "from dadapp_person p" +
                " where %s exists (select * from group_members m where m.group_id = %s and m.person_id = p.id)" +
                " and disabled < %s" +
                " order by name ",
                memberOfGroup ? "" : "not",
                String.valueOf(groupId),
                String.valueOf(disabledFactor));

        return loadPlayers(qryStmt, includeDisabled);
    }


    public static Players createInstance(boolean includeDisabled, RatingLevelType ratingLevelType)
    {
        final int disabledFactor = includeDisabled ? 2 : 1;

        final String ratingLevelClause = ratingLevelType == RatingLevelType.LEVEL_ANY ? "" :
                "and rating_level = '" + ratingLevelType.getRatingLevel() + "'";

        String qryStmt = String.format("select id, name, disabled, phone, email, is_admin, carrier_id, " +
                "rating_level, mbr_type "+
                "from dadapp_person " +
                "where disabled < %s %s " +
                "order by name ",
                String.valueOf(disabledFactor), ratingLevelClause);

        return loadPlayers(qryStmt, includeDisabled);
    }


    public static Players createInstance(boolean includeDisabled,
                                         RatingLevelType ratingLevelType,
                                         String groupShortName)
    {
        Groups groups = Groups.createInstance(groupShortName);

        final int disabledFactor = includeDisabled ? 2 : 1;

        final String ratingLevelClause = ratingLevelType == RatingLevelType.LEVEL_ANY ? "" :
                "and rating_level = '" + ratingLevelType.getRatingLevel() + "'";

        String qryStmt = String.format("select id, name, disabled, phone, email, is_admin, carrier_id, rating_level, mbr_type " +
                "from dadapp_person p" +
                " where exists (select * from group_members m where m.group_id = '%s' and m.person_id = p.id)" +
                " and disabled < %s %s" +
                " order by name ",
                groups.getRecords().get(0).getId(),
                String.valueOf(disabledFactor),
                ratingLevelClause);

        return loadPlayers(qryStmt, includeDisabled);
    }


    public static Players createInstanceForAllUpcomingEvents(Calendar startDate)
    {
        final String startDateStr = DBUtils.getDbFormatted24HrDateString(startDate.getTime());

        String qryStmt = String.format("select id, name, disabled, phone, email, is_admin, carrier_id, rating_level, mbr_type " +
                "from dadapp_person p" +
                " where disabled = 0 and id in(" +
                "select m.person_id from group_members m where m.group_id in(" +
                "select e.group_id from dadapp_event e where date >= '%s'" +
                ")" +
                ")" +
                " order by p.name",
                String.valueOf(startDateStr));

        return loadPlayers(qryStmt, false);
    }



    private static Players loadPlayers(String qryStmt, boolean includeDisabled)
    {
        Players players = new Players();
        players.records = new ArrayList<PlayerBean>();

        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            rs = statement.executeQuery(qryStmt);

            while(rs.next())
            {
                // read the result set

                int id = rs.getInt("id");
                String name = rs.getString("name");
                boolean disabled = rs.getInt("disabled") == 1;
                String phone = rs.getString("phone");
                String email = rs.getString("email");
                boolean is_admin = rs.getInt("is_admin") == 1;
                int carrier_id = rs.getInt("carrier_id");
                String rating_level = rs.getString("rating_level");
                int mbr_type = rs.getInt("mbr_type");

                PlayerBean player = new PlayerBean(id, name, disabled, phone, email, is_admin, carrier_id,
                        rating_level, mbr_type);

                players.records.add(player);
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }

        return players;
    }



    public ArrayList<PlayerBean> getRecords()
    {
        return records;
    }

}
