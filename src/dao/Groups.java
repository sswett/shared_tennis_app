package dao;

import beans.GroupsBean;
import db.DBBroker;
import db.DBUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Groups
{
    private ArrayList<GroupsBean> records;


    private Groups()
    {

    }


    public static Groups createInstance()
    {
        String qryStmt = "select id, name, is_default, short_name from groups order by is_default DESC, name";
        return loadGroups(qryStmt);
    }


    public static Groups createInstance(int groupId)
    {
        String qryStmt = "select id, name, is_default, short_name from groups where id = " + String.valueOf(groupId);
        return loadGroups(qryStmt);
    }


    public static Groups createInstance(String shortName)
    {
        String qryStmt = "select id, name, is_default, short_name from groups where short_name = '" + shortName + "'";
        return loadGroups(qryStmt);
    }


    public static Groups createInstance(boolean isDefault)
    {
        int isDefaultInt = isDefault ? 1 : 0;

        String qryStmt = "select id, name, is_default, short_name from groups where is_default = " +
                String.valueOf(isDefaultInt) + " order by name";

        return loadGroups(qryStmt);
    }


    private static Groups loadGroups(String qryStmt)
    {
        Groups groups = new Groups();
        groups.records = new ArrayList<GroupsBean>();

        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            rs = statement.executeQuery(qryStmt);

            while(rs.next())
            {
                // read the result set

                int id = rs.getInt("id");
                String name = rs.getString("name");
                boolean isDefault = rs.getInt("is_default") == 1;
                String shortName = rs.getString("short_name");

                GroupsBean group = new GroupsBean(id, name, isDefault, shortName);
                groups.records.add(group);
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }

        return groups;
    }


    public ArrayList<GroupsBean> getRecords()
    {
        return records;
    }

}
