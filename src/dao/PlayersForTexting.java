package dao;

import beans.PlayerBean;
import beans.PlayerForTextingBean;
import db.DBBroker;
import db.DBUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PlayersForTexting
{
    private ArrayList<PlayerForTextingBean> records;


    private PlayersForTexting()
    {

    }


    public static PlayersForTexting createInstance(String[] phoneNumbers, boolean includeDisabled)
    {
        final int disabledFactor = includeDisabled ? 2 : 1;

        StringBuilder sb = new StringBuilder();

        for (int x = 0; x < phoneNumbers.length; x++)
        {
            if (x > 0)
            {
                sb.append(",");
            }

            sb.append("'").append(phoneNumbers[x]).append("'");
        }

        String qryStmt = String.format(
                "select p.id as playerId, p.name, disabled, phone, email, is_admin, carrier_id, domain_name, " +
                "rating_level, mbr_type " +
                "from dadapp_person p left outer join carriers c on p.carrier_id = c.id " +
                "where disabled < %s and carrier_id != 0 and phone in (%s)" +
                "order by p.name ",
                String.valueOf(disabledFactor),
                sb.toString());

        PlayersForTexting players = new PlayersForTexting();
        players.records = new ArrayList<PlayerForTextingBean>();

        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            rs = statement.executeQuery(qryStmt);

            while(rs.next())
            {
                // read the result set

                int id = rs.getInt(1);
                String name = rs.getString(2);
                boolean disabled = rs.getInt(3) == 1;
                String phone = rs.getString(4);
                String email = rs.getString(5);
                boolean is_admin = rs.getInt(6) == 1;
                int carrier_id = rs.getInt(7);
                String domain_name = rs.getString(8);
                String rating_level = rs.getString(9);
                int mbr_type = rs.getInt(10);

                PlayerForTextingBean player = new PlayerForTextingBean(id, name, disabled, phone, email,
                        is_admin, carrier_id, domain_name, rating_level, mbr_type);

                players.records.add(player);
            }
        }
        catch(SQLException e)
        {
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }

        return players;
    }



    public ArrayList<PlayerForTextingBean> getRecords()
    {
        return records;
    }

}
