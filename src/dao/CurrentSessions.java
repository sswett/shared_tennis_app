package dao;

import beans.CurrentSessionsBean;
import beans.GroupsBean;
import db.DBBroker;
import db.DBUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CurrentSessions
{
    private ArrayList<CurrentSessionsBean> records;


    private CurrentSessions()
    {

    }


    public static CurrentSessions createInstance()
    {
        String qryStmt = "select id, yr, sess, group_short_name_all, group_short_name_4_0, group_short_name_3_5" +
                " from current_session order by yr, sess";

        return loadCurrentSessions(qryStmt);
    }


    public static CurrentSessions createInstance(int currentSessionId)
    {
        String qryStmt = "select id, yr, sess, group_short_name_all, group_short_name_4_0, group_short_name_3_5" +
                " from current_session where id = " + currentSessionId +
                " order by yr, sess";

        return loadCurrentSessions(qryStmt);
    }


    private static CurrentSessions loadCurrentSessions(String qryStmt)
    {
        CurrentSessions sessions = new CurrentSessions();
        sessions.records = new ArrayList<CurrentSessionsBean>();

        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            rs = statement.executeQuery(qryStmt);

            while(rs.next())
            {
                // read the result set

                int id = rs.getInt("id");
                int yr = rs.getInt("yr");
                int sess = rs.getInt("sess");
                String group_short_name_all = rs.getString("group_short_name_all");
                String group_short_name_4_0 = rs.getString("group_short_name_4_0");
                String group_short_name_3_5 = rs.getString("group_short_name_3_5");

                CurrentSessionsBean sessBean = new CurrentSessionsBean(id, yr, sess,
                        group_short_name_all, group_short_name_4_0, group_short_name_3_5);

                sessions.records.add(sessBean);
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }

        return sessions;
    }


    public ArrayList<CurrentSessionsBean> getRecords()
    {
        return records;
    }

}
