package dao;

import db.DBBroker;
import db.DBUtils;
import other.EventResponseIdType;
import other.GroupMembersIdType;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GroupMembers
{

    public static void add(int groupId, ArrayList<Integer> playerIds)
    {
        Connection connection = null;
        ResultSet rs = null;
        Statement statement = null;

        StringBuilder sb = new StringBuilder();

        boolean first = true;

        for (int playerId : playerIds)
        {
            if (!first)
            {
                sb.append(",");
            }

            // Example: (null, 2, 43),

            sb.append("(null,").
                    append(groupId).append(",").
                    append(playerId).append(")");

            first = false;
        }

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            boolean result = statement.execute( "insert into group_members values " + sb.toString() + ";" );
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }
    }


    public static void delete(int groupId, ArrayList<Integer> playerIds)
    {
        Connection connection = null;
        ResultSet rs = null;
        Statement statement = null;

        StringBuilder sb = new StringBuilder();

        boolean first = true;

        for (Integer playerId : playerIds)
        {
            if (!first)
            {
                sb.append(" or ");
            }

            sb.append("person_id = ").append(playerId);

            first = false;
        }

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            boolean result = statement.execute( "delete from group_members " +
                    "where group_id = " + String.valueOf(groupId) + " and (" + sb.toString() + ")" );
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }
    }


    public static void delete(int id, GroupMembersIdType idType)
    {
        Connection connection = null;
        ResultSet rs = null;
        Statement statement = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            boolean result = statement.execute( "delete from group_members where " +
                    idType.getColName() + " = " + id );
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }
    }


}
