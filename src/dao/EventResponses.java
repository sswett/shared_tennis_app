package dao;

import beans.EventResponseJoinedDataBean;
import beans.EventResponseKeyBean;
import beans.EventResponseNewDataBean;
import db.DBBroker;
import db.DBUtils;
import utils.DateUtils;
import other.EventResponseIdType;
import other.PlayerResponse;
import other.RenderedDateFormat;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class EventResponses
{
    final private static SimpleDateFormat formatForInsert =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private ArrayList<EventResponseJoinedDataBean> records;


    private EventResponses()
    {

    }


    public static EventResponses createInstance(EventResponseIdType eventResponseIdType,
                                                int playerOrEventId, int groupId)
    {
        Calendar cal = DateUtils.getCalendarOldestThisMillenium();
        return createInstance(eventResponseIdType, playerOrEventId, cal, groupId);
    }


    public static EventResponses createInstance(EventResponseIdType eventResponseIdType,
                                                int playerOrEventId, Calendar startDate, int groupId)
    {
        EventResponses eventResponses = new EventResponses();
        eventResponses.records = new ArrayList<EventResponseJoinedDataBean>();

        final String startDateStr = DBUtils.getDbFormatted24HrDateString(startDate.getTime());

        Connection connection = null;
        ResultSet rs = null;
        Statement statement = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            String qryStmt;

            if (eventResponseIdType.isPlayer())
            {
                qryStmt = "select a.id, p.id as person_id, e.id as event_id, e.date, e.description, " +
                        "a.can_make_it, a.date_answered, p.name as player_name, " +
                        "p.phone as player_phone, p.email as player_email, e.group_id, g.short_name, " +
                        "p.mbr_type as player_mbr_type " +
                        " from dadapp_event e, " +
                        "groups g, " +   // added
                        "dadapp_person p left outer join dadapp_attendance a" +
                        " on a.event_id = e.id and a.person_id = p.id" +
                        " where p.id = " + playerOrEventId + " and p.disabled = 0 and e.group_id = " + groupId +
                        " and g.id = e.group_id " +   // added
                        " and e.date >= '" + startDateStr + "'" +
                        " and exists (select * from group_members m where m.group_id = e.group_id and m.person_id = p.id)" +
                        " order by e.date";
            }
            else   /* eventResponseIdType must be event */
            {
                qryStmt = "select a.id, p.id as person_id, e.id as event_id, e.date, e.description, " +
                        "a.can_make_it, a.date_answered, p.name as player_name, " +
                        "p.phone as player_phone, p.email as player_email, e.group_id, g.short_name, " +
                        "p.mbr_type as player_mbr_type " +
                        " from dadapp_event e, " +
                        "groups g, " +   // added
                        "dadapp_person p left outer join dadapp_attendance a" +
                        " on a.event_id = e.id and a.person_id = p.id" +
                        " where e.id = " + playerOrEventId + " and p.disabled = 0 and e.group_id = " + groupId +
                        " and g.id = e.group_id " +   // added
                        " and exists (select * from group_members m where m.group_id = e.group_id and m.person_id = p.id)" +
                        " order by p.name";
            }

            rs = statement.executeQuery(qryStmt);

            while(rs.next())
            {
                // read the result set

                // rs.getInt returns 0 if null in db
                // rs.getString will return null if null in db

                int id = rs.getInt(1);
                int dbPlayerId = rs.getInt(2);
                int eventId = rs.getInt(3);
                // Calendar eventDate = DateUtils.getDateAsCalendar(rs.getString(4), RenderedDateFormat.yyyyMMdd_dash_sep);
                Calendar eventDate = DBUtils.getDateFromResultSetString(rs.getString(4));
                String eventDescr = rs.getString(5);
                String responseValue = rs.getString(6);

                PlayerResponse playerResponse;

                if (responseValue == null)
                {
                    playerResponse = PlayerResponse.unknown;
                }
                else
                {
                    try
                    {
                        playerResponse = PlayerResponse.valueOf(responseValue);
                    }
                    catch (IllegalArgumentException e)
                    {
                        playerResponse = PlayerResponse.unknown;
                    }
                }

                // Calendar responseDate = DateUtils.getDateAsCalendar(rs.getString(7), RenderedDateFormat.yyyyMMdd_dash_sep);
                Calendar responseDate = DBUtils.getDateFromResultSetString(rs.getString(7));

                String playerName = rs.getString(8);
                String playerPhone = rs.getString(9);
                String playerEmail = rs.getString(10);
                // groupID is 11; don't need to fetch because passed
                String groupShortName = rs.getString(12);
                int playerMbrType = rs.getInt(13);

                EventResponseJoinedDataBean event =
                        new EventResponseJoinedDataBean(id, dbPlayerId, eventId, eventDate, eventDescr,
                                playerResponse, responseDate, groupId, playerName, playerPhone, playerEmail,
                                groupShortName, playerMbrType);

                eventResponses.records.add(event);
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }

        return eventResponses;
    }


    // This flavor returns responses for ALL groups/teams
    public static EventResponses createInstance(EventResponseIdType eventResponseIdType,
                                                int playerOrEventId, Calendar startDate)
    {
        EventResponses eventResponses = new EventResponses();
        eventResponses.records = new ArrayList<EventResponseJoinedDataBean>();

        final String startDateStr = DBUtils.getDbFormatted24HrDateString(startDate.getTime());

        Connection connection = null;
        ResultSet rs = null;
        Statement statement = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            String qryStmt;

            if (eventResponseIdType.isPlayer())
            {
                qryStmt = "select a.id, p.id as person_id, e.id as event_id, e.date, e.description, " +
                        "a.can_make_it, a.date_answered, p.name as player_name, " +
                        "p.phone as player_phone, p.email as player_email, e.group_id, g.short_name, " +
                        "p.mbr_type as player_mbr_type " +
                        " from dadapp_event e, " +
                        "groups g, " +   // added
                        "dadapp_person p left outer join dadapp_attendance a" +
                        " on a.event_id = e.id and a.person_id = p.id" +
                        " where p.id = " + playerOrEventId + " and p.disabled = 0 " +
                        " and g.id = e.group_id " +   // added
                        " and e.date >= '" + startDateStr + "'" +
                        " and exists (select * from group_members m where m.group_id = e.group_id and m.person_id = p.id)" +
                        " order by e.date";
            }
            else   /* eventResponseIdType must be event */
            {
                qryStmt = "select a.id, p.id as person_id, e.id as event_id, e.date, e.description, " +
                        "a.can_make_it, a.date_answered, p.name as player_name, " +
                        "p.phone as player_phone, p.email as player_email, e.group_id, g.short_name, " +
                        "p.mbr_type as player_mbr_type " +
                        " from dadapp_event e, " +
                        "groups g, " +   // added
                        "dadapp_person p left outer join dadapp_attendance a" +
                        " on a.event_id = e.id and a.person_id = p.id" +
                        " where e.id = " + playerOrEventId + " and p.disabled = 0 " +
                        " and g.id = e.group_id " +   // added
                        " and exists (select * from group_members m where m.group_id = e.group_id and m.person_id = p.id)" +
                        " order by p.name";
            }

            rs = statement.executeQuery(qryStmt);

            while(rs.next())
            {
                // read the result set

                // rs.getInt returns 0 if null in db
                // rs.getString will return null if null in db

                int id = rs.getInt(1);
                int dbPlayerId = rs.getInt(2);
                int eventId = rs.getInt(3);
                // Calendar eventDate = DateUtils.getDateAsCalendar(rs.getString(4), RenderedDateFormat.yyyyMMdd_dash_sep);
                Calendar eventDate = DBUtils.getDateFromResultSetString(rs.getString(4));
                String eventDescr = rs.getString(5);
                String responseValue = rs.getString(6);

                PlayerResponse playerResponse;

                if (responseValue == null)
                {
                    playerResponse = PlayerResponse.unknown;
                }
                else
                {
                    try
                    {
                        playerResponse = PlayerResponse.valueOf(responseValue);
                    }
                    catch (IllegalArgumentException e)
                    {
                        playerResponse = PlayerResponse.unknown;
                    }
                }

                // Calendar responseDate = DateUtils.getDateAsCalendar(rs.getString(7), RenderedDateFormat.yyyyMMdd_dash_sep);
                Calendar responseDate = DBUtils.getDateFromResultSetString(rs.getString(7));

                String playerName = rs.getString(8);
                String playerPhone = rs.getString(9);
                String playerEmail = rs.getString(10);
                int groupId = rs.getInt(11);
                String groupShortName = rs.getString(12);
                int playerMbrType = rs.getInt(13);

                EventResponseJoinedDataBean event =
                        new EventResponseJoinedDataBean(id, dbPlayerId, eventId, eventDate, eventDescr,
                                playerResponse, responseDate, groupId, playerName, playerPhone, playerEmail,
                                groupShortName, playerMbrType);

                eventResponses.records.add(event);
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }

        return eventResponses;
    }


    public static void delete(ArrayList<EventResponseKeyBean> eventResponseKeys)
    {
        Connection connection = null;
        ResultSet rs = null;
        Statement statement = null;

        StringBuilder sb = new StringBuilder();

        boolean first = true;

        for (EventResponseKeyBean key : eventResponseKeys)
        {
            if (!first)
            {
                sb.append(" or ");
            }

            sb.append("(person_id = ").append(key.getPlayerId()).
                    append(" and event_id = ").append(key.getEventId()).append(")");

            first = false;
        }

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            boolean result = statement.execute( "delete from dadapp_attendance where " + sb.toString() );
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }
    }


    public static void delete(int id, EventResponseIdType idType)
    {
        Connection connection = null;
        ResultSet rs = null;
        Statement statement = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            boolean result = statement.execute( "delete from dadapp_attendance where " +
                    idType.getColName() + " = " + id );
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }
    }


    public static void add(ArrayList<EventResponseNewDataBean> eventResponseData)
    {
        Connection connection = null;
        ResultSet rs = null;
        Statement statement = null;

        StringBuilder sb = new StringBuilder();

        boolean first = true;

        for (EventResponseNewDataBean data : eventResponseData)
        {
            if (!first)
            {
                sb.append(",");
            }

            // Example: (null, 41, 10, "prob", "2012-01-01 00:00:00"),

            sb.append("(");

            sb.append("null,").
                    append(data.getPlayerId()).append(",").
                    append(data.getEventId()).append(",").append("'").
                    append(data.getPlayerResponse().name()).append("',");

            final String dateStr = formatForInsert.format(data.getResponseDate().getTime());

            sb.append("'").append(dateStr).append("'");

            sb.append(")");

            first = false;
        }

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            boolean result = statement.execute( "insert into dadapp_attendance values " + sb.toString() + ";" );
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }
    }


    public ArrayList<EventResponseJoinedDataBean> getRecords()
    {
        return records;
    }

}
