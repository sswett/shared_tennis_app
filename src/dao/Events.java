package dao;

import beans.EventBean;
import db.DBBroker;
import db.DBUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

public class Events
{
    private ArrayList<EventBean> records;


    private Events()
    {

    }


    public static Events createInstance(Calendar startDate, int groupId)
    {
        final String startDateStr = DBUtils.getDbFormatted24HrDateString(startDate.getTime());

        final String qryStmt = "select id, date, description, group_id from dadapp_event" +
                    " where group_id = " + groupId + " and date >= '" + startDateStr + "'" +
                    " order by date";

        return loadRecords(qryStmt);
    }


    public static Events createInstance(int eventId)
    {
        final String qryStmt = "select id, date, description, group_id from dadapp_event" +
                    " where id = " + eventId;

        return loadRecords(qryStmt);
    }


    private static Events loadRecords(String qryStmt)
    {
        Events events = new Events();
        events.records = new ArrayList<EventBean>();

        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            rs = statement.executeQuery(qryStmt);

            while(rs.next())
            {
                // read the result set

                Calendar cal = DBUtils.getDateFromResultSetString(rs.getString("date"));
                int id = rs.getInt("id");
                String descr = rs.getString("description");
                int groupId = rs.getInt("group_id");

                EventBean event = new EventBean(id, cal, descr, groupId);
                events.records.add(event);
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.out.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }

        return events;
    }


    public ArrayList<EventBean> getRecords()
    {
        return records;
    }

}
