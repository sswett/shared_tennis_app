package services_web;

import java.util.ArrayList;
import java.util.Calendar;
import beans.EventBean;
import dao.Events;


public class EventWebService 
{
	private final Calendar startDate = Calendar.getInstance();
	private final Calendar endDate = Calendar.getInstance();

	
	public String[] getEventsForYear(int year, int groupId, String cacheUniqueifier)
	{
		startDate.set(year, 0, 1, 0, 0, 0);   // Jan 1, 00:00:00
		endDate.set(year, 11, 31, 23, 59, 59);   // Dec 31, 23:59:59

		final ArrayList<EventBean> events = Events.createInstance(startDate, groupId).getRecords();
		
		final ArrayList<String> resultsList = new ArrayList<String>();
		
		if (events.size() == 0)
		{
			resultsList.add("No events found");
		}
		else
		{
			for (final EventBean event : events)
			{
				if (event.getDate().after(endDate))
				{
					break;
				}
				
				final String eventInfo = event.getDateFormattedForColumnHeader() + " " + event.getDescription();
				resultsList.add(eventInfo);
			}
		}

		return resultsList.toArray(new String[resultsList.size()]);
	}
	
}
