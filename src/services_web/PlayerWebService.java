
package services_web;

import java.util.ArrayList;

import beans.PlayerBean;
import dao.Players;

public class PlayerWebService {

	public String lookupNameByPhone(String phoneNumber, String cacheUniqueifier) 
	{

		ArrayList<PlayerBean> players = Players.createInstance(phoneNumber).getRecords();
		return players.size() == 0 ? "Player not found" : players.get(0).getName();

	}

}
