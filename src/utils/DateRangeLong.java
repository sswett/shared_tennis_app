package utils;

public class DateRangeLong
{
    private long startDate;
    private long endDate;


    private DateRangeLong()
    {
        // no can do
    }


    public DateRangeLong(long startDate, long endDate)
    {
        this.startDate = startDate;
        this.endDate = endDate;
    }


    public long getStartDate()
    {
        return startDate;
    }


    public long getEndDate()
    {
        return endDate;
    }
}
