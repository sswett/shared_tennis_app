package utils;

import other.RenderedDateFormat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils
{

    private static SimpleDateFormat DATE_FMT_WEEKDAY_NAME_MONTH_NAME_NO_TIME = new SimpleDateFormat("EEE MMM d");
    private static SimpleDateFormat DATE_FMT_WEEKDAY_NAME_MONTH_NAME_WITH_TIME = new SimpleDateFormat("EEE MMM d h:mm a");
    private static final DateFormat EXPIRES_HEADER_DATE_FMT = getExpiresHeaderDateFormat();


    public static Calendar getDateAsCalendar(String dateIn, RenderedDateFormat df)
    {
        Calendar cal = Calendar.getInstance();

        if (dateIn == null)
        {
            cal.setTime(new Date(0));
            return cal;
        }

        try
        {
            cal.setTime(df.get().parse(dateIn));
        }
        catch (Exception ex)
        {
        }
        finally
        {
            return cal;
        }
    }


    public static Date getDateAtBeginningOfToday()
    {
        return getCalendarAtBeginningOfToday().getTime();
    }


    public static Calendar getCalendarAtBeginningOfToday()
    {
        final Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal;
    }


    public static Calendar getCalendarOldestThisMillenium()
    {
        final Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2000);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal;
    }


    public static String getDateFormattedForColumnHeader(Calendar cal)
    {
        Date date = cal.getTime();

        if (cal.get(Calendar.HOUR_OF_DAY) > 0)
        {
            return DATE_FMT_WEEKDAY_NAME_MONTH_NAME_WITH_TIME.format(date);
        }
        else
        {
            return DATE_FMT_WEEKDAY_NAME_MONTH_NAME_NO_TIME.format(date);
        }
    }


    private static DateFormat getExpiresHeaderDateFormat()
    {
        DateFormat httpDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        httpDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return httpDateFormat;
    }


    public static String getDateInExpiresHeaderFormat(Date date)
    {
        return EXPIRES_HEADER_DATE_FMT.format(date);
    }


    public static DateRangeLong getCurrentWeekRange()
    {
        Calendar cal = Calendar.getInstance();
        final int daysFromSunday = cal.get(Calendar.DAY_OF_WEEK) - cal.getFirstDayOfWeek();
        cal.add(Calendar.DATE, -daysFromSunday);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        final long currentWeekStartTime = cal.getTime().getTime();

        cal.add(Calendar.DATE, 6);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        final long currentWeekEndTime = cal.getTime().getTime();

        return new DateRangeLong(currentWeekStartTime, currentWeekEndTime);
    }

}
