package utils;

import org.apache.commons.codec.binary.Base64;

import java.util.ArrayList;

public class StringUtils
{

    public static String join(ArrayList<String> list, String delimiter)
    {
        String[] array = list.toArray(new String[0]);
        return join(array, delimiter);
    }


    public static String join(String[] array, String delimiter)
    {
        StringBuilder sb = new StringBuilder();

        for (int x = 0; x < array.length; x++)
        {
            if (x > 0)
            {
                sb.append(delimiter);
            }

            sb.append(array[x]);
        }

        return sb.toString();
    }


    public static String encodeToBase64(String normalString)
    {
        byte[] encoded = Base64.encodeBase64(normalString.getBytes());
        return new String(encoded);
    }


    public static String decodeFromBase64(String base64String)
    {
        byte[] decoded = Base64.decodeBase64(base64String);
        return new String(decoded);
    }

}
