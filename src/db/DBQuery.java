package db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBQuery
{


    public static void execQuery(String qryStmt)
    {
        Connection connection = null;
        Statement statement = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            statement.execute(qryStmt);
        }
        catch(SQLException e)
        {
            System.err.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(null, statement, connection);
        }
    }


}
