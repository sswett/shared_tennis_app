package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import utils.DateUtils;

public class DBUtils
{

    public static DateFormat dbAmPmDateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
    public static DateFormat dbYyyyMmDdDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    public static DateFormat db24HrDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private static final Calendar DEFAULT_DB_DATE_WHEN_UNREADABLE = getDefaultDbDateWhenUnreadable();


    private static Calendar getDefaultDbDateWhenUnreadable()
    {
    	Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0L);
        return cal;
    }


    public static void cleanupDbConnectionAfterUse(ResultSet rs, Statement stmt, Connection conn)
    {
        if (rs != null)
        {
            try
            {
                rs.close();
            }
            catch (SQLException e)
            {

            }

        }

        if (stmt != null)
        {
            try
            {
                stmt.close();
            }
            catch (SQLException e)
            {

            }

        }

        if (conn != null)
        {
            DBBroker.freeConnection(conn);
        }
    }


    public static Calendar getDateFromResultSetString(String resultSetString)
    {
        // Certain joins will yield a null argument, so deal with it:
    	if (resultSetString == null)
        {
            return getDefaultDbDateWhenUnreadable();
        }
        
    	Calendar cal = Calendar.getInstance();

        try
        {
            cal.setTime(db24HrDateFormatter.parse(resultSetString));
            return cal;
        }
        catch (Exception ex)
        {
            try
            {
                cal.setTime(dbYyyyMmDdDateFormatter.parse(resultSetString));
                return cal;
            }
            catch (Exception e)
            {
                System.out.println("Problem getting date.\n" + ex.getMessage());
            }
        }

        // If it falls through, it's a problem, so provide default:
        return getDefaultDbDateWhenUnreadable();
    }


    public static boolean isSameAsDefaultDbDateWhenUnreadable(Calendar cal)
    {
        return cal.getTimeInMillis() == DEFAULT_DB_DATE_WHEN_UNREADABLE.getTimeInMillis();
    }


    public static String getDbFormatted24HrDateString(Date date)
    {
        return db24HrDateFormatter.format(date.getTime());
    }


    public static String getDbFormattedDateOnlyYyyyMmDdString(Date date)
    {
        return dbYyyyMmDdDateFormatter.format(date.getTime());
    }


    public static String getDbFormattedDateOnlyYyyyMmDdStringAtBeginningOfToday()
    {
        return DBUtils.getDbFormattedDateOnlyYyyyMmDdString(DateUtils.getDateAtBeginningOfToday());
    }


}
