package other;

public class PlayerPerspectiveScores
{
    private int[] gamesWon = { 0, 0, 0 };
    private int[] gamesLost = { 0, 0, 0 };


    public PlayerPerspectiveScores(int[] gamesWon, int[] gamesLost)
    {
        this.gamesWon = gamesWon;
        this.gamesLost = gamesLost;
    }


    public int getGamesWon(int setNo)
    {
        if (setNo >= 1 && setNo <= 3)
        {
            return gamesWon[setNo - 1];
        }

        return 0;
    }


    public int getGamesLost(int setNo)
    {
        if (setNo >= 1 && setNo <= 3)
        {
            return gamesLost[setNo - 1];
        }

        return 0;
    }

}
