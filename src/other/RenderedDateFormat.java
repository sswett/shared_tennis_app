package other;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public enum RenderedDateFormat
{
    yyyyMMdd_dash_sep(new SimpleDateFormat("yyyy-MM-dd"));

    private DateFormat df;


    RenderedDateFormat(DateFormat df)
    {
        this.df = df;
    }


    public DateFormat get()
    {
        return df;
    }

}
