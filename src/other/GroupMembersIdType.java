package other;

public enum GroupMembersIdType
{
    PLAYER("person_id"),
    GROUP("group_id");

    private String colName;


    GroupMembersIdType(String colName)
    {
        this.colName = colName;
    }

    public String getColName()
    {
        return colName;
    }
}
