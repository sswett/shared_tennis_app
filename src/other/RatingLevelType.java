package other;

public enum RatingLevelType
{
    LEVEL_4_0("4.0"),
    LEVEL_3_5("3.5"),
    LEVEL_ANY("ANY");

    private String ratingLevel;


    RatingLevelType(String ratingLevel)
    {
        this.ratingLevel = ratingLevel;
    }


    public String getRatingLevel()
    {
        return ratingLevel;
    }
}
