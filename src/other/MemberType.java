package other;

public enum MemberType
{
    TYPE_0("Not"),
    TYPE_1("Regular"),
    TYPE_2("All Star");

    private String mbrTypeDescr;


    MemberType(String mbrTypeDescr)
    {
        this.mbrTypeDescr = mbrTypeDescr;
    }


    public String getMbrTypeDescr()
    {
        return mbrTypeDescr;
    }
}
