package other;

public enum EventResponseIdType
{
    PLAYER("person_id"),
    EVENT("event_id");

    private String colName;


    EventResponseIdType(String colName)
    {
        this.colName = colName;
    }


    public String getColName()
    {
        return colName;
    }


    public boolean isPlayer()
    {
        return this == EventResponseIdType.PLAYER;
    }


    public boolean isEvent()
    {
        return this == EventResponseIdType.EVENT;
    }
}
