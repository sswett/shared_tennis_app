package other;

public enum View
{
    GROUP("Group/Team"),
    PLAYER("Player"),
    PLAYER_ALL_TEAMS("Player - All Teams");

    private String label;

    View(String label)
    {
        this.label = label;
    }


    public String getLabel()
    {
        return label;
    }


    public boolean isGroupView()
    {
        return this == View.GROUP;
    }


    public boolean isPlayerView()
    {
        return this == View.PLAYER;
    }


    public boolean isPlayerAllTeamsView()
    {
        return this == View.PLAYER_ALL_TEAMS;
    }
}
