package other;

import utils.DateUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CatchAnyExceptionFilter implements Filter
{


    public void init(FilterConfig fc) throws ServletException
    {
    }


    public void doFilter(ServletRequest req,
                         ServletResponse res,
                         FilterChain fc) throws IOException, ServletException
    {
        try
        {
            fc.doFilter(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, null, e);
            HttpServletResponse response = (HttpServletResponse) res;
            response.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }


    public void destroy()
    {
    }

}
