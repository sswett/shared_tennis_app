package other;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DynamicPagesResponseHeaderFilter implements Filter
{

    public void init(FilterConfig fc) throws ServletException
    {
    }


    public void doFilter(ServletRequest req,
                         ServletResponse res,
                         FilterChain fc) throws IOException, ServletException
    {
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Expires", "Mon, 25 Nov 2013 00:00:01 GMT");   // in the past
        fc.doFilter(req, res);
    }


    public void destroy()
    {
    }

}
