package other;

public enum PlayerResponse
{
    unknown(""),
    yes("Yes"),
    no("No"),
    prob("Probably"),
    probnot("Probably not"),
    outdraw("Out - Draw"),
    outlate("Out - Too Late"),
    outvol("Out - Vol"),
    standby("Standby Sub"),
    coach("Coach Only");

    private String label;


    PlayerResponse(String label)
    {
        this.label = label;
    }


    public String getLabel()
    {
        return label;
    }

}
