package other;

import dao.Groups;
import dao.Players;
import servlets.AdminClearance;
import servlets.GroupContactsClearance;
import servlets.SinglesLeagueClearance;
import utils.StringUtils;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.MalformedURLException;
import java.net.URL;

public class SessionHelper
{
    public static final String ADMIN_CLEARANCE_PAGE_PATH = "AdminClearance.page";
    public static final String SINGLES_LEAGUE_CLEARANCE_PAGE_PATH = "SinglesLeagueClearance.page";
    public static final String GROUP_CONTACTS_CLEARANCE_PAGE_PATH = "GroupContactsClearance.page";
    public static final String ERROR_PAGE_PATH = "ErrorPage.jsp";

    public static final String ERROR_PAGE_EXCEPTION_ATTR_KEY = "errorPageExceptionKey";
    public static final String DEFAULT_GROUP_SESS_ATTR_KEY = "defaultGroupKey";
    public static final String DEFAULT_PLAYER_SESS_ATTR_KEY = "defaultPlayerKey";
    public static final String DEFAULT_ADMIN_SESS_ATTR_KEY = "defaultAdminKey";
    public static final String DEFAULT_VIEW_SESS_ATTR_KEY = "defaultViewKey";
    public static final String SHOW_PAST_EVENTS_SESS_ATTR_KEY = "showPastEventsKey";
    public static final String PASSED_ADMIN_SECURITY_CLEARANCE_SESS_ATTR_KEY = "passedClearanceKey";
    public static final String PAGE_AFTER_PWD_SESS_ATTR_KEY = "pageAfterPwdKey";
    public static final String PASSED_SINGLES_LEAGUE_SECURITY_CLEARANCE_SESS_ATTR_KEY = "passedSLClearanceKey";
    public static final String PASSED_GROUP_CONTACTS_SECURITY_CLEARANCE_SESS_ATTR_KEY = "passedGCClearanceKey";

    public static final String MOBILE_GROUP_VIEW_SELECTED_EVENT_SESS_ATTR_KEY = "mobileGroupViewSelectedEventKey";
    public static final String MOBILE_PLAYER_VIEW_SELECTED_EVENT_SESS_ATTR_KEY = "mobilePlayerViewSelectedEventKey";

    public static final String DEFAULT_GROUP_COOKIE_KEY = "defaultGroupCookie";
    public static final String DEFAULT_PLAYER_COOKIE_KEY = "defaultPlayerCookie";
    public static final String DEFAULT_ADMIN_COOKIE_KEY = "defaultAdminCookie";
    public static final String DEFAULT_VIEW_COOKIE_KEY = "defaultViewCookie";
    public static final String REMEMBERED_ADMIN_PASSWORD_COOKIE_KEY = "rememberedCookie";
    private static final int COOKIE_MAX_AGE = 60 * 60 * 24 * 30;   // approx. one month
    private static final int REMEMBERED_PASSWORD_COOKIE_MAX_AGE = 60 * 60 * 24 * 90;   // approx. three months
    public static final String REMEMBERED_SINGLES_LEAGUE_PASSWORD_COOKIE_KEY = "rememberedSLCookie";
    public static final String REMEMBERED_GROUP_CONTACTS_PASSWORD_COOKIE_KEY = "rememberedGCCookie";

    public static final int SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID = 2;   // 2014 session 2


    private static final String[] QUOTES = {
            "\"Tennis is a psychological sport, you have to keep a clear head. That is why I stopped playing.\" " +
                    "Boris Becker"
            ,
            "\"The depressing thing about tennis is that no matter how good I get, I'll never be as good as a wall.\" " +
                    "Mitch Hedberg"
            ,
            "\"Is only a tennis match. At the end, that's life. There is much more important things.\" " +
                    "Rafael Nadal"
            ,
            "\"My tennis skills are okay.\" " +
                    "Enrique Iglesias"
            ,
            "\"I definitely want to get out of tennis and try something completely different.\" " +
                    "Marat Safin"
            ,
            "\"Tennis has given me soul.\" " +
                    "Matrina Navratilova"
            ,
            "\"Tennis is best of three sets, so even if I lose the first set, I still have a chance.\" Li Na"
            ,
            "\"It's true I always try to be as seductive as possible but I wouldn't be here if I couldn't play " +
                    "tennis.\" " +
                    "Anna Kournikova"
            ,
            "\"All my time in rehab has made me appreciate tennis more than ever.\" " +
                    "Kim Cljisters"
            ,
            "\"I have this terrible dark side to my personality, which playing tennis keeps at bay.\" " +
                    "Monica Seles"
            ,
            "\"I've been kicked in the teeth more times in tennis than the law ought to allow.\" " +
                    "Jimmy Connors"
            ,
            "\"The next point — that’s all you must think about.\" " +
                    "Rod Laver"
            ,
            "\"But that won't give me a free hand to hold the beer.\" " +
                    "Billy Carter (while being taught a two-handed backhand)"
            ,
            "\"One important key to success is self-confidence. An important key to self-confidence is preparation.\" " +
                    "Arthur Ashe"
            ,
            "\"Do you have any problems, other than that you're unemployed, a moron, and a dork?\" " +
                    "John McEnroe"
            ,
            "\"If you're not a competitor, you've just got to go home.\" " +
                    "Venus Williams"
            ,
            "\"New Yorkers love it when you spill your guts out there. Spill your guts at Wimbledon and they " +
                    "make you stop and clean it up.\" " +
                    "Jimmy Connors"
            ,
            "\"My game is a lot about footwork. If I move well - I play well.\" " +
                    "Roger Federer"
            ,
            "\"The moment of victory is much too short to live for that and nothing else.\" " +
                    "Martina Navratilova"
            ,
            "\"Ladies, here's a hint. If you're up against a girl with big boobs, bring her to the net and make " +
                    "her hit backhand volleys. That's the hardest shot for the well-endowed.\" " +
                    "Billie Jean King"
            ,
            "\"But sometimes you just don't do well.\" Martina Hingis"
            ,
            "\"This taught me a lesson, but I'm not sure what it is.\" " +
                    "John McEnroe"
            ,
            "\"You are the pits of the world! Vultures! Trash!\" " +
                    "John McEnroe"
            ,
            "\"I learned during all my career to enjoy suffering.\" " +
                    "Rafael Nadal"
            ,
            "\"F--- USTA!! They're full of s---! They have screwed me for the last time!\" " +
                    "Donald Young -- after not receiving the USTA's French Open wild card"
            ,
            "\"I think you should retire\" " +
                    "Andy Roddick -- responding to a question about prospective retirement"
            ,
            "\"I'm not like, X-Man, you know. For 30 seconds I hurt. But maybe something is missing upstairs.\" " +
                    "Gael Monfis -- on his habit of diving on court"
            ,
            "\"I've got more pills in me, right now, than Ozzy Osbourne.\" " +
                    "Andy Murray -- playing through an injured ankle at the French Open"
            ,
            "\"I didn't want to be on 'SportsCenter' for the next 25 years.\" " +
                    "Andrea Petkovic -- on why she decided to run off court to throw up"
            ,
            "\"Umpires are like emotional girlfriends -- once they make up their minds, there's no point in arguing.\" " +
                    "Janko Tipsarevic"
            ,
            "\"I only ever run when there's some point to it - say, if it's in a game of tennis.\" " +
                    "Rafael Nadal"
            ,
            "\"For a year, I had all sorts of weirdos coming on to me.\" " +
                    "Boris Becker"
            ,
            "\"Roger Federer is stylish.\" " +
                    "Bjorn Borg"
            ,
            "\"We have a saying in France. A dog doesn't make a cat.\" " +
                    "Yannick Noah"
            ,
            "\"You arrive at a village, and in this calm environment, one starts to hear echo.\" " +
                    "Yannick Noah"
            ,
            "\"I am very skinny.\" " +
                    "Novak Djokovic"
            ,
            "\"I feel old when I see mousse in my opponent's hair.\" " +
                    "Andre Agassi"
            ,
            "\"I got a hundred bucks says my baby beats Pete's baby. I just think genetics are in my favour.\" " +
                    "Andre Agassi"
            ,
            "\"I want everyone to look back and think that I was awesome.\" " +
                    "Andy Roddick"
            ,
            "\"At one point in your life, you'll have the thing you want or the reasons why you don't.\" " +
                    "Andy Roddick"
            ,
            "\"I'm a very positive thinker, and I think that is what helps me the most in difficult moments.\" " +
                    "Roger Federer"
            ,
            "\"I certainly don't lose any sleep if I lose a tennis match.\" " +
                    "Ivan Lendl"
            ,
            "\"I would like to say now I was very enjoy the life right now.\" " +
                    "Li Na"
            };



    public static void ensureGroupDataLoadedInSession(HttpSession session,
                                                      HttpServletRequest req,
                                                      HttpServletResponse res)
    {
        Object defaultGroupAttr = session.getAttribute(DEFAULT_GROUP_SESS_ATTR_KEY);

        // First time session init:
        if (defaultGroupAttr == null)
        {
            // Set default group:
            int groupIdToUse = getDefaultGroupForNewSession(req);
            setDefaultGroupId(session, groupIdToUse, req, res);

            // Set default player:
            int playerIdToUse = getDefaultPlayerForNewSession(req);
            setDefaultPlayerId(session, playerIdToUse, req, res);

            // Set default admin:
            int adminIdToUse = getDefaultAdminForNewSession(req);
            setDefaultAdminId(session, adminIdToUse, req, res);

            // Set default view:
            Cookie viewCookie = getCookie(req, DEFAULT_VIEW_COOKIE_KEY);
            setDefaultView(session, viewCookie == null ? View.GROUP : View.valueOf(viewCookie.getValue()), req, res);
        }
    }


    private static int getDefaultGroupForNewSession(HttpServletRequest req)
    {
        int groupIdInCookie = -1;
        Cookie groupCookie = getCookie(req, DEFAULT_GROUP_COOKIE_KEY);
        boolean isGroupIdInCookieOk = false;

        if (groupCookie != null)
        {
            groupIdInCookie = Integer.parseInt(groupCookie.getValue());
            isGroupIdInCookieOk = Groups.createInstance(groupIdInCookie).getRecords().size() == 1;
        }

        if (isGroupIdInCookieOk)
        {
            return groupIdInCookie;
        }

        // Use first "default group" if one exists
        Groups defaultGroups = Groups.createInstance(true);

        if (defaultGroups.getRecords().size() > 0)
        {
            return defaultGroups.getRecords().get(0).getId();
        }

        // Use first group period
        Groups allGroups = Groups.createInstance();

        if (allGroups.getRecords().size() > 0)
        {
            return allGroups.getRecords().get(0).getId();
        }

        return -1;
    }


    private static int getDefaultPlayerForNewSession(HttpServletRequest req)
    {
        int playerIdInCookie = -1;
        Cookie playerCookie = getCookie(req, DEFAULT_PLAYER_COOKIE_KEY);
        boolean isPlayerIdInCookieOk = false;

        if (playerCookie != null)
        {
            playerIdInCookie = Integer.parseInt(playerCookie.getValue());
            isPlayerIdInCookieOk = Players.createInstance(playerIdInCookie).getRecords().size() == 1;
        }

        if (isPlayerIdInCookieOk)
        {
            return playerIdInCookie;
        }

        // Grab first available player
        Players defaultPlayers = Players.createInstance(false);

        if (defaultPlayers.getRecords().size() > 0)
        {
            return defaultPlayers.getRecords().get(0).getId();
        }

        return -1;
    }


    private static int getDefaultAdminForNewSession(HttpServletRequest req)
    {
        int playerIdInCookie = -1;
        Cookie playerCookie = getCookie(req, DEFAULT_ADMIN_COOKIE_KEY);
        boolean isPlayerIdInCookieOk = false;

        if (playerCookie != null)
        {
            playerIdInCookie = Integer.parseInt(playerCookie.getValue());
            isPlayerIdInCookieOk = Players.createInstance(playerIdInCookie).getRecords().size() == 1;
        }

        if (isPlayerIdInCookieOk)
        {
            return playerIdInCookie;
        }

        int defaultPlayerId = -1;
        Players defaultPlayers = Players.createInstanceForAdmins();

        if (defaultPlayers.getRecords().size() > 0)
        {
            defaultPlayerId = defaultPlayers.getRecords().get(0).getId();
        }

        return defaultPlayerId;
    }


    private static Cookie getCookie(HttpServletRequest req, String cookieKey)
    {
        Cookie[] cookies = req.getCookies();

        if (cookies == null)
        {
            return null;
        }

        for (Cookie cookie : cookies)
        {

            if(cookie.getName().equals(cookieKey))
            {
                return cookie;
            }
        }

        return null;
    }


    public static View getDefaultView(HttpSession session)
    {
        return (View) session.getAttribute(DEFAULT_VIEW_SESS_ATTR_KEY);
    }


    public static void setDefaultView(HttpSession session,
                                      View view,
                                      HttpServletRequest req,
                                      HttpServletResponse resp)
    {
        session.setAttribute(DEFAULT_VIEW_SESS_ATTR_KEY, view);

        // Store cookie so that value can be retrieved for future sessions
        Cookie cookie = getCookie(req, DEFAULT_VIEW_COOKIE_KEY);

        if (cookie == null)
        {
            cookie = new Cookie(DEFAULT_VIEW_COOKIE_KEY, view.name());
        }
        else
        {
            cookie.setValue(view.name());
        }

        cookie.setMaxAge(COOKIE_MAX_AGE);

        // Set the cookie's domain and path so the browser will send the cookie on future requests
        try
        {
            URL url = new URL(req.getRequestURL().toString());
            cookie.setDomain(url.getHost());
            cookie.setPath("/");
        }
        catch (MalformedURLException e)
        {
            // eat
        }

        resp.addCookie(cookie);   // even editing a cookie value requires adding cookie to response
    }


    public static int getDefaultGroupId(HttpSession session)
    {
        return (Integer) session.getAttribute(DEFAULT_GROUP_SESS_ATTR_KEY);
    }


    public static void setRememberedAdminPassword(String password,
                                                  HttpServletRequest req,
                                                  HttpServletResponse resp)
    {
        // Store cookie so that value can be retrieved for future sessions
        Cookie cookie = getCookie(req, REMEMBERED_ADMIN_PASSWORD_COOKIE_KEY);

        if (cookie == null)
        {
            cookie = new Cookie(REMEMBERED_ADMIN_PASSWORD_COOKIE_KEY, StringUtils.encodeToBase64(password));
        }
        else
        {
            cookie.setValue(StringUtils.encodeToBase64(password));
        }

        cookie.setMaxAge(REMEMBERED_PASSWORD_COOKIE_MAX_AGE);

        // Set the cookie's domain and path so the browser will send the cookie on future requests
        try
        {
            URL url = new URL(req.getRequestURL().toString());
            cookie.setDomain(url.getHost());
            cookie.setPath("/");
        }
        catch (MalformedURLException e)
        {
            // eat
        }

        resp.addCookie(cookie);   // even editing a cookie value requires adding cookie to response
    }


    public static String getRememberedDecodedAdminPassword(HttpServletRequest req)
    {
        Cookie cookie = getCookie(req, REMEMBERED_ADMIN_PASSWORD_COOKIE_KEY);

        if (cookie == null)
        {
            return "";
        }
        else
        {
            return StringUtils.decodeFromBase64(cookie.getValue());
        }
    }


    public static void setRememberedSinglesLeaguePassword(String password,
                                                          HttpServletRequest req,
                                                          HttpServletResponse resp)
    {
        // Store cookie so that value can be retrieved for future sessions
        Cookie cookie = getCookie(req, REMEMBERED_SINGLES_LEAGUE_PASSWORD_COOKIE_KEY);

        if (cookie == null)
        {
            cookie = new Cookie(REMEMBERED_SINGLES_LEAGUE_PASSWORD_COOKIE_KEY, StringUtils.encodeToBase64(password));
        }
        else
        {
            cookie.setValue(StringUtils.encodeToBase64(password));
        }

        cookie.setMaxAge(REMEMBERED_PASSWORD_COOKIE_MAX_AGE);

        // Set the cookie's domain and path so the browser will send the cookie on future requests
        try
        {
            URL url = new URL(req.getRequestURL().toString());
            cookie.setDomain(url.getHost());
            cookie.setPath("/");
        }
        catch (MalformedURLException e)
        {
            // eat
        }

        resp.addCookie(cookie);   // even editing a cookie value requires adding cookie to response
    }


    public static String getRememberedDecodedSinglesLeaguePassword(HttpServletRequest req)
    {
        Cookie cookie = getCookie(req, REMEMBERED_SINGLES_LEAGUE_PASSWORD_COOKIE_KEY);

        if (cookie == null)
        {
            return "";
        }
        else
        {
            return StringUtils.decodeFromBase64(cookie.getValue());
        }
    }

    public static void setRememberedGroupContactsPassword(String password,
                                                          HttpServletRequest req,
                                                          HttpServletResponse resp)
    {
        // Store cookie so that value can be retrieved for future sessions
        Cookie cookie = getCookie(req, REMEMBERED_GROUP_CONTACTS_PASSWORD_COOKIE_KEY);

        if (cookie == null)
        {
            cookie = new Cookie(REMEMBERED_GROUP_CONTACTS_PASSWORD_COOKIE_KEY,
                    StringUtils.encodeToBase64(password));
        }
        else
        {
            cookie.setValue(StringUtils.encodeToBase64(password));
        }

        cookie.setMaxAge(REMEMBERED_PASSWORD_COOKIE_MAX_AGE);

        // Set the cookie's domain and path so the browser will send the cookie on future requests
        try
        {
            URL url = new URL(req.getRequestURL().toString());
            cookie.setDomain(url.getHost());
            cookie.setPath("/");
        }
        catch (MalformedURLException e)
        {
            // eat
        }

        resp.addCookie(cookie);   // even editing a cookie value requires adding cookie to response
    }


    public static String getRememberedDecodedGroupContactsPassword(HttpServletRequest req)
    {
        Cookie cookie = getCookie(req, REMEMBERED_GROUP_CONTACTS_PASSWORD_COOKIE_KEY);

        if (cookie == null)
        {
            return "";
        }
        else
        {
            return StringUtils.decodeFromBase64(cookie.getValue());
        }
    }


    public static void setDefaultGroupId(HttpSession session,
                                         int groupId,
                                         HttpServletRequest req,
                                         HttpServletResponse resp)
    {
        session.setAttribute(DEFAULT_GROUP_SESS_ATTR_KEY, groupId);

        // Store cookie so that value can be retrieved for future sessions
        Cookie cookie = getCookie(req, DEFAULT_GROUP_COOKIE_KEY);

        if (cookie == null)
        {
            cookie = new Cookie(DEFAULT_GROUP_COOKIE_KEY, String.valueOf(groupId));
        }
        else
        {
            cookie.setValue(String.valueOf(groupId));
        }

        cookie.setMaxAge(COOKIE_MAX_AGE);

        // Set the cookie's domain and path so the browser will send the cookie on future requests
        try
        {
            URL url = new URL(req.getRequestURL().toString());
            cookie.setDomain(url.getHost());
            cookie.setPath("/");
        }
        catch (MalformedURLException e)
        {
            // eat
        }

        resp.addCookie(cookie);   // even editing a cookie value requires adding cookie to response
    }


    public static void setErrorPageException(ServletRequest req, HttpSession session, Exception e)
    {
        if (req != null)
        {
            req.setAttribute(ERROR_PAGE_EXCEPTION_ATTR_KEY, e);
        }
        else if (session != null)
        {
            session.setAttribute(ERROR_PAGE_EXCEPTION_ATTR_KEY, e);
        }
    }


    public static String getVersionQueryString()
    {
        return "?version=201501111146";
    }


    public static void setShowPastEvents(HttpSession session, boolean showPastEvents)
    {
        session.setAttribute(SHOW_PAST_EVENTS_SESS_ATTR_KEY, showPastEvents);
    }


    public static boolean isShowingPastEvents(HttpSession session)
    {
        Boolean isShowingPastEvents = (Boolean) session.getAttribute(SHOW_PAST_EVENTS_SESS_ATTR_KEY);

        if (isShowingPastEvents == null)
        {
            return false;
        }

        return isShowingPastEvents;
    }


    public static boolean hasPassedAdminSecurityClearance(HttpSession session,
                                                          HttpServletRequest req)
    {
        Boolean passed = (Boolean) session.getAttribute(PASSED_ADMIN_SECURITY_CLEARANCE_SESS_ATTR_KEY);

        if (passed != null && passed)
        {
            return true;
        }

        // Check remembered password cookie:
        if (getRememberedDecodedAdminPassword(req).toUpperCase().equals(AdminClearance.ADMIN_PWD_UPPER_CASE))
        {
            setHasPassedAdminSecurityClearance(session);
            return true;
        }

        return false;
    }


    public static void setHasPassedAdminSecurityClearance(HttpSession session)
    {
        session.setAttribute(PASSED_ADMIN_SECURITY_CLEARANCE_SESS_ATTR_KEY, true);
    }


    public static boolean hasPassedSinglesLeagueSecurityClearance(HttpSession session,
                                                                  HttpServletRequest req)
    {
        Boolean passed = (Boolean) session.getAttribute(PASSED_SINGLES_LEAGUE_SECURITY_CLEARANCE_SESS_ATTR_KEY);

        if (passed != null && passed)
        {
            return true;
        }

        // Check remembered password cookie:
        if (getRememberedDecodedSinglesLeaguePassword(req).toUpperCase().equals(SinglesLeagueClearance.SINGLES_LEAGUE_PWD_UPPER_CASE))
        {
            setHasPassedSinglesLeagueSecurityClearance(session);
            return true;
        }

        return false;
    }


    public static void setHasPassedSinglesLeagueSecurityClearance(HttpSession session)
    {
        session.setAttribute(PASSED_SINGLES_LEAGUE_SECURITY_CLEARANCE_SESS_ATTR_KEY, true);
    }


    public static boolean hasPassedGroupContactsSecurityClearance(HttpSession session,
                                                                  HttpServletRequest req)
    {
        Boolean passed = (Boolean) session.getAttribute(PASSED_GROUP_CONTACTS_SECURITY_CLEARANCE_SESS_ATTR_KEY);

        if (passed != null && passed)
        {
            return true;
        }

        // Check remembered password cookie:
        if (getRememberedDecodedGroupContactsPassword(req).toUpperCase().
                equals(GroupContactsClearance.GROUP_CONTACTS_PWD_UPPER_CASE))
        {
            setHasPassedGroupContactsSecurityClearance(session);
            return true;
        }

        return false;
    }


    public static void setHasPassedGroupContactsSecurityClearance(HttpSession session)
    {
        session.setAttribute(PASSED_GROUP_CONTACTS_SECURITY_CLEARANCE_SESS_ATTR_KEY, true);
    }


    public static void setPageAfterPwd(HttpSession session, String page)
    {
        session.setAttribute(PAGE_AFTER_PWD_SESS_ATTR_KEY, page);
    }


    public static String getPageAfterPwd(HttpSession session)
    {
        return (String) session.getAttribute(PAGE_AFTER_PWD_SESS_ATTR_KEY);
    }


    public static boolean isMobileBrowser(HttpServletRequest req)
    {
        String ua = req.getHeader("User-Agent");
        return ua.matches("(.*)(Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini)(.*)");

        /*
        // Javascript equivalent:
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            // some code..
        }
        */
    }


    public static boolean isAppleMobileBrowser(HttpServletRequest req)
    {
        String ua = req.getHeader("User-Agent");
        return ua.matches("(.*)(iPhone|iPod)(.*)");
    }


    public static void setMobileGroupViewSelectedEvent(HttpSession session, int eventId)
    {
        session.setAttribute(MOBILE_GROUP_VIEW_SELECTED_EVENT_SESS_ATTR_KEY, eventId);
    }


    public static int getMobileGroupViewSelectedEvent(HttpSession session)
    {
        Integer eventId = (Integer) session.getAttribute(MOBILE_GROUP_VIEW_SELECTED_EVENT_SESS_ATTR_KEY);

        if (eventId == null)
        {
            return -1;
        }

        return eventId;
    }


    public static void setMobilePlayerViewSelectedPlayer(HttpSession session, int playerId)
    {
        session.setAttribute(MOBILE_PLAYER_VIEW_SELECTED_EVENT_SESS_ATTR_KEY, playerId);
    }


    public static int getMobilePlayerViewSelectedPlayer(HttpSession session)
    {
        Integer playerId = (Integer) session.getAttribute(MOBILE_PLAYER_VIEW_SELECTED_EVENT_SESS_ATTR_KEY);

        if (playerId == null)
        {
            return -1;
        }

        return playerId;
    }


    public static int getDefaultPlayerId(HttpSession session)
    {
        Integer id = (Integer) session.getAttribute(DEFAULT_PLAYER_SESS_ATTR_KEY);
        return id == null ? -1 : id;
    }


    public static void setDefaultPlayerId(HttpSession session,
                                         int playerId,
                                         HttpServletRequest req,
                                         HttpServletResponse resp)
    {
        session.setAttribute(DEFAULT_PLAYER_SESS_ATTR_KEY, playerId);

        // Store cookie so that value can be retrieved for future sessions
        Cookie cookie = getCookie(req, DEFAULT_PLAYER_COOKIE_KEY);

        if (cookie == null)
        {
            cookie = new Cookie(DEFAULT_PLAYER_COOKIE_KEY, String.valueOf(playerId));
        }
        else
        {
            cookie.setValue(String.valueOf(playerId));
        }

        cookie.setMaxAge(COOKIE_MAX_AGE);

        // Set the cookie's domain and path so the browser will send the cookie on future requests
        try
        {
            URL url = new URL(req.getRequestURL().toString());
            cookie.setDomain(url.getHost());
            cookie.setPath("/");
        }
        catch (MalformedURLException e)
        {
            // eat
        }

        resp.addCookie(cookie);   // even editing a cookie value requires adding cookie to response
    }


    public static int getDefaultAdminId(HttpSession session)
    {
        Integer id = (Integer) session.getAttribute(DEFAULT_ADMIN_SESS_ATTR_KEY);
        return id == null ? -1 : id;
    }


    public static void setDefaultAdminId(HttpSession session,
                                         int playerId,
                                         HttpServletRequest req,
                                         HttpServletResponse resp)
    {
        session.setAttribute(DEFAULT_ADMIN_SESS_ATTR_KEY, playerId);

        // Store cookie so that value can be retrieved for future sessions
        Cookie cookie = getCookie(req, DEFAULT_ADMIN_COOKIE_KEY);

        if (cookie == null)
        {
            cookie = new Cookie(DEFAULT_ADMIN_COOKIE_KEY, String.valueOf(playerId));
        }
        else
        {
            cookie.setValue(String.valueOf(playerId));
        }

        cookie.setMaxAge(COOKIE_MAX_AGE);

        // Set the cookie's domain and path so the browser will send the cookie on future requests
        try
        {
            URL url = new URL(req.getRequestURL().toString());
            cookie.setDomain(url.getHost());
            cookie.setPath("/");
        }
        catch (MalformedURLException e)
        {
            // eat
        }

        resp.addCookie(cookie);   // even editing a cookie value requires adding cookie to response
    }


    public static String getQuote()
    {
        int randomIdx = Math.round( (float) Math.floor(Math.random() * QUOTES.length) );
        return QUOTES[randomIdx];
    }


}
