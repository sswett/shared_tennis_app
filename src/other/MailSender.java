package other;

// Note: this requires downloading the  JavaMail API from: https://java.net/projects/javamail/pages/Home

// This code WORKS!


import beans.PlayerForTextingBean;
import dao.PlayersForTexting;
import utils.StringUtils;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class MailSender
{
    /*
    private static final String username = "rock.tennis";
    private static final String password = "com3cast4";
    private static final String textSenderEmailAddress = "rock.tennis@comcast.net";
    private static final String smtpHost = "smtp.comcast.net";
    private static final String smtpPort = "587";
    */

    /*
        The first account I used to log in manually at http://www.mandrill.com:
            User = tennis@mandrill.com
            Pwd = rock4tennis6

        The second account I used to log in manually at http://www.mandrill.com:
            User = steve.swett@comcast.net
            Pwd = man3drill5

        Mandrill has an hourly quoted.  Messages will be queued, instead of sent, if quota is reached.
        Will likely be sent at top of next hour.  If I reduce sends to non-existent e-mail addresses,
        then my quota will go up.
     */

    // Following is first user/password (before I knew what I was doing):
    // private static final String username = "tennis@mandrill.com";
    // private static final String password = "p9pKaSSVpMT50axFFycA6w";   // api key

    private static final String username = "steve.swett@comcast.net";
    private static final String password = "CL52pvYBhBZK84vTYZrfHA";   // api key

    // private static final String textSenderEmailAddress = "rock.tennis@comcast.net";
    private static final String smtpHost = "smtp.mandrillapp.com";
    private static final String smtpPort = "587";

    private static final Properties mailProps = getMailProps();
    private static final Authenticator authenticator = getAuthenticator();
    private static final Session session = Session.getInstance(mailProps, authenticator);

    // Web sites where the following mail servers were spotted:

    // http://www.emailtextmessages.com/
    // http://newtech.about.com/od/mobile/a/Sms-Gateway-From-Email-To-Sms-Text-Message.htm


    /*
    private static final String[] carrierTextMsgMailServers = new String[]
            {
                    "txt.att.net",   // AT&T
                    // "comcastpcs.textmsg.com",  Comcast -- apparently can't reach port 25 from tennis.zbasu.net
                    // "pagemci.com",  MCI -- not going to bother sending here for now
                    "mymetropcs.com",   // Metro PCS
                    "messaging.sprintpcs.com",   // Sprint
                    "tmomail.net",   // T-Mobile
                    "vtext.com"   // Verizon
                    // "vmobl.com"  Virgin -- not going to bother sending here for now
            };
    */


    private static Properties getMailProps()
    {
        // Here's where to find all SMTP properties:
        // https://javamail.java.net/nonav/docs/api/com/sun/mail/smtp/package-summary.html

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.port", smtpPort);
        props.put("mail.smtp.starttls.enable", "true");
        return props;
    }


    private static Authenticator getAuthenticator()
    {
        return new javax.mail.Authenticator()
        {
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(username, password);
            }
        };
    }


    private static InternetAddress[] getEmailAddressesForCellPhoneNumbers(String[] cellPhoneNumbers)
    {
        ArrayList<PlayerForTextingBean> playerBeans =
                PlayersForTexting.createInstance(cellPhoneNumbers, true).getRecords();

        String[] numbersWithDomain = new String[playerBeans.size()];

        int x = -1;

        for (PlayerForTextingBean playerBean : playerBeans)
        {
            x++;
            numbersWithDomain[x] = playerBean.getPhone() + "@" + playerBean.getDomainForTexting();
        }

        final String addressList = StringUtils.join(numbersWithDomain, ",");

        try
        {
            // The following parses a comma-separated list of addresses
            InternetAddress[] inetAddresses = InternetAddress.parse(addressList);
            return inetAddresses;
        }
        catch (AddressException e)
        {
            return new InternetAddress[0];
        }
    }


    /**
     *
     * @return true if successful
     */
    public static boolean sendTextMsg(String msg,
                                      String senderName,
                                      String senderEmail,
                                      String senderPhone,
                                      String[] cellPhoneNumbers)
    {
        boolean problem = false;

        // for (String carrier : carrierTextMsgMailServers)
        // {
            InternetAddress[] inetAddresses = getEmailAddressesForCellPhoneNumbers(cellPhoneNumbers);

            boolean anyProblem;

            if (inetAddresses.length > 0)
            {
                boolean success = sendTextMsg(msg, senderName, senderEmail, senderPhone, inetAddresses);
                anyProblem = !success;
            }
            else
            {
                anyProblem = true;
            }

            if (anyProblem)
            {
                problem = true;
            }
        // }

        return !problem;
    }


    /**
     *
     * @return true if successful
     */
    private static boolean sendTextMsg(String msg,
                                       String senderName,
                                       String senderEmail,
                                       String senderPhone,
                                       InternetAddress[] internetAddresses)
    {
        // Session session = Session.getInstance(getMailProps(), getAuthenticator());

        try
        {
            String[] senderNameArray = senderName.split(" ");
            String senderFirstName = senderNameArray.length > 0 ? senderNameArray[0] : null;
            String senderSecondName = senderNameArray.length > 1 ? senderNameArray[1] : null;

            String lowerCaseSenderEmail = senderEmail.toLowerCase();

            boolean skipSentBy =
                    ( senderFirstName != null && lowerCaseSenderEmail.contains(senderFirstName.toLowerCase()) )
                    ||
                    ( senderSecondName != null && lowerCaseSenderEmail.contains(senderSecondName.toLowerCase()) );

            String sentBy = skipSentBy ? "" : String.format("SENT BY:%s\n", senderFirstName);

            String textBody = String.format("%s\n%sREPLY:touch/hold %s", msg, sentBy, senderPhone);

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(senderEmail));

            // Special handling of replies for Steve
            if (senderEmail.contains("swett"))
            {
                // One option: my "rock.tennis@comcast.net" e-mail account will either auto reply or forward replies
                // back to my text# -- depending on its settings.

                InternetAddress[] replyToAddresses = { new InternetAddress("rock.tennis@comcast.net") };
                message.setReplyTo(replyToAddresses);

                // Another option: use bogus "reply to" address so user can't reply.  This will give the
                // following reply when texting, which could seem a little harsh:

                // "Your MSG could not be DELIVERED because SmtpNonReplyableMsg"

                /*
                InternetAddress[] replyToAddresses = { new InternetAddress("noreply@whatever.*invalid*") };
                message.setReplyTo(replyToAddresses);
                */
            }

            message.setRecipients(Message.RecipientType.TO, internetAddresses);
            message.setContent(textBody, "text/plain");
            Transport.send(message);

        }
        catch (MessagingException e)
        {
            // Even though some bad recipients, this is not being reached (good)
            e.printStackTrace();
            return false;
        }

        return true;
    }


    public static boolean sendEmailMsg(String msg,
                                       String senderEmailAddress,
                                       String senderName,
                                       String senderPhone,
                                       String[] emailAddresses)
    {
        try
        {

            StringBuilder formattedSenderPhone = new StringBuilder(senderPhone)
                    .insert(0,"(").insert(4,") ").insert(9,"-");

            String textBody = String.format("%s\n\n" +
                    "Visit the <a href=\"http://tennis.zbasu.net\">tennis site</a> to sign up for practices, " +
                    "matches and social events.\n\n" +
                    "Sent by: %s\n" +
                    "Phone: %s\n" +
                    "<a href=\"tel:%s\">Call me</a>\n" +
                    "<a href=\"sms:%s\">Text me</a>",
                    msg,
                    senderName,
                    formattedSenderPhone,
                    senderPhone,
                    senderPhone);

            // The following carriage return / line feed handling is required in order to get messages to render
            // correctly.  Those special characters in the Subject would really throw things off.  And the
            // special characters in the body would just be treated as white space instead of breaks.

            String formattedSubject = msg.replaceAll("\r\n", " ");
            formattedSubject = formattedSubject.replaceAll("\n", " ");
            int chopPos = Math.min(55, formattedSubject.length());
            formattedSubject = formattedSubject.substring(0, chopPos);

            String formattedTextBody = textBody.replaceAll("\r\n", "<br/>");
            formattedTextBody = formattedTextBody.replaceAll("\n", "<br/>");

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(senderEmailAddress));

            final String addressList = StringUtils.join(emailAddresses, ",");
            InternetAddress[] inetAddresses = InternetAddress.parse(addressList);

            message.setRecipients(Message.RecipientType.TO, inetAddresses);

            String senderFirstName = senderName.split(" ")[0];
            message.setSubject("Tennis site msg from " + senderFirstName + ": " + formattedSubject + " ...");
            message.setContent(formattedTextBody, "text/html");
            Transport.send(message);

        }
        catch (MessagingException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }


}