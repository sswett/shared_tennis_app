package other;

public enum MatchType
{
    REGULAR_SEASON("Regular Season"),
    PLAYOFF("Playoffs"),
    ALL("All");

    private String label;


    MatchType(String label)
    {
        this.label = label;
    }


    public String getLabel()
    {
        return label;
    }

}
