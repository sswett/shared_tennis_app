package crud.beans;

public class PlayerBean implements java.io.Serializable
{

	public int id;
    public String name;
    public int disabled;
    public String phone;
    public String email;
    public int is_admin;
    public int carrier_id;
    public String rating_level;
    public int mbr_type;


    // Constructor:

    public PlayerBean()
    {
        // Just to stick to the JavaBean convention...

        id = 0;
        name = "";
        disabled = 0;
        phone = "";
        email = "";
        is_admin = 0;
        carrier_id = 0;
        rating_level = "3.5";
        mbr_type = 0;
    }


    public PlayerBean(int id, String name, int disabled, String phone, String email, int is_admin,
                      int carrier_id, String rating_level, int mbr_type)
    {
        this.id = id;
        this.name = name;
        this.disabled = disabled;
        this.phone = phone;
        this.email = email;
        this.is_admin = is_admin;
        this.carrier_id = carrier_id;
        this.rating_level = rating_level;
        this.mbr_type = mbr_type;
    }
    
    
    public PlayerBean(beans.PlayerBean bean)
    {
        this.id = bean.getId();
        this.name = bean.getName();
        this.disabled = bean.getDisabled() ? 1 : 0;
        this.phone = bean.getPhone();
        this.email = bean.getEmail();
        this.is_admin = bean.getIsAdmin() ? 1 : 0;
        this.carrier_id = bean.getCarrierId();
        this.rating_level = bean.getRatingLevel();
        this.mbr_type = bean.getMbrType();
    }
    
}