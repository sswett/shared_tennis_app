package services_rest;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.PlayerBean;
import beans.StringBean;
import other.SessionHelper;

@Path("/admincheck")


public class AdminClearanceCheckService {

    @Context HttpServletRequest req;

	// Example: "/pageafter/AngularApp.html#/players"
    
    /*
    @GET @Path("/pageafter/{pagepart1}/{pagepart2}")
	@Produces(MediaType.TEXT_PLAIN)

	public String hasPassedCheck(
			@PathParam("pagepart1") String pagepart1, 
			@PathParam("pagepart2") String pagepart2) 
	{
        HttpSession session = req.getSession();
        SessionHelper.setPageAfterPwd(session, pagepart1 + "/" + pagepart2);
        boolean passed = SessionHelper.hasPassedAdminSecurityClearance(session, req);
        return String.valueOf(passed);
	}
	*/

    
    // Using POST with JSON is a better way to pass in the URI of the page to show after clearance has passed
    
    @POST @Path("/pageafteruri")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
    
    public String hasPassedCheck(StringBean pageAfterURI)
    {
        HttpSession session = req.getSession();
        SessionHelper.setPageAfterPwd(session, pageAfterURI.getString());
        boolean passed = SessionHelper.hasPassedAdminSecurityClearance(session, req);
        return String.valueOf(passed);
    }


}
