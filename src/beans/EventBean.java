package beans;

import utils.DateUtils;

import java.util.Calendar;

public class EventBean
{
    private int id;
    private Calendar date;
    private String description;
    private int groupId;


    public EventBean(int id,
                     Calendar date,
                     String description,
                     int groupId)
    {
        this.id = id;
        this.date = date;
        this.description = description;
        this.groupId = groupId;
    }


    public int getId()
    {
        return id;
    }


    public Calendar getDate()
    {
        return date;
    }


    public String getDateFormattedForColumnHeader()
    {
        return DateUtils.getDateFormattedForColumnHeader(getDate());
    }


    public String getDescription()
    {
        return description;
    }


    public String getDescriptionMinusBrTags()
    {
        return description.replaceAll("<br/>", " ");
    }


    public int getGroupId()
    {
        return groupId;
    }

}
