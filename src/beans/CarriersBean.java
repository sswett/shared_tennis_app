package beans;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

//In order to do automatic JAXB mapping, need public paramter-less constructor and a complete set of public getters/setters. 

@XmlRootElement
@XmlType(propOrder = { "id", "name", "domainName" })
public class CarriersBean
{
    private int id;
    private String name;
    private String domainName;


    public CarriersBean(int id,
                        String name,
                        String domain_name)
    {
        this.id = id;
        this.name = name;
        this.domainName = domain_name;
    }
    
    
    public CarriersBean()
    {
    	
    }


    public void setId(int id)
    {
        this.id = id;
    }


    public void setName(String name)
    {
        this.name = name;
    }


    public void setDomainName(String domain_name)
    {
        this.domainName = domain_name;
    }


    public int getId()
    {
        return this.id;
    }


    public String getName()
    {
        return name;
    }


    public String getDomainName()
    {
        return domainName;
    }

}
