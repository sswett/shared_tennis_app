package beans;

public class GroupsBean
{
    private int id;
    private String name;
    private boolean isDefault;
    private String shortName;


    public GroupsBean(int id,
                      String name,
                      boolean isDefault,
                      String shortName)
    {
        this.id = id;
        this.name = name;
        this.isDefault = isDefault;
        this.shortName = shortName;
    }


    protected void setId(int id)
    {
        this.id = id;
    }


    protected void setName(String name)
    {
        this.name = name;
    }


    protected void setDefault(boolean isDefault)
    {
        this.isDefault = isDefault;
    }


    public int getId()
    {
        return this.id;
    }


    public String getName()
    {
        return name;
    }


    public boolean isDefault()
    {
        return isDefault;
    }


    public String getShortName()
    {
        return shortName;
    }

}
