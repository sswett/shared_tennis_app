package beans;

import other.MemberType;
import other.PlayerResponse;

import java.util.Calendar;

public class EventResponseJoinedDataBean
{
    private int id;
    private int playerId;
    private int eventId;
    private Calendar eventDate;
    private String eventDescr;
    private PlayerResponse playerResponse;
    private Calendar responseDate;
    private int groupId;
    private String playerName;
    private String playerPhone;
    private String playerEmail;
    private String groupShortName;
    private int playerMbrType;


    public EventResponseJoinedDataBean(int id,
                                       int playerId,
                                       int eventId,
                                       Calendar eventDate,
                                       String eventDescr,
                                       PlayerResponse playerResponse,
                                       Calendar responseDate,
                                       int groupId,
                                       String playerName,
                                       String playerPhone,
                                       String playerEmail,
                                       String groupShortName,
                                       int playerMbrType)
    {
        this.id = id;
        this.playerId = playerId;
        this.eventId = eventId;
        this.eventDate = eventDate;
        this.eventDescr = eventDescr;
        this.playerResponse = playerResponse;
        this.responseDate = responseDate;
        this.groupId = groupId;
        this.playerName = playerName;
        this.playerPhone = playerPhone;
        this.playerEmail = playerEmail;
        this.groupShortName = groupShortName;
        this.playerMbrType = playerMbrType;
    }


    protected void setId(int id)
    {
        this.id = id;
    }


    protected void setPlayerId(int playerId)
    {
        this.playerId = playerId;
    }


    protected void setEventId(int eventId)
    {
        this.eventId = eventId;
    }


    protected void setEventDate(Calendar eventDate)
    {
        this.eventDate = eventDate;
    }


    protected void setEventDescr(String eventDescr)
    {
        this.eventDescr = eventDescr;
    }


    protected void setPlayerResponse(PlayerResponse playerResponse)
    {
        this.playerResponse = playerResponse;
    }


    protected void setResponseDate(Calendar responseDate)
    {
        this.responseDate = responseDate;
    }


    protected void setGroupId(int groupId)
    {
        this.groupId = groupId;
    }


    public int getId()
    {
        return id;
    }


    public int getPlayerId()
    {
        return playerId;
    }


    public int getEventId()
    {
        return eventId;
    }


    public Calendar getEventDate()
    {
        return eventDate;
    }


    public String getEventDescr()
    {
        return eventDescr;
    }


    public PlayerResponse getPlayerResponse()
    {
        return playerResponse;
    }


    public Calendar getResponseDate()
    {
        return responseDate;
    }


    public int getGroupId()
    {
        return groupId;
    }


    public String getPlayerName()
    {
        return playerName;
    }


    public String getPlayerPhone()
    {
        return playerPhone;
    }


    public String getPlayerEmail()
    {
        return playerEmail;
    }


    public String getGroupShortName()
    {
        return groupShortName;
    }


    public int getPlayerMbrType()
    {
        return playerMbrType;
    }


    public String getPlayerMbrTypeDescr()
    {
        return MemberType.values()[playerMbrType].getMbrTypeDescr();
    }

}
