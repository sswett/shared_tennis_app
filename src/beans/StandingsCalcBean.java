package beans;

import utils.DateUtils;

import java.util.Calendar;

public class StandingsCalcBean
{
    private int id;
    private String session_id;
    private int player_id;
    private String player_name;
    private int matches_won;
    private int matches_lost;
    private float matches_won_pct;
    private int sets_won;
    private int sets_lost;
    private float sets_won_pct;
    private float games_won;
    private float games_lost;
    private float games_won_pct;
    private int player_count = 0;
    private int player_rank = 0;


    public StandingsCalcBean(int id,
                             String session_id,
                             int player_id,
                             String player_name,
                             int matches_won,
                             int matches_lost,
                             float matches_won_pct,
                             int sets_won,
                             int sets_lost,
                             float sets_won_pct,
                             float games_won,
                             float games_lost,
                             float games_won_pct)
    {
        this.id = id;
        this.session_id = session_id;
        this.player_id = player_id;
        this.player_name = player_name;
        this.matches_won = matches_won;
        this.matches_lost = matches_lost;
        this.matches_won_pct = matches_won_pct;
        this.sets_won = sets_won;
        this.sets_lost = sets_lost;
        this.sets_won_pct = sets_won_pct;
        this.games_won = games_won;
        this.games_lost = games_lost;
        this.games_won_pct = games_won_pct;
    }


    public StandingsCalcBean(int player_count,
                             int player_rank,
                             int id,
                             String session_id,
                             int player_id,
                             String player_name,
                             int matches_won,
                             int matches_lost,
                             float matches_won_pct,
                             int sets_won,
                             int sets_lost,
                             float sets_won_pct,
                             float games_won,
                             float games_lost,
                             float games_won_pct)
    {
        this.player_count = player_count;
        this.player_rank = player_rank;
        this.id = id;
        this.session_id = session_id;
        this.player_id = player_id;
        this.player_name = player_name;
        this.matches_won = matches_won;
        this.matches_lost = matches_lost;
        this.matches_won_pct = matches_won_pct;
        this.sets_won = sets_won;
        this.sets_lost = sets_lost;
        this.sets_won_pct = sets_won_pct;
        this.games_won = games_won;
        this.games_lost = games_lost;
        this.games_won_pct = games_won_pct;
    }


    public int getId()
    {
        return id;
    }


    public String getSessionId()
    {
        return session_id;
    }


    public int getPlayerId()
    {
        return player_id;
    }


    public String getPlayerName()
    {
        return player_name;
    }


    public int getPlayerCount()
    {
        return player_count;
    }


    public int getPlayerRank()
    {
        return player_rank;
    }


    public int getMatchesWon()
    {
        return matches_won;
    }


    public int getMatchesLost()
    {
        return matches_lost;
    }


    public float getMatchesWonPct()
    {
        return matches_won_pct;
    }


    public int getSetsWon()
    {
        return sets_won;
    }


    public int getSetsLost()
    {
        return sets_lost;
    }


    public float getSetsWonPct()
    {
        return sets_won_pct;
    }


    public float getGamesWon()
    {
        return games_won;
    }


    public float getGamesLost()
    {
        return games_lost;
    }


    public float getGamesWonPct()
    {
        return games_won_pct;
    }

}
