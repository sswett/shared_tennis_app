package beans;

public class CurrentSessionsBean
{
    private int id;
    private int yr;
    private int sess;
    private String group_short_name_all;
    private String group_short_name_4_0;
    private String group_short_name_3_5;


    public CurrentSessionsBean(int id,
                               int yr,
                               int sess,
                               String group_short_name_all,
                               String group_short_name_4_0,
                               String group_short_name_3_5)
    {
        this.id = id;
        this.yr = yr;
        this.sess = sess;
        this.group_short_name_all = group_short_name_all;
        this.group_short_name_4_0 = group_short_name_4_0;
        this.group_short_name_3_5 = group_short_name_3_5;
    }


    public int getId()
    {
        return this.id;
    }


    public int getYr()
    {
        return yr;
    }


    public int getSess()
    {
        return sess;
    }


    public String getGroupShortNameAll()
    {
        return group_short_name_all;
    }


    public String getGroupShortName4_0()
    {
        return group_short_name_4_0;
    }


    public String getGroupShortName3_5()
    {
        return group_short_name_3_5;
    }

}
