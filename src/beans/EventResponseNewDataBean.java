package beans;

import other.PlayerResponse;

import java.util.Calendar;

public class EventResponseNewDataBean
{
    private int playerId;
    private int eventId;
    private PlayerResponse playerResponse;
    private Calendar responseDate;


    public EventResponseNewDataBean(int playerId,
                                    int eventId,
                                    PlayerResponse playerResponse,
                                    Calendar responseDate)
    {
        this.playerId = playerId;
        this.eventId = eventId;
        this.playerResponse = playerResponse;
        this.responseDate = responseDate;
    }


    public int getPlayerId()
    {
        return playerId;
    }


    public int getEventId()
    {
        return eventId;
    }


    public PlayerResponse getPlayerResponse()
    {
        return playerResponse;
    }


    public Calendar getResponseDate()
    {
        return responseDate;
    }
}
