package beans;

public class PlayerForTextingBean
{
    private int id;
    private String name;
    private boolean disabled;
    private String phone;
    private String email;
    private boolean is_admin;
    private int carrier_id;
    private String domain_name;
    private String rating_level;
    private int mbr_type;


    public PlayerForTextingBean(int id,
                                String name,
                                boolean disabled,
                                String phone,
                                String email,
                                boolean is_admin,
                                int carrier_id,
                                String domain_name,
                                String rating_level,
                                int mbr_type)
    {
        this.id = id;
        this.name = name;
        this.disabled = disabled;
        this.phone = phone;
        this.email = email;
        this.is_admin = is_admin;
        this.carrier_id = carrier_id;
        this.domain_name = domain_name;
        this.rating_level = rating_level;
        this.mbr_type = mbr_type;
    }


    public int getId()
    {
        return this.id;
    }


    public String getName()
    {
        return name;
    }


    public boolean getDisabled()
    {
        return disabled;
    }


    public boolean getIsAdmin()
    {
        return is_admin;
    }


    public int getCarrierId()
    {
        return carrier_id;
    }


    public String getPhone()
    {
        return phone;
    }


    public String getEmail()
    {
        return email;
    }


    public String getDomainForTexting()
    {
        return domain_name;
    }


    public String getRatingLevel()
    {
        return rating_level;
    }

}
