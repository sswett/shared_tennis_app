package beans;

public class EventResponseKeyBean
{
    private int playerId;
    private int eventId;


    public EventResponseKeyBean(int playerId, int eventId)
    {
        this.playerId = playerId;
        this.eventId = eventId;
    }


    public int getPlayerId()
    {
        return playerId;
    }


    public int getEventId()
    {
        return eventId;
    }
}
