package beans;

import other.PlayerPerspectiveScores;
import utils.DateUtils;

import java.util.Calendar;

public class MatchBean
{
    private int id;
    private int yr;
    private int sess;
    private String division;
    private Calendar match_date;
    private int match_no;
    private int player1_id;
    private int player2_id;
    private int winner_id;
    private int player1_set1_games_won;
    private int player1_set2_games_won;
    private int player1_set3_games_won;
    private int player2_set1_games_won;
    private int player2_set2_games_won;
    private int player2_set3_games_won;
    private int is_set3_tiebreaker;
    private int week_no;
    private String custom_label;
    private String note;
    private String player1_note;
    private String player2_note;
    private int is_playoff;
    private int is_default;


    public MatchBean(int id,
                     int yr,
                     int sess,
                     String division,
                     Calendar match_date,
                     int match_no,
                     int player1_id,
                     int player2_id,
                     int winner_id,
                     int player1_set1_games_won,
                     int player1_set2_games_won,
                     int player1_set3_games_won,
                     int player2_set1_games_won,
                     int player2_set2_games_won,
                     int player2_set3_games_won,
                     int is_set3_tiebreaker,
                     int week_no,
                     String custom_label,
                     String note,
                     String player1_note,
                     String player2_note,
                     int is_playoff,
                     int is_default)
    {
        this.id = id;
        this.yr = yr;
        this.sess = sess;
        this.division = division;
        this.match_date = match_date;
        this.match_no = match_no;
        this.player1_id = player1_id;
        this.player2_id = player2_id;
        this.winner_id = winner_id;
        this.player1_set1_games_won = player1_set1_games_won;
        this.player1_set2_games_won = player1_set2_games_won;
        this.player1_set3_games_won = player1_set3_games_won;
        this.player2_set1_games_won = player2_set1_games_won;
        this.player2_set2_games_won = player2_set2_games_won;
        this.player2_set3_games_won = player2_set3_games_won;
        this.is_set3_tiebreaker = is_set3_tiebreaker;
        this.week_no = week_no;
        this.custom_label = custom_label;
        this.note = note;
        this.player1_note = player1_note;
        this.player2_note = player2_note;
        this.is_playoff = is_playoff;
        this.is_default = is_default;
    }


    public int getId()
    {
        return id;
    }


    public int getYr()
    {
        return yr;
    }


    public int getSess()
    {
        return sess;
    }


    public String getDivision()
    {
        return division;
    }


    public Calendar getMatchDate()
    {
        return match_date;
    }


    public int getMatchNo()
    {
        return match_no;
    }


    public int getPlayer1Id()
    {
        return player1_id;
    }


    public int getPlayer2Id()
    {
        return player2_id;
    }


    public int getOpponentId(int playerId)
    {
        if (playerId == getPlayer1Id())
        {
            return getPlayer2Id();
        }

        return getPlayer1Id();
    }


    public String getOpponentPlayerNote(int playerId)
    {
        if (playerId == getPlayer1Id())
        {
            return getPlayer2Note();
        }

        return getPlayer1Note();
    }


    public int getWinnerId()
    {
        return winner_id;
    }


    public int getMatchesWonOrLost(int playerId, boolean getMatchesWon)
    {
        if (winner_id == 0)
        {
            return 0;
        }

        if (getMatchesWon ? playerId == winner_id : playerId != winner_id)
        {
            return 1;
        }

        return 0;
    }


    public int getSetsWonOrLost(int playerId, boolean getSetsWon)
    {
        int setsWonOrLost = 0;

        final int playerSet1GamesWon = playerId == player1_id ? player1_set1_games_won : player2_set1_games_won;
        final int playerSet2GamesWon = playerId == player1_id ? player1_set2_games_won : player2_set2_games_won;
        final int playerSet3GamesWon = playerId == player1_id ? player1_set3_games_won : player2_set3_games_won;

        final int opponentSet1GamesWon = playerId != player1_id ? player1_set1_games_won : player2_set1_games_won;
        final int opponentSet2GamesWon = playerId != player1_id ? player1_set2_games_won : player2_set2_games_won;
        final int opponentSet3GamesWon = playerId != player1_id ? player1_set3_games_won : player2_set3_games_won;

        if (getSetsWon ? playerSet1GamesWon > opponentSet1GamesWon : playerSet1GamesWon < opponentSet1GamesWon)
        {
            setsWonOrLost++;
        }

        if (getSetsWon ? playerSet2GamesWon > opponentSet2GamesWon : playerSet2GamesWon < opponentSet2GamesWon)
        {
            setsWonOrLost++;
        }

        if (getSetsWon ? playerSet3GamesWon > opponentSet3GamesWon : playerSet3GamesWon < opponentSet3GamesWon)
        {
            setsWonOrLost++;
        }

        return setsWonOrLost;
    }


    public int getGamesWonOrLost(int playerId, boolean getGamesWon)
    {
        final int playerSet1GamesWon = playerId == player1_id ? player1_set1_games_won : player2_set1_games_won;
        final int playerSet2GamesWon = playerId == player1_id ? player1_set2_games_won : player2_set2_games_won;
        final int playerSet3GamesWon = playerId == player1_id ? player1_set3_games_won : player2_set3_games_won;

        final int opponentSet1GamesWon = playerId != player1_id ? player1_set1_games_won : player2_set1_games_won;
        final int opponentSet2GamesWon = playerId != player1_id ? player1_set2_games_won : player2_set2_games_won;
        final int opponentSet3GamesWon = playerId != player1_id ? player1_set3_games_won : player2_set3_games_won;

        if (getGamesWon)
        {
            return playerSet1GamesWon + playerSet2GamesWon + playerSet3GamesWon;
        }
        else
        {
            return opponentSet1GamesWon + opponentSet2GamesWon + opponentSet3GamesWon;
        }
    }


    public float getGamesWonOrLostProrating3rdSetTiebreaker(int playerId, boolean getGamesWon)
    {
        final float set3TiebreakerRatio;   // Used to convert all 3rd set tiebreaker scores to 6 games

        if (isSet3Tiebreaker())
        {
            if (player1_set3_games_won > player2_set3_games_won)
            {
                set3TiebreakerRatio = (float) 6.0f / player1_set3_games_won;
            }
            else
            {
                set3TiebreakerRatio = (float) 6.0f / player2_set3_games_won;
            }
        }
        else
        {
            set3TiebreakerRatio = 1.0f;
        }

        final float playerSet1GamesWon = playerId == player1_id ? player1_set1_games_won : player2_set1_games_won;
        final float playerSet2GamesWon = playerId == player1_id ? player1_set2_games_won : player2_set2_games_won;


        final float playerSet3GamesWon = playerId == player1_id ?
                (float) player1_set3_games_won * set3TiebreakerRatio
                :
                (float) player2_set3_games_won * set3TiebreakerRatio;

        final float opponentSet1GamesWon = playerId != player1_id ? player1_set1_games_won : player2_set1_games_won;
        final float opponentSet2GamesWon = playerId != player1_id ? player1_set2_games_won : player2_set2_games_won;

        final float opponentSet3GamesWon = playerId != player1_id ?
                (float) player1_set3_games_won * set3TiebreakerRatio
                :
                (float) player2_set3_games_won * set3TiebreakerRatio;

        if (getGamesWon)
        {
            return playerSet1GamesWon + playerSet2GamesWon + playerSet3GamesWon;
        }
        else
        {
            return opponentSet1GamesWon + opponentSet2GamesWon + opponentSet3GamesWon;
        }
    }


    public int getGamesWon(int playerNo, int setNo)
    {
        if (playerNo == 1)
        {
            switch (setNo)
            {
                case 1: return player1_set1_games_won;
                case 2: return player1_set2_games_won;
                case 3: return player1_set3_games_won;
            }
        }
        else
        {
            switch (setNo)
            {
                case 1: return player2_set1_games_won;
                case 2: return player2_set2_games_won;
                case 3: return player2_set3_games_won;
            }
        }

        // Should never be reached:
        return 0;
    }


    public String getMatchDateFormattedForColumnHeader()
    {
        return DateUtils.getDateFormattedForColumnHeader(getMatchDate());
    }


    public int getSet3TiebreakerVal()
    {
        return is_set3_tiebreaker;
    }


    public boolean isSet3Tiebreaker()
    {
        return is_set3_tiebreaker == 1;
    }


    public int getIsPlayoffVal()
    {
        return is_playoff;
    }


    public boolean isPlayoff()
    {
        return is_playoff == 1;
    }


    public int getIsDefaultVal()
    {
        return is_default;
    }


    public boolean isDefault()
    {
        return is_default == 1;
    }


    public boolean isWinner(int playerId)
    {
        return playerId == getWinnerId();
    }


    public void setYr(int yr)
    {
        this.yr = yr;
    }


    public void setSess(int sess)
    {
        this.sess = sess;
    }


    public void setDivision(String division)
    {
        this.division = division;
    }


    public void setMatchDate(Calendar matchDate)
    {
        this.match_date = matchDate;
    }


    public void setMatchNo(int matchNo)
    {
        this.match_no = matchNo;
    }


    public void setPlayer1Id(int player1Id)
    {
        this.player1_id = player1Id;
    }


    public void setPlayer2Id(int player2Id)
    {
        this.player2_id = player2Id;
    }


    public void setWinnerId(int winnerId)
    {
        this.winner_id = winnerId;
    }


    public void setGamesWon(int playerNo, int setNo, int gamesWon)
    {
        if (playerNo == 1)
        {
            switch (setNo)
            {
                case 1:
                    player1_set1_games_won = gamesWon;
                    break;
                case 2:
                    player1_set2_games_won = gamesWon;
                    break;
                case 3:
                    player1_set3_games_won = gamesWon;
                    break;
            }
        }
        else
        {
            switch (setNo)
            {
                case 1:
                    player2_set1_games_won = gamesWon;
                    break;
                case 2:
                    player2_set2_games_won = gamesWon;
                    break;
                case 3:
                    player2_set3_games_won = gamesWon;
                    break;
            }
        }
    }


    public void setSet3TiebreakerVal(int tiebreakerVal)
    {
        is_set3_tiebreaker = tiebreakerVal;
    }


    public void setIsPlayoffVal(int playoffVal)
    {
        is_playoff = playoffVal;
    }


    public void setIsDefaultVal(int defaultVal)
    {
        is_default = defaultVal;
    }


    public boolean hasMatchBeenScored()
    {
        return getWinnerId() != 0 && ( getGamesWon(1, 1) != 0 || getGamesWon(2, 1) != 0 );
    }


    public String getScoresFromWinnerPerspectiveAsString()
    {
        return getScoresFromWinnerPerspectiveAsString(true);
    }


    public String getScoresFromLoserPerspectiveAsString()
    {
        return getScoresFromWinnerPerspectiveAsString(false);
    }


    private String getScoresFromWinnerPerspectiveAsString(boolean useWinnerPerspective)
    {
        PlayerPerspectiveScores scores = useWinnerPerspective ? getScoresFromWinnerPerspective() :
                getScoresFromLoserPerspective();

        StringBuilder sb = new StringBuilder();

        boolean firstScore = true;

        for (int s = 1; s <= 3; s++)
        {
            final int gamesWon = scores.getGamesWon(s);
            final int gamesLost = scores.getGamesLost(s);

            if (gamesWon != 0 || gamesLost != 0)
            {
                if (!firstScore)
                {
                    sb.append(", ");
                }

                sb.append(gamesWon).append("-").append(gamesLost);

                firstScore = false;
            }

        }

        if (getSet3TiebreakerVal() == 1)
        {
            sb.append(" tb");
        }

        if (getIsDefaultVal() == 1)
        {
            sb.append(" dft");
        }

        return sb.toString();
    }


    public PlayerPerspectiveScores getScoresFromWinnerPerspective()
    {
        return getScoresFromPlayerPerspective(getWinnerId());
    }


    private PlayerPerspectiveScores getScoresFromLoserPerspective()
    {
        int loserId = getWinnerId() == getPlayer1Id() ? getPlayer2Id() : getPlayer1Id();
        return getScoresFromPlayerPerspective(loserId);
    }


    private PlayerPerspectiveScores getScoresFromPlayerPerspective(int playerId)
    {
        final int player1Id = getPlayer1Id();

        int gamesWon[] = { 0, 0, 0};
        int gamesLost[] = { 0, 0, 0};

        for (int s = 1; s <= 3; s++)
        {
            final int player1GamesWon = getGamesWon(1, s);
            final int player2GamesWon = getGamesWon(2, s);

            if (player1GamesWon != 0 || player2GamesWon != 0)
            {
                gamesWon[s - 1] = playerId == player1Id ? player1GamesWon : player2GamesWon;
                gamesLost[s - 1] = playerId == player1Id ? player2GamesWon : player1GamesWon;
            }
        }

        return new PlayerPerspectiveScores(gamesWon, gamesLost);
    }


    public int getWeekNo()
    {
        return week_no;
    }


    public void setWeekNo(int week_no)
    {
        this.week_no = week_no;
    }


    public boolean hasCustomLabel()
    {
        return custom_label != null && custom_label.length() > 0;
    }


    public String getCustomLabel()
    {
        return custom_label;
    }


    public void setCustomLabel(String custom_label)
    {
        this.custom_label = custom_label;
    }


    public boolean hasNote()
    {
        return note != null && note.length() > 0;
    }


    public String getNote()
    {
        return note;
    }


    public void setNote(String note)
    {
        this.note = note;
    }



    public boolean hasPlayer1Note()
    {
        return player1_note != null && player1_note.length() > 0;
    }


    public String getPlayer1Note()
    {
        return player1_note;
    }


    public void setPlayer1Note(String player1_note)
    {
        this.player1_note = player1_note;
    }


    public boolean hasPlayer2Note()
    {
        return player2_note != null && player2_note.length() > 0;
    }


    public String getPlayer2Note()
    {
        return player2_note;
    }


    public void setPlayer2Note(String player2_note)
    {
        this.player2_note = player2_note;
    }

}
