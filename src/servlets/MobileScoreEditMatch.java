package servlets;

import beans.MatchBean;
import dao.Matches;
import other.MatchIdType;
import other.SessionHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class MobileScoreEditMatch extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        try
        {
            if (!SessionHelper.hasPassedAdminSecurityClearance(session, req))
            {
                SessionHelper.setPageAfterPwd(session, "MobileScoreEditMatch.page");
                res.sendRedirect(SessionHelper.ADMIN_CLEARANCE_PAGE_PATH);
                return;
            }

            if (req.getMethod().equals("POST"))
            {
                String redirectPath = doSaveAndGetRedirectPath(session, req.getParameterMap());
                res.sendRedirect(redirectPath);
                return;
            }

            RequestDispatcher rd = req.getRequestDispatcher("MobileScoreEditMatch.jsp");
            rd.forward(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }


    /**
     *
     * @param parameterMap - note the "value" side of this map is a String[] -- even though it often only has one
     * value in it.  Handle with care.
     */
    private static String doSaveAndGetRedirectPath(HttpSession session,
                                                   Map<String, String[]> parameterMap)
    {
        int matchId = Integer.parseInt(parameterMap.get("matchId")[0]);

        // Be careful with check-able fields like radio groups and checkboxes.  They
        // may not be present in the request parameters.

        int winningPlayerNo = parameterMap.containsKey("winningPlayerNo") ?
                Integer.parseInt(parameterMap.get("winningPlayerNo")[0]) : 0;

        int tiebreaker = parameterMap.containsKey("tiebreaker") ? 1 : 0;
        int is_default = parameterMap.containsKey("is_default") ? 1 : 0;

        MatchBean match = Matches.createInstance(MatchIdType.MATCH, matchId).getRecords().get(0);

        int winnerId;

        if (winningPlayerNo == 1)
        {
            winnerId = match.getPlayer1Id();
        }
        else if (winningPlayerNo == 2)
        {
            winnerId = match.getPlayer2Id();
        }
        else
        {
            winnerId = 0;
        }

        match.setWinnerId(winnerId);

        for (int s = 1; s <= 3; s++)
        {
            final String gamesWonFieldName = String.format("set%sgameswon", s);
            final String gamesWonStr = parameterMap.get(gamesWonFieldName)[0].trim();
            final int gamesWon = gamesWonStr.length() == 0 ? 0 : Integer.parseInt(gamesWonStr);

            final String gamesLostFieldName = String.format("set%sgameslost", s);
            final String gamesLostStr = parameterMap.get(gamesLostFieldName)[0].trim();
            final int gamesLost = gamesLostStr.length() == 0 ? 0 : Integer.parseInt(gamesLostStr);

            if (winningPlayerNo == 1)
            {
                match.setGamesWon(1, s, gamesWon);
                match.setGamesWon(2, s, gamesLost);
            }
            else if (winningPlayerNo == 2)
            {
                match.setGamesWon(2, s, gamesWon);
                match.setGamesWon(1, s, gamesLost);
            }
        }

        match.setSet3TiebreakerVal(tiebreaker);
        match.setIsDefaultVal(is_default);

        Matches.update(match);

        return "MobileScoreSelectMatch.page";

    }

}
