package servlets;

import other.SessionHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SinglesLeagueClearance extends HttpServlet
{
    public static final String SINGLES_LEAGUE_PWD_UPPER_CASE = "SINGLES1";


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        try
        {
            if (req.getMethod().equals("POST"))
            {
                // If password is good, mark as clearance passed and move on.
                String password = req.getParameter("password");

                if (password != null && password.toUpperCase().equals(SINGLES_LEAGUE_PWD_UPPER_CASE))
                {
                    SessionHelper.setHasPassedSinglesLeagueSecurityClearance(session);
                    final String remember = req.getParameter("remember");

                    if (remember != null)
                    {
                        SessionHelper.setRememberedSinglesLeaguePassword(password, req, res);
                    }

                    res.sendRedirect(SessionHelper.getPageAfterPwd(session));
                    return;
                }
            }

            RequestDispatcher rd = req.getRequestDispatcher("/SinglesLeagueClearance.jsp");
            rd.forward(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }

}
