package servlets;

import other.SessionHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

public class MobileScoreSelectMatch extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        try
        {
            if (!SessionHelper.hasPassedAdminSecurityClearance(session, req))
            {
                SessionHelper.setPageAfterPwd(session, "MobileScoreSelectMatch.page");
                res.sendRedirect(SessionHelper.ADMIN_CLEARANCE_PAGE_PATH);
                return;
            }

            if (req.getMethod().equals("POST"))
            {
                String redirectPath = potentiallyChangePlayer(session, req.getParameterMap());
                res.sendRedirect(redirectPath);
                return;
            }

            RequestDispatcher rd = req.getRequestDispatcher("MobileScoreSelectMatch.jsp");
            rd.forward(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }


    /**
     *
     * @param parameterMap - note the "value" side of this map is a String[] -- even though it often only has one
     * value in it.  Handle with care.
     */
    private static String potentiallyChangePlayer(HttpSession session,
                                                Map<String, String[]> parameterMap)
    {
        String action = parameterMap.get("action")[0];

        if (action.equals("changePlayer"))
        {
            String ssmPlayerSelect = parameterMap.get("ssmPlayerSelect")[0];
            return "MobileScoreSelectMatch.page?playerId=" + ssmPlayerSelect;
        }

        return "MobileScoreSelectMatch.page";
    }

}
