package servlets;

import other.SessionHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class MobileSinglesLeagueContacts extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        try
        {
            if (!SessionHelper.hasPassedSinglesLeagueSecurityClearance(session, req))
            {
                SessionHelper.setPageAfterPwd(session, "MobileSinglesLeagueContacts.page?" + req.getQueryString());
                res.sendRedirect(SessionHelper.SINGLES_LEAGUE_CLEARANCE_PAGE_PATH);
                return;
            }

            RequestDispatcher rd = req.getRequestDispatcher("MobileSinglesLeagueContacts.jsp");
            rd.forward(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }

}
