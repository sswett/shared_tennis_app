package servlets;

import other.SessionHelper;
import other.View;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ChangeView extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();
        String changeView = req.getParameter("changeView");

        if (changeView != null && changeView.equals("true"))
        {
            View view = View.valueOf(req.getParameter("viewRadio"));
            SessionHelper.setDefaultView(session, view, req, res);
        }

        String redirectToPage = req.getParameter("redirectToPage");
        res.sendRedirect(redirectToPage);
    }

}
