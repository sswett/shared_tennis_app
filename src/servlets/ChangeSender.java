package servlets;

import other.SessionHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ChangeSender extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();
        int playerId = Integer.parseInt(req.getParameter("playerId"));
        SessionHelper.setDefaultAdminId(session, playerId, req, res);
        res.getWriter().print("ok");
    }

}
