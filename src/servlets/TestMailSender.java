package servlets;

import other.MailSender;
import other.SessionHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class TestMailSender extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        try
        {
            if (!SessionHelper.hasPassedAdminSecurityClearance(session, req))
            {
                SessionHelper.setPageAfterPwd(session, "TestMailSender.page");
                res.sendRedirect(SessionHelper.ADMIN_CLEARANCE_PAGE_PATH);
                return;
            }

            if (req.getMethod().equals("POST"))
            {
                String[] cellPhoneNumbers = new String[] { "6162042664" };

                boolean success = MailSender.sendTextMsg("test msg", "Steve Swett", "steve.swett@comcast.net",
                        "6162042664", cellPhoneNumbers);
            }
            else
            {
                SessionHelper.ensureGroupDataLoadedInSession(session, req, res);
            }

            RequestDispatcher rd = req.getRequestDispatcher("TestMailSender.jsp");
            rd.forward(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }

}
