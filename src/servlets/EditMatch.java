package servlets;

import beans.MatchBean;
import dao.Matches;
import other.MatchIdType;
import other.SessionHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class EditMatch extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        try
        {
            if (!SessionHelper.hasPassedAdminSecurityClearance(session, req))
            {
                SessionHelper.setPageAfterPwd(session, "EditMatch.page");
                res.sendRedirect(SessionHelper.ADMIN_CLEARANCE_PAGE_PATH);
                return;
            }

            if (req.getMethod().equals("POST"))
            {
                String redirectPath = doSaveAndGetRedirectPath(session, req.getParameterMap());
                res.sendRedirect(redirectPath);
                return;
            }

            String deleteMatch = req.getParameter("delete");

            // Delete the match and immediately return to match list
            if (deleteMatch != null && deleteMatch.equals("true"))
            {
                int matchId = Integer.parseInt(req.getParameter("id"));
                MatchBean match = Matches.createInstance(MatchIdType.MATCH, matchId).getRecords().get(0);
                String div = match.getDivision();
                Matches.delete(matchId);

                int prevMatchId = Integer.parseInt(req.getParameter("prevId"));
                int nextMatchId = Integer.parseInt(req.getParameter("nextId"));
                final int returnToMatchId;

                if (prevMatchId != -1)
                {
                    returnToMatchId = prevMatchId;
                }
                else if (nextMatchId != -1)
                {
                    returnToMatchId = nextMatchId;
                }
                else
                {
                    returnToMatchId = -1;
                }

                final String redirectPath = "EditMatchList.page?div=" + div + "&fromMatchId=" + returnToMatchId;
                res.sendRedirect(redirectPath);
                return;
            }

            RequestDispatcher rd = req.getRequestDispatcher("EditMatch.jsp");
            rd.forward(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }


    /**
     *
     * @param parameterMap - note the "value" side of this map is a String[] -- even though it often only has one
     * value in it.  Handle with care.
     */
    private static String doSaveAndGetRedirectPath(HttpSession session,
                                                   Map<String, String[]> parameterMap)
    {
        int matchId = Integer.parseInt(parameterMap.get("matchId")[0]);
        int player1Id = Integer.parseInt(parameterMap.get("player1Id")[0]);
        int player2Id = Integer.parseInt(parameterMap.get("player2Id")[0]);

        // Be careful with check-able fields like radio groups and checkboxes.  They
        // may not be present in the request parameters.

        int winningPlayerNo = parameterMap.containsKey("winningPlayerNo") ?
                Integer.parseInt(parameterMap.get("winningPlayerNo")[0]) : 0;

        int tiebreaker = parameterMap.containsKey("tiebreaker") ? 1 : 0;
        int is_playoff = parameterMap.containsKey("is_playoff") ? 1 : 0;
        int is_default = parameterMap.containsKey("is_default") ? 1 : 0;

        MatchBean match = Matches.createInstance(MatchIdType.MATCH, matchId).getRecords().get(0);

        match.setPlayer1Id(player1Id);
        match.setPlayer2Id(player2Id);

        int winnerId;

        if (winningPlayerNo == 1)
        {
            winnerId = player1Id;
        }
        else if (winningPlayerNo == 2)
        {
            winnerId = player2Id;
        }
        else
        {
            winnerId = 0;
        }

        match.setWinnerId(winnerId);

        for (int p = 1; p <= 2; p++)
        {
            for (int s = 1; s <= 3; s++)
            {
                final String fieldName = String.format("player%sset%sgameswon", p, s);
                int gamesWon = Integer.parseInt(parameterMap.get(fieldName)[0]);
                match.setGamesWon(p, s, gamesWon);
            }
        }

        match.setSet3TiebreakerVal(tiebreaker);

        String player1Note = parameterMap.get("player1_note")[0];
        String player2Note = parameterMap.get("player2_note")[0];
        String customLabel = parameterMap.get("custom_label")[0];
        String note = parameterMap.get("note")[0];

        match.setPlayer1Note(player1Note);
        match.setPlayer2Note(player2Note);
        match.setCustomLabel(customLabel);
        match.setNote(note);

        match.setIsPlayoffVal(is_playoff);
        match.setIsDefaultVal(is_default);

        Matches.update(match);

        String next = parameterMap.get("next")[0];

        if (next.equals("true"))
        {
            ArrayList<MatchBean> matches = Matches.
                    createInstanceGetNextMatchOnly(match.getDivision(),
                            match.getWeekNo(), match.getMatchNo()).getRecords();

            if (matches.size() > 0)
            {
                return "EditMatch.page?id=" + matches.get(0).getId();
            }
        }

        return "EditMatchList.page?div=" + match.getDivision() + "&fromMatchId=" + match.getId();

    }

}
