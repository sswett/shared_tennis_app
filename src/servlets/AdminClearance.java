package servlets;

import other.SessionHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AdminClearance extends HttpServlet
{
    public static final String ADMIN_PWD_UPPER_CASE = "ROCK4";


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        try
        {
            if (req.getMethod().equals("POST"))
            {
                // If password is good, mark as clearance passed and move on.
                String password = req.getParameter("password");

                if (password != null && password.toUpperCase().equals(ADMIN_PWD_UPPER_CASE))
                {
                    SessionHelper.setHasPassedAdminSecurityClearance(session);
                    final String remember = req.getParameter("remember");

                    if (remember != null)
                    {
                        SessionHelper.setRememberedAdminPassword(password, req, res);
                    }

                    // Temporary test of redirecting back to Angular:
                    final boolean redirectToAngular = false;
                    
                    if (redirectToAngular)
                    {
                    	// This indeed works, great!
                    	res.sendRedirect("http://localhost:8080/AngularPlayerList/AngularApp.html#/players");
                        return;
                    }
                    
                    res.sendRedirect(SessionHelper.getPageAfterPwd(session));
                    return;
                }
            }

            RequestDispatcher rd = req.getRequestDispatcher("/AdminClearance.jsp");
            rd.forward(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }

}
