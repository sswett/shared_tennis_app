package servlets;

import other.MailSender;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SendServerMsg extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        String sendServerMsg = req.getParameter("sendServerMsg");

        boolean wasServerMsgSendRequested = false;
        boolean wasTextMsgAttempted = false;
        boolean wasEmailMsgAttempted = false;
        boolean wasTextMsgSuccessful = false;
        boolean wasEmailMsgSuccessful = false;

        if (sendServerMsg != null && sendServerMsg.equals("true"))
        {
            wasServerMsgSendRequested = true;

            String serverMsg = req.getParameter("serverMsg");
            String sendAsText = req.getParameter("sendAsText");
            String sendAsEmail = req.getParameter("sendAsEmail");
            String senderSelect = req.getParameter("senderSelect");

            String[] senderIdNameEmail = senderSelect.split(";");   // 0 = id, 1 = name, 2 = email, 3 = phone
            String senderName = senderIdNameEmail[1];
            String senderEmail = senderIdNameEmail[2];
            String senderPhone = senderIdNameEmail[3];

            // Send text message:
            if (sendAsText != null && sendAsText.equals("1"))
            {
                wasTextMsgAttempted = true;
                String[] cellPhoneNumbers = req.getParameter("filteredPhoneList").split(",");

                wasTextMsgSuccessful = MailSender.sendTextMsg(serverMsg, senderName, senderEmail, senderPhone,
                        cellPhoneNumbers);
            }

            // Send e-mail message:
            if (sendAsEmail != null && sendAsEmail.equals("1"))
            {
                wasEmailMsgAttempted = true;
                String[] emailAddresses = req.getParameter("filteredEmailList").split(";");

                wasEmailMsgSuccessful = MailSender.sendEmailMsg(serverMsg, senderEmail, senderName, senderPhone,
                        emailAddresses);
            }
        }

        StringBuilder sbResponse = new StringBuilder("");

        if (wasServerMsgSendRequested)
        {
            sbResponse.append("Results of send: ");

            if (wasTextMsgAttempted)
            {
                sbResponse.append(" Text msg ").append(wasTextMsgSuccessful ? "was successful." : "failed.");
            }

            if (wasEmailMsgAttempted)
            {
                sbResponse.append(" Email msg ").append(wasEmailMsgSuccessful ? "was successful." : "failed.");
            }
        }

        res.getWriter().print(sbResponse.toString());
    }

}
