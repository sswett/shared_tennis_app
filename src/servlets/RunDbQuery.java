package servlets;

import db.DBQuery;
import other.SessionHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class RunDbQuery extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        try
        {
            if (!SessionHelper.hasPassedAdminSecurityClearance(session, req))
            {
                SessionHelper.setPageAfterPwd(session, "RunDbQuery.page");
                res.sendRedirect(SessionHelper.ADMIN_CLEARANCE_PAGE_PATH);
                return;
            }

            if (req.getMethod().equals("POST"))
            {
                runQry(session, req);
            }
            else
            {
                SessionHelper.ensureGroupDataLoadedInSession(session, req, res);
            }

            RequestDispatcher rd = req.getRequestDispatcher("RunDbQuery.jsp");
            rd.forward(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }


    private static void runQry(HttpSession session,
                               HttpServletRequest req)
    {
        String qry = req.getParameter("qry");
        DBQuery.execQuery(qry);
    }

}
