package servlets;

import dao.GroupMembers;
import other.SessionHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class EditGroupMembers extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        try
        {
            if (!SessionHelper.hasPassedAdminSecurityClearance(session, req))
            {
                SessionHelper.setPageAfterPwd(session, "EditGroupMembers.page");
                res.sendRedirect(SessionHelper.ADMIN_CLEARANCE_PAGE_PATH);
                return;
            }

            if (req.getMethod().equals("POST"))
            {
                doSave(session, req.getParameterMap());
            }
            else
            {
                SessionHelper.ensureGroupDataLoadedInSession(session, req, res);
            }

            RequestDispatcher rd = req.getRequestDispatcher("EditGroupMembers.jsp");
            rd.forward(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }


    /**
     *
     * @param parameterMap - note the "value" side of this map is a String[] -- even though it often only has one
     * value in it.  Handle with care.
     */
    private static void doSave(HttpSession session,
                               Map<String,String[]> parameterMap)
    {
        TreeMap<String,String[]> treeMap = new TreeMap<String,String[]>(parameterMap);
        SortedMap<String,String[]> membersToRemoveMap = treeMap.subMap("mem", "mem~");
        SortedMap<String,String[]> nonMembersToAddMap = treeMap.subMap("non", "non~");

        int groupId = SessionHelper.getDefaultGroupId(session);

        // NOTE: Using a delete-then-add approach because of the way the pre-existing database had its keys
        // structured.

        ArrayList<Integer> membersToRemove = new ArrayList<Integer>();
        ArrayList<Integer> nonMembersToAdd = new ArrayList<Integer>();

        for (String key : membersToRemoveMap.keySet())
        {
            int playerId = Integer.parseInt(key.substring(3));
            membersToRemove.add(playerId);
        }

        for (String key : nonMembersToAddMap.keySet())
        {
            int playerId = Integer.parseInt(key.substring(3));
            nonMembersToAdd.add(playerId);
        }

        if (membersToRemove.size() > 0)
        {
            GroupMembers.delete(groupId, membersToRemove);
        }

        if (nonMembersToAdd.size() > 0)
        {
            GroupMembers.add(groupId, nonMembersToAdd);
        }
    }

}
