package servlets;

import other.SessionHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class MobilePlayerView extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        try
        {
            boolean wasSaved = false;

            if (req.getMethod().equals("POST"))
            {
                String action = req.getParameter("action");

                if (action != null)
                {
                    if (action.equals("save"))
                    {
                        FullSpreadsheet.doSave(req.getParameterMap());
                        wasSaved = true;
                    }
                    else if (action.equals("changePlayer"))
                    {
                        int playerId = Integer.parseInt(req.getParameter("playerSelect"));
                        SessionHelper.setMobilePlayerViewSelectedPlayer(session, playerId);
                        SessionHelper.setDefaultPlayerId(session, playerId, req, res);
                    }
                }
            }
            else
            {
                SessionHelper.ensureGroupDataLoadedInSession(session, req, res);
            }

            String targetPage = "MobilePlayerView.jsp" + (wasSaved ? "?saved=true" : "");
            RequestDispatcher rd = req.getRequestDispatcher(targetPage);
            rd.forward(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }

}
