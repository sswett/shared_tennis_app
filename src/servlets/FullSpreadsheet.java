package servlets;

import beans.EventResponseKeyBean;
import beans.EventResponseNewDataBean;
import dao.EventResponses;
import other.PlayerResponse;
import other.SessionHelper;
import utils.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class FullSpreadsheet extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();


        try
        {
            boolean wasSaved = false;

            if (req.getMethod().equals("POST"))
            {
                doSave(req.getParameterMap());
                wasSaved = true;
            }
            else
            {
                SessionHelper.ensureGroupDataLoadedInSession(session, req, res);
            }

            String targetPage = "Spreadsheet.jsp" + (wasSaved ? "?saved=true" : "");

            RequestDispatcher rd = req.getRequestDispatcher(targetPage);
            rd.forward(req, res);

            // Doesn't seem to matter whether a forward or redirect
            // res.sendRedirect(targetPage);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }


    /**
     *
     * @param parameterMap - note the "value" side of this map is a String[] -- even though it often only has one
     * value in it.  Handle with care.
     */
    protected static void doSave(Map<String,String[]> parameterMap)
    {
        TreeMap<String,String[]> treeMap = new TreeMap<String,String[]>(parameterMap);
        SortedMap<String,String[]> sortedMap = treeMap.subMap("old", "old~");

        // NOTE: Using a delete-then-add approach because of the way the pre-existing database had its keys
        // structured.

        ArrayList<EventResponseKeyBean> keysToDelete = new ArrayList<EventResponseKeyBean>();
        ArrayList<EventResponseNewDataBean> dataToAdd = new ArrayList<EventResponseNewDataBean>();

        Calendar now = Calendar.getInstance();

        for (String oldKey : sortedMap.keySet())
        {
            String newKey = oldKey.replaceFirst("old", "new");
            String oldValue = parameterMap.get(oldKey)[0];
            String newValue = parameterMap.get(newKey)[0];

            if (!oldValue.equals(newValue))
            {
                // Gather up old responses to delete:

                String playerIdAndEventId = oldKey.replaceFirst("old_", "");
                String[] parts = playerIdAndEventId.split("_");
                int playerId = Integer.parseInt(parts[0]);
                int eventId = Integer.parseInt(parts[1]);
                keysToDelete.add(new EventResponseKeyBean(playerId, eventId));

                // Gather up new responses to add:

                PlayerResponse playerResponse;

                try
                {
                    playerResponse = PlayerResponse.valueOf(newValue);
                }
                catch (IllegalArgumentException e)
                {
                    playerResponse = PlayerResponse.unknown;
                }

                dataToAdd.add(new EventResponseNewDataBean(playerId, eventId, playerResponse, now));
            }
        }

        if (keysToDelete.size() > 0)
        {
            EventResponses.delete(keysToDelete);
        }

        if (dataToAdd.size() > 0)
        {
            EventResponses.add(dataToAdd);
        }
    }

}
