package servlets;

import dao.MatchParms;
import dao.Matches;
import other.SessionHelper;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Calendar;

public class AdminMenu extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();

        try
        {
            if (!SessionHelper.hasPassedAdminSecurityClearance(session, req))
            {
                SessionHelper.setPageAfterPwd(session, "AdminMenu.page");
                res.sendRedirect(SessionHelper.ADMIN_CLEARANCE_PAGE_PATH);
                return;
            }

            // Cute way to populate matches table without writing a UI
            //      Example: AdminMenu.page?pop=4.0

            final String populateMatchesForDivision = req.getParameter("pop");

            if (populateMatchesForDivision != null)
            {
                Calendar firstSunday = Calendar.getInstance();
                firstSunday.clear();
                // firstSunday.set(2014, 4, 4);   // month is 0-based -- this represents May 4th
                firstSunday.set(2014, 7, 17);   // month is 0-based -- this represents Aug 17th

                if (populateMatchesForDivision.equals("4.0"))
                {
                    Matches.populateRecords(2014, 2, "4.0", firstSunday, 11, 6);
                }

                else if (populateMatchesForDivision.equals("3.5"))
                {
                    // Matches.populateRecords(2014, 1, "3.5", firstSunday, 13, 7);
                }
            }

            // Cute way to populate playoff matches table without writing a UI
            //      Example: AdminMenu.page?playoff=4.0

            final String populatePlayoffMatchesForDivision = req.getParameter("playoff");

            if (populatePlayoffMatchesForDivision != null)
            {
                Calendar firstSunday = Calendar.getInstance();
                firstSunday.clear();
                firstSunday.set(2014, 4, 4);   // month is 0-based -- this represents May 4th

                if (populatePlayoffMatchesForDivision.equals("4.0"))
                {
                    MatchParms[] matchParms =
                            {
                                    new MatchParms(10, 6),   // example: week 10, 6 matches
                                    new MatchParms(11, 6),
                                    new MatchParms(12, 6),
                                    new MatchParms(13, 4)

                            };

                    Matches.populateRecords(2014, 1, "4.0", firstSunday, matchParms);
                }
            }

            // Cute way to fill the week_no field in the matches table without writing a UI
            //      Example: AdminMenu.page?fillwk=4.0

            final String fillWeekNoForDivision = req.getParameter("fillwk");

            if (fillWeekNoForDivision != null)
            {
                Matches.fillWeekNo(fillWeekNoForDivision);
            }

            RequestDispatcher rd = req.getRequestDispatcher("AdminMenu.jsp");
            rd.forward(req, res);
        }
        catch (Exception e)
        {
            SessionHelper.setErrorPageException(req, session, e);
            res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
        }
    }

}
