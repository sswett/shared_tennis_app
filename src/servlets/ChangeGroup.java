package servlets;

import other.SessionHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ChangeGroup extends HttpServlet
{


    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
    {
        HttpSession session = req.getSession();
        String changeGroup = req.getParameter("changeGroup");

        if (changeGroup != null && changeGroup.equals("true"))
        {
            int newGroupId = Integer.parseInt(req.getParameter("groupSelect"));
            SessionHelper.setDefaultGroupId(session, newGroupId, req, res);
        }

        String redirectToPage = req.getParameter("redirectToPage");
        res.sendRedirect(redirectToPage);
    }

}
