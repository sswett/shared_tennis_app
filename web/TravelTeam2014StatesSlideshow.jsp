<%@ page import="other.SessionHelper" %>
<%
	final boolean isMobile = SessionHelper.isMobileBrowser(request);
	final int imgWidth = 1024;
	final int imgHeight = 768;
%>
<!DOCTYPE html>
<html>

<head>
    <title>Slideshow - Travel Team 2014 States at Midland</title>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <script src="js/basic-jquery-slider-master/js/bjqs-1.3.min.js"></script>

    <link href="js/basic-jquery-slider-master/bjqs.css" rel="stylesheet">
    <link href="css/Slideshow.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <link href="css/Singles.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script>

        <%-- when responsive, the dimensions are the maximums --%>

        jQuery(document).ready(function($) {
            $('#my-slideshow').bjqs({
                height : 600,
                width : 900,
                responsive : true,
                hoverpause : true, // pause the slider on hover
                animspeed : 10000 // the delay between each slide
            });
        });

    </script>

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="container">

<div id="my-slideshow">
    <ul class="bjqs">
        <li>
            <img src="slideshows/travel_teams/2014states/40_plus_team_with_girls.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            <% if (isMobile) { %>
                    title="40 & Over Team: The girls: Kara and Lauren. Front: Jim, Chuck, Mike S, Mark. 
                    Rear: Chris, Butch, Carl, Steve, Paul." 
            <% } else { %>
                    title="40 & Over Team: The girls: Kara and Lauren Wiegand. Front: Jim DeLaFuente, Chuck White, Mike Schmidt, Mark Delongpre. 
                    Rear: Chris Peceny, Butch Zannini, Carl Wiegand, Steve Swett, Paul Brown." 
            <% } %>
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/40_plus_team.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            <% if (isMobile) { %>
                    title="40 & Over Team: Front: Jim, Chuck, Mike S, Mark. 
                    Rear: Chris, Butch, Carl, Steve, Paul." 
            <% } else { %>
                    title="40 & Over Team: Front: Jim DeLaFuente, Chuck White, Mike Schmidt, Mark Delongpre. 
                    Rear: Chris Peceny, Butch Zannini, Carl Wiegand, Steve Swett, Paul Brown." 
            <% } %>
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Al_Mike.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Al & Mike" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Al.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Al" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Butch_Harv_second_serve.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Great picture of Butch while Harv tosses in a second serve" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Carl_serving.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Carl - great form and looking cool" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Chris_Chuck_Jim_watching.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Between matches: Chris, Chuck and Jim" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Chris_in_stands_cute_girl.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Chris in stands somehow watching tennis even though miss 'short shorts' is nearby. Chuck, Jim and Mike O in foreground." 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Chris_Mike_Carl_Kara_Paul_Jim_Chuck_Dawn_Butch_Mishelle_Jill.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Watching: Front: Chris, Mike, Carl, Kara, Paul, Jim, Chuck, Dawn. Back: Butch, Mishelle, Jill." 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Chuck_serving_Jim.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Chuck & Jim" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Harv_Al_Mike_pose.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Should their picture be in the dictionary under 'threesome?'" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Harv_backhand1_Butch.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Harv & Butch" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Harv_backhand2_Butch.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Harv & Butch" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Harv_pissed.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Is it just me, or does Harv look a little pissed?" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Harv_serving.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Harv & Butch" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Mark_forehand_Steve.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Mark & Steve" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Mark_Paul_Carl.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Mark, Paul, Carl" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Mark_serving1.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Mark serving (1 of 3)" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Mark_serving2.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Mark serving (2 of 3)" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Mark_serving3.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Mark serving (3 of 3)" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Mark.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Mark" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Mike_Butch_Chris_Dale_girl.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Mike, Butch, Chris, Dale and 'short shorts'" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/MikeO_Paul_and_Chuck_Jim_and_MikeS_Butch.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="3 dubs courts in action: Mike O & Paul, Chuck & Jim, Mike S & Butch" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Paul_Carl_smash.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Paul & Carl following smash" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Paul_forehand.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Paul about to unleash a forehand while Mike O looks on. Jim & Chuck on next court." 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Paul_serving_Carl_Mark_Steve.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Paul serves in match with Carl while Mark and Steve play singles on adjacent courts." 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Steve_serve1.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Steve serving (1 of 3)" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Steve_serve2.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Steve serving (2 of 3)" 
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014states/Steve_serve3.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            title="Steve serving (3 of 3)" 
                    />
        </li>

    </ul>
</div>


</div>


</body>

</html>