<%@ page import="ui.TestGetEvents" %>
<!doctype html>
<html>
<head>
<title>Test Get Events</title>

    <style type="text/css">

        .event { margin-bottom: 10px; }

    </style>

</head>
<body>

<%
    for (String name : TestGetEvents.getEvents())
    {
%>
<div class="event">
<%= name %>
</div>
<%
    }
%>

</body>
</html>