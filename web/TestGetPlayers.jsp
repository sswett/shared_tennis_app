<%@ page import="ui.TestGetPlayers" %>

<%-- Test with this url: http://localhost:8080/TestGetPlayers.jsp --%>

<!doctype html>
<html>
<head>
<title>Test Get Players</title>
</head>
<body>

<%
    for (String name : TestGetPlayers.getPlayers())
    {
%>
<%= name %><br/>
<%
    }
%>

</body>
</html>