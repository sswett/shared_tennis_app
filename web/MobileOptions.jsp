<%@ page import="other.SessionHelper" %>
<%@ page import="other.View" %>
<%
    View view = SessionHelper.getDefaultView(session);

    String nextPage;

    if (view.isGroupView())
    {
        nextPage = "MobileGroupView.page";
    }
    else if (view.isPlayerView())
    {
        nextPage = "MobilePlayerView.page";
    }
    else if (view.isPlayerAllTeamsView())
    {
        nextPage = "MobilePlayerAllTeamsView.page";
    }
    else
    {
        nextPage = "";
    }

    final boolean hasAdminClearance = SessionHelper.hasPassedAdminSecurityClearance(session, request);
%>
<!DOCTYPE html>
<html>

<head>
    <title>Tennis Mobile Options</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <script type="text/javascript">

        function doNext()
        {
            window.location = "<%= nextPage %>";
        }

        function showContacts()
        {
            window.location = "/GroupContacts.page?groupId=<%= (Integer) session.getAttribute(SessionHelper.DEFAULT_GROUP_SESS_ATTR_KEY) %>"
        }

    </script>

    <script src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>
    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="mobileOptionsPageContainer">

    <jsp:include page="ChangeGroup.jsp" flush="true">
        <jsp:param name="redirectToPage" value="MobileOptions.page" />
    </jsp:include>

    <jsp:include page="ChangeView.jsp" flush="true">
        <jsp:param name="redirectToPage" value="MobileOptions.page" />
    </jsp:include>

    <div id="mobileNextContainer">
        <form name="mobileOptionsForm">
            <input type="button" value="Show Events" onclick="doNext()" />

            <% if (hasAdminClearance) { %>
            <br/><br/>
            <input type="button" class="commButton" value="Communicate" onclick="displayCommunicationsPage('')" />
			<% } %>
            
            <br/><br/>
            <input type="button" value="Show Contacts" onclick="showContacts()" />
        </form>
    </div>


</div>

</body>

</html>