<%@ page import="other.SessionHelper" %>
<%@ page import="utils.DateUtils" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="other.MatchType" %>
<!DOCTYPE html>
<html>

<head>
    <title>Singles League Menu</title>

    <link href="css/Admin.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="pageContainer">

    <div id="menuOuterBox">

        <div id="menuInnerBox">

            <div id="menuLinksContainer">
                <ul>

                    <li><a href="SinglesLeagueGuidelines.page">Guidelines</a></li>

                    <li>
                        <a href="SinglesLeaguePlayerList.page?div=3.5">3.5</a>
                        <a href="SinglesLeaguePlayerList.page?div=4.0">4.0</a>
                        Player Cards (has it all)
                    </li>

                    <li>
                        <a href="SinglesLeagueStandings.page?div=3.5&matchType=<%= MatchType.REGULAR_SEASON.name() %>">3.5</a>
                        <a href="SinglesLeagueStandings.page?div=4.0&matchType=<%= MatchType.REGULAR_SEASON.name() %>">4.0</a>
                        Standings
                    </li>

                    <li>
                        <a href="SinglesLeagueContacts.page?div=3.5">3.5</a>
                        <a href="SinglesLeagueContacts.page?div=4.0">4.0</a>
                        Player Contact Info
                    </li>

                    <li>
                        <a href="SinglesLeagueSchedule.page?div=3.5">3.5</a>
                        <a href="SinglesLeagueSchedule.page?div=4.0">4.0</a>
                        Schedule & Results
                    </li>

                </ul>
            </div>
        </div>

        <div id="timeDiv">
            <%= DateUtils.getDateFormattedForColumnHeader(Calendar.getInstance()) %>
        </div>

    </div>

</div>

</body>

</html>