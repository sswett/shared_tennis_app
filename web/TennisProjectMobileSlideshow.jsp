<%@ page import="other.SessionHelper" %>
<%
%>
<!DOCTYPE html>
<html>

<head>
    <title>Tennis Project Mobile Slideshow</title>

    <script src="js/jquery-1.10.2.min.js"></script>

    <script src="js/basic-jquery-slider-master/js/bjqs-1.3.min.js"></script>

    <link href="js/basic-jquery-slider-master/bjqs.css" rel="stylesheet">
    <link href="css/Slideshow.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <link href="css/Singles.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script>

        <%-- when responsive, the dimensions are the maximums --%>

        <%--
                NOTE: I didn't want screen shot images to shrink responsively, nor did I want the
                      captions or controls to overlay the image.  So I made the slideshow
                      container bigger than the images and turned off the responsive and
                      showcontrols settings.
        --%>

        jQuery(document).ready(function($) {
            $('#my-slideshow').bjqs({
                height : 570,
                width : 640,
                responsive : false,
                hoverpause : true, // pause the slider on hover
                animspeed : 13000, // the delay between each slide
                showcontrols : false
            });
        });

    </script>

</head>

<body>

<div id="container">

<div id="my-slideshow">
    <ul class="bjqs">
        <li>
            <img src="slideshows/TennisProject/Mobile/MatchesGroupView.png<%= SessionHelper.getVersionQueryString() %>" 
            width="288" height="511"
                    title="Events can be viewed from a group or player perspective. The group view (above)
                     is also handy for captains to get a sense of event participation level."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Mobile/MatchesPlayerView.png<%= SessionHelper.getVersionQueryString() %>" 
            width="288" height="511"
                    title="The player view is handy for a player to mark his availability for multiple events."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Mobile/CommunicationsPage.png<%= SessionHelper.getVersionQueryString() %>" 
            width="288" height="511"
                    title="Captains can send text messages and/or e-mails to his team or to event participants.
                    All contact info is stored centrally on the web site."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Mobile/SinglesLeagueScoreEntry.png<%= SessionHelper.getVersionQueryString() %>" 
            width="288" height="511"
                    title="An administrator can enter singles league scores, typically after receiving a text
                    message from players just finishing a match."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Mobile/SinglesLeagueStandings.png<%= SessionHelper.getVersionQueryString() %>" 
            width="288" height="511"
                    title="Players can check standings using their mobile devices."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Mobile/SinglesLeaguePlayerCard.png<%= SessionHelper.getVersionQueryString() %>" 
            width="288" height="511"
                    title="Players can lookup their upcoming matches and scout their opponents."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Mobile/SinglesLeagueScheduleResults.png<%= SessionHelper.getVersionQueryString() %>" 
            width="288" height="511"
                    title="Participants can lookup league-wide schedules and results."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Mobile/NavMenus.png<%= SessionHelper.getVersionQueryString() %>" 
            width="288" height="511"
                    title="For quick and easy access to the whole site, every page has a navigation menu at the top."/>
        </li>
    </ul>
</div>


</div>


</body>

</html>