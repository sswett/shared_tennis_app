<%@ page import="other.SessionHelper" %>
<%
	final int imgWidth = 840;
	final int imgHeight = 540;
%>
<!DOCTYPE html>
<html>

<head>
    <title>Tennis Project JAX-WS Example Slideshow</title>

    <script src="js/jquery-1.10.2.min.js"></script>

    <script src="js/basic-jquery-slider-master/js/bjqs-1.3.min.js"></script>

    <link href="js/basic-jquery-slider-master/bjqs.css" rel="stylesheet">
    <link href="css/Slideshow.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <link href="css/Singles.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    
    <link href="css/prism.css" rel="stylesheet" />

    <script>

        <%-- when responsive, the dimensions are the maximums --%>

        <%--
                NOTE: I didn't want screen shot images to shrink responsively, nor did I want the
                      captions or controls to overlay the image.  So I made the slideshow
                      container bigger than the images and turned off the responsive and
                      showcontrols settings.
        --%>

        jQuery(document).ready(function($) {
            $('#my-slideshow').bjqs({
                height : 610,
                width : 900,
                responsive : false,
                hoverpause : false, // pause the slider on hover
                animspeed : 13000, // the delay between each slide
                randomstart : false,
                showcontrols : false,
                automatic : false
            });
        });

    </script>

</head>

<body>

<div id="container">

<div id="my-slideshow">
    <ul class="bjqs">
        <li>
            <img src="slideshows/TennisProject/JAXWSExample/AxisOnTomcat.png" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="The Apache Axis2 Web Services / SOAP / WSDL engine is used in conjunction with Tomcat to handle web service requests.
                    Axis2 has been installed as a web app within Tomcat. An Axis2 Admin servlet, and a few other pages, provide a GUI for 
                    interacting with Axis2 on the server."/>
        </li>
        
        <li>
            <img src="slideshows/TennisProject/JAXWSExample/ClientAppOutput.png" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Two simple web services (Player, Event) lookup and return info from a database. Before diving into the web services,
                    let's look at the clients. They are simple stand-alone Java applications. The first calls a Player web service 3 times to lookup 
                    players by phone number. The second calls an Event web service once to fetch a list of events in 2015 for a particular group/team."/>
        </li>
        
        <li>
            <jsp:include page="slideshows/TennisProject/JAXWSExample/client_app_java.jsp" flush="true" />
        </li>
        
        <li>
            <jsp:include page="slideshows/TennisProject/JAXWSExample/event_web_service_java.jsp" flush="true" />
        </li>
        
        <li>
            <img src="slideshows/TennisProject/JAXWSExample/CreateWebServiceWizard.png" 
            height="<%= imgHeight %>"
                    title="The EventWebService class shown on the previous slide is used as input to Eclipse's Create Web Service wizard. The 
                    first wizard dialog is shown above.  In this case, it is used to create the service only. (The wizard can be used to create 
                    and test both services and clients.)"/>
        </li>
        
        <li>
            <img src="slideshows/TennisProject/JAXWSExample/AxisServiceArchiverWizard.png" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="After the EventWebService is created with the wizard on the previous slide, the Axis Service Archiver wizard (an Eclipse plugin) 
                    can be used to create an Axis archive file (.aar) that can be uploaded to the Axis server via the Axis Admin servlet GUI.  One of 
                    the wizard's dialogs is shown above. The wizard generates my event_service.aar file."/>
        </li>
        
        <li>
            <img src="slideshows/TennisProject/JAXWSExample/EventServiceAAR.png" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="The screen shot above shows the top-level contents of the event_service.aar archive that was created with the 
                    wizard shown on the previous slide. The archive contains the web service and supporting classes, libraries, etc."/>
        </li>
        
        <li>
            <img src="slideshows/TennisProject/JAXWSExample/UploadDeployEventService.png" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="The Axis2 Admin servlet is used to upload the event_service.aar file so that the EventWebService is deployed and made
                    active on Axis2 running in Tomcat."/>
        </li>
        
        <li>
            <img src="slideshows/TennisProject/JAXWSExample/AvailableAxisServices.png" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="The Axis2 Admin servlet can be used to list available services.  The screen shot above shows information about the 
                    EventWebService.  You can see that it is active and that 'getEventsForYear' is an available operation."/>
        </li>
        
        <li>
            <jsp:include page="slideshows/TennisProject/JAXWSExample/event_web_service_wsdl.jsp" flush="true" />
        </li>
        
        <li>
            <img src="slideshows/TennisProject/JAXWSExample/EventWebServiceStub.png" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Time to circle back to the client application. The client project source
                    directory tree is shown above. Recall that the ClientApp.java file leverages the EventWebServiceStub.  After the web service
                    was created and the WSDL URI was available, the stub and callback handler were automatically generated by using the following Axis cmd: 
                    wsdl2java.bat -uri http://localhost:8080/axis2/services/EventWebService?wsdl -o C:\Users\steve\workspace\TestEventWebServiceClientApplication"/>
        </li>
    </ul>
</div>


</div>

<script src="js/prism.js"></script>

</body>

</html>