var yesResponseList = new Array();


function taPrintElementWithId(pPageContainerId, pElemId)
{
    $("div").each(function() {
        $(this).addClass("unprintable")
    });

    $("#" + pPageContainerId + ",#" + pElemId).removeClass("unprintable");

    $("#" + pElemId + " div").each(function() {
        $(this).removeClass("unprintable");
    });

    window.print();
}


function cpRandomDraw()
{
    var randomIdx = Math.floor(Math.random() * yesResponseList.length);
    // alert("randomIdx = " + randomIdx);
    $("#drawResult").html(yesResponseList[randomIdx]);
}


function cpSelectAllResponseTypes(pEventId)   /* event as in a practice or match */
{
    $(".rtCheckbox").prop("checked", true);
    cpRefreshHtmlElements(pEventId);
}


function cpDeselectAllResponseTypes(pEventId)   /* event as in a practice or match */
{
    $(".rtCheckbox").prop("checked", false);
    cpRefreshHtmlElements(pEventId);
}


function cpSelectAllPlayers(pEventId)   /* event as in a practice or match */
{
    $(".playerCheckbox").prop("checked", true);
    cpRefreshHtmlElements(pEventId);
}


function cpDeselectAllPlayers(pEventId)   /* event as in a practice or match */
{
    $(".playerCheckbox").prop("checked", false);
    cpRefreshHtmlElements(pEventId);
}


function cpEnableOrDisableSend()
{
    var serverMsg = $("#serverMsg").val();

    if (String.prototype.trim)
    {
        serverMsg = serverMsg.trim();
    }
    else
    {
        serverMsg = serverMsg.replace(/^\s+|\s+$/g,'');
    }

    if (serverMsg.length == 0)
    {
        $("#sendServerMsgButton").attr("disabled", "disabled");
        return;
    }

    if ( $("#sendAsText").prop("checked") || $("#sendAsEmail").prop("checked") )
    {
        $("#sendServerMsgButton").removeAttr("disabled");
    }
    else
    {
        $("#sendServerMsgButton").attr("disabled", "disabled");
    }
}


function cpChangeSender()
{
    var valueArray =  $("#senderSelect").val().split(";");
    $( "#hiddenAjaxResponse" ).load( "/ChangeSender.page?playerId=" + valueArray[0] );
}


function cpRefreshHtmlElements(pEventId)   /* event as in a practice or match */
{
    var cpForm = document.copyPasteForm;
    var masterIdList = cpForm.masterIdList.value.split(";");
    var masterNameList = cpForm.masterNameList.value.split(";");
    var masterPhoneList = cpForm.masterPhoneList.value.split(";");
    var masterEmailList = cpForm.masterEmailList.value.split(";");
    var masterResponseDateList = cpForm.masterResponseDateList.value.split(";");
    var masterCategoryList = cpForm.masterCategoryList.value.split(";");
    var masterMbrTypeList = cpForm.masterMbrTypeList.value.split(";");
    var phoneDelimiterForRawCopyPaste = cpForm.phoneDelimiterForRawCopyPaste.value;

    var filteredNameList = new Array();
    var filteredPhoneList = new Array();
    var filteredEmailList = new Array();
    var filteredResponseDateList = new Array();
    var filteredMbrTypeList = new Array();
    yesResponseList = new Array();

    if ($("#drawResult").length)
    {
        $("#drawResult").html("");
    }

    if (pEventId == 0)
    {
        for (var x = 0; x < masterIdList.length; x++)
        {
            var checkBoxId = "p" + masterIdList[x];

            if ($("#" + checkBoxId).prop("checked"))
            {
                var idx = filteredNameList.length;
                filteredNameList[idx] = masterNameList[x];
                filteredPhoneList[idx] = masterPhoneList[x];
                filteredEmailList[idx] = masterEmailList[x];
                filteredResponseDateList[idx] = masterResponseDateList[x];
                filteredMbrTypeList[idx] = masterMbrTypeList[x];
            }
        }
    }
    else
    {
        for (x = 0; x < masterNameList.length; x++)
        {
            checkBoxId = "rt" + masterCategoryList[x];

            if ($("#" + checkBoxId).prop("checked"))
            {
                idx = filteredNameList.length;
                filteredNameList[idx] = masterNameList[x];
                filteredPhoneList[idx] = masterPhoneList[x];
                filteredEmailList[idx] = masterEmailList[x];
                filteredResponseDateList[idx] = masterResponseDateList[x];
                filteredMbrTypeList[idx] = masterMbrTypeList[x];

                if (masterCategoryList[x] == globalYesResponse)
                {
                    var y = yesResponseList.length;
                    yesResponseList[y] = masterNameList[x];
                }
            }
        }
    }

    var smsPhoneList = cpGetTrimmedAndFormattedList(filteredPhoneList, ",");
    var emailList = cpGetTrimmedAndFormattedList(filteredEmailList, ";");

    cpForm.filteredPhoneList.value = smsPhoneList;
    cpForm.filteredEmailList.value = emailList;

    var playerCount = filteredNameList.length;
    var phoneCount = 0;
    var emailCount = 0;

    if (smsPhoneList != "")
    {
        phoneCount = smsPhoneList.split(",").length;
    }

    if (emailList != "")
    {
        emailCount = emailList.split(";").length;
    }

    $("#textQtys").html(phoneCount + "/" + playerCount);
    $("#emailQtys").html(emailCount + "/" + playerCount);

    if (phoneCount == 0 && emailCount == 0)
    {
        $("#copyPasteServerMsgSection").css("display", "none");
    }
    else
    {
        $("#copyPasteServerMsgSection").css("display", "block");
    }

    if (yesResponseList.length == 0)
    {
        $("#copyPasteRandomDrawSection").css("display", "none");
    }
    else
    {
        $("#copyPasteRandomDrawSection").css("display", "block");
    }

    // Handle sms link:
    if ($("#smslink").length)
    {
        if (smsPhoneList == "")
        {
            $("#copyPasteTextLinkSection").removeClass("shownTextLink").addClass("hiddenTextLink");
        }
        else
        {
            $("#copyPasteTextLinkSection").removeClass("hiddenTextLink").addClass("shownTextLink");
            $("#smslink").attr("href", "sms:" + smsPhoneList);
        }
    }

    // Handle mail to link:
    if ($("#mailtolink").length)
    {
        if (emailList == "")
        {
            $("#copyPasteEmailLinkSection").removeClass("shownEmailLink").addClass("hiddenEmailLink");
        }
        else
        {
            $("#copyPasteEmailLinkSection").removeClass("hiddenEmailLink").addClass("shownEmailLink");
            $("#mailtolink").attr("href", "mailto:" + emailList);
        }
    }

    // Handle copy paste raw phone numbers:
    var rawPhoneList = cpGetTrimmedAndFormattedList(filteredPhoneList, phoneDelimiterForRawCopyPaste);
    $("#copyPastePhoneNumbers").html(rawPhoneList);

    // Handle copy paste raw email addresses:
    var rawEmailList = cpGetTrimmedAndFormattedList(filteredEmailList, ";<br/>");
    $("#copyPasteEmails").html(rawEmailList);

    // Handle copy paste raw names (not output for mobile):

    if ($("#copyPasteNames").length)
    {
        var nameOutput = "<ul>";
        var nonMbrCount = 0;

        for (x = 0; x < filteredNameList.length; x++)
        {
            nameOutput += "<li>";

            nameOutput += "<div class='cpnName'>";
            nameOutput += filteredNameList[x];
            nameOutput += "</div>";

            nameOutput += "<div class='cpnMbrType'>";
            nameOutput += "Mbr Type: ";
            nameOutput += filteredMbrTypeList[x];
            nameOutput += "</div>";
            
            if (filteredMbrTypeList[x] == "Not")
            {
            	nonMbrCount++;
            }

            if (filteredResponseDateList[x].length > 0)
            {
                nameOutput += "<div class='cpnResponseDate'>";
                nameOutput += "Responded: ";
                nameOutput += filteredResponseDateList[x];
                nameOutput += "</div>";
            }

            nameOutput += "</li>";
        }

        nameOutput += "</ul>";
        $("#copyPasteNames").html(nameOutput);
        $("#cpPlayerCount").html("" + filteredNameList.length);
        var mbrCount = filteredNameList.length - nonMbrCount; 
        $("#cpMbrCount").html("" + mbrCount);
        $("#cpNonMbrCount").html("" + nonMbrCount);
    }

    // Handle Contact List

    var contactList = "";

    if ($("#cpContactListTable").length)
    {
        // HANDLE NON-MOBILE VERSION
        contactList = "<tr><th>Name</th><th>Phone</th><th>E-mail</th></tr>";

        for (var cl = 0; cl < filteredNameList.length; cl++)
        {
            contactList += "<tr>";

            // Name:
            contactList += "<td>";
            contactList += filteredNameList[cl];
            contactList += "</td>";

            // Phone:
            contactList += "<td>";
            var phone = filteredPhoneList[cl];

            if (phone.length > 0)
            {
                var fmtPhone = phone.substr(0,3) + "-" + phone.substr(3,3) + "-" + phone.substr(6);
                contactList += "<a href='sms:" + phone + "'>" + fmtPhone + "</a>";
            }
            else
            {
                contactList += "&nbsp;";
            }

            contactList += "</td>";

            // Email:
            contactList += "<td>";
            var email = filteredEmailList[cl];

            if (email.length > 0)
            {
                contactList += "<a href='mailto:" + email + "'>" + email + "</a>";
            }
            else
            {
                contactList += "&nbsp;";
            }

            contactList += "</td>";

            contactList += "</tr>";
        }

        $("#cpContactListTable").html(contactList);
    }
    else
    {
        // HANDLE MOBILE VERSION
        for (cl = 0; cl < filteredNameList.length; cl++)
        {
            // Name:
            contactList += filteredNameList[cl];
            contactList += "<br/>";

            // Phone:
            phone = filteredPhoneList[cl];

            if (phone.length > 0)
            {
                fmtPhone = phone.substr(0,3) + "-" + phone.substr(3,3) + "-" + phone.substr(6);
                contactList += "<a href='tel:" + phone + "'>call " + fmtPhone + "</a><br/>";
                contactList += "<a href='sms:" + phone + "'>text " + fmtPhone + "</a><br/>";
            }

            // Email:
            email = filteredEmailList[cl];

            if (email.length > 0)
            {
                contactList += "<a href='mailto:" + email + "'>" + email + "</a><br/>";
            }

            contactList += "<br/>";
        }

        $("#cpContactListContainer").html(contactList);
    }

}


function cpGetTrimmedAndFormattedList(pListArray, pDelimiter)
{
    var result = "";

    for (var x = 0; x < pListArray.length; x++)
    {
        if (pListArray[x] != "")
        {
            if (result != "")
            {
                result += pDelimiter;
            }

            result += pListArray[x];
        }
    }

    return result;
}


function cpSendServerMsg()
{
    document.copyPasteForm.sendServerMsg.value = "true";

    $.ajax({
        type: "POST",
        url: "/SendServerMsg.page",
        data: $("form").serialize(),   /* Very cool trick, treats same way as POSTed form data */
        success: cpSendServerMsgResponseHandler,
        dataType: "text"
    });
}


function cpSendServerMsgResponseHandler(data, textStatus, jqXHR)
{
    $("#sendServerMsgResults").html(data);
    window.scrollTo(0, 0);
}


function taChangeGroup()
{
    document.changeGroupForm.changeGroup.value = "true";
    document.changeGroupForm.submit();
}


function taChangeView()
{
    document.changeViewForm.changeView.value = "true";
    document.changeViewForm.submit();
}


function mgvChangeEvent()
{
    document.mobileForm.action.value = "changeEvent";
    document.mobileForm.submit();
}


function mgvSave()
{
    document.mobileForm.action.value = "save";
    document.mobileForm.submit();
}


function mgvCancel()
{
    document.mobileForm.reset();
    $("#eventSelect,#optionsButton").removeAttr("disabled");
    $(".commButton,.tooLateButton").show();
    $(".msgDiv").css("display", "none");
}


function mgvOptions()
{
    window.location = "MobileOptions.page";
}


function mgvWarnAboutUnsaved()
{
    $("#optionsButton,#eventSelect").attr("disabled", "disabled");
    $(".commButton,.tooLateButton").hide();
    $(".msgDiv").css("display", "block");
    $("#changesSaved").css("display", "none");
}


function mpvChangePlayer()
{
    document.mobileForm.action.value = "changePlayer";
    document.mobileForm.submit();
}


function mpvSave()
{
    document.mobileForm.action.value = "save";
    document.mobileForm.submit();
}


function mpvCancel()
{
    document.mobileForm.reset();
    $("#playerSelect,#optionsButton").removeAttr("disabled");
    $(".msgDiv").css("display", "none");
}


function mpvOptions()
{
    window.location = "MobileOptions.page";
}


function mpvWarnAboutUnsaved()
{
    $("#optionsButton,#playerSelect").attr("disabled", "disabled");
    $(".msgDiv").css("display", "block");
    $("#changesSaved").css("display", "none");
}


function ssShowSaveOrCancelButtons()
{
    $('.saveOrCancelButtons').css("display", "inline-block");
    $('.savemsg').html("");
    $('#groupSelect').attr("disabled", "disabled");
}


function ssHideSaveOrCancelButtons()
{
    $('.saveOrCancelButtons').css("display", "none");
    $('#groupSelect').removeAttr("disabled");
    $('.commButton,.tooLateButton').show();
}


function ssShowCrossHair(responseCell)
{
    var responseId = $(responseCell).attr("id");
    var classAttr = $(responseCell).attr("class");
    var classes = classAttr.split(" ");

    // Column portion of cross hair:
    var colClassVal = "";

    for (var x = 0; x < classes.length; x++)
    {
        var classVal = classes[x];

        if (classVal.indexOf("col") == 0)
        {
            colClassVal = classVal;
            break;
        }
    }

    if (colClassVal.length > 0)
    {
        $("." + colClassVal).addClass("crosshair");
    }

    // Row portion of cross hair:
    $(responseCell).closest("tr").find("td").addClass("crosshair");
}


function ssHideCrossHairs()
{
    $(".crosshair").removeClass("crosshair");
}


function ssHighlightRow(responseInput)
{
    $(".rowHighlighter").removeClass("rowHighlighter");
    $(responseInput).closest("tr").find("td").addClass("rowHighlighter");
}


function displayCommunicationsPage(queryString)
{
    window.location = "Communications.page" + queryString;
}


function markTooLate(pEventId, pTooLateValue)
{
    $("select.responseInput.e" + pEventId).each(function() {
    	var anyChanged = false;
    	
    	if ($(this).val() == 'unknown')
    	{
    		$(this).val(pTooLateValue);
    		anyChanged = true;
    	}
    	
    	if(anyChanged)
    	{
    		ssShowSaveOrCancelButtons();
    		mgvWarnAboutUnsaved();
    	}
    });
	
}
