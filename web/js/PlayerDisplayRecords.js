var PlayerBeanEditRow = -1;

function addNewPlayerBean() {
    if (PlayerBeanEditRow == -1) {
        PlayerBeanEditRow = -2;
        submitData("GET", null, "PlayerForm.jsp?action=addNew", addNewPlayerBeanCallback, true);
    }
}

function addNewPlayerBeanCallback(response) {
    document.getElementById("addNewPlayerBeanButtonSpan").innerHTML = response;
}

function cancelPlayerBean() {
    submitData("GET", null, "PlayerDisplayRecordsTable.jsp", refreshPlayerBeanTable, true);
}

function submitPlayerBean() {

    // Test player name input:
    var playerName = $("#name").val();

    if (playerName.length == 0)
    {
        alert("Player name is required.");
        return;
    }

    var nameParts = playerName.split(" ");

    if (nameParts.length < 2)
    {
        alert("A player name should consist of a first and last name.  Example: John Doe");
        return;
    }

    submitData("POST", "playerBeanForm", "PlayerSubmit.page", refreshPlayerBeanTable, true);
}

function refreshPlayerBeanTable(response) {
    PlayerBeanEditRow = -1;
    document.getElementById("playerBeanTable").innerHTML = response;
}

function editPlayerBean(index) {
    if (PlayerBeanEditRow == -1) {
        PlayerBeanEditRow = index;
        submitData("GET", null, "PlayerEdit.page?index=" + index, openRowForEditPlayerBeanTable, true);
    }
}

function openRowForEditPlayerBeanTable(response) {
    document.getElementById("playerBeanRow" + PlayerBeanEditRow).innerHTML = response;
}

function deletePlayerBean(index) {
    if (PlayerBeanEditRow == -1) {
        var result = confirm("Are you sure you want to delete this record?");
        if (result) submitData("GET", null, "PlayerDelete.page?index=" + index, refreshPlayerBeanTable, true);
    }
}
