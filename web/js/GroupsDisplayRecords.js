var GroupsBeanEditRow = -1;

function addNewGroupsBean() {
  if (GroupsBeanEditRow == -1) {
    GroupsBeanEditRow = -2;
    submitData("GET", null, "GroupsForm.jsp?action=addNew", addNewGroupsBeanCallback, true);
  }
}

function addNewGroupsBeanCallback(response) {
  document.getElementById("addNewGroupsBeanButtonSpan").innerHTML = response;
}

function cancelGroupsBean() {
  submitData("GET", null, "GroupsDisplayRecordsTable.jsp", refreshGroupsBeanTable, true);
}

function submitGroupsBean() {
  submitData("POST", "groupsBeanForm", "GroupsSubmit.page", refreshGroupsBeanTable, true);
}

function refreshGroupsBeanTable(response) {
  GroupsBeanEditRow = -1;
  document.getElementById("groupsBeanTable").innerHTML = response;
}

function editGroupsBean(index) {
  if (GroupsBeanEditRow == -1) {
    GroupsBeanEditRow = index;
    submitData("GET", null, "GroupsEdit.page?index=" + index, openRowForEditGroupsBeanTable, true);
  }
}

function openRowForEditGroupsBeanTable(response) {
  document.getElementById("groupsBeanRow" + GroupsBeanEditRow).innerHTML = response;
}

function deleteGroupsBean(index) {
  if (GroupsBeanEditRow == -1) {
    var result = confirm("Are you sure you want to delete this record?");
    if (result) submitData("GET", null, "GroupsDelete.page?index=" + index, refreshGroupsBeanTable, true);
  }
}
