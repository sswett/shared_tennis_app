var EventBeanEditRow = -1;

function addNewEventBean() {
  if (EventBeanEditRow == -1) {
    EventBeanEditRow = -2;
    submitData("GET", null, "EventForm.jsp?action=addNew", addNewEventBeanCallback, true);
  }
}

function addNewEventBeanCallback(response) {
  document.getElementById("addNewEventBeanButtonSpan").innerHTML = response;
}

function cancelEventBean() {
  submitData("GET", null, "EventDisplayRecordsTable.jsp", refreshEventBeanTable, true);
}

function submitEventBean() {

    // Test date input:
    var dateVal = $("#date").val();
    var dateAsLong = Date.parse(dateVal);

    // iPad doesn't like "2013-12-11" so try "2013/12/11":
    if (isNaN(dateAsLong))
    {
        dateVal = dateVal.replace(/\-/g,'/');
        dateAsLong = Date.parse(dateVal);
    }

    if (isNaN(dateAsLong))
    {
        alert("An invalid date was entered.  It may not be formatted correctly.  Here are two examples of " +
                "properly formatted dates:\n" +
                "\n2014-03-15 07:30 PM" +
                 "\n2014-3-20 7:30 AM");

        return;
    }

    // Test description input:
    if ($("#description").val().length == 0)
    {
        alert("Description is required.");
        return;
    }

  submitData("POST", "eventBeanForm", "EventSubmit.page", refreshEventBeanTable, true);
}

function refreshEventBeanTable(response) {
  EventBeanEditRow = -1;
  document.getElementById("eventBeanTable").innerHTML = response;
}

function editEventBean(index) {
  if (EventBeanEditRow == -1) {
    EventBeanEditRow = index;
    submitData("GET", null, "EventEdit.page?index=" + index, openRowForEditEventBeanTable, true);
  }
}

function openRowForEditEventBeanTable(response) {
  document.getElementById("eventBeanRow" + EventBeanEditRow).innerHTML = response;
}

function deleteEventBean(index) {
  if (EventBeanEditRow == -1) {
    var result = confirm("Are you sure you want to delete this record?");
    if (result) submitData("GET", null, "EventDelete.page?index=" + index, refreshEventBeanTable, true);
  }
}

function showHidePastEvents(showHideButton)
{
    if (showHideButton.value.indexOf("Show") != -1)
    {
        document.showHideForm.showHideActionToTake.value = "show";
    }
    else
    {
        document.showHideForm.showHideActionToTake.value = "hide";
    }

    document.showHideForm.submit();
}

function showDatePicker()
{
    // $( "#myDatePicker" ).datepicker( "option", "dateFormat", "yyyy-mm-dd" );

    var existingDateTimeValue = $("#date").val();
    var existingDateTimeParts = existingDateTimeValue.split(" ");

    var existingDateString = existingDateTimeParts[0];

    if (existingDateString && existingDateString.length == 10)
    {
        // $( ".selector" ).datepicker( "setDate", "10/12/2012" );

        var existingDateParts = existingDateString.split("-");
        var date = new Date();
        date.setYear(parseInt(existingDateParts[0]));
        date.setMonth(parseInt(existingDateParts[1]) - 1);   // month is zero-based
        date.setDate(parseInt(existingDateParts[2]));

        $( "#myDatePicker" ).datepicker( "dialog", date, handleDateSelect );
    }
    else
    {
        $( "#myDatePicker" ).datepicker( "dialog", new Date(), handleDateSelect );
    }
}


function handleDateSelect(dateText, datePicker)
{
    var actualMonth = parseInt(datePicker.currentMonth) + 1;
    var formattedDate = datePicker.currentYear + "-" + actualMonth + "-" + datePicker.currentDay;
    var existingDateTimeValue = $("#date").val();
    var existingDateTimeParts = existingDateTimeValue.split(" ");
    var existingDate = existingDateTimeParts[0];

    // Begin to build replacement:

    var newValue = formattedDate;

    for (var x = 0; x < existingDateTimeParts.length; x++ )
    {
        var part = existingDateTimeParts[x];

        // Skip a potential date
        if (part.length > 5 || part.indexOf("-") != -1)
        {
            continue;
        }

        newValue = newValue + " " + part;
    }

    $("#date").val(newValue);
}
