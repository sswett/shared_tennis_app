<%@ page import="other.SessionHelper" %>
<%@ page import="other.MatchType" %>

<%
    final boolean isMobile = SessionHelper.isMobileBrowser(request);
%>


<% if (isMobile) { %>

 <div class="nmMenuBar">
    <table class="rootVoices">
      <tr>
        <td class="rootVoice {menu: 'nmMobileEventsMenu'}" >Events</td>
        <td class="rootVoice {menu: 'nmMobileSinglesMenu'}" >Singles</td>
        <td class="rootVoice {menu: 'nmMobileUSTAMenu'}" >USTA</td>
        <td class="rootVoice {menu: 'nmMobileSlideshowMenu'}" >Slideshows</td>
    </table>
  </div>



<div id="nmMobileEventsMenu" class="menu">
	<a href="MobileGroupView.page">Group/Team View</a>
	<a href="MobilePlayerView.page">Player View</a>
	<a href="MobilePlayerAllTeamsView.page">Player All Teams View</a>
	<a href="MobileOptions.page">Mobile Options</a>
  </div>


<div id="nmMobileSinglesMenu" class="menu">
    <a href="SinglesLeagueGuidelines.page">Guidelines</a>
    <a menu="nmMobileSingles35Menu">3.5</a>
    <a menu="nmMobileSingles40Menu">4.0</a>
  </div>


<div id="nmMobileSingles35Menu" class="menu">
	<a href="MobileSinglesLeaguePlayerList.page?div=3.5">Player Cards</a>
	<a href="MobileSinglesLeagueStandings.page?div=3.5&matchType=<%= MatchType.REGULAR_SEASON.name() %>">Standings</a>
 	<a href="MobileSinglesLeagueContacts.page?div=3.5">Player Contact Info</a>
 	<a href="MobileSinglesLeagueSchedule.page?div=3.5">Schedule & Results</a> 
  </div>


<div id="nmMobileSingles40Menu" class="menu">
	<a href="MobileSinglesLeaguePlayerList.page?div=4.0">Player Cards</a>
	<a href="MobileSinglesLeagueStandings.page?div=4.0&matchType=<%= MatchType.REGULAR_SEASON.name() %>">Standings</a>
 	<a href="MobileSinglesLeagueContacts.page?div=4.0">Player Contact Info</a>
 	<a href="MobileSinglesLeagueSchedule.page?div=4.0">Schedule & Results</a> 
  </div>


<div id="nmMobileUSTAMenu" class="menu">
        <a href="http://m.tennislink.usta.com/statsandstandings?search=8558008208&t=R-17">Thunder ESL 3.5</a>
        <a href="http://m.tennislink.usta.com/statsandstandings?search=8558008305&t=R-17">Lightning ESL 3.5</a>
        <a href="http://m.tennislink.usta.com/statsandstandings?search=8558008204&t=R-17">Just4Fuzz ESL 4.0</a>
        <a href="http://m.tennislink.usta.com/statsandstandings?search=8558024099&t=R-17">Maioho 3.5 18+</a>
        <a href="http://m.tennislink.usta.com/statsandstandings?search=8558023864&t=R-17">Swett 3.5 18+</a>
        <a href="http://m.tennislink.usta.com/statsandstandings?search=8558023872&t=R-17">Schmidt 3.5 40+</a>
        <a href="http://m.tennislink.usta.com/statsandstandings?search=8558023871&t=R-17">Wiegand 3.5 40+</a>
        <a href="http://m.tennislink.usta.com/statsandstandings?search=8558024043&t=R-17">MVP 4.0</a>
        <a href="http://m.tennislink.usta.com">Tennis Link</a>
        
  </div>


<div id="nmMobileSlideshowMenu" class="menu">
	<a href="/TravelTeam2013StatesSlideshow.page">3.5 2013 States</a>
	<a href="/TravelTeam2014DistrictsSlideshow.page">3.5 2014 Districts</a>
	<a href="/TravelTeam2014StatesSlideshow.page">3.5 2014 States</a>
  </div>



<% } else { %>   <%-- The big "if / else" mobile above; desktop below --%>



 <div class="nmMenuBar">
    <table class="rootVoices">
      <tr>
        <td class="rootVoice {menu: 'nmAdminMenu'}" >Admin</td>
        <td class="rootVoice {menu: 'nmEventsMenu'}" >Events</td>
        <td class="rootVoice {menu: 'nmSinglesMenu'}" >Singles</td>
        <td class="rootVoice {menu: 'nmUSTAMenu'}" >USTA</td>
        <td class="rootVoice {menu: 'nmSlideshowMenu'}" >Slideshows</td>
    </table>
  </div>


 <div id="nmAdminMenu" class="menu">
    <a href="GroupsDisplayRecords.page">Edit Groups/Teams</a>
    <a href="PlayerDisplayRecords.page">Edit Players</a>
    <a href="EditGroupMembers.page">Edit Group/Team Members</a>
    <a href="EventDisplayRecords.page">Edit Events (practices, matches, etc.)</a>
    <a href="EditMatchList.page?div=4.0">Edit 4.0 Singles Match List</a>
    <a href="EditMatchList.page?div=3.5">Edit 3.5 Singles Match List</a>

<%--

	Examples of other ways to setup menu items:
	
    <a rel="title" >Admin Menu</a> <!-- menuvoice title-->
    <a href="someUrl.html" target="_blank" img="image.gif" >menu_1.1 (href)</a> <!-- menuvoice with href-->
    <a action="doSomething()" >menu_1.2</a> <!-- menuvoice with js action-->
    <a rel="separator"></a> <!-- menuvoice separator-->
    <a action="doSomething()" disabled=true>menu_1.3</a> <!-- menuvoice disabled-->
    <a action="doSomething()" menu="menu_1" img="image.png">menu_1.4</a><!-- menuvoice with js action, image and submenu-->
    
 --%>
     
  </div>


<div id="nmSinglesMenu" class="menu">
    <a href="SinglesLeagueGuidelines.page">Guidelines</a>
    <a menu="nmSingles35Menu">3.5</a>
    <a menu="nmSingles40Menu">4.0</a>
  </div>


<div id="nmSingles35Menu" class="menu">
	<a href="SinglesLeaguePlayerList.page?div=3.5">Player Cards</a>
	<a href="SinglesLeagueStandings.page?div=3.5&matchType=<%= MatchType.REGULAR_SEASON.name() %>">Standings</a>
 	<a href="SinglesLeagueContacts.page?div=3.5">Player Contact Info</a>
 	<a href="SinglesLeagueSchedule.page?div=3.5">Schedule & Results</a> 
  </div>


<div id="nmSingles40Menu" class="menu">
	<a href="SinglesLeaguePlayerList.page?div=4.0">Player Cards</a>
	<a href="SinglesLeagueStandings.page?div=4.0&matchType=<%= MatchType.REGULAR_SEASON.name() %>">Standings</a>
 	<a href="SinglesLeagueContacts.page?div=4.0">Player Contact Info</a>
 	<a href="SinglesLeagueSchedule.page?div=4.0">Schedule & Results</a> 
  </div>


<div id="nmUSTAMenu" class="menu">
        <a href="http://tennislink.usta.com/Leagues/Main/StatsAndStandings.aspx?t=T-0&par1=SRrjJZ8fewlcXK7%2bv7O4vg%3d%3d&e=1#&&s=3%7c%7c0%7c%7c8558008208%7c%7c2015">Thunder ESL 3.5</a>
        <a href="http://tennislink.usta.com/Leagues/Main/StatsAndStandings.aspx?par1=jttxreWH4fh5nP06%2ficMyg%3d%3d&e=1#&&s=3%7c%7c0%7c%7c8558008305%7c%7c2015">Lightning ESL 3.5</a>
        <a href="http://tennislink.usta.com/Leagues/Main/StatsAndStandings.aspx?t=T-0&par1=s1qtWUQrh770AujYn2N7sA%3d%3d&e=1#&&s=3%7c%7c0%7c%7c8558008204%7c%7c2015">Just4Fuzz ESL 4.0</a>
        <a href="http://tennislink.usta.com/Leagues/Main/StatsAndStandings.aspx?t=T-0&par1=s1qtWUQrh770AujYn2N7sA%3d%3d&e=1#&&s=3%7c%7c0%7c%7c8558024099%7c%7c2015">Maioho 3.5 18+</a>
        <a href="http://tennislink.usta.com/Leagues/Main/StatsAndStandings.aspx?t=T-0&par1=s1qtWUQrh770AujYn2N7sA%3d%3d&e=1#&&s=3%7c%7c0%7c%7c8558023864%7c%7c2015">Swett 3.5 18+</a>
        <a href="http://tennislink.usta.com/Leagues/Main/StatsAndStandings.aspx?t=T-0&par1=s1qtWUQrh770AujYn2N7sA%3d%3d&e=1#&&s=3%7c%7c0%7c%7c8558023872%7c%7c2015">Schmidt 3.5 40+</a>
        <a href="http://tennislink.usta.com/Leagues/Main/StatsAndStandings.aspx?t=T-0&par1=s1qtWUQrh770AujYn2N7sA%3d%3d&e=1#&&s=3%7c%7c0%7c%7c8558023871%7c%7c2015">Wiegand 3.5 40+</a>
        <a href="http://tennislink.usta.com/Leagues/Main/StatsAndStandings.aspx?t=T-0&par1=s1qtWUQrh770AujYn2N7sA%3d%3d&e=1#&&s=3%7c%7c0%7c%7c8558024043%7c%7c2015">MVP 4.0</a>
        <a href="http://www.westernmichigan.usta.com/USTA_League_Tennis/home/">West MI USTA</a>
        <a href="http://tennislink.usta.com/Leagues/Common/Default.aspx">Tennis Link</a>
        
  </div>


<div id="nmEventsMenu" class="menu">
	<a href="Spreadsheet.page">Availability for Practices, Matches, etc.</a>
  </div>


<div id="nmSlideshowMenu" class="menu">
	<a href="/TravelTeam2013StatesSlideshow.page">3.5 2013 States</a>
	<a href="/TravelTeam2014DistrictsSlideshow.page">3.5 2014 Districts</a>
	<a href="/TravelTeam2014StatesSlideshow.page">3.5 2014 States</a>
  </div>

<% } %>

