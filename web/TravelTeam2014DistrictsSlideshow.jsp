<%@ page import="other.SessionHelper" %>
<%
	final boolean isMobile = SessionHelper.isMobileBrowser(request);
	final int imgWidth = 1024;
	final int imgHeight = 768;
%>
<!DOCTYPE html>
<html>

<head>
    <title>Slideshow - Travel Team 2014 Districts at MSU</title>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <script src="js/basic-jquery-slider-master/js/bjqs-1.3.min.js"></script>

    <link href="js/basic-jquery-slider-master/bjqs.css" rel="stylesheet">
    <link href="css/Slideshow.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <link href="css/Singles.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script>

        <%-- when responsive, the dimensions are the maximums --%>

        jQuery(document).ready(function($) {
            $('#my-slideshow').bjqs({
                height : 600,
                width : 900,
                responsive : true,
                hoverpause : true, // pause the slider on hover
                animspeed : 10000 // the delay between each slide
            });
        });

    </script>

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="container">

<div id="my-slideshow">
    <ul class="bjqs">
        <li>
            <img src="slideshows/travel_teams/2014districts/18_plus_with_cool_girls.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            <% if (isMobile) { %>
                    title="18 & Over Team: The cool girls: Kara and Lauren. Front: Carl, Chris, Mike S, Paul, Dale. Rear: Mark, Butch, Jim, Mike O." 
            <% } else { %>
                    title="18 & Over Team: The cool girls: Kara and Lauren Wiegand. Front: Carl Wiegand, Chris Peceny, Mike Schmidt, Paul Brown, Dale Momber. Rear: Mark Delongpre, 
                    Butch Zannini, Jim DeLaFuente, Mike Osterink." 
            <% } %>
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/18_plus_team.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            <% if (isMobile) { %>
                    title="18 & Over Team: Front: Carl, Chris, Mike S, Paul, Dale. Rear: Mark, Butch, Jim, Mike O." 
            <% } else { %>
                    title="18 & Over Team: Front: Carl Wiegand, Chris Peceny, Mike Schmidt, Paul Brown, Dale Momber. Rear: Mark Delongpre, 
                    Butch Zannini, Jim DeLaFuente, Mike Osterink." 
            <% } %>
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/40_plus_team.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
            <% if (isMobile) { %>
                    title="40 & Over Team: Front: Chuck, Mike, Al, Jim. Rear: Carl, Butch, Paul, Steve, Chris, Harv." 
            <% } else { %>
                    title="40 & Over Team: Front: Chuck White, Mike Schmidt, Al Druckenmiller, Jim DeLaFuente. Rear: Carl Wiegand, Butch Zannini,
                    Paul Brown, Steve Swett, Chris Peceny, Harv Allen." 
            <% } %>
                    />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Al_Harv_warmups.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Al & Harv warming up" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Al_return_ready.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Al getting ready to return a serve" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Al_serving.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Al serving" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Butch_Mike_changeover.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Butch & Mike - always thinking" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Butch_Mike_return_ready.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Butch & Mike getting ready to return serve" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Butch_Mike_serving.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Butch & Mike" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Carl_Butch_changeover.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Carl & Butch" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Carl_Butch.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Carl & Butch" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Carl_fence.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Carl through the fence" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Chris_Steve_warmups.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Chris & Steve warming up" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Chuck_backhand.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="The classic Chuck backhand setup" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Chuck_return_ready.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Chuck ready to return" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Chuck_T_pressure.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Chuck putting pressure on opposing server" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Harv_Al.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Harv & Al" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Harv_and_Al_putaway.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Harv & Al during Al's putaway" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Harv_and_cute_girl.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Oh, Harv is in this picture? I didn't notice." />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Jim_forehand_MI.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Jim's classic forehand. Is that a Michigan slap in the MSU face?" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Jim_forehand.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="More classic Jim forehand" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Jim_handshake.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Jim being nice in victory over opponent" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Jim_Harv_synchronized_stretching.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Jim & Harv. Glad only two of us caught doing this at the same time." />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Jim_serve.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Jim serving" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Mike_Butch_Al.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Mike & Butch with Al in the background" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Paul_Chuck_changeover.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Paul & Chuck during changeover" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Paul_forehand.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Paul's big forehand" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Paul_serve.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Paul's big serve" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Paul_which_stroke.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Paul after hitting something ... maybe a backhand?" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Steve_about_to_serve.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Steve getting the serve started" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Steve_before_or_after.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Steve - weird court position for a backhand; could be movement after a forehand?" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Steve_overhead.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Steve hitting an overhead" />
        </li>

        <li>
            <img src="slideshows/travel_teams/2014districts/Steve_serve.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Steve serving" />
        </li>

    </ul>
</div>


</div>


</body>

</html>