<%@ page import="other.SessionHelper" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="utils.DateUtils" %>
<%@ page import="java.util.Calendar" %>
<!DOCTYPE html>
<html>

<head>
    <title>Tennis Admin Menu</title>

    <link href="css/Admin.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

</head>

<body>

<div id="pageContainer">

    <div id="menuOuterBox">

        <div id="menuInnerBox">

            <div id="menuLinksContainer">
                <ul>
                    <li><a href="GroupsDisplayRecords.page">Edit Groups/Teams</a></li>
                    <li><a href="PlayerDisplayRecords.page">Edit Players</a></li>
                    <li><a href="EditGroupMembers.page">Edit Group/Team Members</a></li>
                    <li><a href="EventDisplayRecords.page">Edit Events (practices, matches, etc.)</a></li>
                    <li><a href="EditMatchList.page?div=4.0">Edit 4.0 Singles Match List</a></li>
                    <li><a href="EditMatchList.page?div=3.5">Edit 3.5 Singles Match List</a></li>
                </ul>
            </div>
        </div>

        <div id="timeDiv">
            <%= DateUtils.getDateFormattedForColumnHeader(Calendar.getInstance()) %>
        </div>

    </div>

</div>

</body>

</html>