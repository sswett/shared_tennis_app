<%@ page import="dao.Players" %>
<%@ page import="other.SessionHelper" %>
<%@ page import="beans.MatchBean" %>
<%@ page import="dao.Matches" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="other.MatchIdType" %>
<%@ page import="other.PlayerPerspectiveScores" %>

<%
    final int matchId = Integer.parseInt(request.getParameter("id"));
    final ArrayList<MatchBean> matches = Matches.createInstance(MatchIdType.MATCH, matchId).getRecords();
    MatchBean match = matches.get(0);

    final String matchLabel = match.hasCustomLabel() ? match.getCustomLabel() :
            "M" + match.getMatchNo() + ":";

    final String player1Name = match.getPlayer1Id() == 0 ? match.getPlayer1Note() :
            Players.createInstance(match.getPlayer1Id()).getRecords().get(0).getName();

    final String player2Name = match.getPlayer2Id() == 0 ? match.getPlayer2Note() :
            Players.createInstance(match.getPlayer2Id()).getRecords().get(0).getName();

    final PlayerPerspectiveScores scores = match.getScoresFromWinnerPerspective();

    final boolean showSet1ScoresAsBlanks = scores.getGamesWon(1) == 0 && scores.getGamesLost(1) == 0;
    final boolean showSet2ScoresAsBlanks = scores.getGamesWon(2) == 0 && scores.getGamesLost(2) == 0;
    final boolean showSet3ScoresAsBlanks = scores.getGamesWon(3) == 0 && scores.getGamesLost(3) == 0;

%>

<!DOCTYPE html>
<html>

<head>
    <title>Mobile Score Entry</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <%-- TODO: remove following later, if not needed --%>
    <script type="text/javascript" src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="mobileViewPageContainer">

    <h3>Score Entry</h3>

    <div id="semFormContainer">

    <div id="semHeader">
        <%= match.getYr() %> Sess <%= match.getSess() %> Div <%= match.getDivision() %>
        <br/>
        Week <%= match.getWeekNo() %> <%= matchLabel %> <%= match.getMatchDateFormattedForColumnHeader() %> <%= match.isPlayoff() ? "playoff" : "" %>
    </div>

    <form name="editMatchForm" action="MobileScoreEditMatch.page" method="POST">

        <input type="hidden" name="matchId" value="<%= matchId %>" />


        <div id="semPlayerSection">
            Select Winner
            <br/>
            <input type="radio" name="winningPlayerNo" id="winningPlayerNo1" value="1"
                    <%= match.getWinnerId() != 0 && match.getWinnerId() == match.getPlayer1Id() ? "checked=checked" : "" %> />

            <label for="winningPlayerNo1"><%= player1Name %></label>

            <br/>

            <input type="radio" name="winningPlayerNo" id="winningPlayerNo2" value="2"
                    <%= match.getWinnerId() != 0 && match.getWinnerId() == match.getPlayer2Id() ? "checked=checked" : "" %> />

            <label for="winningPlayerNo2"><%= player2Name %></label>

        </div>


        <div id="semScoreSection">
            Winner's Perspective
            <br/>
            Set 1:
            <input type="text" pattern="[0-9]*" name="set1gameswon" size="2"
                   value="<%= showSet1ScoresAsBlanks ? "" : scores.getGamesWon(1) %>" />
            -
            <input type="text" pattern="[0-9]*" name="set1gameslost" size="2"
                   value="<%= showSet1ScoresAsBlanks ? "" : scores.getGamesLost(1) %>" />
            <br/>
            Set 2:
            <input type="text" pattern="[0-9]*" name="set2gameswon" size="2"
                   value="<%= showSet2ScoresAsBlanks ? "" : scores.getGamesWon(2) %>" />
            -
            <input type="text" pattern="[0-9]*" name="set2gameslost" size="2"
                   value="<%= showSet2ScoresAsBlanks ? "" : scores.getGamesLost(2) %>" />
            <br/>
            Set 3:
            <input type="text" pattern="[0-9]*" name="set3gameswon" size="2"
                   value="<%= showSet3ScoresAsBlanks ? "" : scores.getGamesWon(3) %>" />
            -
            <input type="text" pattern="[0-9]*" name="set3gameslost" size="2"
                   value="<%= showSet3ScoresAsBlanks ? "" : scores.getGamesLost(3) %>" />
        </div>


        <div id="semTiebreakerSection">

            <label for="tiebreaker">Set 3 was tiebreaker?</label>

            <input type="checkbox" name="tiebreaker" id="tiebreaker" value="1"
                    <%= match.getSet3TiebreakerVal() == 0 ? "" : "checked=checked" %> />

        </div>


        <div id="semDefaultSection">

            <label for="is_default">Default?</label>

            <input type="checkbox" name="is_default" id="is_default" value="1"
                    <%= match.getIsDefaultVal() == 0 ? "" : "checked=checked" %> />

        </div>

        <div id="semSubmitContainer">
            <input type="button" value="Save" onclick="document.editMatchForm.submit()" />

            <input type="button" value="Cancel"
                   onclick="javascript:window.location='MobileScoreSelectMatch.page'" />
        </div>

    </form>


    </div>


</div>

</body>

</html>