<%@ page import="other.SessionHelper" %>
<%@ page import="other.View" %>

<%
    View defaultView = (View) session.getAttribute(SessionHelper.DEFAULT_VIEW_SESS_ATTR_KEY);
    String redirectToPage = request.getParameter("redirectToPage");
%>

<div id="changeViewSection">

    <form name="changeViewForm" action="ChangeView.page" method="POST">
        <input type="hidden" name="changeView" value="" />
        <input type="hidden" name="redirectToPage" value="<%= redirectToPage %>" />

        View Events by:
        <% if (SessionHelper.isMobileBrowser(request)) { %>
        <br/>
        <% } %>

        <div class="viewChoice">
        <input type="radio" id="groupChoice" name="viewRadio" value="<%= View.GROUP.name() %>"
            <%= defaultView == View.GROUP ? "checked=\"checked\"" : "" %>  onchange="taChangeView()" >
        <label for="groupChoice"><%= View.GROUP.getLabel()%></label>
        </div>

        <div class="viewChoice">
        <input type="radio" id="playerChoice" name="viewRadio" value="<%= View.PLAYER.name() %>"
            <%= defaultView == View.PLAYER ? "checked=\"checked\"" : "" %>  onchange="taChangeView()" >
        <label for="playerChoice"><%= View.PLAYER.getLabel()%></label>
        </div>

        <div class="viewChoice">
        <input type="radio" id="playerAllTeamsChoice" name="viewRadio" value="<%= View.PLAYER_ALL_TEAMS.name() %>"
            <%= defaultView == View.PLAYER_ALL_TEAMS ? "checked=\"checked\"" : "" %>  onchange="taChangeView()" >
        <label for="playerAllTeamsChoice"><%= View.PLAYER_ALL_TEAMS.getLabel()%></label>
        </div>

    </form>
</div>
