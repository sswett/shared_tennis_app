<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="other.RatingLevelType" %>
<%@ page import="dao.CurrentSessions" %>
<%@ page import="beans.CurrentSessionsBean" %>
<%
    ArrayList<PlayerBean> players = null;

    // Player ID can be passed as query string parm to show contact info for one player only
    String playerIdStr = request.getParameter("playerId");

    if (playerIdStr != null)
    {
        int playerId = Integer.parseInt(playerIdStr);

        if (playerId != 0)
        {
            players = Players.createInstance(playerId).getRecords();
        }
    }

    String division = request.getParameter("div");

    if (division == null)
    {
        division = "4.0";
    }

    // If not showing contact info for one player only, show everybody in division
    if (players == null)
    {
        CurrentSessionsBean sessBean = CurrentSessions.
                createInstance(SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID).getRecords().get(0);

        players =
                Players.createInstance(false,
                        division.equals("3.5") ? RatingLevelType.LEVEL_3_5 : RatingLevelType.LEVEL_4_0,
                        division.equals("3.5") ? sessBean.getGroupShortName3_5() :
                                sessBean.getGroupShortName4_0()).getRecords();
    }


%>
<!DOCTYPE html>
<html>

<head>
    <title>Singles League Contacts</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />


    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="mobileViewPageContainer">

<div class="slContactsSection">

    <h3><%= division %> Division Contacts</h3>

    <div id="slContacts">

        <%
            for (PlayerBean player : players)
            {
        %>

        <ul>
            <li><%= player.getName() %></li>
            <li><a href="tel:<%= player.getPhone() %>">call <%= player.getFormattedPhone() %></a></li>
            <li><a href="sms:<%= player.getPhone() %>">text <%= player.getFormattedPhone() %></a></li>
            <li><a href="mailto:<%= player.getEmail() %>"><%= player.getEmail() %></a></li>
        </ul>

        <% } %>

    </div>

</div>


</div>

</body>

</html>