<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="other.RatingLevelType" %>
<%@ page import="dao.CurrentSessions" %>
<%@ page import="beans.CurrentSessionsBean" %>
<%
    String div = request.getParameter("div");

    if (div == null)
    {
        div = "4.0";
    }

    CurrentSessionsBean sessBean = CurrentSessions.
            createInstance(SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID).getRecords().get(0);

    ArrayList<PlayerBean> players =
            Players.createInstance(false,
                    div.equals("3.5") ? RatingLevelType.LEVEL_3_5 : RatingLevelType.LEVEL_4_0,
                    div.equals("3.5") ? sessBean.getGroupShortName3_5() :
                            sessBean.getGroupShortName4_0()).getRecords();


%>
<!DOCTYPE html>
<html>

<head>
    <title>Singles League Player Cards</title>

    <link href="css/Singles.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="slPageContainer">

<div class="slPlayerListSection">

    <h3 class="slSubTitle"><%= div %> Division Player Cards</h3>


    <table class="slDataTable">
        <thead>
        <tr>
            <th class="slNameCol">Name</th>
        </tr>
        </thead>

        <tbody>

        <%
            boolean even = true;

            for (PlayerBean player : players)
            {
                even = !even;
                String rowClass = even ? "class=\"even\"" : "";
        %>

        <tr <%= rowClass %>>
            <td class="slNameCol">
                <a href="SinglesLeaguePlayerCard.page?playerId=<%= player.getId() %>&div=<%= div %>"><%= player.getName() %></a>
            </td>
        </tr>

        <% } %>

        </tbody>
    </table>

</div>

</div>

</body>

</html>