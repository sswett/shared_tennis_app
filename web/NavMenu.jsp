<%@ page import="other.SessionHelper" %>
<%
    final boolean isMobile = SessionHelper.isMobileBrowser(request);
%>
<!DOCTYPE html>
<html>

<head>
    <title>Navigation Menu</title>

    <% if (isMobile) { %>
    <meta name="viewport" content="width=device-width" />
    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <% } else { %>
    <link href="css/Desktop.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>

    <jsp:include page='NavMenuHead.jsp' flush='true'/>

</head>

<body>

    <jsp:include page='NavMenuBody.jsp' flush='true'/>

</body>

</html>