<%@ page import="other.SessionHelper" %>

<%

%>

<!DOCTYPE html>
<html>

<head>
    <title>Tennis - Test Mail Sender</title>

    <link href="css/Admin.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script type="text/javascript" src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>

</head>

<body>

<div id="pageContainer">

    <form name="mailSenderForm" action="TestMailSender.page" method="POST">

        This is a very basic test.  Just hit Submit to see if it will successfully send a message.
        <br/>
        <br/>

        <input type="submit" value="Submit" />

    </form>

</div>

</body>

</html>