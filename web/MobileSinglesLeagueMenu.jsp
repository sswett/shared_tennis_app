<%@ page import="other.SessionHelper" %>
<%@ page import="other.View" %>
<%@ page import="other.MatchType" %>
<%
%>
<!DOCTYPE html>
<html>

<head>
    <title>Mobile Singles League Menu</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <script type="text/javascript">
    </script>

    <script src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>
    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <%-- Following doesn't seem to make a difference for iPhone
    <jsp:include page="FavIconHeadSection.jsp" flush="true" />
    --%>

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="mobileViewPageContainer">

    <h3>Singles League Menu</h3>

    <div id="mobileSinglesMenuContainer">
        <ul>

            <li>
                <a href="SinglesLeagueGuidelines.page">Guidelines</a>
            </li>

            <li>
                <a href="MobileSinglesLeaguePlayerList.page?div=3.5">3.5</a>
                <a href="MobileSinglesLeaguePlayerList.page?div=4.0">4.0</a>
                Player Cards (has it all)
            </li>

            <li>
                <a href="MobileSinglesLeagueStandings.page?div=3.5&matchType=<%= MatchType.REGULAR_SEASON.name() %>">3.5</a>
                <a href="MobileSinglesLeagueStandings.page?div=4.0&matchType=<%= MatchType.REGULAR_SEASON.name() %>">4.0</a>
                Standings
            </li>

            <li>
                <a href="MobileSinglesLeagueContacts.page?div=3.5">3.5</a>
                <a href="MobileSinglesLeagueContacts.page?div=4.0">4.0</a>
                Player Contact Info
            </li>

            <li>
                <a href="MobileSinglesLeagueSchedule.page?div=3.5">3.5</a>
                <a href="MobileSinglesLeagueSchedule.page?div=4.0">4.0</a>
                Schedule & Results
            </li>

        </ul>
    </div>


</div>

</body>

</html>