<%@ page import="other.SessionHelper" %>
<%
	final int imgWidth = 840;
	final int imgHeight = 540;
%>
<!DOCTYPE html>
<html>

<head>
    <title>Tennis Project Angular CRUD Slideshow</title>

    <script src="js/jquery-1.10.2.min.js"></script>

    <script src="js/basic-jquery-slider-master/js/bjqs-1.3.min.js"></script>

    <link href="js/basic-jquery-slider-master/bjqs.css" rel="stylesheet">
    <link href="css/Slideshow.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <link href="css/Singles.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    
    <link href="css/prism.css" rel="stylesheet" />

    <script>

        <%-- when responsive, the dimensions are the maximums --%>

        <%--
                NOTE: I didn't want screen shot images to shrink responsively, nor did I want the
                      captions or controls to overlay the image.  So I made the slideshow
                      container bigger than the images and turned off the responsive and
                      showcontrols settings.
        --%>

        jQuery(document).ready(function($) {
            $('#my-slideshow').bjqs({
                height : 610,
                width : 900,
                responsive : false,
                hoverpause : false, // pause the slider on hover
                animspeed : 13000, // the delay between each slide
                randomstart : false,
                showcontrols : false,
                automatic : false
            });
        });

    </script>

</head>

<body>

<div id="container">

<div id="my-slideshow">
    <ul class="bjqs">
        <li>
            <img src="slideshows/TennisProject/AngularCRUD/PlayerList.png<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="This is page 1 of 2 of a simple CRUD app used to create and edit player records.  Technologies used 
                    include Jersey RESTful APIs (Java), AngularJS and Bootstrap (for responsive layouts)."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/AngularCRUD/PlayerDetail.png<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="This is page 2 of 2 of a simple CRUD app used to create and edit player records.  Angular's 
                    form validation gives instant feedback as values are entered."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/AngularCRUD/PlayerDetailAtSave.png<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="When Save is clicked, all error messages triggered by validation are shown."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/AngularCRUD/DirectoryStructure.png<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Files related to AngularJS are stored within a directory named 'Angular', which is under the 
                    web content directory of the larger tennis project.  The key files are app.js, controllers.js and services.js --
                    along with a file named AngularApp.html, which is the single full page template in the app."/>
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/angular_app_html.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/player_details_html.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/app_js.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/controllers_js.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/services_js.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/player_resource_java.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/rest_services_filter_java.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/player_bean_java.jsp" flush="true" />
        </li>
        <li>
            <img src="slideshows/TennisProject/AngularCRUD/PlayerListWithNav.png<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Notice the nav menu at the top of the page. The menu was created previously using 
                    'jquery.mb.menu' by Matteo Bicocchi (Pupunzi), and appears on most pages in the web app. 
                    The remaining slides show how it was added to the AngularJS CRUD app."/>
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/angular_app_html_with_nav.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/app_js_with_nav.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/controllers_js_with_nav.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/AngularCRUD/nav_menu_html.jsp" flush="true" />
        </li>
    </ul>
</div>


</div>

<script src="js/prism.js"></script>

</body>

</html>