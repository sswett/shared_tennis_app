<%@ page import="other.SessionHelper" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="dao.StandingsCalc" %>
<%@ page import="beans.StandingsCalcBean" %>
<%@ page import="other.MatchType" %>
<%
    String div = request.getParameter("div");

    if (div == null)
    {
        div = "4.0";
    }

    String matchTypeParm = request.getParameter("matchType");
    MatchType matchType = MatchType.ALL;   // default

    if (matchTypeParm != null)
    {
        try
        {
            matchType = MatchType.valueOf(matchTypeParm);
        }
        catch (Exception e)
        {
            // eat
        }
    }

%>
<!DOCTYPE html>
<html>

<head>
    <title>Singles League Standings</title>

    <link href="css/Singles.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <script>

        function slsChangeMatchType(pElem)
        {
            var matchType = pElem.value;
            window.location='SinglesLeagueStandings.page?div=<%= div %>&matchType=' + matchType;
        }

    </script>

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="slPageContainer">

<div class="slScheduleSection">

    <h3 class="slSubTitle">
        <div class="slSubTitlePart1">
            <%= div %> Division Standings

            &nbsp;

            <select id="slsMatchTypeSelect" name="slsMatchTypeSelect" onchange="slsChangeMatchType(this)">
                <%
                    for (MatchType type : MatchType.values())
                    {
                        final boolean isSelected = type == matchType;
                %>
                <option <%= isSelected ? "selected=\"selected\"" : "" %>
                        value="<%= type.name() %>" >
                    <%= type.getLabel() %>
                </option>
                <% }   // for MatchType %>
            </select>

        </div>
        <div class="slSubTitlePart2">
            Rank calculation: Match %, Set %, Game %
        </div>
    </h3>

    <div id="slSubTitleClear"></div>

    <div id="slStandingsTableContainer">

    <table class="slDataTable">
        <thead>
        <tr>
            <th colspan="2">&nbsp;</th>
            <th colspan="3">Matches</th>
            <th colspan="3">Sets</th>
            <th colspan="3">Games</th>
        </tr>

        <tr>
            <th class="slRankCol">Rank</th>
            <th class="slPlayerCol">Player</th>
            <th class="slMatchWinsCol">W</th>
            <th class="slMatchLossesCol">L</th>
            <th class="slMatchWinPctCol">%</th>
            <th class="slSetWinsCol">W</th>
            <th class="slSetLossesCol">L</th>
            <th class="slSetWinPctCol">%</th>
            <th class="slGameWinsCol">W</th>
            <th class="slGameLossesCol">L</th>
            <th class="slGameWinPctCol">%</th>
        </tr>
        </thead>

        <tbody>

        <%
            boolean even = true;

            ArrayList<StandingsCalcBean> standingsCalcs = StandingsCalc.createInstance(session.getId(), div, matchType).getRecords();

            for (StandingsCalcBean standingsCalc : standingsCalcs)
            {
                even = !even;
                String rowClass = even ? "class=\"even\"" : "";
        %>

        <tr <%= rowClass %>>
            <td class="slRankCol"><%= standingsCalc.getPlayerRank() %></td>

            <td class="slPlayerCol">
                <a href="SinglesLeaguePlayerCard.page?div=<%= div %>&playerId=<%= standingsCalc.getPlayerId() %>">
                <%= standingsCalc.getPlayerName() %></a>
            </td>

            <td class="slMatchWinsCol"><%= standingsCalc.getMatchesWon() %></td>
            <td class="slMatchLossesCol"><%= standingsCalc.getMatchesLost() %></td>
            <td class="slMatchWinPctCol"><%= String.format("%.3f", standingsCalc.getMatchesWonPct()) %></td>
            <td class="slSetWinsCol"><%= standingsCalc.getSetsWon() %></td>
            <td class="slSetLossesCol"><%= standingsCalc.getSetsLost() %></td>
            <td class="slSetWinPctCol"><%= String.format("%.3f", standingsCalc.getSetsWonPct()) %></td>
            <td class="slGameWinsCol"><%= String.format("%.2f", standingsCalc.getGamesWon()) %></td>
            <td class="slGameLossesCol"><%= String.format("%.2f", standingsCalc.getGamesLost()) %></td>
            <td class="slGameWinPctCol"><%= String.format("%.3f", standingsCalc.getGamesWonPct()) %></td>
        </tr>

        <% } %>

        </tbody>
    </table>


    </div>


</div>


</div>

</body>

</html>