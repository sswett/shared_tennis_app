<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="other.SessionHelper" %>
<%@ page import="beans.MatchBean" %>
<%@ page import="dao.Matches" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="other.RatingLevelType" %>
<%@ page import="other.MatchIdType" %>
<%@ page import="beans.CurrentSessionsBean" %>
<%@ page import="dao.CurrentSessions" %>

<%
    final int matchId = Integer.parseInt(request.getParameter("id"));
    final ArrayList<MatchBean> matches = Matches.createInstance(MatchIdType.MATCH, matchId).getRecords();
    final boolean matchFound = matches.size() > 0;
    MatchBean match = null;
    ArrayList<PlayerBean> players = null;
    String division = "4.0";   // default

    if (matchFound)
    {
        match = matches.get(0);

        division = match.getDivision();

        CurrentSessionsBean sessBean = CurrentSessions.
                createInstance(SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID).getRecords().get(0);

        players = Players.createInstance(false,
                division.equals("4.0") ? RatingLevelType.LEVEL_4_0 : RatingLevelType.LEVEL_3_5,
                division.equals("4.0") ? sessBean.getGroupShortName4_0() :
                        sessBean.getGroupShortName3_5()).getRecords();
    }
%>

<!DOCTYPE html>
<html>

<head>
    <title>Edit Match</title>

    <link href="css/Admin.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <%-- TODO: remove following later, if not needed --%>
    <script type="text/javascript" src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>

</head>

<body>

<div id="pageContainer">

        <div id="emHeader">
            <h2>
                Edit Match
            </h2>
        </div>

    <div id="emSubHeader">

<% if (matchFound) { %>

        <div class="emSubHeaderPart">
            Year: <%= match.getYr() %>
        </div>

        <div class="emSubHeaderPart">
            Session: <%= match.getSess() %>
        </div>

        <div class="emSubHeaderPart">
            Division: <%= match.getDivision() %>
        </div>

        <div class="emSubHeaderPart">
            Week#: <%= match.getWeekNo() %>
        </div>

        <div class="emSubHeaderPart">
            Week Of: <%= match.getMatchDateFormattedForColumnHeader() %>
        </div>

        <div class="emSubHeaderPart">
            Match#: <%= match.getMatchNo() %>
        </div>

<% } else { %>
        <div class="emSubHeaderPart">
            Match record could not be found.
        </div>
<% } %>

        <div class="emSubHeaderPartLast">
            &nbsp;
        </div>


    </div>


<% if (matchFound) { %>

    <div id="emDataEntry">

        <form name="editMatchForm" action="EditMatch.page" method="POST">

            <input type="hidden" name="matchId" value="<%= matchId %>" />
            <input type="hidden" name="next" value="false" />

            <div id="emdeCustomLabelRow" class="emdeRow">

                <div class="emdeCol1">
                    Custom label:
                </div>

                <div class="emdeCol2">
                    <input type="text" name="custom_label" size="10" value="<%= match.getCustomLabel() %>" />
                </div>

                <div class="emdeCol3">
                    &nbsp;
                </div>

                <div class="emdeCol4">
                    &nbsp;
                </div>

            </div>

            <div id="emdeHeadingRow" class="emdeRow">

                <div class="emdeCol1">
                    &nbsp;
                </div>

                <div class="emdeCol2">
                    Player 1
                </div>

                <div class="emdeCol3">
                    Player 2
                </div>

                <div class="emdeCol4">
                    &nbsp;
                </div>

            </div>

            <div id="emdeNameRow" class="emdeRow">

                <div class="emdeCol1">
                    Name:
                </div>

                <div class="emdeCol2">
                    <select name="player1Id">
                        <option value="0">not specified</option>
                        <% for (PlayerBean player : players)
                        {
                            final boolean isSelected = player.getId() == match.getPlayer1Id();
                        %>
                        <option <%= isSelected ? "selected=selected" : "" %>
                                value="<%= player.getId() %>"><%= player.getName() %></option>
                        <% }   // for player %>
                    </select>
                </div>

                <div class="emdeCol3">
                    <select name="player2Id">
                        <option value="0">not specified</option>
                        <% for (PlayerBean player : players)
                        {
                            final boolean isSelected = player.getId() == match.getPlayer2Id();
                        %>
                        <option <%= isSelected ? "selected=selected" : "" %>
                                value="<%= player.getId() %>"><%= player.getName() %></option>
                        <% }   // for player %>
                    </select>
                </div>

                <div class="emdeCol4">
                    &nbsp;
                </div>

            </div>

            <div id="emdePlayerNotesRow" class="emdeRow">

                <div class="emdeCol1">
                    Player notes:
                </div>

                <div class="emdeCol2">
                    <input type="text" name="player1_note" size="30" value="<%= match.getPlayer1Note() %>" />
                </div>

                <div class="emdeCol3">
                    <input type="text" name="player2_note" size="30" value="<%= match.getPlayer2Note() %>" />
                </div>

                <div class="emdeCol4">
                    &nbsp;
                </div>

            </div>

            <div id="emdeWinnerRow" class="emdeRow">

                <div class="emdeCol1">
                    Winner:
                </div>

                <div class="emdeCol2">
                    <input type="radio" name="winningPlayerNo" value="1"
                            <%= match.getWinnerId() != 0 && match.getWinnerId() == match.getPlayer1Id() ? "checked=checked" : "" %> />
                </div>

                <div class="emdeCol3">
                    <input type="radio" name="winningPlayerNo" value="2"
                            <%= match.getWinnerId() != 0 && match.getWinnerId() == match.getPlayer2Id() ? "checked=checked" : "" %> />
                </div>

                <div class="emdeCol4">
                    &nbsp;
                </div>

            </div>

            <div id="emdeSet1Row" class="emdeRow">

                <div class="emdeCol1">
                    Set 1 games won:
                </div>

                <div class="emdeCol2">
                    <input type="text" name="player1set1gameswon" size="2" value="<%= match.getGamesWon(1, 1) %>" />
                </div>

                <div class="emdeCol3">
                    <input type="text" name="player2set1gameswon" size="2" value="<%= match.getGamesWon(2, 1) %>" />
                </div>

                <div class="emdeCol4">
                    &nbsp;
                </div>

            </div>

            <div id="emdeSet2Row" class="emdeRow">

                <div class="emdeCol1">
                    Set 2 games won:
                </div>

                <div class="emdeCol2">
                    <input type="text" name="player1set2gameswon" size="2" value="<%= match.getGamesWon(1, 2) %>" />
                </div>

                <div class="emdeCol3">
                    <input type="text" name="player2set2gameswon" size="2" value="<%= match.getGamesWon(2, 2) %>" />
                </div>

                <div class="emdeCol4">
                    &nbsp;
                </div>

            </div>

            <div id="emdeSet3Row" class="emdeRow">

                <div class="emdeCol1">
                    Set 3 games won:
                </div>

                <div class="emdeCol2">
                    <input type="text" name="player1set3gameswon" size="2" value="<%= match.getGamesWon(1, 3) %>" />
                </div>

                <div class="emdeCol3">
                    <input type="text" name="player2set3gameswon" size="2" value="<%= match.getGamesWon(2, 3) %>" />
                </div>

                <div class="emdeCol4">
                    &nbsp;
                </div>

            </div>

            <div id="emdeTiebreakerRow" class="emdeRow">

                <div class="emdeCol1">
                    <label for="tiebreaker">Set 3 was tiebreaker?</label>
                </div>

                <div class="emdeCol2">
                    <input type="checkbox" name="tiebreaker" id="tiebreaker" value="1"
                            <%= match.getSet3TiebreakerVal() == 0 ? "" : "checked=checked" %> />
                </div>

                <div class="emdeCol3">
                    &nbsp;
                </div>

                <div class="emdeCol4">
                    &nbsp;
                </div>

            </div>

            <div id="emdePlayoffDefaultRow" class="emdeRow">

                <div class="emdeCol1">
                    <label for="is_playoff">Is playoff match?</label>
                </div>

                <div class="emdeCol2">
                    <input type="checkbox" name="is_playoff" id="is_playoff" value="1"
                            <%= match.getIsPlayoffVal() == 0 ? "" : "checked=checked" %> />
                </div>

                <div class="emdeCol3">
                    <label for="is_default">Is default?</label>

                    <input type="checkbox" name="is_default" id="is_default" value="1"
                            <%= match.getIsDefaultVal() == 0 ? "" : "checked=checked" %> />
                </div>

                <div class="emdeCol4">
                    &nbsp;
                </div>

            </div>

            <div id="emdeNoteRow" class="emdeRow">

                <div class="emdeCol1">
                    Note:
                </div>

                <div class="emdeCol2">
                    <input type="text" name="note" size="50" value="<%= match.getNote() %>" />
                </div>

                <div class="emdeCol3">
                    &nbsp;
                </div>

                <div class="emdeCol4">
                    &nbsp;
                </div>

            </div>

            <div id="emdeSubmitContainer">
                <input type="button" value="Save" onclick="document.editMatchForm.submit()" />

                <input type="button" value="Save and Next"
                       onclick="document.editMatchForm.next.value='true';document.editMatchForm.submit()" />

                <input type="button" value="Cancel"
                       onclick="javascript:window.location='EditMatchList.page?div=<%= division %>&fromMatchId=<%= matchId %>'" />
            </div>

        </form>

    </div>

<% } %>

</div>

</body>

</html>