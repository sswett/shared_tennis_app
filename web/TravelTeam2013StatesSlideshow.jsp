<%@ page import="other.SessionHelper" %>
<%
	final boolean isMobile = SessionHelper.isMobileBrowser(request);
%>
<!DOCTYPE html>
<html>

<head>
    <title>Slideshow - Travel Team 2013 States at MSU</title>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <script src="js/basic-jquery-slider-master/js/bjqs-1.3.min.js"></script>

    <link href="js/basic-jquery-slider-master/bjqs.css" rel="stylesheet">
    <link href="css/Slideshow.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <link href="css/Singles.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script>

        <%-- when responsive, the dimensions are the maximums --%>

        jQuery(document).ready(function($) {
            $('#my-slideshow').bjqs({
                height : 600,
                width : 900,
                responsive : true,
                hoverpause : true, // pause the slider on hover
                animspeed : 10000 // the delay between each slide
            });
        });

    </script>

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="container">

<div id="my-slideshow">
    <ul class="bjqs">
        <li>
            <img src="slideshows/travel_teams/2013states/2013_states_team_1024x768.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="1024" height="768"
            
            <%-- Use shorter caption on mobile device so doesn't overlay so much of image. --%>
            
            <% if (isMobile) { %>
                    title="Rockford MVP 3.5 at 2013 MI State Championships at U of M Tennis Center.
                    Front: Peceny, Wiegand, White, Schmidt.  Back: Lane, Zannini, Brown, Swett, DeLaFuente, 
                    B Allen, H Allen. Clark played but not shown."
            <% } else { %>
                    title="Rockford MVP 3.5 Men's tennis team at the 2013 Michigan State Championships held at
                    University of Michigan's Varsity Tennis Center.  Front row: Chris Peceny, Carl Wiegand,
                    Chuck White, Mike Schmidt.  Back row: Mark Lane, Butch Zannini, Paul Brown, Steve Swett,
                    Jim DeLaFuente, Ben Allen, Harvey Allen. Chris Clark also played in the championships but
                    was not present for this photo."
            <% } %>
                    />
        </li>
        <li>
            <img src="slideshows/travel_teams/2013states/2013_states_chris_c_and_carl_1200x900.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="1024" height="768"
                    title="2013 State Championships. Near court: Carl Wiegand and Chris Clark. Far court: Mark
                    Lane and Chuck White" />
        </li>
        <li>
            <img src="slideshows/travel_teams/2013states/2013_states_jim_near_steve_far_1200x900.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="1024" height="768"
                    title="2013 State Championships. Near court: Paul Brown and Chris Peceny.  Middle court:
                    Jim DeLaFuente's opponent. Far court: Steve Swett"/>
        </li>
        <li>
            <img src="slideshows/travel_teams/2013states/2013_states_carl_1200x900.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="1024" height="768"
                    title="2013 State Championships. Carl Wiegand."/>
        </li>
        <li>
            <img src="slideshows/travel_teams/2013states/2013_states_chris_c_1200x900.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="1024" height="768"
                    title="2013 State Championships. Chris Clark and Carl Wiegand."/>
        </li>
        <li>
            <img src="slideshows/travel_teams/2013states/2013_states_chris_p_and_paul_1200x900.jpg<%= SessionHelper.getVersionQueryString() %>" 
            width="1024" height="768"
                    title="2013 State Championships. Chris Peceny and Paul Brown."/>
        </li>
    </ul>
</div>


</div>


</body>

</html>