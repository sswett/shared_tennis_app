<%@ page import="other.SessionHelper" %>
<%@ page import="utils.DateUtils" %>
<%@ page import="java.util.Calendar" %>
<%
    boolean isMobile = SessionHelper.isMobileBrowser(request);
%>
<!DOCTYPE html>
<html>

<head>
    <title>Tennis Project Info</title>

    <% if (isMobile) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <% if (isMobile) { %>
    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <% } else { %>
    <link href="css/Admin.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <% } %>

</head>

<body>

<div id="projectInfoContainer">

<div id="hdrOutOfBounds"></div>
<div id="hdrSideline"></div>
<div id="hdrCourt"></div>

            <div id="projectInfoItemsContainer">
                <ul>
                    <li>

					<div class="piItemHdr">
						<div><h1>Project Overview and History (for Steve Swett's tennis	site)</h1></div>

						<div class="itemOutOfBounds"></div>
						<div class="itemSideline"></div>
						<div class="itemCourt"></div>
					</div>

					<div class="piItemBody">
                            <p>
                            One of my hobbies is tennis. A few years ago I seized an opportunity to play on
                            Rockford MVP Athletic Club's USTA League tennis team.  Later, after becoming team
                            captain, I recruited new players and expanded to multiple teams.  In 2014 I started
                            a non-USTA outdoor singles league for players in the greater Grand Rapids area.
                            </p>

                            <p>
                                Initially, my teenage son developed the spreadsheet portion of the desktop web app
                                using Python and Django.  However, I wanted many more features than his time
                                allowed, so I rewrote it in Java with servlets and continue to enhance it.
                            </p>
                            
                            <p>
                            	In November 2014 I began using AngularJS and RESTful APIs in portions of the project.
                            </p>

                        </div>
                    </li>

                    <li>

					<div class="piItemHdr">
						<div><h2>Desktop Features</h2></div>

						<div class="itemOutOfBounds"></div>
						<div class="itemSideline"></div>
						<div class="itemCourt"></div>
					</div>

                        <div class="piItemBody">
                        	<p>
                            For now, the features are shown/described via this
                            <a href="/TennisProjectDesktopSlideshow.page">slideshow</a>.
                            </p>
                        </div>
                    </li>

                    <li>

					<div class="piItemHdr">
						<div><h2>Mobile Features</h2></div>

						<div class="itemOutOfBounds"></div>
						<div class="itemSideline"></div>
						<div class="itemCourt"></div>
					</div>

                        <div class="piItemBody">
                        	<p>
                            Shown/described via this
                            <a href="/TennisProjectMobileSlideshow.page">slideshow</a>.
                            </p>
                        </div>
                    </li>

                    <li>

					<div class="piItemHdr">
						<div><h2>Architecture</h2></div>

						<div class="itemOutOfBounds"></div>
						<div class="itemSideline"></div>
						<div class="itemCourt"></div>
					</div>

                        <div class="piItemBody">
                        	<p>
                            The web stack is Ubuntu, Apache, Tomcat, SQLite, Java -- hosted with AWS.  
                            A few select source files and items are shown via this
                            <a href="/TennisProjectArchitectureSlideshow.page">slideshow</a>.
                            </p>
                        </div>
                    </li>

                    <li>

					<div class="piItemHdr">
						<div><h2>CRUD App using AngularJS, RESTful APIs and Bootstrap CSS</h2></div>

						<div class="itemOutOfBounds"></div>
						<div class="itemSideline"></div>
						<div class="itemCourt"></div>
					</div>
                        <div class="piItemBody">
                        	<p>
                            I created a CRUD app for tennis player records to demonstrate my ability to use AngularJS and 
                            RESTful APIs.  Some screen shots and select source files are shown via this
                            <a href="/TennisProjectAngularCRUDSlideshow.page">slideshow</a>.
                            </p>
                        </div>
                    </li>

                    <li>

					<div class="piItemHdr">
						<div><h2>JAX-WS Examples using Axis2 and Eclipse Wizards</h2></div>

						<div class="itemOutOfBounds"></div>
						<div class="itemSideline"></div>
						<div class="itemCourt"></div>
					</div>
                        <div class="piItemBody">
                        	<p>
                            I created a couple simple Java XML web services and Java application clients to demonstrate my ability    
                            to use Axis2 and JAX-WS APIs.  Some screen shots and select source files are shown via this
                            <a href="/TennisProjectJAXWSExampleSlideshow.page">slideshow</a>.
                            </p>
                        </div>
                    </li>

                    <li>

					<div class="piItemHdr">
						<div><h2>Source Code Repository</h2></div>

						<div class="itemOutOfBounds"></div>
						<div class="itemSideline"></div>
						<div class="itemCourt"></div>
					</div>
                        <div class="piItemBody">
                        	<p>
                            Although this project is "private" and not open source, I have decided to share the source code at
                            <a href="https://bitbucket.org/sswett/shared_tennis_app" target="_blank">Bitbucket</a> so that prospective
                            employers can take a look.
                            </p>
                        </div>
                    </li>

                </ul>
            </div>

</div>

</body>

</html>