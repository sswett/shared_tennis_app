<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Groups" %>
<%@ page import="beans.GroupsBean" %>
<%@ page import="dao.Events" %>
<%@ page import="beans.EventBean" %>
<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="dao.EventResponses" %>
<%@ page import="other.EventResponseIdType" %>
<%@ page import="beans.EventResponseJoinedDataBean" %>
<%@ page import="other.PlayerResponse" %>
<%@ page import="utils.StringUtils" %>
<%@ page import="utils.DateUtils" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="db.DBUtils" %>
<%
    final boolean showTextLinkWhileTesting = false;

    final boolean isMobile = SessionHelper.isMobileBrowser(request);
    final boolean isAppleMobileBrowser = SessionHelper.isAppleMobileBrowser(request);
    final String phoneNumberDelimiter = isMobile && isAppleMobileBrowser ? "<br/>" : ",<br/>";

    final int serverMsgRows = isMobile ? 8 : 6;
    final int serverMsgCols = isMobile ? 35 : 105;

    int groupId = SessionHelper.getDefaultGroupId(session);
    GroupsBean group = Groups.createInstance(groupId).getRecords().get(0);

    String eventIdParm = request.getParameter("eventId");
    int eventId = eventIdParm != null && eventIdParm.length() > 0 ? Integer.parseInt(eventIdParm) : 0;

    // Players used to produce output:
    ArrayList<PlayerBean> players = new ArrayList<PlayerBean>();
    ArrayList<EventResponseJoinedDataBean> responses = new ArrayList<EventResponseJoinedDataBean>();

    if (eventId == 0)
    {
        players = Players.createInstance(false, groupId, true).getRecords();
    }
    else
    {
        responses = EventResponses.createInstance(EventResponseIdType.EVENT, eventId, groupId).getRecords();
    }

    ArrayList<String> masterIdList = new ArrayList<String>();
    ArrayList<String> masterNameList = new ArrayList<String>();
    ArrayList<String> masterPhoneList = new ArrayList<String>();
    ArrayList<String> masterEmailList = new ArrayList<String>();
    ArrayList<String> masterResponseDateList = new ArrayList<String>();
    ArrayList<String> masterCategoryList = new ArrayList<String>();
    ArrayList<String> masterMbrTypeList = new ArrayList<String>();

    if (eventId == 0) {

        for (PlayerBean player : players)
        {
            masterIdList.add(String.valueOf(player.getId()));
            masterNameList.add(player.getName());
            masterPhoneList.add(player.getPhone());
            masterEmailList.add(player.getEmail());
            masterCategoryList.add("none");
            masterResponseDateList.add("");
            masterMbrTypeList.add(player.getMbrTypeDescr());
        }
    }

    else {

        for (EventResponseJoinedDataBean respBean : responses)
        {
            masterIdList.add(String.valueOf(respBean.getPlayerId()));
            masterNameList.add(respBean.getPlayerName());
            masterPhoneList.add(respBean.getPlayerPhone());
            masterEmailList.add(respBean.getPlayerEmail());
            masterCategoryList.add(respBean.getPlayerResponse().name());
            Calendar responseDate = respBean.getResponseDate();

            if (DBUtils.isSameAsDefaultDbDateWhenUnreadable(responseDate))
            {
                masterResponseDateList.add("");
            }
            else
            {
                masterResponseDateList.add(respBean.getPlayerResponse().getLabel() + " " +
                        DateUtils.getDateFormattedForColumnHeader(responseDate));
            }

            masterMbrTypeList.add(respBean.getPlayerMbrTypeDescr());
        }
    }

    int defaultAdminId = (Integer) session.getAttribute(SessionHelper.DEFAULT_ADMIN_SESS_ATTR_KEY);
    Players admins = Players.createInstanceForAdmins();
    ArrayList<PlayerBean> adminRecords = admins.getRecords();


%>
<!DOCTYPE html>
<html>

<head>
    <title>Tennis Communications</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <meta name="format-detection" content="telephone=no" />
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />
    
    <script src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>

    <% if (isMobile) { %>
    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <% } else { %>
    <link href="css/Admin.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <link href="css/Print.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet" media="print">
    <% } %>

    <script>

        var globalYesResponse = '<%= PlayerResponse.yes.name() %>';

        $(document).ready(function() {
            cpRefreshHtmlElements(<%= eventId %>);
        });

    </script>

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<form name="copyPasteForm">

<input type="hidden" name="masterIdList" value="<%= StringUtils.join(masterIdList, ";") %>" />
<input type="hidden" name="masterNameList" value="<%= StringUtils.join(masterNameList, ";") %>" />
<input type="hidden" name="masterPhoneList" value="<%= StringUtils.join(masterPhoneList, ";") %>" />
<input type="hidden" name="masterEmailList" value="<%= StringUtils.join(masterEmailList, ";") %>" />
<input type="hidden" name="masterCategoryList" value="<%= StringUtils.join(masterCategoryList, ";") %>" />
<input type="hidden" name="masterResponseDateList" value="<%= StringUtils.join(masterResponseDateList, ";") %>" />
<input type="hidden" name="masterMbrTypeList" value="<%= StringUtils.join(masterMbrTypeList, ";") %>" />
<input type="hidden" name="phoneDelimiterForRawCopyPaste" value="<%= phoneNumberDelimiter %>" />

<%-- filtered output to pass to servlet with POST for processing: --%>
<input type="hidden" name="filteredPhoneList" value="" />
<input type="hidden" name="filteredEmailList" value="" />

<input type="hidden" name="sendServerMsg" value="false" />

<div id="copyPastePageContainer">

    <%-- This div gets populated by Ajax --%>
    <div id="sendServerMsgResults"></div>

    <div id="copyPasteDescrSection" class="copyPasteSection">
        <h2>Group: <%= group.getName() %>

            <% if (eventId != 0)
            {
                EventBean event = Events.createInstance(eventId).getRecords().get(0);
            %>
            <br/>Event: <%= event.getDateFormattedForColumnHeader() %> <%= event.getDescriptionMinusBrTags() %></h2>
        <% } %>
    </div>

    <%-- Player checkboxes --%>
    <% if (eventId == 0) { %>
    <div id="copyPastePlayerCheckboxSection" class="copyPasteSection">
        <div id="playerCheckboxButtons">
            <input type="button" name="selectAll" value="Select All" onclick="cpSelectAllPlayers(<%= eventId %>)" />
            <input type="button" name="deselectAll" value="Deselect All" onclick="cpDeselectAllPlayers(<%= eventId %>)" />
        </div>

        <div id="playerCheckboxes">
            <% for (PlayerBean player : players) { %>
            <div class="checkBoxListDiv">
                <input type="checkbox" name="p<%= player.getId() %>" id="p<%= player.getId() %>" checked="true"
                       class="playerCheckbox" onchange="cpRefreshHtmlElements(<%= eventId %>)" />
                <label for="p<%= player.getId() %>"><%= player.getName() %></label>
            </div>
            <% } %>
        </div>
    </div>

    <%-- Response type checkboxes --%>
    <% } else  { %>
    <div id="copyPasteResponseTypeSection" class="copyPasteSection">
        <div id="responseTypeButtons">
            <input type="button" name="selectAll" value="Select All" onclick="cpSelectAllResponseTypes(<%= eventId %>)" />
            <input type="button" name="deselectAll" value="Deselect All" onclick="cpDeselectAllResponseTypes(<%= eventId %>)" />
        </div>

        <div id="responseTypeCheckboxes">
            <% for (PlayerResponse pr : PlayerResponse.values()) { %>
            <div class="checkBoxListDiv">
                <input type="checkbox" name="rt<%= pr.name() %>" id="rt<%= pr.name() %>" class="rtCheckbox"
                       onchange="cpRefreshHtmlElements(<%= eventId %>)" />
                <label for="rt<%= pr.name() %>"><%= pr.getLabel().equals("") ? "(no response)" : pr.getLabel() %></label>
            </div>
            <% } %>
        </div>
    </div>
    <% } %>


        <%-- Draw a "Yes" Responder Section --%>
        <div id="copyPasteRandomDrawSection" class="copyPasteSection">
            <input type="button" id="randomDrawButton" value='Draw a "Yes" Responder' onclick="cpRandomDraw();" />
            <div id="drawResult"></div>
        </div>


    <% if (showTextLinkWhileTesting || !isAppleMobileBrowser) { %>

    <div id="copyPasteTextLinkSection" class="copyPasteSection">
        <a id="smslink" href="sms:replaced_by_javascript">Send Text via Device</a>
    </div>

    <% } %>


    <div id="copyPasteEmailLinkSection" class="copyPasteSection">
        <a id="mailtolink" href="mailto:replaced_by_javascript">Send Email via Device</a>
    </div>


    <div id="copyPasteServerMsgSection" class="copyPasteSection">
        <h2><label for="servermsg">Send Message via Server</label></h2>
        <label for="senderSelect">Sender:</label>

        <select id="senderSelect" name="senderSelect" onchange="cpChangeSender()">
            <%
                for (PlayerBean admin : adminRecords)
                {
                    final boolean isSelected = admin.getId() == defaultAdminId;
            %>
            <option <%= isSelected ? "selected=\"selected\"" : "" %>
                    value="<%= admin.getId() + ";" + admin.getName() + ";" + admin.getEmail() + ";" + admin.getPhone() %>"><%= admin.getName() %></option>
            <% }   // for admin %>
        </select>

        <br/>

        <textarea rows="<%= serverMsgRows %>" cols="<%= serverMsgCols %>" name="serverMsg" id="serverMsg"
                  placeholder="Type Msg Here (be brief if texting)" onkeyup="cpEnableOrDisableSend()" ></textarea>
        <br/>
        <input id="sendServerMsgButton" type="button" value="Send" disabled="disabled" onclick="cpSendServerMsg()" />

        <% if (isMobile) { %><br/><% } %>

        as

        <input type="checkbox" name="sendAsText" id="sendAsText" value="1" checked="true" onchange="cpEnableOrDisableSend()" />
        <label for="sendAsText">Text (<span id="textQtys"></span>)</label>

        <input type="checkbox" name="sendAsEmail" id="sendAsEmail" value="1" onchange="cpEnableOrDisableSend()" />
        <label for="sendAsEmail">E-mail (<span id="emailQtys"></span>)</label>

    </div>


    <% if (!isMobile) { %>

        <div class="copyPasteSection">
            <input type="button" value="Print Money Collection Sheet"
                   onclick="taPrintElementWithId('copyPastePageContainer', 'copyPasteNameSection')" />
        </div>

    <div id="copyPasteNameSection" class="copyPasteSection">
        <h2>Money Collection Sheet</h2>
        <h3>Please <span id="cpCircle">circle your name</span> to indicate paid. COST: mbr: $ ____ non-mbr: $ ____</h3>

        <div id="copyPasteNames">
            replaced_by_javascript
        </div>

        <div id="copyPasteNamesFooter">
            Players: <span id="cpPlayerCount">replaced_by_javascript</span> &nbsp;&nbsp; 
            M: <span id="cpMbrCount">replaced_by_javascript</span> &nbsp;&nbsp; 
            NM: <span id="cpNonMbrCount">replaced_by_javascript</span> &nbsp;&nbsp; 
            Seeded: $ _____
        </div>

    </div>

    <div class="copyPasteSection">
        <input type="button" value="Print Player Contact List"
               onclick="taPrintElementWithId('copyPastePageContainer', 'copyPasteContactListSection')" />
    </div>
    <% } %>


    <div id="copyPasteContactListSection" class="copyPasteSection">
        <h2>Player Contact List</h2>

        <div id="cpContactListContainer">

            <% if (!isMobile) { %>
            <table id="cpContactListTable">
                <%--
                replaced_by_javascript

                <tr>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>E-mail</th>
                </tr>
                    <tr>
                        <td>Sample Steve</td>
                        <td><a href="sms:6162042664">616-204-2664</a></td>
                        <td><a href="mailto:steve.swett@comcast.net">steve.swett@comcast.net</a></td>
                    </tr>
                    <tr>
                        <td>Sample Peggy</td>
                        <td><a href="sms:6168086857">616-808-6857</a></td>
                        <td><a href="mailto:swetts@comcast.net">swetts@comcast.net</a></td>
                    </tr>
                --%>
            </table>
            <% } %>

        </div>

    </div>


    <div id="copyPasteManualSection" class="copyPasteSection">
        <h2>The rest (below) is for manual copy/paste</h2>
    </div>



    <div id="copyPastePhoneSection" class="copyPasteSection">
        <%-- <h2>Phone Numbers</h2> --%>

        <div id="copyPastePhoneNumbers">
            replaced_by_javascript
        </div>
    </div>

    <div id="copyPasteEmailSection" class="copyPasteSection">
        <%-- <h2>Email Addresses</h2> --%>

        <div id="copyPasteEmails">
            replaced_by_javascript
        </div>
    </div>


</div>

<div id="hiddenAjaxResponse">

</div>

</form>

</body>

</html>