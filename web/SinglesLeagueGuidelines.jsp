<%@ page import="other.SessionHelper" %>
<%@ page import="utils.DateUtils" %>
<%@ page import="java.util.Calendar" %>
<%
    boolean isMobile = SessionHelper.isMobileBrowser(request);
%>
<!DOCTYPE html>
<html>

<head>
    <title>Singles League Guidelines</title>

    <% if (isMobile) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <% if (isMobile) { %>
    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <% } else { %>
    <link href="css/Admin.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <% } %>

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="guidelinePageContainer">

    <div id="guidelineOuterBox">   <%-- was menuOuterBox --%>

        <div id="guidelineInnerBox">   <%-- was menuInnerBox --%>

            <div id="guidelineItemsContainer">   <%-- was menuLinksContainer --%>
                <ul>
                    <li>
                        <div class="guidelineHdr">
                            Match Location and Time
                        </div>
                        <div class="guidelineBody">
                            There is no notion of "home" or "away". Players contact each other and agree upon location,
                            which could be a neutral place between them.  Players also agree upon a day/time during the
                            week that works well.
                        </div>
                    </li>

                    <li>
                        <div class="guidelineHdr">
                            Match Format
                        </div>
                        <div class="guidelineBody">
                            Play best of 3 sets.  Players need to agree before start of match whether third set will
                            be full or a 10 point tiebreaker.
                        </div>
                    </li>

                    <li>
                        <div class="guidelineHdr">
                            New Balls
                        </div>
                        <div class="guidelineBody">
                            Both players should bring a new can of balls.  Only open one can.  Winner of the match
                            takes home the unopened can of balls.
                        </div>
                    </li>

                    <li>
                        <div class="guidelineHdr">
                            Reporting Scores
                        </div>
                        <div class="guidelineBody">
                            The winning player should verify the scores with his opponent and text them to Steve
                            immediately following match.  Example: "I won 6-4, 5-7, 6-3". If borrowing somebody
                            else's phone, indicate name of winner.
                        </div>
                    </li>

                    <li>
                        <div class="guidelineHdr">
                            Making up a Missed Match
                        </div>
                        <div class="guidelineBody">
                            If you are unable to play your week's scheduled match, make arrangements with your
                            opponent to make it up ASAP. When reporting scores, indicate for which week it is a
                            makeup.
                        </div>
                    </li>

                </ul>
            </div>
        </div>

        <div id="timeDiv">
            <%= DateUtils.getDateFormattedForColumnHeader(Calendar.getInstance()) %>
        </div>

    </div>

</div>

</body>

</html>