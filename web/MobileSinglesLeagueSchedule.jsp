<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="beans.MatchBean" %>
<%@ page import="dao.Matches" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="utils.DateUtils" %>
<%@ page import="utils.DateRangeLong" %>
<%
    String div = request.getParameter("div");

    if (div == null)
    {
        div = "4.0";
    }

%>
<!DOCTYPE html>
<html>

<head>
    <title>Singles League Schedule/Results</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />


    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="mobileViewPageContainer">

<div class="slScheduleSection">

    <h3><%= div %> Div Schedule & Results</h3>

    <div id="slsJumpTo">
        <a href="javascript:document.getElementById('slsCurrentWeek').scrollIntoView(true);">Jump to current week</a>
    </div>

    <div id="slsNotes">
        Winner in bold<br/>
        Touch name for contact info
    </div>

    <%
        final DateRangeLong dateRange = DateUtils.getCurrentWeekRange();
        final long currentWeekStartTime = dateRange.getStartDate();
        final long currentWeekEndTime = dateRange.getEndDate();

        int prevWeekNo = -1;

        ArrayList<MatchBean> matches = div.length() > 0 ?
                Matches.createInstance(div).getRecords() :
                Matches.createInstance().getRecords();

        for (MatchBean match : matches)
        {

            PlayerBean player1 = null;

            if (match.getPlayer1Id() > 0)
            {
                player1 = Players.createInstance(match.getPlayer1Id()).getRecords().get(0);
            }

            PlayerBean player2 = null;

            if (match.getPlayer2Id() > 0)
            {
                player2 = Players.createInstance(match.getPlayer2Id()).getRecords().get(0);
            }

            final String player1AnchorClass = player1 != null && match.getPlayer1Id() == match.getWinnerId() ?
                    "class=\"slsWinningPlayer\"" : "";

            final String player2AnchorClass = player2 != null && match.getPlayer2Id() == match.getWinnerId() ?
                    "class=\"slsWinningPlayer\"" : "";

            final String formattedDate = match.getMatchDateFormattedForColumnHeader();

            boolean needWeekHeading = prevWeekNo == -1 || match.getWeekNo() != prevWeekNo;
            prevWeekNo = match.getWeekNo();

            if (needWeekHeading) {
                final long matchDateLong = match.getMatchDate().getTime().getTime();

                final boolean isCurrentWeek = matchDateLong >= currentWeekStartTime &&
                        matchDateLong <= currentWeekEndTime;

                final String currentWeekId = isCurrentWeek ? "id=\"slsCurrentWeek\"" : "";

    %>
    <div class="slsWeekHeading" <%= currentWeekId %> >
        Week: <%= match.getWeekNo() %> Begins: <%= formattedDate %>
    </div>
    <% } %>

    <div class="slsMatch">

        <div class="slsMatchNames">

            <div class="slsMatchCol1">
                <% if (match.hasCustomLabel()) { %>
                <%= match.getCustomLabel() %>
                <% } else { %>
                M<%= match.getMatchNo() %>:
                <% } %>
            </div>

            <div class="slsMatchCol2">

                <% if (player1 == null) { %>
                <%= match.getPlayer1Note() %>
                <% } else { %>
                <a <%= player1AnchorClass %> href="MobileSinglesLeaguePlayerCard.page?div=<%= div %>&playerId=<%= player1.getId() %>">
                    <%= player1.getName() %></a>
                <% } %>

                &nbsp;v&nbsp;

                <% if (player2 == null) { %>
                <%= match.getPlayer2Note() %>
                <% } else { %>
                <a <%= player2AnchorClass %> href="MobileSinglesLeaguePlayerCard.page?div=<%= div %>&playerId=<%= player2.getId() %>">
                    <%= player2.getName() %></a>
                <% } %>

                <%= match.isPlayoff() ? "playoff" : "" %>

            </div>

            <div class="slsMatchCol3"></div>

        </div>

        <% if (match.hasMatchBeenScored()) { %>
        <div class="slsMatchScores">

            <div class="slsMatchCol1">
                &nbsp;
            </div>

            <div class="slsMatchCol2">
                <%= match.getScoresFromWinnerPerspectiveAsString() %>
            </div>

            <div class="slsMatchCol3"></div>
        </div>
        <% } %>

        <% if (match.hasNote()) { %>
        <div class="slsMatchNote">

            <div class="slsMatchCol1">
                &nbsp;
            </div>

            <div class="slsMatchCol2">
                <%= match.getNote() %>
            </div>

            <div class="slsMatchCol3"></div>
        </div>
        <% } %>

    </div>

    <% } %>

</div>


</div>

</body>

</html>