<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="beans.GroupsBean" %>
<%@ page import="dao.Groups" %>
<%
    // Group ID must be passed as query string parm
    String groupIdStr = request.getParameter("groupId");
    int groupId = Integer.parseInt(groupIdStr);
    GroupsBean group = Groups.createInstance(groupId).getRecords().get(0);

    ArrayList<PlayerBean> players = Players.createInstance(false, groupId, true).getRecords();

    // Player ID can be passed as query string parm to show contact info for one player only
    String playerIdStr = request.getParameter("playerId");
    int playerId = 0;

    if (playerIdStr != null)
    {
        playerId = Integer.parseInt(playerIdStr);
    }

    final boolean isMobile = SessionHelper.isMobileBrowser(request);

%>
<!DOCTYPE html>
<html>

<head>
    <title>Group Contacts</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <% if (playerId != 0) { %>
    <script>

        $(document).ready(function() {
            document.getElementById('scrollToPlayer').scrollIntoView(true);
        })

    </script>
    <% } %>

    <% if (isMobile) { %>
    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <% } else { %>
    <link href="css/Desktop.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <% } %>

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="gcPageContainer">

<div class="gcContactsSection">

    <h3>Contacts for <%= group.getName() %></h3>

    <div id="gcContacts">

        <%
            int count = 0;

            for (PlayerBean player : players)
            {
                count++;
                String scrollToPlayerId = "";

                if (playerId != 0 && playerId == player.getId())
                {
                    scrollToPlayerId = "id=\"scrollToPlayer\"";
                }
        %>

        <ul <%= scrollToPlayerId %>>
            <li><%= player.getName() %></li>

            <% if (player.getPhone().length() > 0) { %>
            <li><a href="tel:<%= player.getPhone() %>">call <%= player.getFormattedPhone() %></a></li>
            <li><a href="sms:<%= player.getPhone() %>">text <%= player.getFormattedPhone() %></a></li>
            <% } %>

            <% if (player.getEmail().length() > 0) { %>
            <li><a href="mailto:<%= player.getEmail() %>"><%= player.getEmail() %></a></li>
            <% } %>
        </ul>

        <% if (!isMobile && count == 4) {
            count = 0;
        %>
        <div class="clearer"></div>
        <% } %>

        <% } %>

        <% if (!isMobile) { %>
        <div class="clearer"></div>
        <% } %>

    </div>

</div>


</div>

</body>

</html>