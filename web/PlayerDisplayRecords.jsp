<%@ page import="crud.beans.*" %>
<%@ page import="other.SessionHelper" %>
<%-- <%@ page import="timechannels.webserviceclient.TimeChannels" %> --%>

<%
    /*
    * This JSP generated by POJO Jenerator on 2013-11-16 05:36 AM
    * Pojo Jenerator is available at:  www.timechannels.com
    */
%>

<!DOCTYPE html>
<html>

<head>
    <title>Display and Edit Players</title>

    <link href="css/Crud.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <link href="js/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">



    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />
    
    <script type="text/javascript" src="js/xmlhttp.js<%= SessionHelper.getVersionQueryString() %>"></script>
    <script type="text/javascript" src="js/PlayerDisplayRecords.js<%= SessionHelper.getVersionQueryString() %>"></script>


</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="editPlayerTableContainer">

<span id="playerBeanTable">

<jsp:include page='PlayerDisplayRecordsTable.jsp' flush='true'/>

</span>

    <div class="footerLinkContainer">
        <a href="AdminMenu.page">Admin Menu</a>
    </div>

</div>

</body>

</html>