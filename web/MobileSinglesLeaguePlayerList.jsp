<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="other.RatingLevelType" %>
<%@ page import="dao.CurrentSessions" %>
<%@ page import="beans.CurrentSessionsBean" %>
<%
    String div = request.getParameter("div");

    if (div == null)
    {
        div = "4.0";
    }

    CurrentSessionsBean sessBean = CurrentSessions.
            createInstance(SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID).getRecords().get(0);

    ArrayList<PlayerBean> players =
            Players.createInstance(false,
                    div.equals("3.5") ? RatingLevelType.LEVEL_3_5 : RatingLevelType.LEVEL_4_0,
                    div.equals("3.5") ? sessBean.getGroupShortName3_5() :
                            sessBean.getGroupShortName4_0()).getRecords();


%>
<!DOCTYPE html>
<html>

<head>
    <title>Singles League Player Cards</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />


    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="mobileViewPageContainer">

<div class="plListContainer">

    <h3><%= div %> Division Player Cards</h3>


        <%
            for (PlayerBean player : players)
            {
        %>

        <a href="MobileSinglesLeaguePlayerCard.page?playerId=<%= player.getId() %>&div=<%= div %>"><%= player.getName() %></a>

        <% } %>

</div>

</div>

</body>

</html>