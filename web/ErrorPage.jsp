<%@ page import="other.SessionHelper" %>
<%
    Exception e;

    if (request != null)
    {
        e = (Exception) request.getAttribute(other.SessionHelper.ERROR_PAGE_EXCEPTION_ATTR_KEY);
    }
    else if (session != null)
    {
        e = (Exception) session.getAttribute(other.SessionHelper.ERROR_PAGE_EXCEPTION_ATTR_KEY);
    }
    else
    {
        e = null;
    }

    String msg = e == null ? null : e.getMessage();
%>

<!DOCTYPE html>
<html>

<head>
    <title>Tennis App Error Page</title>
</head>

<body>

<div>
    <p>
        Sorry.  Something went wrong (perhaps a session timeout).  There might be some details below.
    </p>

    <p>
        Please try going to the <a href="/Index.html">front page</a> again.
    </p>

    <% if (msg != null) { %>
    <p>
        Message: <%= msg %>
    </p>
    <% } %>

    <% if (e != null) { %>
    <p>
        <%
            StackTraceElement[] elems = e.getStackTrace();

            for (StackTraceElement elem : elems) {
        %>
        <%= elem %><br/>
        <% } %>
    </p>
    <% } %>

</div>

</body>

</html>