<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="beans.MatchBean" %>
<%@ page import="dao.Matches" %>
<%@ page import="utils.DateUtils" %>
<%@ page import="utils.DateRangeLong" %>
<%
    String div = request.getParameter("div");

    if (div == null)
    {
        div = "4.0";
    }

%>
<!DOCTYPE html>
<html>

<head>
    <title>Singles League Schedule/Results</title>

    <link href="css/Singles.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="slPageContainer">

<div class="slScheduleSection">

    <h3 class="slSubTitle">
        <div class="slSubTitlePart1">
            <%= div %> Div Schedule/Results
            &nbsp;&nbsp;&nbsp;
            <a class="slSubTitleLink"
               href="javascript:document.getElementById('slsCurrentWeek').scrollIntoView(true);">Jump to current week</a>
        </div>
        <div class="slSubTitlePart2">
            Winner in bold. Click name for contact info.
        </div>
    </h3>

    <div id="slSubTitleClear"></div>

    <div id="slScheduleTableContainer">

    <table class="slDataTable">
        <thead>
        <tr>
            <th class="slWeekNoCol">Week#</th>
            <th class="slMatchDateCol">Begins</th>
            <th class="slMatchLabelCol">Match#</th>
            <th class="slPlayoffCol">Playoff</th>
            <th class="slPlayersCol">Player 1</th>
            <th class="slPlayersCol">Player 2</th>
            <th class="slResultsCol">Results</th>
            <th class="slNotesCol">Notes</th>
        </tr>
        </thead>

        <tbody>

        <%
            boolean even = true;
            int prevWeekNo = -1;

            final DateRangeLong dateRange = DateUtils.getCurrentWeekRange();
            final long currentWeekStartTime = dateRange.getStartDate();
            final long currentWeekEndTime = dateRange.getEndDate();


            ArrayList<MatchBean> matches = div.length() > 0 ?
                    Matches.createInstance(div).getRecords() :
                    Matches.createInstance().getRecords();

            for (MatchBean match : matches)
            {
                even = !even;
                String rowClass = even ? "class=\"even\"" : "";

                PlayerBean player1 = null;

                if (match.getPlayer1Id() > 0)
                {
                    player1 = Players.createInstance(match.getPlayer1Id()).getRecords().get(0);
                }

                PlayerBean player2 = null;

                if (match.getPlayer2Id() > 0)
                {
                    player2 = Players.createInstance(match.getPlayer2Id()).getRecords().get(0);
                }

                final String player1AnchorClass = player1 != null && match.getPlayer1Id() == match.getWinnerId() ?
                        "class=\"slsWinningPlayer\"" : "";

                final String player2AnchorClass = player2 != null && match.getPlayer2Id() == match.getWinnerId() ?
                        "class=\"slsWinningPlayer\"" : "";

                String currentWeekId = "";

                if (match.getWeekNo() != prevWeekNo)
                {
                    final long matchDateLong = match.getMatchDate().getTime().getTime();

                    if (matchDateLong >= currentWeekStartTime && matchDateLong <= currentWeekEndTime)
                    {
                        currentWeekId = "id=\"slsCurrentWeek\"";
                    }
                }

                boolean needsBlankRow = prevWeekNo != -1 && match.getWeekNo() != prevWeekNo;
                prevWeekNo = match.getWeekNo();

                String matchLabel = match.hasCustomLabel() ? match.getCustomLabel() : "M" + match.getMatchNo() + ":";
        %>

        <% if (needsBlankRow) { %>
            <tr <%= rowClass %>>
                <td colspan="8">
                    &nbsp;
                </td>
            </tr>
        <%
                even = !even;
                rowClass = even ? "class=\"even\"" : "";
            }
        %>

        <tr <%= rowClass %> <%= currentWeekId %> >
            <td class="slWeekNoCol"><%= match.getWeekNo() %></td>
            <td class="slMatchDateCol"><%= match.getMatchDateFormattedForColumnHeader() %></td>
            <td class="slMatchLabelCol"><%= matchLabel %></td>
            <td class="slPlayoffCol"><%= match.isPlayoff() ? "Yes" : "" %></td>

            <td class="slPlayersCol">
                <% if (player1 != null) { %>
                <a <%= player1AnchorClass %> href="SinglesLeaguePlayerCard.page?div=<%= div %>&playerId=<%= player1.getId() %>">
                    <%= player1.getName() %></a>
                <% } %>

                <% if (player1 != null && match.hasPlayer1Note()) { %>
                <br/>
                <% } %>

                <% if (match.hasPlayer1Note()) { %>
                <%= match.getPlayer1Note() %>
                <% } %>
            </td>

            <td class="slPlayersCol">
                <% if (player2 != null) { %>
                <a <%= player2AnchorClass %> href="SinglesLeaguePlayerCard.page?div=<%= div %>&playerId=<%= player2.getId() %>">
                    <%= player2.getName() %></a>
                <% } %>

                <% if (player2 != null && match.hasPlayer2Note()) { %>
                <br/>
                <% } %>

                <% if (match.hasPlayer2Note()) { %>
                <%= match.getPlayer2Note() %>
                <% } %>
            </td>

            <td class="slResultsCol">
                <% if (match.hasMatchBeenScored()) { %>
                <%= match.getScoresFromWinnerPerspectiveAsString() %>
                <% } %>
            </td>

            <td class="slNotesCol">
                <% if (match.hasNote()) { %>
                <%= match.getNote() %>
                <% } %>
            </td>
        </tr>

        <% } %>

        </tbody>
    </table>


    </div>


</div>


</div>

</body>

</html>