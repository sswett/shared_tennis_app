<%@ page import="other.SessionHelper" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="beans.StandingsCalcBean" %>
<%@ page import="dao.StandingsCalc" %>
<%@ page import="other.MatchType" %>
<%
    String div = request.getParameter("div");

    if (div == null)
    {
        div = "4.0";
    }

    String matchTypeParm = request.getParameter("matchType");
    MatchType matchType = MatchType.ALL;   // default

    if (matchTypeParm != null)
    {
        try
        {
            matchType = MatchType.valueOf(matchTypeParm);
        }
        catch (Exception e)
        {
            // eat
        }
    }

%>
<!DOCTYPE html>
<html>

<head>
    <title>Singles League Standings</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />


    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script>

        function slsChangeMatchType(pElem)
        {
            var matchType = pElem.value;
            window.location='MobileSinglesLeagueStandings.page?div=<%= div %>&matchType=' + matchType;
        }

    </script>

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="mobileViewPageContainer">

<div class="slStandingsSection">

    <h3>
        <%= div %> Div Standings

        &nbsp;

        <select id="slsMatchTypeSelect" name="slsMatchTypeSelect" onchange="slsChangeMatchType(this)">
            <%
                for (MatchType type : MatchType.values())
                {
                    final boolean isSelected = type == matchType;
            %>
            <option <%= isSelected ? "selected=\"selected\"" : "" %>
                    value="<%= type.name() %>" >
                <%= type.getLabel() %>
            </option>
            <% }   // for MatchType %>
        </select>

    </h3>

    <div id="slsNotes">
        Rank calculation:<br/>
        Match %, Set %, Game %
    </div>

    <%
        ArrayList<StandingsCalcBean> standingsCalcs = StandingsCalc.createInstance(session.getId(), div, matchType).getRecords();

        for (StandingsCalcBean standingsCalc : standingsCalcs)
        {
 %>

    <div class="slsStanding">

        <div class="slsStandingPlayer">

            <div class="slsStandingCol1">
                # <%= standingsCalc.getPlayerRank() %>
            </div>

            <div class="slsStandingNameCol">
                <a href="MobileSinglesLeaguePlayerCard.page?div=<%= div %>&playerId=<%= standingsCalc.getPlayerId() %>">
                        <%= standingsCalc.getPlayerName() %>
                </a>
            </div>

            <div class="slsStandingCol5"></div>

        </div>

        <div class="slsStandingSubheading">

            <div class="slsStandingCol1">
                &nbsp;
            </div>

            <div class="slsStandingCol2">
                W
            </div>

            <div class="slsStandingCol3">
                L
            </div>

            <div class="slsStandingCol4">
                %
            </div>

            <div class="slsStandingCol5"></div>

        </div>

        <div class="slsStandingMatches">

            <div class="slsStandingCol1">
                Match:
            </div>

            <div class="slsStandingCol2">
                <%= standingsCalc.getMatchesWon() %>
            </div>

            <div class="slsStandingCol3">
                <%= standingsCalc.getMatchesLost() %>
            </div>

            <div class="slsStandingCol4">
                <%= String.format("%.3f", standingsCalc.getMatchesWonPct()) %>
            </div>

            <div class="slsStandingCol5"></div>

        </div>

        <div class="slsStandingSets">

            <div class="slsStandingCol1">
                Set:
            </div>

            <div class="slsStandingCol2">
                <%= standingsCalc.getSetsWon() %>
            </div>

            <div class="slsStandingCol3">
                <%= standingsCalc.getSetsLost() %>
            </div>

            <div class="slsStandingCol4">
                <%= String.format("%.3f", standingsCalc.getSetsWonPct()) %>
            </div>

            <div class="slsStandingCol5"></div>

        </div>

        <div class="slsStandingGames">

            <div class="slsStandingCol1">
                Game:
            </div>

            <div class="slsStandingCol2">
                <%= String.format("%.2f", standingsCalc.getGamesWon()) %>
            </div>

            <div class="slsStandingCol3">
                <%= String.format("%.2f", standingsCalc.getGamesLost()) %>
            </div>

            <div class="slsStandingCol4">
                <%= String.format("%.3f", standingsCalc.getGamesWonPct()) %>
            </div>

            <div class="slsStandingCol5"></div>

        </div>


    </div>

    <% } %>

</div>


</div>

</body>

</html>