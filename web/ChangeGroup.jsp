<%@ page import="beans.GroupsBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Groups" %>

<%
    int defaultGroupId = (Integer) session.getAttribute(SessionHelper.DEFAULT_GROUP_SESS_ATTR_KEY);
    Groups groups = Groups.createInstance();
    ArrayList<GroupsBean> groupRecords = groups.getRecords();
    String redirectToPage = request.getParameter("redirectToPage");
%>

<div id="changeGroupSection">

    <form name="changeGroupForm" action="ChangeGroup.page" method="POST">
        <input type="hidden" name="changeGroup" value="" />
        <input type="hidden" name="redirectToPage" value="<%= redirectToPage %>" />

        <label for="groupSelect">Group/Team:</label>

        <% if (SessionHelper.isMobileBrowser(request)) { %>
        <br/>
        <% } %>

        <select id="groupSelect" name="groupSelect" onchange="taChangeGroup()">
            <%
                for (GroupsBean group : groupRecords)
                {
                    final boolean isSelected = group.getId() == defaultGroupId;
            %>
            <option <%= isSelected ? "selected=\"selected\"" : "" %>
                    value="<%= group.getId() %>"><%= group.getName() %></option>
            <% }   // for group %>
        </select>

    </form>
</div>
