<%@ page import="other.SessionHelper" %>
<%
%>
<!DOCTYPE html>
<html>

<head>
    <title>Tennis Project Desktop Slideshow</title>

    <script src="js/jquery-1.10.2.min.js"></script>

    <script src="js/basic-jquery-slider-master/js/bjqs-1.3.min.js"></script>

    <link href="js/basic-jquery-slider-master/bjqs.css" rel="stylesheet">
    <link href="css/Slideshow.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <link href="css/Singles.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script>

        <%-- when responsive, the dimensions are the maximums --%>

        <%--
                NOTE: I didn't want screen shot images to shrink responsively, nor did I want the
                      captions or controls to overlay the image.  So I made the slideshow
                      container bigger than the images and turned off the responsive and
                      showcontrols settings.
        --%>

        jQuery(document).ready(function($) {
            $('#my-slideshow').bjqs({
                height : 610,
                width : 640,
                responsive : false,
                hoverpause : true, // pause the slider on hover
                animspeed : 13000, // the delay between each slide
                randomstart : false,
                showcontrols : false
            });
        });

    </script>

</head>

<body>

<div id="container">

<div id="my-slideshow">
    <ul class="bjqs">
        <li>
            <img src="slideshows/TennisProject/Desktop/Spreadsheet.png<%= SessionHelper.getVersionQueryString() %>" 
            width="640" height="540"
                    title="Players use this page to indicate availability for practices, matches,
                    tournaments and social events. Simple to use: just pick options and click Save.
                    'Crosshairs' help locate spreadsheet cells."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/CommunicationsPage.png<%= SessionHelper.getVersionQueryString() %>" 
            width="640" height="540"
                    title="Captains use this page to send text and/or e-mail messages to players.  This might be
                    a team-wide communique or a reminder to those who signed up for an event. All contact info
                    is stored centrally."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/DisplayEditGroups.png<%= SessionHelper.getVersionQueryString() %>" 
            width="640" height="540"
                    title="This page shows a typical CRUD page for maintaining records -- in this case groups/teams."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/EditGroupMembers.png<%= SessionHelper.getVersionQueryString() %>" 
            width="640" height="540"
                    title="Captains/administrators use this page to add/remove players to/from groups/teams."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/EditSinglesMatchList.png<%= SessionHelper.getVersionQueryString() %>" 
            width="640" height="540"
                    title="An administrator uses this page pre-season to tweak singles league schedules after the schedule
                    records are generated programatically."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/NavMenus.png<%= SessionHelper.getVersionQueryString() %>" 
            width="640" height="540"
                    title="For quick and easy access to the whole site, every page has a navigation menu at the top."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/Slideshow.png<%= SessionHelper.getVersionQueryString() %>" 
            width="640" height="540"
                    title="Travel team and singles league slideshows bring out the human element."/>
        </li>

        <%--
        <li>
            <img src="slideshows/TennisProject/Desktop/Spreadsheet640x479.png"
                    title="Players use this page to indicate their availability for practices, matches,
                    tournaments and social events. It is simple for them: just pick options and click Save when
                    that button appears. 'Crosshairs' help them locate spreadsheet cells."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/CommunicationsPage640x549.png"
                    title="Captains use this page to send text and/or e-mail messages to players.  This might be
                    a team-wide communique or a reminder to those who signed up for an event."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/AdminMenu640x575.png"
                    title="This page shows the options available to captains/administrators."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/SinglesLeagueMenu640x533.png"
                    title="This page shows the options available to players who participate in the singles league."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/DisplayEditGroups640x548.png"
                    title="This page shows a typical CRUD page for maintaining records -- in this case groups/teams."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/EditGroupMembers640x514.png"
                    title="Captains/administrators use this page to add/remove players to/from groups/teams."/>
        </li>
        <li>
            <img src="slideshows/TennisProject/Desktop/EditSinglesMatchList640x476.png"
                    title="An administrator uses this page pre-season to tweak singles league schedules after the schedule
                    records are generated programatically."/>
        </li>
        --%>
    </ul>
</div>


</div>


</body>

</html>