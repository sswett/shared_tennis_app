<%@ page import="other.RatingLevelType"%>
<%@ page import="other.SessionHelper"%>
<%
	final boolean isMobile = SessionHelper.isMobileBrowser(request);
%>
<!DOCTYPE html>
<html>

<head>
<title>Test Player REST Service</title>

<% if (isMobile) { %>
<meta name="viewport" content="width=device-width" />
<% } %>

<script src="js/jquery-1.10.2.min.js"></script>

<% if (isMobile) { %>
<link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>"
	rel="stylesheet">
<% } else { %>
<link href="css/Desktop.css<%= SessionHelper.getVersionQueryString() %>"
	rel="stylesheet">
<% } %>

	<script>

        $(document).ready(function() {
        	$("#ratingSelect").change(function() {
        		tpsRatingChanged();
        	});
        });
        
        
        function tpsRatingChanged()
        {
        	var ratingOptionValue = $("#ratingSelect").val();
        	
        	if (ratingOptionValue == "na")
            {
        		$("#playerListSection").html("");
            }
        	else
            {
        	    $.getJSON('/services_rest/player/rating/' + ratingOptionValue, function(players)
        	    		{
			    	        if (players == null)
			    	        {
			            		$("#playerListSection").html("");
			    	        }
			    	        else
        	    	        {
								var playerListHtml = "";
								
        	    	        	for (var i = 0; i < players.length; i++)
							    {
									playerListHtml += players[i].name + " - " + players[i].ratingLevel + "<br/>";									
							    }
        	    	        	
        	            		$("#playerListSection").html(playerListHtml);
        	    	        }
        	    	    });
            }
        }

    </script>


</head>

<body>

	<div id="tpsPageContainer">

		<p>When you choose or change the rating level below, the Player
			List will be updated without reloading the entire page. jQuery is
			used to make a GET request to a REST service, which returns a JSON
			response.</p>

		<form name="testPlayerServiceForm">

			<label for="ratingSelect">Rating Level:</label>

			<% if (SessionHelper.isMobileBrowser(request)) { %>
			<br />
			<% } %>

			<select id="ratingSelect" name="ratingSelect">

				<option value="na"></option>
				<%-- n/a (nothing selected yet) --%>

				<%  for (RatingLevelType type : RatingLevelType.values()) { %>
				<option value="<%= type.name() %>"><%= type.getRatingLevel() %></option>
				<% } %>
			</select>

		</form>

		<p>Player List</p>

		<div id="playerListSection"></div>

	</div>
	
</body>
	
</html>
	