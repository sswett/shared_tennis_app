<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="other.RatingLevelType" %>
<%@ page import="dao.CurrentSessions" %>
<%@ page import="beans.CurrentSessionsBean" %>
<%@ page import="dao.StandingsCalc" %>
<%@ page import="beans.StandingsCalcBean" %>
<%@ page import="beans.MatchBean" %>
<%@ page import="dao.Matches" %>
<%@ page import="other.MatchType" %>
<%
    ArrayList<PlayerBean> players = null;

    // Player ID can be passed as query string parm to show player card for one player only (typical use case)
    String playerIdStr = request.getParameter("playerId");

    if (playerIdStr != null)
    {
        int playerId = Integer.parseInt(playerIdStr);

        if (playerId != 0)
        {
            players = Players.createInstance(playerId).getRecords();
        }
    }

    String div = request.getParameter("div");

    if (div == null)
    {
        div = "4.0";
    }

    // If not showing player card for one player only, show everybody in division
    if (players == null)
    {
        CurrentSessionsBean sessBean = CurrentSessions.
                createInstance(SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID).getRecords().get(0);

        players =
                Players.createInstance(false,
                        div.equals("3.5") ? RatingLevelType.LEVEL_3_5 : RatingLevelType.LEVEL_4_0,
                        div.equals("3.5") ? sessBean.getGroupShortName3_5() :
                                sessBean.getGroupShortName4_0()).getRecords();
    }

    ArrayList<StandingsCalcBean> standingsCalcsRegSeason =
            StandingsCalc.createInstance(session.getId(), div, MatchType.REGULAR_SEASON).getRecords();

    ArrayList<StandingsCalcBean> standingsCalcsPlayoffs =
            StandingsCalc.createInstance(session.getId(), div, MatchType.PLAYOFF).getRecords();

    for (PlayerBean player : players)
    {
        // Get regular season calc bean for player:
        StandingsCalcBean playerStandingsRegSeason = standingsCalcsRegSeason.get(0);   // simply a default
        int standingsCalcsRegSeasonSize = standingsCalcsRegSeason.size();

        for (int x = 0; x < standingsCalcsRegSeasonSize; x++)
        {
            if (player.getId() == standingsCalcsRegSeason.get(x).getPlayerId())
            {
                playerStandingsRegSeason = standingsCalcsRegSeason.get(x);
                break;
            }
        }

        // Get playoffs calc bean for player:
        StandingsCalcBean playerStandingsPlayoffs = standingsCalcsPlayoffs.get(0);   // simply a default
        int standingsCalcsPlayoffsSize = standingsCalcsPlayoffs.size();

        for (int x = 0; x < standingsCalcsPlayoffsSize; x++)
        {
            if (player.getId() == standingsCalcsPlayoffs.get(x).getPlayerId())
            {
                playerStandingsPlayoffs = standingsCalcsPlayoffs.get(x);
                break;
            }
        }

        ArrayList<MatchBean> matches = Matches.createInstance(div, player.getId()).getRecords();

%>
<!DOCTYPE html>
<html>

<head>
    <title>Singles League Player Card</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

</head>

<body>

    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="mobileViewPageContainer">

    <div class="pcCard">

        <h3>Player Card</h3>

        <%= player.getName() %>
        &nbsp;
        Div: <%= div %>

        <br/>

        Season Rank: <%= playerStandingsRegSeason.getPlayerRank() %>/<%= playerStandingsRegSeason.getPlayerCount() %>

        <br/>

        Season Record: <%= playerStandingsRegSeason.getMatchesWon() %>-<%= playerStandingsRegSeason.getMatchesLost() %>
        (<%= String.format("%.3f", playerStandingsRegSeason.getMatchesWonPct()) %>%)

        <br/>

        Playoffs Rank: <%= playerStandingsPlayoffs.getPlayerRank() %>/<%= playerStandingsPlayoffs.getPlayerCount() %>

        <br/>

        Playoffs Record: <%= playerStandingsPlayoffs.getMatchesWon() %>-<%= playerStandingsPlayoffs.getMatchesLost() %>
        (<%= String.format("%.3f", playerStandingsPlayoffs.getMatchesWonPct()) %>%)

        <div class="pcContactInfo">
            <a href="tel:<%= player.getPhone() %>">call <%= player.getFormattedPhone() %></a>
            <a href="sms:<%= player.getPhone() %>">text <%= player.getFormattedPhone() %></a>
            <a href="mailto:<%= player.getEmail() %>"><%= player.getEmail() %></a>
        </div>


        <h4>Unplayed Matches</h4>

        <%
            int count = 0;

            for (MatchBean match : matches)
            {
                if (!match.hasMatchBeenScored())
                {
                    final int opponentId = match.getOpponentId(player.getId());
                    final String opponentInfo;

                    if (opponentId == 0)
                    {
                        opponentInfo = match.getOpponentPlayerNote(player.getId());
                    }
                    else
                    {
                        final PlayerBean opponent = Players.createInstance(opponentId).getRecords().get(0);

                        opponentInfo = "<a href=\"MobileSinglesLeaguePlayerCard.page?playerId=" + opponentId + "&div=" +
                                div + "\">" + opponent.getName() + "</a>";
                    }

                    final String matchLabel = match.hasCustomLabel() ? match.getCustomLabel() :
                            "M" + match.getMatchNo() + ":";

                    count++;

        %>

        <div class="pcUnplayed">
            W<%= match.getWeekNo() %> <%= matchLabel %> <%= match.getMatchDateFormattedForColumnHeader() %>
            <%= match.isPlayoff() ? "playoff" : "" %>
            <%= match.hasNote() ? "(" + match.getNote() + ")" : "" %>

            <br/>

            vs. <%= opponentInfo %>
        </div>

        <%
                }
            }

            if (count == 0)
            {
        %>

        <div class="pcUnplayed">
            (none)
        </div>

        <%

            }
        %>


        <h4>Results</h4>

        <%
            count = 0;

            for (MatchBean match : matches)
            {
                if (match.hasMatchBeenScored())
                {
                    final int opponentId = match.getOpponentId(player.getId());
                    final PlayerBean opponent = Players.createInstance(opponentId).getRecords().get(0);

                    final String matchLabel = match.hasCustomLabel() ? match.getCustomLabel() :
                            "M" + match.getMatchNo() + ":";

                    count++;

        %>

        <div class="pcResult">
            W<%= match.getWeekNo() %> <%= matchLabel %> <%= match.getMatchDateFormattedForColumnHeader() %>
            <%= match.isPlayoff() ? "playoff" : "" %>
            <%= match.hasNote() ? "(" + match.getNote() + ")" : "" %>

            <br/>

            vs. <a href="MobileSinglesLeaguePlayerCard.page?playerId=<%= opponentId %>&div=<%= div %>"><%= opponent.getName() %></a>

            <br/>

            <%= match.isWinner(player.getId()) ? "Won" : "Lost" %>
            <%= match.isWinner(player.getId()) ? match.getScoresFromWinnerPerspectiveAsString() :
                    match.getScoresFromLoserPerspectiveAsString() %>
        </div>

        <%
                }
            }

            if (count == 0)
            {
        %>

        <div class="pcResult">
            (none)
        </div>

        <%

            }
        %>


    </div>

        <% } %>



</div>

</body>

</html>