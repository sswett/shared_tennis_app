<%@ page import="ui.TestGetPlayers" %>

<%-- Test with this url: http://localhost:8080/TestGetPlayers.jsp --%>

<!doctype html>
<html>
<head>
<title>Test SMS Anchor</title>
    <meta name="viewport" content="width=device-width" />
</head>
<body>

<%-- comma and semi-colon don't seem to work for iPhone --%>
<a href="sms:6162042664;6168086857">Send Text to Two Numbers</a>

<br/>
<br/>

<%-- the following still pauses in the phone's text application --%>
<a href="sms:6162042664;body=sometext">Send Text and Message</a>

</body>
</html>