<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="other.SessionHelper" %>
<%@ page import="java.util.ArrayList" %>

<%
    int defaultGroupId = (Integer) session.getAttribute(SessionHelper.DEFAULT_GROUP_SESS_ATTR_KEY);
    ArrayList<PlayerBean> members = Players.createInstance(false, defaultGroupId, true).getRecords();
    ArrayList<PlayerBean> nonMembers = Players.createInstance(false, defaultGroupId, false).getRecords();
    final boolean hasAdminClearance = SessionHelper.hasPassedAdminSecurityClearance(session, request);
%>

<!DOCTYPE html>
<html>

<head>
    <title>Edit Group Members</title>

    <link href="css/Admin.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <script type="text/javascript" src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>

</head>

<body>

    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="pageContainer">

    <jsp:include page="ChangeGroup.jsp" flush="true">
        <jsp:param name="redirectToPage" value="EditGroupMembers.page" />
    </jsp:include>

    <form name="groupMembersForm" action="EditGroupMembers.page" method="POST">

        <div id="membersSection" class="listSection">
            <h2>
                Members (<%= members.size() %>):
            </h2>

            <table class="listTable">
                <thead>
                <tr>
                    <th class="mbrPlayerCol">Player</th>
                    <th class="ratingLevelCol">Level</th>
                    <th class="mbrTypeCol">Mbr Type</th>
                    <th class="actionCol">Remove</th>
                </tr>
                </thead>

                <tbody>
                <%  boolean even = true;
                    for (PlayerBean player : members)
                    {
                        even = !even;
                %>
                <tr <%= even ? "class=\"even\"" : "" %>>
                    <td class="mbrPlayerCol"><%= player.getName()%></td>
                    <td class="ratingLevelCol"><%= player.getRatingLevel()%></td>
                    <td class="mbrTypeCol"><%= player.getMbrTypeDescr()%></td>
                    <td class="actionCol">
                        <input type="checkbox" name="mem<%= player.getId() %>" value="1" />
                    </td>
                </tr>
                <% } %>
                </tbody>
            </table>

        </div>


        <div id="nonMembersSection" class="listSection">
            <h2>
                Non-members (<%= nonMembers.size() %>):
            </h2>

            <table class="listTable">
                <thead>
                <tr>
                    <th class="mbrPlayerCol">Player</th>
                    <th class="ratingLevelCol">Level</th>
                    <th class="mbrTypeCol">Mbr Type</th>
                    <th class="actionCol">Add</th>
                </tr>
                </thead>

                <tbody>
                <%  even = true;
                    for (PlayerBean player : nonMembers)
                    {
                        even = !even;
                %>
                <tr <%= even ? "class=\"even\"" : "" %>>
                    <td class="mbrPlayerCol"><%= player.getName()%></td>
                    <td class="ratingLevelCol"><%= player.getRatingLevel()%></td>
                    <td class="mbrTypeCol"><%= player.getMbrTypeDescr()%></td>
                    <td class="actionCol">
                        <input type="checkbox" name="non<%= player.getId() %>" value="1" />
                    </td>
                </tr>
                <% } %>
                </tbody>
            </table>

        </div>


        <div id="footerSection">
            <div class="submitButtonContainer">
                <input type="submit" value="Submit" />

                <% if (hasAdminClearance) { %>
				<img class="commButton" src="images/megaphone_24x24.png"
											title="Display communications page"
											onclick="displayCommunicationsPage('')" />
				<% } %>
                
            </div>

            <div class="footerLinkContainer">
                <a href="AdminMenu.page">Admin Menu</a>
            </div>
        </div>



    </form>

</div>

</body>

</html>