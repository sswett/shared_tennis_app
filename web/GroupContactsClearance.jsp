<%@ page import="other.SessionHelper" %>
<%
    boolean isMobile = SessionHelper.isMobileBrowser(request);
%>
<!DOCTYPE html>
<html>

<head>
    <title>Group Contacts Password</title>

    <% if (isMobile) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <script type="text/javascript">

        $(document).ready(function() {
            $("#password").focus();
        })

    </script>

    <% if (isMobile) { %>
    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <% } else { %>
    <link href="css/Admin.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    <% } %>



</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="pageContainer">


    <form name="slClearanceForm" action="GroupContactsClearance.page" method="POST">

        <div id="pwdOuterBox">

            <div id="pwdInnerBox">

                <div id="pwdInputContainer">
                    <label for="password">Group contacts password:</label>
                    <input type="password" name="password" id="password" maxlength="10" />
                </div>

                <div id="pwdRememberContainer">
                    <input type="checkbox" name="remember" id="remember" checked="checked" />
                    <label for="remember">Remember password on this device</label>
                </div>

                <div id="pwdSubmitContainer">
                    <input type="submit" value="Submit" />
                </div>
            </div>

        </div>

    </form>

</div>

</body>

</html>