<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="other.RatingLevelType" %>
<%@ page import="beans.MatchBean" %>
<%@ page import="dao.Matches" %>
<%@ page import="other.MatchIdType" %>
<%@ page import="dao.CurrentSessions" %>
<%@ page import="beans.CurrentSessionsBean" %>
<%

%>
<!DOCTYPE html>
<html>

<head>
    <title>Mobile Score Entry</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <script src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>

    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="mobileViewPageContainer">

    <form name="mobileForm" action="MobileScoreSelectMatch.page" method="POST">
        <input type="hidden" name="action" value="" />

        <h3>Score Entry - Select Match</h3>

        <div class="ssmPlayerChooser">

            <select id="ssmPlayerSelect" name="ssmPlayerSelect" onchange="mpvChangePlayer()">
                <%
                    CurrentSessionsBean sessBean = CurrentSessions.
                            createInstance(SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID).getRecords().get(0);

                    ArrayList<PlayerBean> players = Players.createInstance(
                            false, RatingLevelType.LEVEL_ANY, sessBean.getGroupShortNameAll()).getRecords();

                    final String selectedPlayerIdStr = request.getParameter("playerId");

                    int selectedPlayerId = -1;

                    if (selectedPlayerIdStr != null && selectedPlayerIdStr.length() > 0)
                    {
                        selectedPlayerId = Integer.parseInt(selectedPlayerIdStr);
                    }
                    else if (players.size() > 0)
                    {
                        selectedPlayerId = players.get(0).getId();
                    }

                    for (PlayerBean player : players)
                    {
                        final boolean isSelected = player.getId() == selectedPlayerId;
                %>
                <option <%= isSelected ? "selected=\"selected\"" : "" %>
                        value="<%= player.getId() %>" >
                    <%= player.getName() %>
                </option>
                <% }   // for player %>
            </select>


        </div>

        <% if (selectedPlayerId > 0) { %>

        <div class="ssmMatchSection">

            <%
                ArrayList<MatchBean> matches =
                        Matches.createInstance(MatchIdType.PLAYER, selectedPlayerId).getRecords();

                for (MatchBean match : matches)
                {
                    PlayerBean opponent = null;
                    String opponentName = "unknown";

                    if (match.getPlayer1Id() == selectedPlayerId)
                    {
                        if (match.getPlayer2Id() > 0)
                        {
                            opponent = Players.createInstance(match.getPlayer2Id()).getRecords().get(0);
                            opponentName = opponent.getName();
                        }
                        else
                        {
                            opponentName = match.getPlayer2Note();
                        }
                    }
                    else if (match.getPlayer2Id() == selectedPlayerId)
                    {
                        if (match.getPlayer1Id() > 0)
                        {
                            opponent = Players.createInstance(match.getPlayer1Id()).getRecords().get(0);
                            opponentName = opponent.getName();
                        }
                        else
                        {
                            opponentName = match.getPlayer1Note();
                        }
                    }

                    final String matchLabel = match.hasCustomLabel() ? match.getCustomLabel() :
                            "M" + match.getMatchNo() + ":";


                    final String opponentSpanClass = opponent != null && opponent.getId() == match.getWinnerId() ?
                            "class=\"slsWinningPlayer\"" : "";

            %>

            <div class="ssmMatch">
                <a href="MobileScoreEditMatch.page?id=<%= match.getId() %>">
                    <span <%=opponentSpanClass %>><%= opponentName %></span> W<%= match.getWeekNo() %>
                    <%= matchLabel %> <%= match.getMatchDateFormattedForColumnHeader() %>
                    <%= match.isPlayoff() ? "playoff" : "" %></a>
                <% if (match.hasMatchBeenScored()) { %>
                <br/>
                <%= match.getScoresFromWinnerPerspectiveAsString() %>
                <% } %>
            </div>

            <% } %>

        </div>

        <% } %>

    </form>

</div>

</body>

</html>