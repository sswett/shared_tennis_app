<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Groups" %>
<%@ page import="beans.GroupsBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="beans.EventBean" %>
<%@ page import="dao.Events" %>
<%@ page import="utils.DateUtils" %>
<%@ page import="beans.EventResponseJoinedDataBean" %>
<%@ page import="dao.EventResponses" %>
<%@ page import="other.EventResponseIdType" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="other.PlayerResponse" %>
<%
    final String savedParm = request.getParameter("saved");
    final boolean wasJustSaved = savedParm != null && savedParm.equals("true");
    int yesCount = 0;
    int groupId = SessionHelper.getDefaultGroupId(session);
    GroupsBean group = Groups.createInstance(groupId).getRecords().get(0);

    Calendar startDate = DateUtils.getCalendarAtBeginningOfToday();

    ArrayList<EventBean> eventRecords = Events.createInstance(startDate, groupId).getRecords();

    int selectedEventId = 0;
    boolean showCopyPasteButton = false;
    final boolean hasAdminClearance = SessionHelper.hasPassedAdminSecurityClearance(session, request);
%>

<!DOCTYPE html>
<html>

<head>
    <title>Tennis Mobile - Group View</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <script src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>

    <script type="text/javascript">

        <% if (wasJustSaved) { %>
        $(document).ready(function() {
            $("#changesSaved").css("display", "block");
        });
        <% } %>

    </script>

    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div class="mobileViewPageContainer">

    <form name="mobileForm" action="MobileGroupView.page" method="POST">
        <input type="hidden" name="action" value="" />

        <div id="changesSaved">
            Your changes were saved successfully.
        </div>

        <div id="topMsgDiv" class="msgDiv">
            <input type="button" value="Save" onclick="mgvSave()" /> or
            <input type="button" value="Cancel" onclick="mgvCancel()" /> change(s)
        </div>

        <div id="groupDescr">
            <%= group.getName() %>
        </div>

        <% if (eventRecords.size() == 0) { %>

        <div class="noPlayersOrEventsMsg">
            There are no events for this group/team.
        </div>

        <% } else {

            selectedEventId = SessionHelper.getMobileGroupViewSelectedEvent(session);

            EventBean selectedEvent = null;

            for (EventBean event: eventRecords)
            {
                if (event.getId() == selectedEventId)
                {
                    selectedEvent = event;
                    break;
                }
            }

            if (selectedEvent == null)
            {
                selectedEvent = eventRecords.get(0);
            }

            selectedEventId = selectedEvent.getId();

            ArrayList<EventResponseJoinedDataBean> responseRecords =
                    EventResponses.createInstance(EventResponseIdType.EVENT, selectedEventId, startDate, groupId ).
                            getRecords();

            if (responseRecords.size() == 0) { %>

        <div class="noPlayersOrEventsMsg">
            There are no players for this group/team.
        </div>

        <% } else {
            showCopyPasteButton = true;
        %>


        <div id="eventChooser">

            <select id="eventSelect" name="eventSelect" onchange="mgvChangeEvent()">
                <%
                    for (EventBean event : eventRecords)
                    {
                        final boolean isSelected = event.getId() == selectedEventId;
                %>
                <option <%= isSelected ? "selected=\"selected\"" : "" %>
                        value="<%= event.getId() %>" >
                    <%= event.getDateFormattedForColumnHeader() + " " + event.getDescriptionMinusBrTags() %>
                </option>
                <% }   // for event %>
            </select>

        </div>

        <div id="playersContainer">

            <table id="playersTable">

                <%
                    int relativeEventNumber = -1;

                    for (EventResponseJoinedDataBean eventResponse : responseRecords)
                    {
                        relativeEventNumber++;
                        final PlayerResponse currentResponse = eventResponse.getPlayerResponse();

                %>

                <tr>
                    <td class="player">
                        <a href="/GroupContacts.page?groupId=<%= eventResponse.getGroupId() %>&playerId=<%= eventResponse.getPlayerId() %>">
                        <%= eventResponse.getPlayerName() %>
                        </a>
                    </td>

                    <td class="response">

                        <input type="hidden"
                               name="old_<%= eventResponse.getPlayerId() %>_<%= eventResponse.getEventId() %>"
                               value="<%= currentResponse.name() %>"/>

                        <select class="responseInput e<%= eventResponse.getEventId() %>"
                                name="new_<%= eventResponse.getPlayerId() %>_<%= eventResponse.getEventId() %>"
                                onchange="mgvWarnAboutUnsaved()">
                            <% for (PlayerResponse playerResponse : PlayerResponse.values())
                            {
                                final boolean isSelected = playerResponse == currentResponse;

                                if (isSelected && playerResponse == PlayerResponse.yes)
                                {
                                    yesCount++;
                                }
                            %>
                            <option <%= isSelected ? "selected=selected" : "" %>
                                    value="<%= playerResponse.name() %>"><%= playerResponse.getLabel() %></option>
                            <% }   // for playerResponse %>
                        </select>

                    </td>
                </tr>

                <%
                    }
                %>
            </table>

        </div>

        <div id="yesCountContainer">
            Yes count: <%= yesCount %>
        </div>

        <div id="bottomMsgDiv" class="msgDiv">
            <input type="button" value="Save" onclick="mgvSave()" /> or
            <input type="button" value="Cancel" onclick="mgvCancel()" /> change(s)
        </div>

        <% } } %>

        <div id="mobileViewButtonsContainer">
            <input type="button" id="optionsButton" value="Options" onclick="mgvOptions()" />

            <% if (hasAdminClearance && showCopyPasteButton) { %>
            <input type="button" class="commButton" value="Communicate" 
            onclick="displayCommunicationsPage('?eventId=<%= selectedEventId %>&responseTypes=yes')" />
			
			<br/><br/>								
            <input type="button" class="tooLateButton" value="Mark Too Late" 
            onclick="markTooLate('<%= selectedEventId %>', '<%= PlayerResponse.outlate.name() %>')" />
                   
            <% } %>
        </div>

        <div id="mobileViewQuote">
            <%= SessionHelper.getQuote() %>
        </div>

    </form>

</div>

</body>

</html>