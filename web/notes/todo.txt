LEFT TO DO:

SHORT TERM:

(0.3) Add individualized login process.  Must be logged in to use the system.

(A) Non-admin can only see groups for which they are a member.

(B) Non-admin can only edit responses for themselves.

(0.4) After doing standard login, and not putting online, allowing Single Sign-On via Facebook or Google or Twitter.

(0.5) Add unit tests and load tests: JMeter

(1) For captains, add a “show since xx/xx/xx” date field to spreadsheet to reveal the hidden/old stuff.
Would help Mike keep track of playing time in matches.

(2) For captains, on the spreadsheet page, have a "Mark Too Late" action that replaces all blanks with "Too Late".

(3) Edit Group Members: put non-members into 2 columns, if window is wide enough (to reduce scrolling)

(4) Consider simplifying/improving color scheme -- like I did recently to TennisProjectInfo.page

(5) Change EditMatchList.page to improve color scheme

(6) For image files, especially in slideshow areas, consider using the versioned query string suffix --
since images are cached for a year.  Otherwise, changed images won't be reflected.

(7) Consider calculating member/non-member practice fee and printing on Money Collection Sheet

(8) Project/demo page: Try recording a video presentation using a free video app.

Top 5 choices here:
https://www.ezvid.com/top_5_free_video_capture_software_for_windows

(10) Create ability to copy team -- or at least team members from one to another

(11) On EditMatchList.page, provide ability to add matches.

(12) On Communications page, when it is displayed along with an event, next to the "Send" button add a checkbox to
"Include link to event" (or some such).  The link would be for the spreadsheet page (or mobile?) with the query
string parm ?overrideGroupId=8 (for example).

(13) Change the Communications page to add a checkbox option for sending the text contents without any formatting --
so words like REPLY: are not present.  Only make checkbox option available for me.  This will help in sending links
to an Android phone for testing.

(14) For singles league score entry, consider having own password so somebody besides me can do it.

(15) On Communications page, consider, when texting only, to allow a keyword like "elink" for an event link.


LONG TERM:

(1) start using a source control system (DONE)

http://info.perforce.com/free.html
http://www.perforce.com/downloads/Perforce/20-User

Good intro/concepts manual: http://www.perforce.com/perforce/doc.current/manuals/intro/intro.pdf

(2) MAYBE DON'T DO AT ALL.

Clear standings_calc table entirely at servlet init and destroy.  See StandingsCalc.clearRecords.

Can't be just any old servlet, because a servlet is only loaded if it is used.  Init declaration:

    public void init() throws javax.servlet.ServletException
    {
        super.init();
    }


(3) Consider using the following elements in the web.xml file:

<error-page> -- see p. 665 in Java Servlets programming book

<welcome-file> -- see p. 683

(6) Need to improve exception handling -- for when exception occurs on a JSP page and the error isn't written to our
error page.  Especially a problem for included pages, like table jsps.


FOR THE PROBLEM BELOW, I FIXED IT BY DOING THIS:

1. Moved the sqlite.jar file from my application's web/WEB-INF/lib directory to the [TOMCAT_HOME]/lib directory
2. Delete the servlet-api.jar file from my application's web/WEB-INF/lib directory
3. The new ROOT.jar file is now 25% of its previous size


  Noticed once when hitting refresh while editing events.  May only happen after editing an SQL statement in the
  source code or changing the schema of a bean populated by SQL.  The message:

"java.lang.UnsatisfiedLinkError: Native Library C:\apache-tomcat-6.0.33\temp\sqlite-3.7.151-x86-sqlitejdbc.dll
already loaded in another classloader"
