<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-markup">
&lt;%@ page import="other.SessionHelper" %&gt;

&lt;%
    final boolean isMobile = SessionHelper.isMobileBrowser(request);
	final int dropDownMenuWidth = isMobile ? 80 : 150;
	final boolean openOnClick = isMobile ? true : false;   // desktop: use false
	final boolean closeOnMouseOut = isMobile ? false : true;   // desktop: use true
%&gt;


	&lt;script src="js/jquery.mb.menu-2.9.7/inc/jquery.metadata.js"&gt;&lt;/script&gt;
	&lt;script src="js/jquery.mb.menu-2.9.7/inc/jquery.hoverIntent.js"&gt;&lt;/script&gt;
	&lt;script src="js/jquery.mb.menu-2.9.7/inc/mbMenu.js"&gt;&lt;/script&gt;
	
	&lt;% if (isMobile) { %&gt;
	&lt;link rel="stylesheet" type="text/css" href="js/jquery.mb.menu-2.9.7/css/menu_ss_mobile.css&lt;%= SessionHelper.getVersionQueryString() %&gt;" media="screen" /&gt;
	&lt;% } else { %&gt;
	&lt;link rel="stylesheet" type="text/css" href="js/jquery.mb.menu-2.9.7/css/menu_ss_desktop.css&lt;%= SessionHelper.getVersionQueryString() %&gt;" media="screen" /&gt;
	&lt;% } %&gt;
	
    &lt;script type="text/javascript"&gt;

    $(document).ready(function() {
        nmBuildMenu();
    })

    
	function nmBuildMenu()
	{
	    $(".nmMenuBar").buildMenu({
	        /* template:"yourMenuVoiceTemplate",
	        additionalData:"", */
	        menuSelector:".menuContainer",
	        menuWidth:&lt;%= dropDownMenuWidth %&gt;,   /* drop-down menu minimum width */
	        openOnRight:false,
	        /* containment:"window", */
	        iconPath:"ico/",
	        hasImages:false,
	        fadeInTime:0,
	        fadeOutTime:200,
	        menuTop:0,
	        menuLeft:0,
	        submenuTop:0,
	        submenuLeft:4,
	        opacity:1,
	        shadow:false,
	        shadowColor:"black",
	        shadowOpacity:.2,
	        openOnClick:&lt;%= openOnClick %&gt;,
	        closeOnMouseOut:&lt;%= closeOnMouseOut %&gt;,
	        closeAfter:100,
	        minZindex:"auto",
	        hoverIntent:250, //if you use jquery.hoverIntent.js set this to time in milliseconds; 0= false;
	        submenuHoverIntent:0, //if you use jquery.hoverIntent.js set this to time in milliseconds; 0= false;
	    });
	}
    
    &lt;/script&gt;
</code></pre>

<div class="caption">
    Here is a JSP "View" fragment named NavMenuHead.jsp.  Coupled with NavMenuBody.jsp (previous slide), this CSS/Javascript fragment 
    (included in the head element of every page) renders pretty navigation using the "jquery.mb.menu" tool developed by Matteo 
    Bicocchi (aka Pupunzi).  
</div>

</div>