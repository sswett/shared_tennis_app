<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-java">
    package other;

    import utils.DateUtils;

    import javax.servlet.Filter;
    import javax.servlet.FilterChain;
    import javax.servlet.FilterConfig;
    import javax.servlet.ServletException;
    import javax.servlet.ServletRequest;
    import javax.servlet.ServletResponse;
    import javax.servlet.http.HttpServletResponse;
    import java.io.IOException;
    import java.util.Calendar;

    public class JsCssImgResponseHeaderFilter implements Filter
    {
        static final int SECONDS_TO_CACHE = 60 * 60 * 24 * 365;   // 1 year


        public void init(FilterConfig fc) throws ServletException
        {
        }


        public void doFilter(ServletRequest req,
                             ServletResponse res,
                             FilterChain fc) throws IOException, ServletException
        {
            HttpServletResponse response = (HttpServletResponse) res;

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.SECOND, SECONDS_TO_CACHE);
            response.setHeader("Cache-Control", "PUBLIC, max-age=" + SECONDS_TO_CACHE + ", must-revalidate");
            response.setHeader("Expires", DateUtils.getDateInExpiresHeaderFormat(cal.getTime()));

            fc.doFilter(req, res);
        }


        public void destroy()
        {
        }

    }
</code></pre>

<div class="caption">
    JsCssImgResponseHeaderFilter.java: This filter class is used to instruct the browser to cache all Javascript,
    CSS and image files for a year.
</div>

</div>