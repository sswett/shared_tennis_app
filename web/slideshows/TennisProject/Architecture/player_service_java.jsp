<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-java">
package services_rest;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import other.RatingLevelType;
import beans.PlayerBean;
import dao.Players;

@Path("/player")

public class PlayerService 
{
	
	@GET @Path("/all")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })

	public ArrayList&lt;PlayerBean&gt; getAllPlayers() 
	{
		return Players.createInstance(true).getRecords();
	}

	
	@GET @Path("/rating/{ratingLevel}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })

	public ArrayList&lt;PlayerBean&gt; getPlayersWithRatingLevel(@PathParam("ratingLevel") String ratingLevel) 
	{
		RatingLevelType ratingLevelType;
		
		try
		{
			ratingLevelType = RatingLevelType.valueOf(ratingLevel);
		}
		catch(IllegalArgumentException e)
		{
			ratingLevelType = RatingLevelType.LEVEL_ANY;
		}
		
		return Players.createInstance(true, ratingLevelType).getRecords();
	}

}
</code></pre>

<div class="caption">
    PlayerService.java: This class provides methods for responding to two different service URI paths -- one to
    return all players and one to return players having a particular rating level (2 of 3).
</div>

</div>