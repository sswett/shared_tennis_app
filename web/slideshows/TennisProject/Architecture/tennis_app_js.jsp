<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-javascript">
var yesResponseList = new Array();


function taPrintElementWithId(pPageContainerId, pElemId)
{
    $("div").each(function() {
        $(this).addClass("unprintable")
    });

    $("#" + pPageContainerId + ",#" + pElemId).removeClass("unprintable");

    $("#" + pElemId + " div").each(function() {
        $(this).removeClass("unprintable");
    });

    window.print();
}


function cpRandomDraw()
{
    var randomIdx = Math.floor(Math.random() * yesResponseList.length);
    // alert("randomIdx = " + randomIdx);
    $("#drawResult").html(yesResponseList[randomIdx]);
}


function cpSelectAllResponseTypes(pEventId)   /* event as in a practice or match */
{
    $(".rtCheckbox").prop("checked", true);
    cpRefreshHtmlElements(pEventId);
}


function cpDeselectAllResponseTypes(pEventId)   /* event as in a practice or match */
{
    $(".rtCheckbox").prop("checked", false);
    cpRefreshHtmlElements(pEventId);
}


function cpSelectAllPlayers(pEventId)   /* event as in a practice or match */
{
    $(".playerCheckbox").prop("checked", true);
    cpRefreshHtmlElements(pEventId);
}


function cpDeselectAllPlayers(pEventId)   /* event as in a practice or match */
{
    $(".playerCheckbox").prop("checked", false);
    cpRefreshHtmlElements(pEventId);
}


function cpEnableOrDisableSend()
{
    var serverMsg = $("#serverMsg").val();

    if (String.prototype.trim)
    {
        serverMsg = serverMsg.trim();
    }
    else
    {
        serverMsg = serverMsg.replace(/^\s+|\s+$/g,'');
    }

    if (serverMsg.length == 0)
    {
        $("#sendServerMsgButton").attr("disabled", "disabled");
        return;
    }

    if ( $("#sendAsText").prop("checked") || $("#sendAsEmail").prop("checked") )
    {
        $("#sendServerMsgButton").removeAttr("disabled");
    }
    else
    {
        $("#sendServerMsgButton").attr("disabled", "disabled");
    }
}


function cpChangeSender()
{
    var valueArray =  $("#senderSelect").val().split(";");
    $( "#hiddenAjaxResponse" ).load( "/ChangeSender.page?playerId=" + valueArray[0] );
}


function cpRefreshHtmlElements(pEventId)   /* event as in a practice or match */
{
    var cpForm = document.copyPasteForm;
    var masterIdList = cpForm.masterIdList.value.split(";");
    var masterNameList = cpForm.masterNameList.value.split(";");
    var masterPhoneList = cpForm.masterPhoneList.value.split(";");
    var masterEmailList = cpForm.masterEmailList.value.split(";");
    var masterResponseDateList = cpForm.masterResponseDateList.value.split(";");
    var masterCategoryList = cpForm.masterCategoryList.value.split(";");
    var masterMbrTypeList = cpForm.masterMbrTypeList.value.split(";");
    var phoneDelimiterForRawCopyPaste = cpForm.phoneDelimiterForRawCopyPaste.value;

    var filteredNameList = new Array();
    var filteredPhoneList = new Array();
    var filteredEmailList = new Array();
    var filteredResponseDateList = new Array();
    var filteredMbrTypeList = new Array();
    yesResponseList = new Array();

    if ($("#drawResult").length)
    {
        $("#drawResult").html("");
    }

    if (pEventId == 0)
    {
        for (var x = 0; x &lt; masterIdList.length; x++)
        {
            var checkBoxId = "p" + masterIdList[x];

            if ($("#" + checkBoxId).prop("checked"))
            {
                var idx = filteredNameList.length;
                filteredNameList[idx] = masterNameList[x];
                filteredPhoneList[idx] = masterPhoneList[x];
                filteredEmailList[idx] = masterEmailList[x];
                filteredResponseDateList[idx] = masterResponseDateList[x];
                filteredMbrTypeList[idx] = masterMbrTypeList[x];
            }
        }
    }
    else
    {
        for (x = 0; x &lt; masterNameList.length; x++)
        {
            checkBoxId = "rt" + masterCategoryList[x];

            if ($("#" + checkBoxId).prop("checked"))
            {
                idx = filteredNameList.length;
                filteredNameList[idx] = masterNameList[x];
                filteredPhoneList[idx] = masterPhoneList[x];
                filteredEmailList[idx] = masterEmailList[x];
                filteredResponseDateList[idx] = masterResponseDateList[x];
                filteredMbrTypeList[idx] = masterMbrTypeList[x];

                if (masterCategoryList[x] == globalYesResponse)
                {
                    var y = yesResponseList.length;
                    yesResponseList[y] = masterNameList[x];
                }
            }
        }
    }

    var smsPhoneList = cpGetTrimmedAndFormattedList(filteredPhoneList, ",");
    var emailList = cpGetTrimmedAndFormattedList(filteredEmailList, ";");

    cpForm.filteredPhoneList.value = smsPhoneList;
    cpForm.filteredEmailList.value = emailList;

    var playerCount = filteredNameList.length;
    var phoneCount = 0;
    var emailCount = 0;

    if (smsPhoneList != "")
    {
        phoneCount = smsPhoneList.split(",").length;
    }

    if (emailList != "")
    {
        emailCount = emailList.split(";").length;
    }

    $("#textQtys").html(phoneCount + "/" + playerCount);
    $("#emailQtys").html(emailCount + "/" + playerCount);

    if (phoneCount == 0 && emailCount == 0)
    {
        $("#copyPasteServerMsgSection").css("display", "none");
    }
    else
    {
        $("#copyPasteServerMsgSection").css("display", "block");
    }

    if (yesResponseList.length == 0)
    {
        $("#copyPasteRandomDrawSection").css("display", "none");
    }
    else
    {
        $("#copyPasteRandomDrawSection").css("display", "block");
    }

    // Handle sms link:
    if ($("#smslink").length)
    {
        if (smsPhoneList == "")
        {
            $("#copyPasteTextLinkSection").removeClass("shownTextLink").addClass("hiddenTextLink");
        }
        else
        {
            $("#copyPasteTextLinkSection").removeClass("hiddenTextLink").addClass("shownTextLink");
            $("#smslink").attr("href", "sms:" + smsPhoneList);
        }
    }

    // Handle mail to link:
    if ($("#mailtolink").length)
    {
        if (emailList == "")
        {
            $("#copyPasteEmailLinkSection").removeClass("shownEmailLink").addClass("hiddenEmailLink");
        }
        else
        {
            $("#copyPasteEmailLinkSection").removeClass("hiddenEmailLink").addClass("shownEmailLink");
            $("#mailtolink").attr("href", "mailto:" + emailList);
        }
    }

    // Handle copy paste raw phone numbers:
    var rawPhoneList = cpGetTrimmedAndFormattedList(filteredPhoneList, phoneDelimiterForRawCopyPaste);
    $("#copyPastePhoneNumbers").html(rawPhoneList);

    // Handle copy paste raw email addresses:
    var rawEmailList = cpGetTrimmedAndFormattedList(filteredEmailList, ";&lt;br/&gt;");
    $("#copyPasteEmails").html(rawEmailList);

    // Handle copy paste raw names (not output for mobile):

    if ($("#copyPasteNames").length)
    {
        var nameOutput = "&lt;ul&gt;";

        for (x = 0; x &lt; filteredNameList.length; x++)
        {
            nameOutput += "&lt;li&gt;";

            nameOutput += "&lt;div class='cpnName'&gt;";
            nameOutput += filteredNameList[x];
            nameOutput += "&lt;/div&gt;";

            nameOutput += "&lt;div class='cpnMbrType'&gt;";
            nameOutput += "Mbr Type: ";
            nameOutput += filteredMbrTypeList[x];
            nameOutput += "&lt;/div&gt;";

            if (filteredResponseDateList[x].length &gt; 0)
            {
                nameOutput += "&lt;div class='cpnResponseDate'&gt;";
                nameOutput += "Responded: ";
                nameOutput += filteredResponseDateList[x];
                nameOutput += "&lt;/div&gt;";
            }

            nameOutput += "&lt;/li&gt;";
        }

        nameOutput += "&lt;/ul&gt;";
        $("#copyPasteNames").html(nameOutput);
        $("#cpPlayerCount").html("" + filteredNameList.length);
    }

    // Handle Contact List

    var contactList = "";

    if ($("#cpContactListTable").length)
    {
        // HANDLE NON-MOBILE VERSION
        contactList = "&lt;tr&gt;&lt;th&gt;Name&lt;/th&gt;&lt;th&gt;Phone&lt;/th&gt;&lt;th&gt;E-mail&lt;/th&gt;&lt;/tr&gt;";

        for (var cl = 0; cl &lt; filteredNameList.length; cl++)
        {
            contactList += "&lt;tr&gt;";

            // Name:
            contactList += "&lt;td&gt;";
            contactList += filteredNameList[cl];
            contactList += "&lt;/td&gt;";

            // Phone:
            contactList += "&lt;td&gt;";
            var phone = filteredPhoneList[cl];

            if (phone.length &gt; 0)
            {
                var fmtPhone = phone.substr(0,3) + "-" + phone.substr(3,3) + "-" + phone.substr(6);
                contactList += "&lt;a href='sms:" + phone + "'&gt;" + fmtPhone + "&lt;/a&gt;";
            }
            else
            {
                contactList += "&nbsp;";
            }

            contactList += "&lt;/td&gt;";

            // Email:
            contactList += "&lt;td&gt;";
            var email = filteredEmailList[cl];

            if (email.length &gt; 0)
            {
                contactList += "&lt;a href='mailto:" + email + "'&gt;" + email + "&lt;/a&gt;";
            }
            else
            {
                contactList += "&nbsp;";
            }

            contactList += "&lt;/td&gt;";

            contactList += "&lt;/tr&gt;";
        }

        $("#cpContactListTable").html(contactList);
    }
    else
    {
        // HANDLE MOBILE VERSION
        for (cl = 0; cl &lt; filteredNameList.length; cl++)
        {
            // Name:
            contactList += filteredNameList[cl];
            contactList += "&lt;br/&gt;";

            // Phone:
            phone = filteredPhoneList[cl];

            if (phone.length &gt; 0)
            {
                fmtPhone = phone.substr(0,3) + "-" + phone.substr(3,3) + "-" + phone.substr(6);
                contactList += "&lt;a href='tel:" + phone + "'&gt;call " + fmtPhone + "&lt;/a&gt;&lt;br/&gt;";
                contactList += "&lt;a href='sms:" + phone + "'&gt;text " + fmtPhone + "&lt;/a&gt;&lt;br/&gt;";
            }

            // Email:
            email = filteredEmailList[cl];

            if (email.length &gt; 0)
            {
                contactList += "&lt;a href='mailto:" + email + "'&gt;" + email + "&lt;/a&gt;&lt;br/&gt;";
            }

            contactList += "&lt;br/&gt;";
        }

        $("#cpContactListContainer").html(contactList);
    }

}


function cpGetTrimmedAndFormattedList(pListArray, pDelimiter)
{
    var result = "";

    for (var x = 0; x &lt; pListArray.length; x++)
    {
        if (pListArray[x] != "")
        {
            if (result != "")
            {
                result += pDelimiter;
            }

            result += pListArray[x];
        }
    }

    return result;
}


function cpSendServerMsg()
{
    document.copyPasteForm.sendServerMsg.value = "true";

    $.ajax({
        type: "POST",
        url: "/SendServerMsg.page",
        data: $("form").serialize(),   /* Very cool trick, treats same way as POSTed form data */
        success: cpSendServerMsgResponseHandler,
        dataType: "text"
    });
}


function cpSendServerMsgResponseHandler(data, textStatus, jqXHR)
{
    $("#sendServerMsgResults").html(data);
    window.scrollTo(0, 0);
}


function taChangeGroup()
{
    document.changeGroupForm.changeGroup.value = "true";
    document.changeGroupForm.submit();
}


function taChangeView()
{
    document.changeViewForm.changeView.value = "true";
    document.changeViewForm.submit();
}


function mgvChangeEvent()
{
    document.mobileForm.action.value = "changeEvent";
    document.mobileForm.submit();
}


function mgvSave()
{
    document.mobileForm.action.value = "save";
    document.mobileForm.submit();
}


function mgvCancel()
{
    document.mobileForm.reset();
    $("#eventSelect,#optionsButton").removeAttr("disabled");
    $(".commButton,.tooLateButton").show();
    $(".msgDiv").css("display", "none");
}


function mgvOptions()
{
    window.location = "MobileOptions.page";
}


function mgvWarnAboutUnsaved()
{
    $("#optionsButton,#eventSelect").attr("disabled", "disabled");
    $(".commButton,.tooLateButton").hide();
    $(".msgDiv").css("display", "block");
    $("#changesSaved").css("display", "none");
}


function mpvChangePlayer()
{
    document.mobileForm.action.value = "changePlayer";
    document.mobileForm.submit();
}


function mpvSave()
{
    document.mobileForm.action.value = "save";
    document.mobileForm.submit();
}


function mpvCancel()
{
    document.mobileForm.reset();
    $("#playerSelect,#optionsButton").removeAttr("disabled");
    $(".msgDiv").css("display", "none");
}


function mpvOptions()
{
    window.location = "MobileOptions.page";
}


function mpvWarnAboutUnsaved()
{
    $("#optionsButton,#playerSelect").attr("disabled", "disabled");
    $(".msgDiv").css("display", "block");
    $("#changesSaved").css("display", "none");
}


function ssShowSaveOrCancelButtons()
{
    $('.saveOrCancelButtons').css("display", "inline-block");
    $('.savemsg').html("");
    $('#groupSelect').attr("disabled", "disabled");
}


function ssHideSaveOrCancelButtons()
{
    $('.saveOrCancelButtons').css("display", "none");
    $('#groupSelect').removeAttr("disabled");
}


function ssShowCrossHair(responseCell)
{
    var responseId = $(responseCell).attr("id");
    var classAttr = $(responseCell).attr("class");
    var classes = classAttr.split(" ");

    // Column portion of cross hair:
    var colClassVal = "";

    for (var x = 0; x &lt; classes.length; x++)
    {
        var classVal = classes[x];

        if (classVal.indexOf("col") == 0)
        {
            colClassVal = classVal;
            break;
        }
    }

    if (colClassVal.length &gt; 0)
    {
        $("." + colClassVal).addClass("crosshair");
    }

    // Row portion of cross hair:
    $(responseCell).closest("tr").find("td").addClass("crosshair");
}


function ssHideCrossHairs()
{
    $(".crosshair").removeClass("crosshair");
}


function ssHighlightRow(responseInput)
{
    $(".rowHighlighter").removeClass("rowHighlighter");
    $(responseInput).closest("tr").find("td").addClass("rowHighlighter");
}


function displayCommunicationsPage(queryString)
{
    window.location = "Communications.page" + queryString;
}
</code></pre>

<div class="caption">
    Here is the TennisApp.js javascript file.  It demonstrates use of jQuery, Ajax, etc.
</div>

</div>