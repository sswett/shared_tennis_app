<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-java">
package dao;

import beans.CurrentSessionsBean;
import beans.MatchBean;
import beans.PlayerBean;
import beans.StandingsCalcBean;
import db.DBBroker;
import db.DBUtils;
import other.MatchType;
import other.RatingLevelType;
import other.SessionHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class StandingsCalc
{
    private ArrayList&lt;StandingsCalcBean&gt; records;


    private StandingsCalc()
    {

    }


    /*
        NOTE: The standings_calc table is used like a temporary table.  The reason a table is used at all for
        standings calculations is so that SQL can be used to sort by winning percentage.  A "slice" of the table is
        manipulated, where the slice is limited to the current HTTP session (to allow multiuser use).
     */

    public static StandingsCalc createInstance(String httpSessionId, String division, MatchType matchType)
    {
        // Prepare temp db table:
        clearRecords(false, httpSessionId);

        CurrentSessionsBean sessBean = CurrentSessions.
                createInstance(SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID).getRecords().get(0);

        // Get players in division
        RatingLevelType ratingLevel = RatingLevelType.LEVEL_4_0;
        String groupShortName = sessBean.getGroupShortName4_0();

        if (division.equals("3.5"))
        {
            ratingLevel = RatingLevelType.LEVEL_3_5;
            groupShortName = sessBean.getGroupShortName3_5();
        }

        ArrayList&lt;StandingsCalcBean&gt; standingsBeans = new ArrayList&lt;StandingsCalcBean&gt;();

        ArrayList&lt;PlayerBean&gt; players = Players.createInstance(false, ratingLevel, groupShortName).getRecords();

        for (PlayerBean player : players)
        {
            int matchesWon = 0;
            int matchesLost = 0;
            int setsWon = 0;
            int setsLost = 0;
            float gamesWon = 0;
            float gamesLost = 0;

            int playerId = player.getId();

            ArrayList&lt;MatchBean&gt; matches = Matches.createInstance(division, playerId, matchType).getRecords();

            for (MatchBean match : matches)
            {
                matchesWon += match.getMatchesWonOrLost(playerId, true);
                matchesLost += match.getMatchesWonOrLost(playerId, false);
                setsWon += match.getSetsWonOrLost(playerId, true);
                setsLost += match.getSetsWonOrLost(playerId, false);
                gamesWon += match.getGamesWonOrLostProrating3rdSetTiebreaker(playerId, true);
                gamesLost += match.getGamesWonOrLostProrating3rdSetTiebreaker(playerId, false);
            }

            final int matchesTotal = matchesWon + matchesLost;
            final int setsTotal = setsWon + setsLost;
            final float gamesTotal = gamesWon + gamesLost;

            final float matchesWonPct = ( (float) matchesWon / matchesTotal ) * 100.0F;
            final float setsWonPct = ( (float) setsWon / setsTotal ) * 100.0F;
            final float gamesWonPct = ( (float) gamesWon / gamesTotal ) * 100.0F;

            StandingsCalcBean standingsCalcBean = new StandingsCalcBean(0, httpSessionId, playerId, player.getName(),
                    matchesWon, matchesLost, matchesWonPct, setsWon, setsLost, setsWonPct,
                    gamesWon, gamesLost, gamesWonPct);

            standingsBeans.add(standingsCalcBean);
        }

        // Insert records into table:

        insertRecords(standingsBeans);

        // Read them right back out again (to get them sorted):

        final String qryStmt = "select id, session_id, player_id, player_name, " +
                "matches_won, matches_lost, matches_won_pct, " +
                "sets_won, sets_lost, sets_won_pct, " +
                "games_won, games_lost, games_won_pct " +
                "from standings_calc " +
                "where session_id = '" + httpSessionId + "' " +
                "order by matches_won_pct desc, sets_won_pct desc, games_won_pct desc, " +
                "matches_won desc, sets_won desc, games_won desc, player_name";

        StandingsCalc standingsCalc = loadRecords(qryStmt, standingsBeans.size());

        // Clean up temp db table:
        clearRecords(false, httpSessionId);

        return standingsCalc;
    }


    public static void insertRecords(ArrayList&lt;StandingsCalcBean&gt; standingsCalcBeans)
    {
        Connection connection = null;
        PreparedStatement ps = null;

        try
        {
            connection = DBBroker.getConnection();

            String sql = "insert into standings_calc " +
                    "(session_id, player_id, player_name, " +
                    "matches_won, matches_lost, matches_won_pct, " +
                    "sets_won, sets_lost, sets_won_pct, " +
                    "games_won, games_lost, games_won_pct) " +
                    "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            ps = connection.prepareStatement(sql);

            for (StandingsCalcBean standingsBean : standingsCalcBeans)
            {
                int i = 1;

                ps.setString(i++, standingsBean.getSessionId());
                ps.setInt(i++, standingsBean.getPlayerId());
                ps.setString(i++, standingsBean.getPlayerName());
                ps.setInt(i++, standingsBean.getMatchesWon());
                ps.setInt(i++, standingsBean.getMatchesLost());
                ps.setFloat(i++, standingsBean.getMatchesWonPct());
                ps.setInt(i++, standingsBean.getSetsWon());
                ps.setInt(i++, standingsBean.getSetsLost());
                ps.setFloat(i++, standingsBean.getSetsWonPct());
                ps.setFloat(i++, standingsBean.getGamesWon());
                ps.setFloat(i++, standingsBean.getGamesLost());
                ps.setFloat(i++, standingsBean.getGamesWonPct());

                ps.executeUpdate();
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.out.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(null, ps, connection);
        }
    }


    private static StandingsCalc loadRecords(String qryStmt, int player_count)
    {
        StandingsCalc standingsCalc = new StandingsCalc();
        standingsCalc.records = new ArrayList&lt;StandingsCalcBean&gt;();

        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            rs = statement.executeQuery(qryStmt);

            int player_rank = 0;

            while(rs.next())
            {
                // read the result set

                player_rank++;

                // int player_count = rs.getInt("player_count");
                int id = rs.getInt("id");
                String session_id = rs.getString("session_id");
                int player_id = rs.getInt("player_id");
                String player_name = rs.getString("player_name");
                int matches_won = rs.getInt("matches_won");
                int matches_lost = rs.getInt("matches_lost");
                float matches_won_pct = rs.getFloat("matches_won_pct");
                int sets_won = rs.getInt("sets_won");
                int sets_lost = rs.getInt("sets_lost");
                float sets_won_pct = rs.getFloat("sets_won_pct");
                float games_won = rs.getFloat("games_won");
                float games_lost = rs.getFloat("games_lost");
                float games_won_pct = rs.getFloat("games_won_pct");

                StandingsCalcBean standingsCalcBean = new StandingsCalcBean(player_count, player_rank,
                        id, session_id,
                        player_id, player_name,
                        matches_won, matches_lost, matches_won_pct,
                        sets_won, sets_lost, sets_won_pct,
                        games_won, games_lost, games_won_pct);

                standingsCalc.records.add(standingsCalcBean);
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.out.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }

        return standingsCalc;
    }


    public static void clearRecords(boolean clearAllSessions, String oneSessionToClear)
    {
        Connection connection = null;
        PreparedStatement ps = null;

        try
        {
            connection = DBBroker.getConnection();

            String sql = "delete from standings_calc " +
                    "where session_id = ?;";

            ps = connection.prepareStatement(sql);
            ps.setString(1, oneSessionToClear);
            ps.execute();
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.out.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(null, ps, connection);
        }
    }


    public ArrayList&lt;StandingsCalcBean&gt; getRecords()
    {
        return records;
    }

}
</code></pre>

<div class="caption">
    StandingsCalc.java: This is a helper or business rules class.  The createInstance method is used to calculate
    the standings of the singles league and to get a reference.  The getRecords method is used to fetch a
    StandingsCalcBean for each row (player) in the standings.
</div>

</div>