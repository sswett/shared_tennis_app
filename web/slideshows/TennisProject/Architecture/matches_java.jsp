<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-java">
package dao;

import beans.MatchBean;
import db.DBBroker;
import db.DBUtils;
import other.MatchIdType;
import other.MatchType;
import other.SessionHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

public class Matches
{
    private ArrayList&lt;MatchBean&gt; records;


    private Matches()
    {

    }


    public static Matches createInstance()
    {
        // final String startDateStr = DBUtils.getDbFormatted24HrDateString(startDate.getTime());

        final String qryStmt = "select " +
                "m.id, m.yr, m.sess, division, match_date, match_no, player1_id, player2_id, winner_id," +
                "player1_set1_games_won, player1_set2_games_won, player1_set3_games_won, " +
                "player2_set1_games_won, player2_set2_games_won, player2_set3_games_won, " +
                "is_set3_tiebreaker, week_no, custom_label, note, player1_note, player2_note, is_playoff, is_default " +
                "from matches as m join current_session as cs " +
                "on m.yr = cs.yr and m.sess = cs.sess " +
                "where cs.id = " + SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID +
                " order by division, match_date, match_no";

        return loadRecords(qryStmt);
    }


    public static Matches createInstance(String division)
    {
        // final String startDateStr = DBUtils.getDbFormatted24HrDateString(startDate.getTime());

        final String qryStmt = "select " +
                "m.id, m.yr, m.sess, division, match_date, match_no, player1_id, player2_id, winner_id," +
                "player1_set1_games_won, player1_set2_games_won, player1_set3_games_won, " +
                "player2_set1_games_won, player2_set2_games_won, player2_set3_games_won, " +
                "is_set3_tiebreaker, week_no, custom_label, note, player1_note, player2_note, is_playoff, is_default " +
                "from matches as m join current_session as cs " +
                "on m.yr = cs.yr and m.sess = cs.sess " +
                "where cs.id = " + SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID +
                " and division = '" + division + "' " +
                "order by division, match_date, match_no";

        return loadRecords(qryStmt);
    }


    public static Matches createInstance(String division, int playerId)
    {
        // final String startDateStr = DBUtils.getDbFormatted24HrDateString(startDate.getTime());

        final String qryStmt = "select " +
                "m.id, m.yr, m.sess, division, match_date, match_no, player1_id, player2_id, winner_id," +
                "player1_set1_games_won, player1_set2_games_won, player1_set3_games_won, " +
                "player2_set1_games_won, player2_set2_games_won, player2_set3_games_won, " +
                "is_set3_tiebreaker, week_no, custom_label, note, player1_note, player2_note, is_playoff, is_default " +
                "from matches as m join current_session as cs " +
                "on m.yr = cs.yr and m.sess = cs.sess " +
                "where cs.id = " + SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID +
                " and division = '" + division + "' " +
                "and (player1_id = " + playerId + " or player2_id = " + playerId + ")" +
                "order by division, match_date, match_no";

        return loadRecords(qryStmt);
    }


    public static Matches createInstance(String division, int playerId, MatchType matchType)
    {
        // final String startDateStr = DBUtils.getDbFormatted24HrDateString(startDate.getTime());

        final String matchTypeSelection = matchType == MatchType.ALL ? "" :
                " and is_playoff = " + (matchType == MatchType.PLAYOFF ? 1 : 0);

        final String qryStmt = "select " +
                "m.id, m.yr, m.sess, division, match_date, match_no, player1_id, player2_id, winner_id," +
                "player1_set1_games_won, player1_set2_games_won, player1_set3_games_won, " +
                "player2_set1_games_won, player2_set2_games_won, player2_set3_games_won, " +
                "is_set3_tiebreaker, week_no, custom_label, note, player1_note, player2_note, is_playoff, is_default " +
                "from matches as m join current_session as cs " +
                "on m.yr = cs.yr and m.sess = cs.sess " +
                "where cs.id = " + SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID +
                " and division = '" + division + "'" +
                matchTypeSelection +
                " and (player1_id = " + playerId + " or player2_id = " + playerId + ")" +
                "order by division, match_date, match_no";

        return loadRecords(qryStmt);
    }


    public static Matches createInstanceGetNextMatchOnly(String division,
                                                         int currentWeekNo,
                                                         int currentMatchNo)
    {
        final int weekMatchCombo = currentWeekNo * 100 + currentMatchNo;

        final String qryStmt = "select " +
                "m.id, m.yr, m.sess, division, match_date, match_no, player1_id, player2_id, winner_id," +
                "player1_set1_games_won, player1_set2_games_won, player1_set3_games_won, " +
                "player2_set1_games_won, player2_set2_games_won, player2_set3_games_won, " +
                "is_set3_tiebreaker, week_no, custom_label, note, player1_note, player2_note, is_playoff, is_default " +
                "from matches as m join current_session as cs " +
                "on m.yr = cs.yr and m.sess = cs.sess " +
                "where cs.id = " + SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID +
                " and division = '" + division + "' " +
                "and week_no * 100 + match_no &gt; " + weekMatchCombo +
                " order by division, match_date, match_no " +
                "limit 1";

        return loadRecords(qryStmt);
    }


    public static Matches createInstance(MatchIdType matchIdType, int id)
    {
        // final String startDateStr = DBUtils.getDbFormatted24HrDateString(startDate.getTime());

        final String whereClause;

        if (matchIdType == MatchIdType.MATCH)
        {
            whereClause = "where cs.id = " + SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID +
                    " and m.id = " + id;
        }
        else if (matchIdType == MatchIdType.PLAYER)
        {
            whereClause = "where cs.id = " + SessionHelper.SINGLES_LEAGUE_CURRENT_SESSION_TABLE_ROW_ID +
                    " and (player1_id = " + id + " or player2_id = " + id + ")";
        }
        else
        {
            whereClause = "";
        }


        final String qryStmt = "select " +
                "m.id, m.yr, m.sess, division, match_date, match_no, player1_id, player2_id, winner_id," +
                "player1_set1_games_won, player1_set2_games_won, player1_set3_games_won, " +
                "player2_set1_games_won, player2_set2_games_won, player2_set3_games_won, " +
                "is_set3_tiebreaker, week_no, custom_label, note, player1_note, player2_note, is_playoff, is_default " +
                "from matches as m join current_session as cs " +
                "on m.yr = cs.yr and m.sess = cs.sess " +
                whereClause;

        return loadRecords(qryStmt);
    }


    private static Matches loadRecords(String qryStmt)
    {
        Matches matches = new Matches();
        matches.records = new ArrayList&lt;MatchBean&gt;();

        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;

        try
        {
            connection = DBBroker.getConnection();
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            rs = statement.executeQuery(qryStmt);

            while(rs.next())
            {
                // read the result set

                int id = rs.getInt("id");
                int yr = rs.getInt("yr");
                int sess = rs.getInt("sess");
                String division = rs.getString("division");
                Calendar match_date = DBUtils.getDateFromResultSetString(rs.getString("match_date"));
                int match_no = rs.getInt("match_no");
                int player1_id = rs.getInt("player1_id");
                int player2_id = rs.getInt("player2_id");
                int winner_id = rs.getInt("winner_id");
                int player1_set1_games_won = rs.getInt("player1_set1_games_won");
                int player1_set2_games_won = rs.getInt("player1_set2_games_won");
                int player1_set3_games_won = rs.getInt("player1_set3_games_won");
                int player2_set1_games_won = rs.getInt("player2_set1_games_won");
                int player2_set2_games_won = rs.getInt("player2_set2_games_won");
                int player2_set3_games_won = rs.getInt("player2_set3_games_won");
                int is_set3_tiebreaker = rs.getInt("is_set3_tiebreaker");
                int week_no = rs.getInt("week_no");
                String custom_label = rs.getString("custom_label");
                String note = rs.getString("note");
                String player1_note = rs.getString("player1_note");
                String player2_note = rs.getString("player2_note");
                int is_playoff = rs.getInt("is_playoff");
                int is_default = rs.getInt("is_default");

                MatchBean match = new MatchBean(id, yr, sess, division, match_date, match_no,
                        player1_id, player2_id, winner_id,
                        player1_set1_games_won, player1_set2_games_won, player1_set3_games_won,
                        player2_set1_games_won, player2_set2_games_won, player2_set3_games_won,
                        is_set3_tiebreaker, week_no, custom_label, note, player1_note, player2_note,
                        is_playoff, is_default);

                matches.records.add(match);
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.out.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(rs, statement, connection);
        }

        return matches;
    }


    public static void populateRecords(int yr,
                                       int sess,
                                       String division,
                                       Calendar firstSunday,
                                       int noWeeks,
                                       int noMatchesPerWeek)
    {
        Connection connection = null;
        PreparedStatement ps = null;

        try
        {
            connection = DBBroker.getConnection();

            String sql = "insert into matches " +
                    "(yr, sess, division, match_date, match_no, player1_id, player2_id, winner_id," +
                    "player1_set1_games_won, player1_set2_games_won, player1_set3_games_won," +
                    "player2_set1_games_won, player2_set2_games_won, player2_set3_games_won," +
                    "is_set3_tiebreaker, week_no, custom_label, note, player1_note, player2_note, is_playoff, is_default) " +
                    "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            ps = connection.prepareStatement(sql);

            for (int weekNo = 0; weekNo &lt; noWeeks; weekNo++)
            {
                Calendar matchDate = Calendar.getInstance();
                matchDate.setTime(firstSunday.getTime());

                if (weekNo &gt; 0)
                {
                    matchDate.add(Calendar.DATE, 7 * weekNo);
                }

                for (int matchNo = 1; matchNo &lt;= noMatchesPerWeek; matchNo++)
                {
                    int i = 1;

                    ps.setInt(i++, yr);
                    ps.setInt(i++, sess);
                    ps.setString(i++, division);
                    ps.setString(i++, DBUtils.getDbFormatted24HrDateString(matchDate.getTime()));
                    ps.setInt(i++, matchNo);

                    // Fill remaining columns with 0
                    for (int cols = 1; cols &lt;= 10; cols++)
                    {
                        ps.setInt(i++, 0);
                    }

                    ps.setInt(i++, weekNo);
                    ps.setString(i++, "");   // custom label
                    ps.setString(i++, "");   // note
                    ps.setString(i++, "");   // player1 note
                    ps.setString(i++, "");   // player2 note
                    ps.setInt(i++, 0);   // is_playoff
                    ps.setInt(i++, 0);   // is_default

                    ps.executeUpdate();
                }
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.out.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(null, ps, connection);
        }
    }


    public static void populateRecords(int yr,
                                       int sess,
                                       String division,
                                       Calendar firstSunday,
                                       MatchParms[] matchParms)
    {
        Connection connection = null;
        PreparedStatement ps = null;

        try
        {
            connection = DBBroker.getConnection();

            String sql = "insert into matches " +
                    "(yr, sess, division, match_date, match_no, player1_id, player2_id, winner_id," +
                    "player1_set1_games_won, player1_set2_games_won, player1_set3_games_won," +
                    "player2_set1_games_won, player2_set2_games_won, player2_set3_games_won," +
                    "is_set3_tiebreaker, week_no, custom_label, note, player1_note, player2_note, is_playoff, is_default) " +
                    "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            ps = connection.prepareStatement(sql);

            for (MatchParms matchParm : matchParms)
            {
                int weekNo = matchParm.weekNo;
                int noMatchesPerWeek = matchParm.noMatchesPerWeek;

                Calendar matchDate = Calendar.getInstance();
                matchDate.setTime(firstSunday.getTime());

                matchDate.add(Calendar.DATE, 7 * (weekNo - 1) );

                for (int matchNo = 1; matchNo &lt;= noMatchesPerWeek; matchNo++)
                {
                    int i = 1;

                    ps.setInt(i++, yr);
                    ps.setInt(i++, sess);
                    ps.setString(i++, division);
                    ps.setString(i++, DBUtils.getDbFormatted24HrDateString(matchDate.getTime()));
                    ps.setInt(i++, matchNo);

                    // Fill remaining columns with 0
                    for (int cols = 1; cols &lt;= 10; cols++)
                    {
                        ps.setInt(i++, 0);
                    }

                    ps.setInt(i++, weekNo);
                    ps.setString(i++, "");   // custom label
                    ps.setString(i++, "");   // note
                    ps.setString(i++, "");   // player1 note
                    ps.setString(i++, "");   // player2 note
                    ps.setInt(i++, 0);   // is_playoff
                    ps.setInt(i++, 0);   // is_default

                    ps.executeUpdate();
                }
            }
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.out.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(null, ps, connection);
        }
    }


    public static void fillWeekNo(String division)
    {
        ArrayList&lt;MatchBean&gt; matchBeans = createInstance(division).getRecords();

        if (matchBeans.size() == 0)
        {
            return;
        }

        MatchBean firstMatchBean = matchBeans.get(0);

        if (firstMatchBean.getWeekNo() != 0)
        {
            return;   // already done
        }

        long firstMatchDateInMs = firstMatchBean.getMatchDate().getTime().getTime();

        for (MatchBean matchBean : matchBeans)
        {
            long matchDateInMs = matchBean.getMatchDate().getTime().getTime();
            int weeksBetween = (int)( (matchDateInMs - firstMatchDateInMs) / (1000 * 60 * 60 * 24 * 7));
            matchBean.setWeekNo(weeksBetween + 1);
            update(matchBean);   // this will be a bit slow in a loop like this, but it is a rare, one-time event
        }
    }


    public static void update(MatchBean match)
    {
        Connection connection = null;
        PreparedStatement ps = null;

        try
        {
            connection = DBBroker.getConnection();

            String sql = "update matches " +
                    "set yr = ?, sess = ?, division = ?, match_date = ?, match_no = ?, " +
                    "player1_id = ?, player2_id = ?, winner_id = ?," +
                    "player1_set1_games_won = ?, player1_set2_games_won = ?, player1_set3_games_won = ?," +
                    "player2_set1_games_won = ?, player2_set2_games_won = ?, player2_set3_games_won = ?," +
                    "is_set3_tiebreaker = ?, week_no = ?, custom_label = ?, " +
                    "note = ?, player1_note = ?, player2_note = ?, is_playoff = ?, is_default = ? " +
                    " where id = " + match.getId() + ";";

            ps = connection.prepareStatement(sql);
            int i = 1;

            ps.setInt(i++, match.getYr());
            ps.setInt(i++, match.getSess());
            ps.setString(i++, match.getDivision());
            ps.setString(i++, DBUtils.getDbFormatted24HrDateString(match.getMatchDate().getTime()));
            ps.setInt(i++, match.getMatchNo());
            ps.setInt(i++, match.getPlayer1Id());
            ps.setInt(i++, match.getPlayer2Id());
            ps.setInt(i++, match.getWinnerId());

            for (int p = 1; p &lt;= 2; p++)
            {
                for (int s = 1; s &lt;= 3; s++)
                {
                    ps.setInt(i++, match.getGamesWon(p, s));
                }
            }

            ps.setInt(i++, match.getSet3TiebreakerVal());
            ps.setInt(i++, match.getWeekNo());
            ps.setString(i++, match.getCustomLabel());
            ps.setString(i++, match.getNote());
            ps.setString(i++, match.getPlayer1Note());
            ps.setString(i++, match.getPlayer2Note());
            ps.setInt(i++, match.getIsPlayoffVal());
            ps.setInt(i++, match.getIsDefaultVal());

            ps.executeUpdate();

        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.out.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(null, ps, connection);
        }
    }


    public static void delete(int matchId)
    {
        Connection connection = null;
        PreparedStatement ps = null;

        try
        {
            connection = DBBroker.getConnection();

            String sql = "delete from matches where id = ?;";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, matchId);
            ps.executeUpdate();

        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.out.println(e.getMessage());
        }
        finally
        {
            DBUtils.cleanupDbConnectionAfterUse(null, ps, connection);
        }
    }


    public ArrayList&lt;MatchBean&gt; getRecords()
    {
        return records;
    }

}
</code></pre>

<div class="caption">
    Matches.java: This  "model" class represents rows in the singles league "matches" table.
    Each row (or record) is represented by a MatchBean class.  To fetch database records from code, a call
    is made to one of the Matches.createInstance methods.
</div>

</div>