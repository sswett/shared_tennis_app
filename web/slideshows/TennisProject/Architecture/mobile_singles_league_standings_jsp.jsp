<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-markup">
&lt;%@ page import="other.SessionHelper" %&gt;
&lt;%@ page import="java.util.ArrayList" %&gt;
&lt;%@ page import="beans.StandingsCalcBean" %&gt;
&lt;%@ page import="dao.StandingsCalc" %&gt;
&lt;%@ page import="other.MatchType" %&gt;
&lt;%
    String div = request.getParameter("div");

    if (div == null)
    {
        div = "4.0";
    }

    String matchTypeParm = request.getParameter("matchType");
    MatchType matchType = MatchType.ALL;   // default

    if (matchTypeParm != null)
    {
        try
        {
            matchType = MatchType.valueOf(matchTypeParm);
        }
        catch (Exception e)
        {
            // eat
        }
    }

%&gt;
&lt;!DOCTYPE html&gt;
&lt;html&gt;

&lt;head&gt;
    &lt;title&gt;Singles League Standings&lt;/title&gt;

    &lt;% if (SessionHelper.isMobileBrowser(request)) { %&gt;
    &lt;meta name="viewport" content="width=device-width" /&gt;
    &lt;% } %&gt;

    &lt;script src="js/jquery-1.10.2.min.js"&gt;&lt;/script&gt;
    
    &lt;jsp:include page="NavMenuHead.jsp" flush="true" /&gt;


    &lt;link href="css/Mobile.css&lt;%= SessionHelper.getVersionQueryString() %&gt;" rel="stylesheet"&gt;

    &lt;script&gt;

        function slsChangeMatchType(pElem)
        {
            var matchType = pElem.value;
            window.location='MobileSinglesLeagueStandings.page?div=&lt;%= div %&gt;&matchType=' + matchType;
        }

    &lt;/script&gt;

&lt;/head&gt;

&lt;body&gt;
    
    &lt;jsp:include page="NavMenuBody.jsp" flush="true" /&gt;

&lt;div id="mobileViewPageContainer"&gt;

&lt;div class="slStandingsSection"&gt;

    &lt;h3&gt;
        &lt;%= div %&gt; Div Standings

        &nbsp;

        &lt;select id="slsMatchTypeSelect" name="slsMatchTypeSelect" onchange="slsChangeMatchType(this)"&gt;
            &lt;%
                for (MatchType type : MatchType.values())
                {
                    final boolean isSelected = type == matchType;
            %&gt;
            &lt;option &lt;%= isSelected ? "selected=\"selected\"" : "" %&gt;
                    value="&lt;%= type.name() %&gt;" &gt;
                &lt;%= type.getLabel() %&gt;
            &lt;/option&gt;
            &lt;% }   // for MatchType %&gt;
        &lt;/select&gt;

    &lt;/h3&gt;

    &lt;div id="slsNotes"&gt;
        Rank calculation:&lt;br/&gt;
        Match %, Set %, Game %
    &lt;/div&gt;

    &lt;%
        ArrayList&lt;StandingsCalcBean&gt; standingsCalcs = StandingsCalc.createInstance(session.getId(), div, matchType).getRecords();

        for (StandingsCalcBean standingsCalc : standingsCalcs)
        {
 %&gt;

    &lt;div class="slsStanding"&gt;

        &lt;div class="slsStandingPlayer"&gt;

            &lt;div class="slsStandingCol1"&gt;
                # &lt;%= standingsCalc.getPlayerRank() %&gt;
            &lt;/div&gt;

            &lt;div class="slsStandingNameCol"&gt;
                &lt;a href="MobileSinglesLeaguePlayerCard.page?div=&lt;%= div %&gt;&playerId=&lt;%= standingsCalc.getPlayerId() %&gt;"&gt;
                        &lt;%= standingsCalc.getPlayerName() %&gt;
                &lt;/a&gt;
            &lt;/div&gt;

            &lt;div class="slsStandingCol5"&gt;&lt;/div&gt;

        &lt;/div&gt;

        &lt;div class="slsStandingSubheading"&gt;

            &lt;div class="slsStandingCol1"&gt;
                &nbsp;
            &lt;/div&gt;

            &lt;div class="slsStandingCol2"&gt;
                W
            &lt;/div&gt;

            &lt;div class="slsStandingCol3"&gt;
                L
            &lt;/div&gt;

            &lt;div class="slsStandingCol4"&gt;
                %
            &lt;/div&gt;

            &lt;div class="slsStandingCol5"&gt;&lt;/div&gt;

        &lt;/div&gt;

        &lt;div class="slsStandingMatches"&gt;

            &lt;div class="slsStandingCol1"&gt;
                Match:
            &lt;/div&gt;

            &lt;div class="slsStandingCol2"&gt;
                &lt;%= standingsCalc.getMatchesWon() %&gt;
            &lt;/div&gt;

            &lt;div class="slsStandingCol3"&gt;
                &lt;%= standingsCalc.getMatchesLost() %&gt;
            &lt;/div&gt;

            &lt;div class="slsStandingCol4"&gt;
                &lt;%= String.format("%.3f", standingsCalc.getMatchesWonPct()) %&gt;
            &lt;/div&gt;

            &lt;div class="slsStandingCol5"&gt;&lt;/div&gt;

        &lt;/div&gt;

        &lt;div class="slsStandingSets"&gt;

            &lt;div class="slsStandingCol1"&gt;
                Set:
            &lt;/div&gt;

            &lt;div class="slsStandingCol2"&gt;
                &lt;%= standingsCalc.getSetsWon() %&gt;
            &lt;/div&gt;

            &lt;div class="slsStandingCol3"&gt;
                &lt;%= standingsCalc.getSetsLost() %&gt;
            &lt;/div&gt;

            &lt;div class="slsStandingCol4"&gt;
                &lt;%= String.format("%.3f", standingsCalc.getSetsWonPct()) %&gt;
            &lt;/div&gt;

            &lt;div class="slsStandingCol5"&gt;&lt;/div&gt;

        &lt;/div&gt;

        &lt;div class="slsStandingGames"&gt;

            &lt;div class="slsStandingCol1"&gt;
                Game:
            &lt;/div&gt;

            &lt;div class="slsStandingCol2"&gt;
                &lt;%= String.format("%.2f", standingsCalc.getGamesWon()) %&gt;
            &lt;/div&gt;

            &lt;div class="slsStandingCol3"&gt;
                &lt;%= String.format("%.2f", standingsCalc.getGamesLost()) %&gt;
            &lt;/div&gt;

            &lt;div class="slsStandingCol4"&gt;
                &lt;%= String.format("%.3f", standingsCalc.getGamesWonPct()) %&gt;
            &lt;/div&gt;

            &lt;div class="slsStandingCol5"&gt;&lt;/div&gt;

        &lt;/div&gt;


    &lt;/div&gt;

    &lt;% } %&gt;

&lt;/div&gt;


&lt;/div&gt;

&lt;/body&gt;

&lt;/html&gt;
</code></pre>

<div class="caption">
    Here is a JSP "view" file for displaying singles league standings for mobile devices.  The key Java classes that
    it works with are the MobileSinglesLeagueStandings servlet and the StandingsCalc helper or business rules class.
</div>

</div>