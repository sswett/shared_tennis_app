<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-markup">
    &lt;!-- This filter is used to catch any exceptions not caught by individual servlets --&gt;
    &lt;filter&gt;
        &lt;filter-name&gt;CatchAnyExceptionFilter&lt;/filter-name&gt;
        &lt;filter-class&gt;other.CatchAnyExceptionFilter&lt;/filter-class&gt;
    &lt;/filter&gt;

    &lt;filter-mapping&gt;
        &lt;filter-name&gt;CatchAnyExceptionFilter&lt;/filter-name&gt;
        &lt;url-pattern&gt;*&lt;/url-pattern&gt;
    &lt;/filter-mapping&gt;


    &lt;!-- This filter is used to set the cache header(s) for dynamic pages --&gt;
    &lt;filter&gt;
        &lt;filter-name&gt;DynamicPagesResponseHeaderFilter&lt;/filter-name&gt;
        &lt;filter-class&gt;other.DynamicPagesResponseHeaderFilter&lt;/filter-class&gt;
    &lt;/filter&gt;

    &lt;filter-mapping&gt;
        &lt;filter-name&gt;DynamicPagesResponseHeaderFilter&lt;/filter-name&gt;
        &lt;url-pattern&gt;*.page&lt;/url-pattern&gt;
    &lt;/filter-mapping&gt;


    &lt;!-- This filter is used to set the cache header(s) for Javascript, CSS and image files --&gt;
    &lt;filter&gt;
        &lt;filter-name&gt;JsCssImgResponseHeaderFilter&lt;/filter-name&gt;
        &lt;filter-class&gt;other.JsCssImgResponseHeaderFilter&lt;/filter-class&gt;
    &lt;/filter&gt;

    &lt;filter-mapping&gt;
        &lt;filter-name&gt;JsCssImgResponseHeaderFilter&lt;/filter-name&gt;
        &lt;url-pattern&gt;*.js&lt;/url-pattern&gt;
        &lt;url-pattern&gt;*.css&lt;/url-pattern&gt;
        &lt;url-pattern&gt;*.png&lt;/url-pattern&gt;
        &lt;url-pattern&gt;*.jpg&lt;/url-pattern&gt;
    &lt;/filter-mapping&gt;
</code></pre>

<div class="caption">
    This XML markup shows the filters used in the web app's web.xml file.
</div>

</div>