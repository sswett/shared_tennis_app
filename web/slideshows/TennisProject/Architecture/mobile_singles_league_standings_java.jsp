<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-java">
    package servlets;

    import other.SessionHelper;

    import javax.servlet.RequestDispatcher;
    import javax.servlet.ServletException;
    import javax.servlet.http.HttpServlet;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;
    import javax.servlet.http.HttpSession;
    import java.io.IOException;


    public class MobileSinglesLeagueStandings extends HttpServlet
    {


        public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
        {
            HttpSession session = req.getSession();

            try
            {
                SessionHelper.ensureGroupDataLoadedInSession(session, req, res);
                RequestDispatcher rd = req.getRequestDispatcher("MobileSinglesLeagueStandings.jsp");
                rd.forward(req, res);
            }
            catch (Exception e)
            {
                SessionHelper.setErrorPageException(req, session, e);
                res.sendRedirect(SessionHelper.ERROR_PAGE_PATH);
            }
        }

    }
</code></pre>

<div class="caption">
    MobileSinglesLeagueStandings.java: The primary use of this servlet class is to map a URI to a JSP "view" file.
</div>

</div>