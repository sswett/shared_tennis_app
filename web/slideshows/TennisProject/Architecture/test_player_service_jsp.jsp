<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-markup">
&lt;%@ page import="other.RatingLevelType"%&gt;
&lt;%@ page import="other.SessionHelper"%&gt;
&lt;%
	final boolean isMobile = SessionHelper.isMobileBrowser(request);
%&gt;
&lt;!DOCTYPE html&gt;
&lt;html&gt;

&lt;head&gt;
&lt;title&gt;Test Player REST Service&lt;/title&gt;

&lt;% if (isMobile) { %&gt;
&lt;meta name="viewport" content="width=device-width" /&gt;
&lt;% } %&gt;

&lt;script src="js/jquery-1.10.2.min.js"&gt;&lt;/script&gt;

&lt;% if (isMobile) { %&gt;
&lt;link href="css/Mobile.css&lt;%= SessionHelper.getVersionQueryString() %&gt;"
	rel="stylesheet"&gt;
&lt;% } else { %&gt;
&lt;link href="css/Desktop.css&lt;%= SessionHelper.getVersionQueryString() %&gt;"
	rel="stylesheet"&gt;
&lt;% } %&gt;

	&lt;script&gt;

        $(document).ready(function() {
        	$("#ratingSelect").change(function() {
        		tpsRatingChanged();
        	});
        });
        
        
        function tpsRatingChanged()
        {
        	var ratingOptionValue = $("#ratingSelect").val();
        	
        	if (ratingOptionValue == "na")
            {
        		$("#playerListSection").html("");
            }
        	else
            {
        	    $.getJSON('/services_rest/player/rating/' + ratingOptionValue, function(players)
        	    		{
			    	        if (players == null)
			    	        {
			            		$("#playerListSection").html("");
			    	        }
			    	        else
        	    	        {
								var playerListHtml = "";
								
        	    	        	for (var i = 0; i &lt; players.length; i++)
							    {
									playerListHtml += players[i].name + " - " + players[i].ratingLevel + "&lt;br/&gt;";									
							    }
        	    	        	
        	            		$("#playerListSection").html(playerListHtml);
        	    	        }
        	    	    });
            }
        }

    &lt;/script&gt;


&lt;/head&gt;

&lt;body&gt;

	&lt;div id="tpsPageContainer"&gt;

		&lt;p&gt;When you choose or change the rating level below, the Player
			List will be updated without reloading the entire page. jQuery is
			used to make a GET request to a REST service, which returns a JSON
			response.&lt;/p&gt;

		&lt;form name="testPlayerServiceForm"&gt;

			&lt;label for="ratingSelect"&gt;Rating Level:&lt;/label&gt;

			&lt;% if (SessionHelper.isMobileBrowser(request)) { %&gt;
			&lt;br /&gt;
			&lt;% } %&gt;

			&lt;select id="ratingSelect" name="ratingSelect"&gt;

				&lt;option value="na"&gt;&lt;/option&gt;
				&lt;%-- n/a (nothing selected yet) --%&gt;

				&lt;%  for (RatingLevelType type : RatingLevelType.values()) { %&gt;
				&lt;option value="&lt;%= type.name() %&gt;"&gt;&lt;%= type.getRatingLevel() %&gt;&lt;/option&gt;
				&lt;% } %&gt;
			&lt;/select&gt;

		&lt;/form&gt;

		&lt;p&gt;Player List&lt;/p&gt;

		&lt;div id="playerListSection"&gt;&lt;/div&gt;

	&lt;/div&gt;

&lt;/body&gt;
	
&lt;/html&gt;
</code></pre>

<div class="caption">
    This TestPlayerService JSP file uses jQuery's "getJSON" method to make a request to "/services_rest/player/rating/[ratingLevel]".
    The returned array of JSON objects is used to populate the Player List section (3 of 3).
</div>

</div>