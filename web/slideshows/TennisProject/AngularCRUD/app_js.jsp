<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-javascript">
'use strict';

/* App Module */

var angularApp = angular.module('angularApp', [
                                               'ngRoute',
                                               'ngCookies',
                                               'angularAppControllers', 
                                               'angularAppServices'
                                               ]);


angularApp.config(['$routeProvider',
                   function($routeProvider) {
	$routeProvider.
	when('/contents', {
		templateUrl: 'Angular/partials/contents.html',
		controller: 'ContentsCtrl'
	}).
	when('/players', {
		templateUrl: 'Angular/partials/playerList.html',
		controller: 'PlayerListCtrl'
	}).
	when('/players/:playerId', {
		templateUrl: 'Angular/partials/playerDetails.html',
		controller: 'PlayerDetailsCtrl'
	}).
	when('/cookieTest', {
		templateUrl: 'Angular/partials/cookieTest.html',
		controller: 'CookieTestCtrl'
	}).
	when('/redirectTest', {
		templateUrl: 'Angular/partials/redirectTest.html',
		controller: 'RedirectTestCtrl'
	}).
	when('/clearanceTest', {
		templateUrl: 'Angular/partials/clearanceTest.html',
		controller: 'ClearanceTestCtrl'
	}).
	otherwise({
		redirectTo: '/contents'
	});
}]);


/*
 * NOTE: If I need to provide global handling for requests and responses, Angular uses "HTTP interceptors."  See the
 * following article: http://djds4rce.wordpress.com/2013/08/13/understanding-angular-http-interceptors/
 */ 
</code></pre>

<div class="caption">
    app.js: This file defines the 'angularApp' module and its dependencies on other services and controllers.
    The configuration section shows routing, including the partial template file and controller used for each route.
</div>

</div>