<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-markup">
&lt;!--  &lt;div class="container-fluid playerDetailsContainer"&gt; --&gt;

&lt;div ng-show="passedClearance" class="container ng-hide"&gt;

		&lt;div class="row"&gt;
		
		&lt;!--  Left Column (gap) --&gt;
		
		&lt;div class="col-sm-1 col-md-1"&gt;
		&lt;/div&gt;
		
		
		&lt;!--  Middle Column (content) --&gt;
		
		&lt;div class="col-sm-10 col-md-10"&gt;

			&lt;div class="col-sm-12 col-md-3"&gt;

				&lt;div class="sidebar row"&gt;
					&lt;h2&gt;{{ Page.title() }}&lt;/h2&gt;

					&lt;div class="sideContent"&gt;
						Player ID: {{player.id}} 
					&lt;/div&gt;

				&lt;/div&gt;

			&lt;/div&gt;


			&lt;div class="col-sm-12 col-md-9"&gt;

				&lt;form name="detailsForm" class="form-horizontal"&gt;

				&lt;!--  Name with validation/error markup --&gt;

				&lt;div class="form-group"&gt;
					&lt;label for="name" class="{{getLabelColumnClass()}}"&gt;Name:&lt;/label&gt;

					&lt;div class="{{getInputColumnClass()}}"&gt;
						&lt;input type="text" name="name" id="name" class="form-control" ng-model="player.name"
							ng-minlength=3 ng-maxlength=30 required&gt;

						&lt;div class="error"
							ng-show="(hasInvalidDataAtSave() || detailsForm.name.$dirty) && detailsForm.name.$invalid"&gt;
							&lt;small class="error" ng-show="detailsForm.name.$error.required"&gt;
								Name is required. &lt;/small&gt; &lt;small class="error"
								ng-show="detailsForm.name.$error.minlength"&gt; Name must
								be at least 3 characters &lt;/small&gt; &lt;small class="error"
								ng-show="detailsForm.name.$error.maxlength"&gt; Name cannot
								be longer than 30 characters &lt;/small&gt;
						&lt;/div&gt;

					&lt;/div&gt;
				&lt;/div&gt;
				

				&lt;!--  Email with validation/error markup --&gt;
				
				&lt;div class="form-group"&gt;

					&lt;label for="email" class="{{getLabelColumnClass()}}"&gt;Email:&lt;/label&gt;

					&lt;div class="{{getInputColumnClass()}}"&gt;
						&lt;input type="email" name="email" id="email" class="form-control" ng-model="player.email" required&gt;

						&lt;div class="error"
							ng-show="(hasInvalidDataAtSave() || detailsForm.email.$dirty) && detailsForm.email.$invalid"&gt;
							&lt;small class="error" ng-show="detailsForm.email.$error.required"&gt;
								Email is required. &lt;/small&gt; &lt;small class="error"
								ng-show="!detailsForm.email.$error.required && detailsForm.email.$invalid"&gt;
								Email is invalid. &lt;/small&gt;
						&lt;/div&gt;
					&lt;/div&gt;

				&lt;/div&gt;



				&lt;!--  Phone with validation/error markup --&gt;

				&lt;div class="form-group"&gt;

					&lt;label for="phone" class="{{getLabelColumnClass()}}"&gt;Phone:&lt;/label&gt;

					&lt;div class="{{getInputColumnClass()}}"&gt;
						&lt;input type="text" name="phone" id="phone" class="form-control" ng-model="player.phone"
							ng-pattern="/^\d+$/" ng-minlength=10 ng-maxlength=10 required&gt;

						&lt;div class="error"
							ng-show="(hasInvalidDataAtSave() || detailsForm.phone.$dirty) && detailsForm.phone.$invalid"&gt;
							&lt;small class="error" ng-show="detailsForm.phone.$error.required"&gt;
								Phone is required. &lt;/small&gt; &lt;small class="error"
								ng-show="!detailsForm.phone.$error.required && detailsForm.phone.$invalid"&gt;
								Phone is invalid. Must be exactly 10 digits with no punctuation.
							&lt;/small&gt;
						&lt;/div&gt;
					&lt;/div&gt;

				&lt;/div&gt;


				&lt;!--  Rating Level with validation/error markup.  Don't have a server-side db table to assist with this. --&gt;
				
				&lt;div class="form-group"&gt;

					&lt;label for="ratingLevel" class="{{getLabelColumnClass()}}"&gt;Rating Level:&lt;/label&gt;

					&lt;div class="{{getInputColumnClass()}}"&gt;
						&lt;select name="ratingLevel" id="ratingLevel" class="form-control" ng-model="player.ratingLevel" required&gt;
							&lt;option ng-selected="{{player.ratingLevel == '3.5'}}" value="3.5"&gt;3.5&lt;/option&gt;
							&lt;option ng-selected="{{player.ratingLevel == '4.0'}}" value="4.0"&gt;4.0&lt;/option&gt;
						&lt;/select&gt;

						&lt;div class="error"
							ng-show="(hasInvalidDataAtSave() || detailsForm.ratingLevel.$dirty) && detailsForm.ratingLevel.$invalid"&gt;
							&lt;small class="error"
								ng-show="detailsForm.ratingLevel.$error.required"&gt;
								Rating Level is required. &lt;/small&gt;
						&lt;/div&gt;
					&lt;/div&gt;

				&lt;/div&gt;



				&lt;!--  Carrier ID with validation/error markup.  Note: the db table includes a "blank carrier" option.   
				Note: an "ng-options" attribute is available, but read that it isn't fully functional unless value is String.
				Here it is integer, so I used an "option" tag with ng-repeat. --&gt;

				&lt;div class="form-group"&gt;

					&lt;label for="carrierId" class="{{getLabelColumnClass()}}"&gt;Carrier ID:&lt;/label&gt;

					&lt;div class="{{getInputColumnClass()}}"&gt;
						&lt;select name="carrierId" id="carrierId" class="form-control" ng-model="player.carrierId" required&gt;
							&lt;option ng-repeat="carrier in carriers"
								ng-selected="{{carrier.id == player.carrierId}}"
								value="{{carrier.id}}"&gt;{{carrier.name}}&lt;/option&gt;
						&lt;/select&gt;

						&lt;div class="error"
							ng-show="(hasInvalidDataAtSave() || detailsForm.carrierId.$dirty) && detailsForm.carrierId.$invalid"&gt;
							&lt;small class="error"
								ng-show="detailsForm.carrierId.$error.required"&gt; Carrier
								ID is required. &lt;/small&gt;
						&lt;/div&gt;
					&lt;/div&gt;

				&lt;/div&gt;


				&lt;!--  MVP Member: --&gt;

				&lt;div class="form-group"&gt;

					&lt;!--  Assigning the "outer label" to one of the radio buttons makes for good alignment --&gt;
					
					&lt;label for="mtNot" class="{{getLabelColumnClass()}}"&gt;MVP Member?&lt;/label&gt;

					&lt;div class="{{getInputColumnClass()}}"&gt;
					
						&lt;label class="radio-inline"&gt;
							&lt;input id="mtNot" type="radio" ng-model="player.mbrType" value="0"&gt; Not
						&lt;/label&gt;
						
						&lt;label class="radio-inline"&gt;
							&lt;input id="mtReg" type="radio"	ng-model="player.mbrType" value="1"&gt; Regular 
						&lt;/label&gt;

						&lt;label class="radio-inline"&gt;
							&lt;input id="mtAllStar" type="radio" ng-model="player.mbrType" value="2"&gt; All Star 
						&lt;/label&gt;
						
					&lt;/div&gt;

				&lt;/div&gt;


				&lt;!--  Is Administrator? --&gt;

				&lt;div class="form-group"&gt;

					&lt;label for="isAdmin" class="{{getLabelColumnClass()}}"&gt;Administrator?&lt;/label&gt;

					&lt;div class="{{getInputColumnClass()}}"&gt;
						&lt;div class="checkbox"&gt;   &lt;!-- centers the checkbox vertically  --&gt;
							&lt;input id="isAdmin" type="checkbox" ng-model="player.isAdmin"&gt;
						&lt;/div&gt;
					&lt;/div&gt;

				&lt;/div&gt;


				&lt;!--  Is Player Disabled (de-activated)? --&gt;
				
				&lt;div class="form-group"&gt;

					&lt;label for="isDisabled" class="{{getLabelColumnClass()}}"&gt;Disabled?&lt;/label&gt;

					&lt;div class="{{getInputColumnClass()}}"&gt;
						&lt;div class="checkbox"&gt;   &lt;!-- centers the checkbox vertically  --&gt;
							&lt;input id="isDisabled" type="checkbox" ng-model="player.disabled"&gt;
						&lt;/div&gt;
					&lt;/div&gt;

				&lt;/div&gt;


				&lt;!--  Action Buttons/Links --&gt;

				&lt;div class="form-group"&gt;

					&lt;div class="{{getLabelColumnClass()}}"&gt;&lt;/div&gt;

					&lt;div class="{{getInputColumnClass()}}"&gt;

						&lt;!-- Could apply following on the Save link, but don't think I want to:  ng-disabled="detailsForm.$invalid" --&gt;
						&lt;a class="actionLink" href="" ng-click="savePlayer()"&gt;Save&lt;/a&gt; &lt;a
							class="actionLink" href="" ng-show="player.id &gt; 0"
							ng-click="deletePlayer()"&gt;Delete&lt;/a&gt; &lt;a class="actionLink"
							href="" ng-click="goToList()"&gt;Cancel&lt;/a&gt;

					&lt;/div&gt;

				&lt;/div&gt;


				&lt;/form&gt;

			&lt;/div&gt;


		&lt;/div&gt;   &lt;!--  end of middle column (content) --&gt;
		
		&lt;!--  Right Column (gap) --&gt;
		
		&lt;div class="col-sm-1 col-md-1"&gt;
		&lt;/div&gt;

		&lt;/div&gt;

&lt;/div&gt;
</code></pre>

<div class="caption">
    playerDetails.html: This is an example of a partial page/template which fills the 'ng-view' div of the AngularApp.html page.
    Note the use of rows and columns for Bootstrap responsive layout. Also note the use of a variety of Angular directives (ng-xxx).
</div>

</div>