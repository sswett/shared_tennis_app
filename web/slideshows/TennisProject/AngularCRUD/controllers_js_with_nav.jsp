<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-javascript">
'use strict';

/* Controllers */

var angularAppControllers = angular.module('angularAppControllers', []);


angularAppControllers.controller('PageCtrl', ['$scope', 'Page', 
                                              function($scope, Page) {
	
	$scope.Page = Page;
	
}]);


angularAppControllers.controller('NavCtrl', ['$scope', 'Page',  
                                              function($scope, Page) {
	
	$scope.isMobile = Page.isMobile();
	$scope.dropDownMenuWidth = $scope.isMobile ? 80 : 150;
	$scope.openOnClick = $scope.isMobile ? true : false;   // desktop: use false
	$scope.closeOnMouseOut = $scope.isMobile ? false : true;   // desktop: use true

	// MatchType.REGULAR_SEASON.name(); hard-wire for now:
	$scope.matchTypeRegularSeasonName = "REGULAR_SEASON";
	
}]);


angularAppControllers.controller('ContentsCtrl', ['$scope', 'Page', 
                                                  function($scope, Page) {
	Page.setTitle("Table of Contents");
}]);


angularAppControllers.controller('RedirectTestCtrl', ['$scope', '$window', 'Page', 
                                                      function($scope, $window, Page) {
	
	Page.setTitle("Redirect Test");
	
	var doRedirect = true;
	
	if (doRedirect)
	{
		$window.location.replace("AdminClearance.page");
	}

}]);


angularAppControllers.controller('CookieTestCtrl', ['$scope', '$cookieStore', 'Page', 
                                                    function($scope, $cookieStore, Page) {
	
	Page.setTitle("Cookie Test");

	$scope.cookieKey = "one";
	$scope.cookieValue = "1";
	
	$scope.showSidebar = function() {
		// If set to false, Angular definitely shows it momentarily before hiding it (not good).  Use class "ng-hide" on element
		// to make it INITIALLY hidden, but still call a "showXXX" method to make dynamic.
		// return false;   
		return true;
	}
	
	$scope.storeCookieValue = function() {
		$cookieStore.put($scope.cookieKey, $scope.cookieValue);
	}
	
	$scope.getStoredCookieValue = function() {
		return $cookieStore.get($scope.cookieKey);
	}
	
}]);


angularAppControllers.controller('ClearanceTestCtrl', ['$scope', '$http', 'Page', 'ClearanceCheck', 
                                                       function($scope, $http, Page, ClearanceCheck) {
	$scope.passedClearance = false;
	Page.setTitle("Admin Clearance Test");
	
	// Code to run if clearance passes:
	ClearanceCheck.hasAdminClearance(function() {
		$scope.passedClearance = true;
	});
	
}]);


angularAppControllers.controller('PlayerListCtrl', ['$scope', '$location', '$window', 'Players', 'Page', 'ClearanceCheck', 
                                                    function($scope, $location, $window, Players, Page, ClearanceCheck) {

	$scope.passedClearance = false;
	Page.setTitle("Player List");
	var MAX_PLAYERS_PER_COL = 15;
	$scope.players = {};
	
	// Continue init if clearance passes:
	ClearanceCheck.hasAdminClearance(function() {
		$scope.players = Players.query();
		$scope.passedClearance = true;
	});
	
	// Function defs:
	
	$scope.getSidebarColumnMedDeviceClass = function()
	{
		if ($scope.showOneColumn())
		{
			return "col-md-6";
		}
		else if ($scope.showTwoColumns())
		{
			return "col-md-4";
		}
		else if ($scope.showThreeColumns())
		{
			return "col-md-3";
		}
		else
	    {
			return "col-md-3";   // default
	    }
	}
	
	$scope.showOneColumn = function()
	{
		return $scope.players.length &lt;= MAX_PLAYERS_PER_COL;
	}
	
	$scope.showTwoColumns = function()
	{
		return $scope.players.length &gt; MAX_PLAYERS_PER_COL && $scope.players.length &lt;= MAX_PLAYERS_PER_COL * 2;
	}
	
	$scope.showThreeColumns = function()
	{
		return $scope.players.length &gt; MAX_PLAYERS_PER_COL * 2;
	}
	
	$scope.getListMidPoint = function()
	{
		return Math.round($scope.players.length / 2);
	}
	
	$scope.getListLowThirdPoint = function()
	{
		return Math.round($scope.players.length / 3);
	}
	
	$scope.getListHighThirdPoint = function()
	{
		return $scope.getListLowThirdPoint() * 2;
	}
	
  }]);


angularAppControllers.controller('PlayerDetailsCtrl', ['$scope', '$routeParams', '$location', 'Players', 'Carriers', 'Page', 'ClearanceCheck',
  function($scope, $routeParams, $location, Players, Carriers, Page, ClearanceCheck) {
	
	$scope.passedClearance = false;
	Page.setTitle("Player Detail");	
	$scope.invalidDataAtSave = false;
	$scope.player = {};
	$scope.carriers = {};
	
	
	// Continue init if clearance passes:
	ClearanceCheck.hasAdminClearance(function() {

		if ($routeParams.playerId == "0")
	    {
			$scope.player = new Players();
			$scope.player.id = 0;
			$scope.player.disabled = false;
			$scope.player.isAdmin = false;
			$scope.player.mbrType = 0;
	    }
		else
		{
			// In below, the "id:" part corresponds to the "id" parm in the services.js file
			
			$scope.player = Players.get({id: $routeParams.playerId}, function(player) {
		    	// Callback after get: could do something with "player" object here, if needed
		    });
		}
		
		$scope.carriers = Carriers.query();

		$scope.passedClearance = true;
	});

	
	// Function defs:
	
	$scope.hasInvalidDataAtSave = function() {
		return $scope.invalidDataAtSave;
	}
	
	// Save a player -- whether new or existing
	$scope.savePlayer = function() {
		
		if ($scope.detailsForm.$invalid)
		{
			$scope.invalidDataAtSave = true;
			return;
		}
		
		if ($scope.player.id == 0)
	    {
			Players.save($scope.player, function(createdPlayer) {
			    // Callback after save:
				$scope.player.id = createdPlayer.id;
				$scope.goToList();
			  });
	    }
		else
	    {
			$scope.player.$update(function() {
			    // Callback after update:
				$scope.goToList();
			});
	    }
	}
	
	$scope.deletePlayer = function() {
		$scope.player.$delete(function() {
		    // Callback after delete:
			$scope.goToList();
		  });
	}
	
	$scope.goToList = function() {
		$location.path( "/players/");
	}
	
  }]);
  </code></pre>

<div class="caption">
    controllers.js: The controller named NavCtrl was added near line 17.  Its main purpose is to set three scope
    variables which affect the behavior of the nav menu.
    
</div>

</div>