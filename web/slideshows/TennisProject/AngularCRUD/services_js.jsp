<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-javascript">
'use strict';

/* Services */

var angularAppServices = angular.module('angularAppServices', ['ngResource']);


angularAppServices.factory('Page', function() {
	   var title = 'Angular App';
	   
	   return {
	     title: function() { return title; },
	     setTitle: function(newTitle) { title = newTitle }
	   };
	   
	});


angularAppServices.factory('ClearanceCheck', ['$http', '$location', '$window', 
                                              function($http, $location, $window) {

	return {		
		hasAdminClearance: function(passedClearanceCallback) {
			return $http.post('services_rest/admincheck/pageafteruri', {string: $location.absUrl()})
			.success(function(response) {
				if (response == "true")
				{
					passedClearanceCallback();
				}
				else
				{
					$window.location.replace("AdminClearance.page");
				}
			})
			.error(function() {
				// service call failed, so in future give error page and redirect to index page or elsewhere
			})
		}
	};
	
}]);


angularAppServices.factory('Players', ['$resource',
  function($resource){

	/*
	 * The following technique seemed best.  It came from: 
	 *   http://www.sitepoint.com/creating-crud-app-minutes-angulars-resource/
	 * 
	 * In below, notice two things:
	 *   1. The URL always includes all the way through ":id"
	 *   2. The "@id" is a pointer indicating that the resource id is stored in a "field" named "id".  Online documentation
	 *      mentioned field "_id" -- but I didn't have that in my POJO bean, so I changed it to "id".
	 */
	
	return $resource('services_rest/players/:id', { id: '@id' }, {
	    update: {
	      method: 'PUT' // this method issues a PUT request
	    }
	  });
	
  }]);


angularAppServices.factory('Carriers', ['$resource',
  function($resource){
	
	return $resource('services_rest/carriers/:id', { id: '@id' }, {
	    update: {
	      method: 'PUT' // this method issues a PUT request
	    }
	  });
	
  }]);
</code></pre>

<div class="caption">
    services.js: The ClearanceCheck service uses $http and a RESTful API to check for admin clearance. If no clearance, redirects 
    browser to legacy page to establish clearance.  The Players and Carriers services use $resource to work with backend resources
    via RESTful APIs.
</div>

</div>