<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-java">
package services_rest;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.PlayerBean;
import crud.dao.PlayerDAO;
import dao.Players;

@Path("/players")

public class PlayerResource 
{
	
	// Get all records:
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })

	public ArrayList&lt;PlayerBean&gt; getAllPlayers() 
	{
		return Players.createInstance(true).getRecords();
	}

	
	// Create new record:
	@POST
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response createPlayer(final PlayerBean playerBean)
	{
		crud.beans.PlayerBean crudBean = new crud.beans.PlayerBean(playerBean);
		crud.beans.PlayerBean existingPlayerBean = PlayerDAO.getPlayerBeanById(playerBean.getId());
		
		if (existingPlayerBean == null)
		{
			int newId = PlayerDAO.insertNewPlayerBeanReturnNewKey(crudBean);
			
			// SQLite will auto-generate the id, so need to reflect that in response:
			playerBean.setId(newId);
		}
		else
		{
			PlayerDAO.updatePlayerBean(crudBean);
		}
		
		return Response.ok(playerBean).build();
	}

	
	// Get one record:
	@GET @Path("/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })

	public PlayerBean getPlayer(@PathParam("id") int id) 
	{
		return Players.createInstance(id).getRecords().get(0);
	}

	
	// Update existing record:
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response updatePlayer(final PlayerBean playerBean)
	{
		crud.beans.PlayerBean crudBean = new crud.beans.PlayerBean(playerBean);
		crud.beans.PlayerBean existingPlayerBean = PlayerDAO.getPlayerBeanById(playerBean.getId());
		
		if (existingPlayerBean == null)
		{
			PlayerDAO.insertNewPlayerBean(crudBean);
		}
		else
		{
			PlayerDAO.updatePlayerBean(crudBean);
		}
		
		return Response.ok(playerBean).build();
	}
	
	
	// Delete existing record:
	@DELETE 
	@Path("/{id}")

	public Response deletePlayer(@PathParam("id") int id) 
	{
		crud.beans.PlayerBean existingPlayerBean = PlayerDAO.getPlayerBeanById(id);
		
		if (existingPlayerBean != null)
		{
			PlayerDAO.deletePlayerBean(existingPlayerBean);
		}
		
		return Response.ok().build();
	}

}
</code></pre>

<div class="caption">
    PlayerResource.java: This RESTful API provides methods for getting player data in 
    JSON fmt. It also provides methods to create, update and delete a player.  The API uses a 
    JAXB annotated PlayerBean. Auth checking is absent here because it is handled in a central servlet filter instead.
</div>

</div>