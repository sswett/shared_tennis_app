<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-markup">
&lt;div ng-show="isMobile"&gt;

&lt;link rel="stylesheet" href="js/jquery.mb.menu-2.9.7/css/menu_ss_mobile.css" media="screen" /&gt;

	&lt;div class="nmMenuBar"&gt;
		&lt;table class="rootVoices"&gt;
			&lt;tr&gt;
				&lt;td class="rootVoice {menu: 'nmMobileEventsMenu'}"&gt;Events&lt;/td&gt;
				&lt;td class="rootVoice {menu: 'nmMobileSinglesMenu'}"&gt;Singles&lt;/td&gt;
				&lt;td class="rootVoice {menu: 'nmMobileUSTAMenu'}"&gt;USTA&lt;/td&gt;
				&lt;td class="rootVoice {menu: 'nmMobileSlideshowMenu'}"&gt;Slideshows&lt;/td&gt;
		&lt;/table&gt;
	&lt;/div&gt;



	&lt;div id="nmMobileEventsMenu" class="menu"&gt;
		&lt;a href="MobileGroupView.page"&gt;Group/Team View&lt;/a&gt; &lt;a
			href="MobilePlayerView.page"&gt;Player View&lt;/a&gt; &lt;a
			href="MobilePlayerAllTeamsView.page"&gt;Player All Teams View&lt;/a&gt; &lt;a
			href="MobileOptions.page"&gt;Mobile Options&lt;/a&gt;
	&lt;/div&gt;


	&lt;div id="nmMobileSinglesMenu" class="menu"&gt;
		&lt;a href="SinglesLeagueGuidelines.page"&gt;Guidelines&lt;/a&gt; &lt;a
			menu="nmMobileSingles35Menu"&gt;3.5&lt;/a&gt; &lt;a menu="nmMobileSingles40Menu"&gt;4.0&lt;/a&gt;
	&lt;/div&gt;


	&lt;div id="nmMobileSingles35Menu" class="menu"&gt;
		&lt;a href="MobileSinglesLeaguePlayerList.page?div=3.5"&gt;Player Cards&lt;/a&gt;
		&lt;a
			href="MobileSinglesLeagueStandings.page?div=3.5&matchType={{ matchTypeRegularSeasonName }}"&gt;Standings&lt;/a&gt;
		&lt;a href="MobileSinglesLeagueContacts.page?div=3.5"&gt;Player Contact
			Info&lt;/a&gt; &lt;a href="MobileSinglesLeagueSchedule.page?div=3.5"&gt;Schedule
			& Results&lt;/a&gt;
	&lt;/div&gt;


	&lt;div id="nmMobileSingles40Menu" class="menu"&gt;
		&lt;a href="MobileSinglesLeaguePlayerList.page?div=4.0"&gt;Player Cards&lt;/a&gt;
		&lt;a
			href="MobileSinglesLeagueStandings.page?div=4.0&matchType={{ matchTypeRegularSeasonName }}"&gt;Standings&lt;/a&gt;
		&lt;a href="MobileSinglesLeagueContacts.page?div=4.0"&gt;Player Contact
			Info&lt;/a&gt; &lt;a href="MobileSinglesLeagueSchedule.page?div=4.0"&gt;Schedule
			& Results&lt;/a&gt;
	&lt;/div&gt;


	&lt;div id="nmMobileUSTAMenu" class="menu"&gt;
		&lt;a
			href="http://m.tennislink.usta.com/statsandstandings?search=8558008208&t=R-17"&gt;Thunder
			(3.5)&lt;/a&gt; &lt;a
			href="http://m.tennislink.usta.com/statsandstandings?search=8558008305&t=R-17"&gt;Lightning
			(3.5)&lt;/a&gt; &lt;a
			href="http://m.tennislink.usta.com/statsandstandings?search=8558008204&t=R-17"&gt;Just4Fuzz
			(4.0)&lt;/a&gt; &lt;a href="http://m.tennislink.usta.com"&gt;Tennis Link&lt;/a&gt;

	&lt;/div&gt;


	&lt;div id="nmMobileSlideshowMenu" class="menu"&gt;
		&lt;a href="/TravelTeam2013StatesSlideshow.page"&gt;3.5 2013 States&lt;/a&gt; &lt;a
			href="/TravelTeam2014DistrictsSlideshow.page"&gt;3.5 2014 Districts&lt;/a&gt;
		&lt;a href="/TravelTeam2014StatesSlideshow.page"&gt;3.5 2014 States&lt;/a&gt;
	&lt;/div&gt;

&lt;/div&gt;


&lt;div ng-show="!isMobile"&gt;

&lt;link rel="stylesheet" href="js/jquery.mb.menu-2.9.7/css/menu_ss_desktop.css" media="screen" /&gt;


	&lt;div class="nmMenuBar"&gt;
		&lt;table class="rootVoices"&gt;
			&lt;tr&gt;
				&lt;td class="rootVoice {menu: 'nmAdminMenu'}"&gt;Admin&lt;/td&gt;
				&lt;td class="rootVoice {menu: 'nmEventsMenu'}"&gt;Events&lt;/td&gt;
				&lt;td class="rootVoice {menu: 'nmSinglesMenu'}"&gt;Singles&lt;/td&gt;
				&lt;td class="rootVoice {menu: 'nmUSTAMenu'}"&gt;USTA&lt;/td&gt;
				&lt;td class="rootVoice {menu: 'nmSlideshowMenu'}"&gt;Slideshows&lt;/td&gt;
		&lt;/table&gt;
	&lt;/div&gt;


	&lt;div id="nmAdminMenu" class="menu"&gt;
		&lt;a href="GroupsDisplayRecords.page"&gt;Edit Groups/Teams&lt;/a&gt; &lt;a
			href="PlayerDisplayRecords.page"&gt;Edit Players&lt;/a&gt; &lt;a
			href="EditGroupMembers.page"&gt;Edit Group/Team Members&lt;/a&gt; &lt;a
			href="EventDisplayRecords.page"&gt;Edit Events (practices, matches,
			etc.)&lt;/a&gt; &lt;a href="EditMatchList.page?div=4.0"&gt;Edit 4.0 Singles Match
			List&lt;/a&gt; &lt;a href="EditMatchList.page?div=3.5"&gt;Edit 3.5 Singles Match
			List&lt;/a&gt;

	&lt;/div&gt;


	&lt;div id="nmSinglesMenu" class="menu"&gt;
		&lt;a href="SinglesLeagueGuidelines.page"&gt;Guidelines&lt;/a&gt; &lt;a
			menu="nmSingles35Menu"&gt;3.5&lt;/a&gt; &lt;a menu="nmSingles40Menu"&gt;4.0&lt;/a&gt;
	&lt;/div&gt;


	&lt;div id="nmSingles35Menu" class="menu"&gt;
		&lt;a href="SinglesLeaguePlayerList.page?div=3.5"&gt;Player Cards&lt;/a&gt; &lt;a
			href="SinglesLeagueStandings.page?div=3.5&matchType={{ matchTypeRegularSeasonName }}"&gt;Standings&lt;/a&gt;
		&lt;a href="SinglesLeagueContacts.page?div=3.5"&gt;Player Contact Info&lt;/a&gt; &lt;a
			href="SinglesLeagueSchedule.page?div=3.5"&gt;Schedule & Results&lt;/a&gt;
	&lt;/div&gt;


	&lt;div id="nmSingles40Menu" class="menu"&gt;
		&lt;a href="SinglesLeaguePlayerList.page?div=4.0"&gt;Player Cards&lt;/a&gt; &lt;a
			href="SinglesLeagueStandings.page?div=4.0&matchType={{ matchTypeRegularSeasonName }}"&gt;Standings&lt;/a&gt;
		&lt;a href="SinglesLeagueContacts.page?div=4.0"&gt;Player Contact Info&lt;/a&gt; &lt;a
			href="SinglesLeagueSchedule.page?div=4.0"&gt;Schedule & Results&lt;/a&gt;
	&lt;/div&gt;


	&lt;div id="nmUSTAMenu" class="menu"&gt;
		&lt;a
			href="http://tennislink.usta.com/Leagues/Main/StatsAndStandings.aspx?t=T-0&par1=SRrjJZ8fewlcXK7%2bv7O4vg%3d%3d&e=1#&&s=3%7c%7c0%7c%7c8558008208%7c%7c2015"&gt;Thunder
			(3.5)&lt;/a&gt; &lt;a
			href="http://tennislink.usta.com/Leagues/Main/StatsAndStandings.aspx?par1=jttxreWH4fh5nP06%2ficMyg%3d%3d&e=1#&&s=3%7c%7c0%7c%7c8558008305%7c%7c2015"&gt;Lightning
			(3.5)&lt;/a&gt; &lt;a
			href="http://tennislink.usta.com/Leagues/Main/StatsAndStandings.aspx?t=T-0&par1=s1qtWUQrh770AujYn2N7sA%3d%3d&e=1#&&s=3%7c%7c0%7c%7c8558008204%7c%7c2015"&gt;Just4Fuzz
			(4.0)&lt;/a&gt; &lt;a
			href="http://www.westernmichigan.usta.com/USTA_League_Tennis/home/"&gt;West
			MI USTA&lt;/a&gt; &lt;a
			href="http://tennislink.usta.com/Leagues/Common/Default.aspx"&gt;Tennis
			Link&lt;/a&gt;

	&lt;/div&gt;


	&lt;div id="nmEventsMenu" class="menu"&gt;
		&lt;a href="Spreadsheet.page"&gt;Availability for Practices, Matches,
			etc.&lt;/a&gt;
	&lt;/div&gt;


	&lt;div id="nmSlideshowMenu" class="menu"&gt;
		&lt;a href="/TravelTeam2013StatesSlideshow.page"&gt;3.5 2013 States&lt;/a&gt; &lt;a
			href="/TravelTeam2014DistrictsSlideshow.page"&gt;3.5 2014 Districts&lt;/a&gt;
		&lt;a href="/TravelTeam2014StatesSlideshow.page"&gt;3.5 2014 States&lt;/a&gt;
	&lt;/div&gt;

&lt;/div&gt;
</code></pre>

<div class="caption">
    navMenu.html: This HTML fragment contains markup in the format required by the Pupunzi 'jquery.mb.menu' tool.
    In summary, AngularJS 'ng-include' and custom directives were used to bring in a pre-existing nav menu dependent on a 3rd
    party jQuery library. 
</div>

</div>