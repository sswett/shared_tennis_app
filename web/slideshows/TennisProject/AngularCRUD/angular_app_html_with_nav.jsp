<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-markup">
 &lt;!doctype html&gt;
&lt;html lang="en" ng-app="angularApp" ng-controller="PageCtrl"&gt;

&lt;head&gt;
&lt;meta charset="utf-8"&gt;
&lt;title&gt;{{ Page.title() }}&lt;/title&gt;

&lt;link rel="stylesheet" href="Angular/bootstrap/dist/css/bootstrap.css"&gt;
&lt;link rel="stylesheet" href="Angular/css/app.css"&gt;

&lt;script src="Angular/js/angular.js"&gt;&lt;/script&gt;
&lt;script src="Angular/js/angular-route.js"&gt;&lt;/script&gt;
&lt;script src="Angular/js/angular-resource.js"&gt;&lt;/script&gt;

&lt;!-- TODO: determine whether best to download and use local copies on server or point to Google. --&gt;

&lt;script	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.3/angular-cookies.js"&gt;&lt;/script&gt;

	
&lt;!--  BEGIN: Navigation scripts --&gt;

    &lt;script src="js/jquery-1.10.2.min.js"&gt;&lt;/script&gt;
	&lt;script src="js/jquery.mb.menu-2.9.7/inc/jquery.metadata.js"&gt;&lt;/script&gt;
	&lt;script src="js/jquery.mb.menu-2.9.7/inc/jquery.hoverIntent.js"&gt;&lt;/script&gt;
	&lt;script src="js/jquery.mb.menu-2.9.7/inc/mbMenu.js"&gt;&lt;/script&gt;
	&lt;script src="Angular/js/legacyFunctions.js"&gt;&lt;/script&gt;

&lt;!--  END: Navigation scripts --&gt;

	
&lt;script src="Angular/js/app.js"&gt;&lt;/script&gt;
&lt;script src="Angular/js/controllers.js"&gt;&lt;/script&gt;
&lt;script src="Angular/js/services.js"&gt;&lt;/script&gt;
&lt;/head&gt;

&lt;body&gt;

	&lt;div ng-controller="NavCtrl"&gt;
		&lt;div ng-include="'Angular/partials/navMenu.html'" ta-bind-nav-menu&gt;
		&lt;/div&gt;
	&lt;/div&gt;

	&lt;div ng-view&gt;&lt;/div&gt;

&lt;/body&gt;

&lt;/html&gt;
</code></pre>

<div class="caption">
    AngularApp.html: Near line 23, third party Javascript libs and dependencies 
    were added. Near line 40, the 'ng-include' directive was used to bring in the nav menu's view along with 
    its NavCtrl controller. Also note the 'ta-bind-nav-menu' custom directive (explained soon). 
</div>

</div>