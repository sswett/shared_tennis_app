<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-markup">
&lt;!doctype html&gt;
&lt;html lang="en" ng-app="angularApp" ng-controller="PageCtrl"&gt;

&lt;head&gt;
&lt;meta charset="utf-8"&gt;
&lt;title&gt;{{ Page.title() }}&lt;/title&gt;
&lt;link rel="stylesheet" href="Angular/bootstrap/dist/css/bootstrap.css"&gt;
&lt;link rel="stylesheet" href="Angular/css/app.css"&gt;
&lt;script src="Angular/js/angular.js"&gt;&lt;/script&gt;
&lt;script src="Angular/js/angular-route.js"&gt;&lt;/script&gt;
&lt;script src="Angular/js/angular-resource.js"&gt;&lt;/script&gt;

&lt;!-- TODO: determine whether best to download and use local copies on server or point to Google. --&gt;

&lt;script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.3/angular-cookies.js"&gt;&lt;/script&gt;
&lt;script src="Angular/js/app.js"&gt;&lt;/script&gt;
&lt;script src="Angular/js/controllers.js"&gt;&lt;/script&gt;
&lt;script src="Angular/js/services.js"&gt;&lt;/script&gt;
&lt;/head&gt;

&lt;body&gt;

	&lt;div ng-view&gt;&lt;/div&gt;

&lt;/body&gt;

&lt;/html&gt;
</code></pre>

<div class="caption">
    AngularApp.html: This is the only full page. It is used to load CSS and Javascript resources as well as to setup the page
    'framework'.  Note that the 'angularApp' and 'PageCtrl' are bound to the html element.  During navigation, 
    actual page content will fill the div with the 'ng-view' directive.
</div>

</div>