<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-java">
package beans;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import other.MemberType;


// In order to do automatic JAXB mapping, need public paramter-less constructor and a complete set of public getters/setters. 

@XmlRootElement
@XmlType(propOrder = { "id", "name", "disabled", "phone", "email", "isAdmin", "carrierId", "ratingLevel", "mbrType" })
public class PlayerBean
{
    private int id;
    private String name;
    private boolean disabled;
    private String phone;
    private String email;
    private boolean isAdmin;
    private int carrierId;
    private String ratingLevel;
    private int mbrType;


    public PlayerBean(int id,
                      String name,
                      boolean disabled,
                      String phone,
                      String email,
                      boolean is_admin,
                      int carrier_id,
                      String rating_level,
                      int mbr_type)
    {
        this.id = id;
        this.name = name;
        this.disabled = disabled;
        this.phone = phone;
        this.email = email;
        this.isAdmin = is_admin;
        this.carrierId = carrier_id;
        this.ratingLevel = rating_level;
        this.mbrType = mbr_type;
    }
    
    
    public PlayerBean()
    {
    	
    }
    
    
    public void setId(int id)
    {
    	this.id = id;
    }
    
    
    public void setName(String name)
    {
    	this.name = name;
    }
    
    
    public void setDisabled(boolean disabled)
    {
    	this.disabled = disabled;
    }
    
    
    public void setPhone(String phone)
    {
    	this.phone = phone;
    }
    
    
    public void setEmail(String email)
    {
    	this.email = email;
    }
    
    
    public void setIsAdmin(boolean isAdmin)
    {
    	this.isAdmin = isAdmin;
    }
    
    
    public void setCarrierId(int carrierId)
    {
    	this.carrierId = carrierId;
    }
    
    
    public void setRatingLevel(String ratingLevel)
    {
    	this.ratingLevel = ratingLevel;
    }
    
    
    public void setMbrType(int mbrType)
    {
    	this.mbrType = mbrType;
    }


    public int getId()
    {
        return this.id;
    }


    public String getName()
    {
        return name;
    }


    public boolean getDisabled()
    {
        return disabled;
    }


    public boolean getIsAdmin()
    {
        return isAdmin;
    }


    public int getCarrierId()
    {
        return carrierId;
    }


    public int getMbrType()
    {
        return mbrType;
    }


    public String getPhone()
    {
        return phone;
    }


    public String getFormattedPhone()
    {
        if (phone.length() &lt; 7)
        {
            return phone;
        }

        return String.format("%s-%s-%s",
                phone.substring(0,3),
                phone.substring(3,6),
                phone.substring(6)
        );
    }


    public String getEmail()
    {
        return email;
    }


    public String getRatingLevel()
    {
        return ratingLevel;
    }


    public String getMbrTypeDescr()
    {
        return MemberType.values()[mbrType].getMbrTypeDescr();
    }

}
</code></pre>

<div class="caption">
    PlayerBean.java: This bean class is used for automatic object-to-XML and object-to-JSON mapping using JAXB and MOXY.  The
    bean is used by the RESTful APIs for service request/response I/O.
</div>

</div>