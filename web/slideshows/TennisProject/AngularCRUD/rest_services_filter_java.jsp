<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-java">
package other;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RestServicesFilter implements Filter 
{

    public void init(FilterConfig fc) throws ServletException
    {
    }


    public void doFilter(ServletRequest req,
                         ServletResponse res,
                         FilterChain fc) throws IOException, ServletException
    {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        
		final String serviceURI = request.getPathInfo();
		
		if (!serviceURI.startsWith("/admincheck/") && !serviceURI.startsWith("/player/"))
		{
	        HttpSession session = request.getSession();
	        
	        if (!SessionHelper.hasPassedAdminSecurityClearance(session, request))
	        {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED);   // 401
				return;
	        }
		}
        
        // CORS - Cross Origin Resource Sharing: allow service requests from other domains -- 
        // such as local development domains hosting AngularJS projects.
        
		response.setHeader("Access-Control-Allow-Origin", "http://localhost.domain.com:8080");
		
		response.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
		// response.setHeader("Access-Control-Allow-Methods", "*");
		
		response.setHeader("Access-Control-Allow-Headers", "Content-Type, X-Requested-With");

        fc.doFilter(req, res);
    }


    public void destroy()
    {
    }

}
</code></pre>

<div class="caption">
    RestServicesFilter.java: This servlet filter is applied to all requests for RESTful API services. It rejects
    unauthorized requests and allows a certain amount of cross origin resource sharing.
</div>

</div>