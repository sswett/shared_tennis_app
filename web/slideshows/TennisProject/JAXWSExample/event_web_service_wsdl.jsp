<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-markup">

&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;wsdl:definitions xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ns1="http://org.apache.axis2/xsd" xmlns:ns="http://services_web" xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" targetNamespace="http://services_web"&gt;
    &lt;wsdl:documentation&gt;EventWebService&lt;/wsdl:documentation&gt;
    &lt;wsdl:types&gt;
        &lt;xs:schema attributeFormDefault="qualified" elementFormDefault="qualified" targetNamespace="http://services_web"&gt;
            &lt;xs:element name="getEventsForYear"&gt;
                &lt;xs:complexType&gt;
                    &lt;xs:sequence&gt;
                        &lt;xs:element minOccurs="0" name="year" type="xs:int"/&gt;
                        &lt;xs:element minOccurs="0" name="groupId" type="xs:int"/&gt;
                        &lt;xs:element minOccurs="0" name="cacheUniqueifier" nillable="true" type="xs:string"/&gt;
                    &lt;/xs:sequence&gt;
                &lt;/xs:complexType&gt;
            &lt;/xs:element&gt;
            &lt;xs:element name="getEventsForYearResponse"&gt;
                &lt;xs:complexType&gt;
                    &lt;xs:sequence&gt;
                        &lt;xs:element maxOccurs="unbounded" minOccurs="0" name="return" nillable="true" type="xs:string"/&gt;
                    &lt;/xs:sequence&gt;
                &lt;/xs:complexType&gt;
            &lt;/xs:element&gt;
        &lt;/xs:schema&gt;
    &lt;/wsdl:types&gt;
    &lt;wsdl:message name="getEventsForYearRequest"&gt;
        &lt;wsdl:part name="parameters" element="ns:getEventsForYear"/&gt;
    &lt;/wsdl:message&gt;
    &lt;wsdl:message name="getEventsForYearResponse"&gt;
        &lt;wsdl:part name="parameters" element="ns:getEventsForYearResponse"/&gt;
    &lt;/wsdl:message&gt;
    &lt;wsdl:portType name="EventWebServicePortType"&gt;
        &lt;wsdl:operation name="getEventsForYear"&gt;
            &lt;wsdl:input message="ns:getEventsForYearRequest" wsaw:Action="urn:getEventsForYear"/&gt;
            &lt;wsdl:output message="ns:getEventsForYearResponse" wsaw:Action="urn:getEventsForYearResponse"/&gt;
        &lt;/wsdl:operation&gt;
    &lt;/wsdl:portType&gt;
    &lt;wsdl:binding name="EventWebServiceSoap11Binding" type="ns:EventWebServicePortType"&gt;
        &lt;soap:binding transport="http://schemas.xmlsoap.org/soap/http" style="document"/&gt;
        &lt;wsdl:operation name="getEventsForYear"&gt;
            &lt;soap:operation soapAction="urn:getEventsForYear" style="document"/&gt;
            &lt;wsdl:input&gt;
                &lt;soap:body use="literal"/&gt;
            &lt;/wsdl:input&gt;
            &lt;wsdl:output&gt;
                &lt;soap:body use="literal"/&gt;
            &lt;/wsdl:output&gt;
        &lt;/wsdl:operation&gt;
    &lt;/wsdl:binding&gt;
    &lt;wsdl:binding name="EventWebServiceSoap12Binding" type="ns:EventWebServicePortType"&gt;
        &lt;soap12:binding transport="http://schemas.xmlsoap.org/soap/http" style="document"/&gt;
        &lt;wsdl:operation name="getEventsForYear"&gt;
            &lt;soap12:operation soapAction="urn:getEventsForYear" style="document"/&gt;
            &lt;wsdl:input&gt;
                &lt;soap12:body use="literal"/&gt;
            &lt;/wsdl:input&gt;
            &lt;wsdl:output&gt;
                &lt;soap12:body use="literal"/&gt;
            &lt;/wsdl:output&gt;
        &lt;/wsdl:operation&gt;
    &lt;/wsdl:binding&gt;
    &lt;wsdl:binding name="EventWebServiceHttpBinding" type="ns:EventWebServicePortType"&gt;
        &lt;http:binding verb="POST"/&gt;
        &lt;wsdl:operation name="getEventsForYear"&gt;
            &lt;http:operation location="getEventsForYear"/&gt;
            &lt;wsdl:input&gt;
                &lt;mime:content type="application/xml" part="parameters"/&gt;
            &lt;/wsdl:input&gt;
            &lt;wsdl:output&gt;
                &lt;mime:content type="application/xml" part="parameters"/&gt;
            &lt;/wsdl:output&gt;
        &lt;/wsdl:operation&gt;
    &lt;/wsdl:binding&gt;
    &lt;wsdl:service name="EventWebService"&gt;
        &lt;wsdl:port name="EventWebServiceHttpSoap11Endpoint" binding="ns:EventWebServiceSoap11Binding"&gt;
            &lt;soap:address location="http://localhost:8080/axis2/services/EventWebService.EventWebServiceHttpSoap11Endpoint/"/&gt;
        &lt;/wsdl:port&gt;
        &lt;wsdl:port name="EventWebServiceHttpSoap12Endpoint" binding="ns:EventWebServiceSoap12Binding"&gt;
            &lt;soap12:address location="http://localhost:8080/axis2/services/EventWebService.EventWebServiceHttpSoap12Endpoint/"/&gt;
        &lt;/wsdl:port&gt;
        &lt;wsdl:port name="EventWebServiceHttpEndpoint" binding="ns:EventWebServiceHttpBinding"&gt;
            &lt;http:address location="http://localhost:8080/axis2/services/EventWebService.EventWebServiceHttpEndpoint/"/&gt;
        &lt;/wsdl:port&gt;
    &lt;/wsdl:service&gt;
&lt;/wsdl:definitions&gt;
</code></pre>

<div class="caption">
    After the <strong>event_service.aar</strong> file is uploaded to the Axis Admin servlet, the service's WSDL file, shown above, can be seen by visiting 
    the URI http://localhost:8080/axis2/services/EventWebService?wsdl (for example). 
</div>

</div>