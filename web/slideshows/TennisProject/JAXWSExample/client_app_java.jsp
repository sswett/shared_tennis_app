<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-java">
package core;

import java.util.Date;
import services_web.EventWebServiceStub;
import services_web.EventWebServiceStub.GetEventsForYear;
import services_web.EventWebServiceStub.GetEventsForYearResponse;


public class ClientApp 
{

	private static final int YEAR_FOR_EVENTS = 2015;
	private static final int GROUP_ID = 2;   // For dev database, this is "Rockford Area Practice Group"


	public static void main(String[] args) 
	{
		final EventWebServiceStub stub;

		try 
		{
			stub = new EventWebServiceStub();
			final GetEventsForYear myADBBean = new GetEventsForYear();   // ADB = Axis Data Binding

			myADBBean.setYear(YEAR_FOR_EVENTS);
			myADBBean.setGroupId(GROUP_ID);
			final String uniqueifier = String.valueOf(new Date().getTime());
			myADBBean.setCacheUniqueifier(uniqueifier);

			final GetEventsForYearResponse response = stub.getEventsForYear(myADBBean);
			final String[] returnValue = response.get_return();

			System.out.println(String.format("For year: %s and group ID: %s, String[] return value was:\n", YEAR_FOR_EVENTS, GROUP_ID));

			for (final String eventDescr : returnValue)
			{
				System.out.println(eventDescr);
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();   // Nothing more robust needed for this client/test application
		}
	}

}
</code></pre>

<div class="caption">
    ClientApp.java: This stand-alone Java application calls the Event web service to get and display a String[] of tennis events.  It looks
    straightfoward because of the use of the EventWebServiceStub class. (The stub class was generated from the service's WSDL URI 
    by using the Axis2 "wsdl2java.bat" command.)
</div>

</div>