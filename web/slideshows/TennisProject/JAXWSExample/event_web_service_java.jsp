<div class="arch_slideshow_item">

<pre class="line-numbers"><code class="language-java">
package services_web;

import java.util.ArrayList;
import java.util.Calendar;
import beans.EventBean;
import dao.Events;


public class EventWebService 
{
	private final Calendar startDate = Calendar.getInstance();
	private final Calendar endDate = Calendar.getInstance();

	
	public String[] getEventsForYear(int year, int groupId, String cacheUniqueifier)
	{
		startDate.set(year, 0, 1, 0, 0, 0);   // Jan 1, 00:00:00
		endDate.set(year, 11, 31, 23, 59, 59);   // Dec 31, 23:59:59

		final ArrayList&lt;EventBean&gt; events = Events.createInstance(startDate, groupId).getRecords();
		
		final ArrayList&lt;String&gt; resultsList = new ArrayList&lt;String&gt;();
		
		if (events.size() == 0)
		{
			resultsList.add("No events found");
		}
		else
		{
			for (final EventBean event : events)
			{
				if (event.getDate().after(endDate))
				{
					break;
				}
				
				final String eventInfo = event.getDateFormattedForColumnHeader() + " " + event.getDescription();
				resultsList.add(eventInfo);
			}
		}

		return resultsList.toArray(new String[resultsList.size()]);
	}
	
}
</code></pre>

<div class="caption">
    EventWebService.java: Now that the client app has been discussed, let's turn our attention to the web service.  The one method in
    this class, getEventsForYear, returns a String[] of event info. This plain Java class is the starting point for building a 
    "bottom up Java bean" web service. It is used as input to Eclipse's Create Web Service wizard. 
</div>

</div>