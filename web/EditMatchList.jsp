<%@ page import="dao.Players" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="other.SessionHelper" %>
<%@ page import="beans.MatchBean" %>
<%@ page import="dao.Matches" %>
<%@ page import="java.util.ArrayList" %>

<%
    final String div = request.getParameter("div");
    final String fromMatchId = request.getParameter("fromMatchId");

    final boolean returningFromMatchEdit = fromMatchId != null;
%>

<!DOCTYPE html>
<html>

<head>
    <title>Edit Match List</title>

    <link href="css/Admin.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <%-- TODO: remove following later, if not needed --%>
    <script type="text/javascript" src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>

<% if (returningFromMatchEdit) { %>

    <script>

        $(document).ready(function() {
            document.getElementById("m<%= fromMatchId %>").scrollIntoView(true);
        });

    </script>

<% } %>

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="widePageContainer">

        <div id="matchListSection" class="listSection">
            <h2>
                Match List
            </h2>

            <table id="matchListTable" class="listTable">
                <thead>
                <tr>
                    <th class="mlYrCol">Year</th>
                    <th class="mlSessCol">Sess</th>
                    <th class="mlDivCol">Div</th>
                    <th class="mlWeekNoCol">Wk#</th>
                    <th class="mlDateCol">Week Of</th>
                    <th class="mlMatchNoCol">Match#</th>
                    <th class="mlLabelCol">Label</th>
                    <th class="mlPlayoffCol">Playoff</th>
                    <th class="mlActionCol">Actions</th>
                    <th class="mlPlayerCol">Player 1</th>
                    <th class="mlPlayerCol">Player 2</th>
                    <th class="mlNoteCol">Notes</th>
                </tr>
                </thead>

                <tbody>
                <%
                    boolean even = true;
                    int prevWeekNo = -1;

                    ArrayList<MatchBean> matches = div != null && div.length() > 0 ?
                            Matches.createInstance(div).getRecords() :
                            Matches.createInstance().getRecords();

                    final int matchesSize = matches.size();

                    for (int m = 0; m < matchesSize; m++)
                    {
                        final MatchBean match = matches.get(m);
                        final int prevMatchId = m == 0 ? -1 : matches.get(m - 1).getId();
                        final int nextMatchId = m == matchesSize - 1 ? -1 : matches.get(m + 1).getId();
                        even = !even;

                        PlayerBean player1 = null;

                        if (match.getPlayer1Id() > 0)
                        {
                            player1 = Players.createInstance(match.getPlayer1Id()).getRecords().get(0);
                        }

                        PlayerBean player2 = null;

                        if (match.getPlayer2Id() > 0)
                        {
                            player2 = Players.createInstance(match.getPlayer2Id()).getRecords().get(0);
                        }

                        final String player1SpanClass = player1 != null && match.getPlayer1Id() == match.getWinnerId() ?
                                "class=\"mlWinningPlayer\"" : "";

                        final String player2SpanClass = player2 != null && match.getPlayer2Id() == match.getWinnerId() ?
                                "class=\"mlWinningPlayer\"" : "";

                        final String formattedDate = match.getMatchDateFormattedForColumnHeader();

                        boolean printBlankRow = prevWeekNo != -1 && match.getWeekNo() != prevWeekNo;
                        prevWeekNo = match.getWeekNo();

                        if (printBlankRow) { %>
                <tr <%= even ? "class=\"even\"" : "" %>>
                    <td colspan="12">&nbsp;</td>
                </tr>
                <%
                        even = !even;
                    } %>
                <tr id="m<%= match.getId() %>" <%= even ? "class=\"even\"" : "" %>>
                    <td class="mlYrCol"><%= match.getYr() %></td>
                    <td class="mlSessCol"><%= match.getSess() %></td>
                    <td class="mlDivCol"><%= match.getDivision() %></td>
                    <td class="mlWeekNoCol"><%= match.getWeekNo() %></td>
                    <td class="mlDateCol"><%= formattedDate %></td>
                    <td class="mlMatchNoCol"><%= match.getMatchNo() %></td>
                    <td class="mlLabelCol"><%= match.getCustomLabel() %></td>
                    <td class="mlPlayoffCol"><%= match.isPlayoff() ? "Yes" : "" %></td>

                    <td class="mlActionCol">
                        <a href="EditMatch.page?id=<%= match.getId() %>">Edit</a>
                        &nbsp;
                        <a href="EditMatch.page?id=<%= match.getId() %>&delete=true&prevId=<%= prevMatchId %>&nextId=<%= nextMatchId %>">Delete</a>
                    </td>

                    <td class="mlPlayerCol">
                        <% if (player1 != null) { %>
                        <span <%= player1SpanClass %>>
                            <%= player1.getName() %>
                        </span>
                        <% } %>

                        <% if (player1 != null && match.hasPlayer1Note()) { %>
                        <br/>
                        <% } %>

                        <% if (match.hasPlayer1Note()) { %>
                        <%= match.getPlayer1Note() %>
                        <% } %>
                    </td>

                    <td class="mlPlayerCol">
                        <% if (player2 != null) { %>
                        <span <%= player2SpanClass %>>
                            <%= player2.getName() %>
                        </span>
                        <% } %>

                        <% if (player2 != null && match.hasPlayer2Note()) { %>
                        <br/>
                        <% } %>

                        <% if (match.hasPlayer2Note()) { %>
                        <%= match.getPlayer2Note() %>
                        <% } %>
                    </td>

                    <td class="mlNoteCol"><%= match.getNote() %></td>
                </tr>
                <% } %>
                </tbody>
            </table>

        </div>




        <div id="footerSection">
            <div class="footerLinkContainer">
                <a href="AdminMenu.page">Admin Menu</a>
            </div>
        </div>




</div>

</body>

</html>