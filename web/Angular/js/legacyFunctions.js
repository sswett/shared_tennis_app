	function nmBuildMenu(pDropDownMenuWidth, pOpenOnClick, pCloseOnMouseOut )
	{
	    $(".nmMenuBar").buildMenu({
	        /* template:"yourMenuVoiceTemplate",
	        additionalData:"", */
	        menuSelector:".menuContainer",
	        menuWidth:pDropDownMenuWidth,   /* drop-down menu minimum width */
	        openOnRight:false,
	        /* containment:"window", */
	        iconPath:"ico/",
	        hasImages:false,
	        fadeInTime:0,
	        fadeOutTime:200,
	        menuTop:0,
	        menuLeft:0,
	        submenuTop:0,
	        submenuLeft:4,
	        opacity:1,
	        shadow:false,
	        shadowColor:"black",
	        shadowOpacity:.2,
	        openOnClick:pOpenOnClick,
	        closeOnMouseOut:pCloseOnMouseOut,
	        closeAfter:100,
	        minZindex:"auto",
	        hoverIntent:250, //if you use jquery.hoverIntent.js set this to time in milliseconds; 0= false;
	        submenuHoverIntent:0, //if you use jquery.hoverIntent.js set this to time in milliseconds; 0= false;
	    });
	}
