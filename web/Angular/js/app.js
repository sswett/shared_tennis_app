'use strict';

/* App Module */

var angularApp = angular.module('angularApp', [
                                               'ngRoute',
                                               'ngCookies',
                                               'angularAppControllers', 
                                               'angularAppServices'
                                               ]);


angularApp.directive('taBindNavMenu', function() {
    return {
        restrict: 'A',   /* can be 'A' (attribute) or 'E' (element) or 'AE' (both) */

        // Can use "compile" (once) or "link" (every time data is bound to the element).  The format varies.
        // For more info see http://tutorials.jenkov.com/angularjs/custom-directives.html 

        link: function(scope, element, attrs) {
            $(document).ready(function() {
                nmBuildMenu(scope.dropDownMenuWidth, scope.openOnClick, scope.closeOnMouseOut);
            })
        }
    
    };
});


angularApp.config(['$routeProvider',
                   function($routeProvider) {
	$routeProvider.
	when('/contents', {
		templateUrl: 'Angular/partials/contents.html',
		controller: 'ContentsCtrl'
	}).
	when('/players', {
		templateUrl: 'Angular/partials/playerList.html',
		controller: 'PlayerListCtrl'
	}).
	when('/players/:playerId', {
		templateUrl: 'Angular/partials/playerDetails.html',
		controller: 'PlayerDetailsCtrl'
	}).
	when('/cookieTest', {
		templateUrl: 'Angular/partials/cookieTest.html',
		controller: 'CookieTestCtrl'
	}).
	when('/redirectTest', {
		templateUrl: 'Angular/partials/redirectTest.html',
		controller: 'RedirectTestCtrl'
	}).
	when('/clearanceTest', {
		templateUrl: 'Angular/partials/clearanceTest.html',
		controller: 'ClearanceTestCtrl'
	}).
/*
	when('/navMenu', {
		templateUrl: 'Angular/partials/navMenu.html',
		controller: 'NavCtrl'
	}).
*/	
	otherwise({
		redirectTo: '/contents'
	});
}]);


/*
 * NOTE: If I need to provide global handling for requests and responses, Angular uses "HTTP interceptors."  See the
 * following article: http://djds4rce.wordpress.com/2013/08/13/understanding-angular-http-interceptors/
 */ 
