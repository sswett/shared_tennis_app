<%@ page import="other.SessionHelper" %>
<%@ page import="dao.Groups" %>
<%@ page import="beans.GroupsBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="beans.EventBean" %>
<%@ page import="dao.Events" %>
<%@ page import="utils.DateUtils" %>
<%@ page import="beans.EventResponseJoinedDataBean" %>
<%@ page import="dao.EventResponses" %>
<%@ page import="other.EventResponseIdType" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="other.PlayerResponse" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="dao.Players" %>
<%
    final String savedParm = request.getParameter("saved");
    final boolean wasJustSaved = savedParm != null && savedParm.equals("true");
    int yesCount = 0;
    Calendar startDate = DateUtils.getCalendarAtBeginningOfToday();
    ArrayList<PlayerBean> playerRecords = Players.createInstanceForAllUpcomingEvents(startDate).getRecords();
%>

<!DOCTYPE html>
<html>

<head>
    <title>Tennis Mobile - Player All Teams View</title>

    <% if (SessionHelper.isMobileBrowser(request)) { %>
    <meta name="viewport" content="width=device-width" />
    <% } %>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />

    <script src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>

    <script type="text/javascript">

        <% if (wasJustSaved) { %>
        $(document).ready(function() {
            $("#changesSaved").css("display", "block");
        });
        <% } %>

    </script>

    <link href="css/Mobile.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

</head>

<body>
    
    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div class="mobileViewPageContainer">

    <form name="mobileForm" action="MobilePlayerAllTeamsView.page" method="POST">
        <input type="hidden" name="action" value="" />

        <div id="changesSaved">
            Your changes were saved successfully.
        </div>

        <div id="topMsgDiv" class="msgDiv">
            <input type="button" value="Save" onclick="mpvSave()" /> or
            <input type="button" value="Cancel" onclick="mpvCancel()" /> change(s)
        </div>


            <% if (playerRecords.size() == 0) { %>

            <div class="noPlayersOrEventsMsg">
                There are no players to display.
            </div>

            <% } else {
                int selectedPlayerId = SessionHelper.getMobilePlayerViewSelectedPlayer(session);

                if (selectedPlayerId == -1)
                {
                    // Get default player from cookie created during last visit
                    selectedPlayerId = SessionHelper.getDefaultPlayerId(session);
                }

                PlayerBean selectedPlayer = null;

                for (PlayerBean player: playerRecords)
                {
                    if (player.getId() == selectedPlayerId)
                    {
                        selectedPlayer = player;
                        break;
                    }
                }

                if (selectedPlayer == null)
                {
                    selectedPlayer = playerRecords.get(0);
                }

                selectedPlayerId = selectedPlayer.getId();

                ArrayList<EventResponseJoinedDataBean> responseRecords =
                        EventResponses.createInstance(EventResponseIdType.PLAYER, selectedPlayerId, startDate ).
                                getRecords();

                if (responseRecords.size() == 0) { %>

            <div class="noPlayersOrEventsMsg">
                There are no events to display.
            </div>

            <% } else { %>

        <div id="playerChooser">

            <select id="playerSelect" name="playerSelect" onchange="mpvChangePlayer()">
                <%
                    for (PlayerBean player : playerRecords)
                    {
                        final boolean isSelected = player.getId() == selectedPlayerId;
                %>
                <option <%= isSelected ? "selected=\"selected\"" : "" %>
                        value="<%= player.getId() %>" >
                    <%= player.getName() %>
                </option>
                <% }   // for player %>
            </select>

        </div>

        <div id="eventsContainer">

            <table id="eventsTable">

                <%
                    int relativeEventNumber = -1;
                    PlayerResponse currentResponse;
                    String formattedDate;
                    String descrMinusBrTags;

                    for (EventResponseJoinedDataBean eventResponse : responseRecords)
                    {
                        relativeEventNumber++;
                        currentResponse = eventResponse.getPlayerResponse();
                        formattedDate = DateUtils.getDateFormattedForColumnHeader(eventResponse.getEventDate());
                        String descr = eventResponse.getEventDescr().replaceAll("<br/>", " ");
                %>

                <tr>
                    <td class="event">
                        <%= formattedDate %>
                        <br/>
                        <%= descr %>
                    </td>

                    <td class="response allTeamsViewResponse">

                        <%= eventResponse.getGroupShortName() %>

                        <br/>

                        <input type="hidden"
                               name="old_<%= eventResponse.getPlayerId() %>_<%= eventResponse.getEventId() %>"
                               value="<%= currentResponse.name() %>"/>

                        <select class="responseInput"
                                name="new_<%= eventResponse.getPlayerId() %>_<%= eventResponse.getEventId() %>"
                                onchange="mpvWarnAboutUnsaved()">
                            <% for (PlayerResponse playerResponse : PlayerResponse.values())
                            {
                                final boolean isSelected = playerResponse == currentResponse;

                                if (isSelected && playerResponse == PlayerResponse.yes)
                                {
                                    yesCount++;
                                }
                            %>
                            <option <%= isSelected ? "selected=selected" : "" %>
                                    value="<%= playerResponse.name() %>"><%= playerResponse.getLabel() %></option>
                            <% }   // for playerResponse %>
                        </select>

                    </td>
                </tr>

                <%
                    }
                %>
            </table>

        </div>

        <div id="yesCountContainer">
            Yes count: <%= yesCount %>
        </div>

        <div id="bottomMsgDiv" class="msgDiv">
            <input type="button" value="Save" onclick="mpvSave()" /> or
            <input type="button" value="Cancel" onclick="mpvCancel()" /> change(s)
        </div>

        <% } } %>

        <div id="mobileViewButtonsContainer">
            <input type="button" id="optionsButton" value="Options" onclick="mpvOptions()" />
        </div>

        <div id="mobileViewQuote">
            <%= SessionHelper.getQuote() %>
        </div>

    </form>

</div>

</body>

</html>