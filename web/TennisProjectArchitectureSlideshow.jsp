<%@ page import="other.SessionHelper" %>
<%
	final int imgWidth = 840;
	final int imgHeight = 540;
%>
<!DOCTYPE html>
<html>

<head>
    <title>Tennis Project Architecture Slideshow</title>

    <script src="js/jquery-1.10.2.min.js"></script>

    <script src="js/basic-jquery-slider-master/js/bjqs-1.3.min.js"></script>

    <link href="js/basic-jquery-slider-master/bjqs.css" rel="stylesheet">
    <link href="css/Slideshow.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <link href="css/Singles.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">
    
    <link href="css/prism.css" rel="stylesheet" />

    <script>

        <%-- when responsive, the dimensions are the maximums --%>

        <%--
                NOTE: I didn't want screen shot images to shrink responsively, nor did I want the
                      captions or controls to overlay the image.  So I made the slideshow
                      container bigger than the images and turned off the responsive and
                      showcontrols settings.
        --%>

        jQuery(document).ready(function($) {
            $('#my-slideshow').bjqs({
                height : 610,
                width : 900,
                responsive : false,
                hoverpause : false, // pause the slider on hover
                animspeed : 13000, // the delay between each slide
                randomstart : false,
                showcontrols : false,
                automatic : false
            });
        });

    </script>

</head>

<body>

<div id="container">

<div id="my-slideshow">
    <ul class="bjqs">
        <li>
            <img src="slideshows/TennisProject/Architecture/MatchesTable.png<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="Here is an example of a database table schema -- for the singles league 'matches' table."/>
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/matches_java.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/match_bean_java.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/mobile_singles_league_standings_jsp.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/mobile_singles_league_standings_java.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/standings_calc_java.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/tennis_app_js.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/web_xml_filters.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/JsCssImgResponseHeaderFilter_java.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/nav_menu_body_jsp.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/nav_menu_head_jsp.jsp" flush="true" />
        </li>
        <li>
            <img src="slideshows/TennisProject/Architecture/PlayerService.png<%= SessionHelper.getVersionQueryString() %>" 
            width="<%= imgWidth %>" height="<%= imgHeight %>"
                    title="The test page above uses Jersey REST and JAXB on the server.  The Player List is dynamically
                     loaded from the database via a Javascript function call (1 of 3)."/>
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/player_service_java.jsp" flush="true" />
        </li>
        <li>
            <jsp:include page="slideshows/TennisProject/Architecture/test_player_service_jsp.jsp" flush="true" />
        </li>
    </ul>
</div>


</div>


<script src="js/prism.js"></script>

</body>

</html>