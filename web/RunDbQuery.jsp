<%@ page import="other.SessionHelper" %>

<%

%>

<!DOCTYPE html>
<html>

<head>
    <title>Tennis - Run DB Query</title>

    <link href="css/Admin.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <script type="text/javascript" src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>

</head>

<body>

<div id="pageContainer">

    <form name="dbQueryForm" action="RunDbQuery.page" method="POST">

        <textarea name="qry" rows="5" cols="80" autofocus="autofocus">

        </textarea>

        <input type="submit" value="Submit" />

    </form>

</div>

</body>

</html>