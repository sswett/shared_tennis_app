<%@ page import="beans.EventBean" %>
<%@ page import="beans.PlayerBean" %>
<%@ page import="ui.FullSpreadsheetComponent" %>
<%@ page import="other.PlayerResponse" %>
<%@ page import="beans.EventResponseJoinedDataBean" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="other.SessionHelper" %>
<%
    String overrideGroupIdStr = request.getParameter("overrideGroupId");

    if (overrideGroupIdStr != null)
    {
        int overrideGroupId = Integer.parseInt(overrideGroupIdStr);
        SessionHelper.setDefaultGroupId(session, overrideGroupId, request, response);
    }

    FullSpreadsheetComponent component = new FullSpreadsheetComponent(session);
    final String savedParm = request.getParameter("saved");
    final boolean wasJustSaved = savedParm != null && savedParm.equals("true");
    final boolean hasAdminClearance = SessionHelper.hasPassedAdminSecurityClearance(session, request);
%>
<!DOCTYPE html>

<html>

<head>
    <title>Tennis Events Participation</title>

    <%-- production version <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script> --%>

    <script src="js/jquery-1.10.2.min.js"></script>
    
    <jsp:include page="NavMenuHead.jsp" flush="true" />
    
    
    <script src="js/TennisApp.js<%= SessionHelper.getVersionQueryString() %>"></script>

    <script>

        $(document).ready(function() {

            var tableWidth = $("#spreadsheetTable").outerWidth();

            // Page container needs to be at least 670px wide
            $("#spreadsheetPageContainer").css("width", (Math.max(670, tableWidth)) + "px");

            $("td.response").mouseenter(function() {
                ssShowCrossHair(this);
            });

            $("td.response").mouseleave(function() {
                ssHideCrossHairs();
            });

            $(".responseInput").click(function() {
                ssHighlightRow(this);
            });

        })

    </script>

    <link href="css/Spreadsheet.css<%= SessionHelper.getVersionQueryString() %>" rel="stylesheet">

    <jsp:include page="FavIconHeadSection.jsp" flush="true" />

</head>

<body>

    <jsp:include page="NavMenuBody.jsp" flush="true" />

<div id="spreadsheetPageContainer">

<jsp:include page="ChangeGroup.jsp" flush="true">
    <jsp:param name="redirectToPage" value="Spreadsheet.page" />
</jsp:include>


<form name="spreadsheetForm" method="post">

    <div class="topSaveChangesContainer">
        <span class="saveOrCancelButtons">
            <input type="button" value="Save" onclick="document.spreadsheetForm.submit();"/> or
            <input type="button" value="Cancel" onclick="document.spreadsheetForm.reset();ssHideSaveOrCancelButtons()"/>
            change(s) when ready
        </span>
        <span class="savemsg">
        <%= wasJustSaved ? "Your changes were saved successfully." : "" %>
        </span>
    </div>

    <%
        final ArrayList<EventBean> events = component.getEvents().getRecords();
        final ArrayList<PlayerBean> players = component.getPlayers().getRecords();
        final int lastRelativeEventNumber = events.size() - 1;
        int relativeEventNumber;
    %>

    <% if (events.size() == 0) { %>

    <div class="noPlayersOrEventsMsg">
        There are no events for this group/team.
    </div>

    <% } else if (players.size() == 0) { %>

    <div class="noPlayersOrEventsMsg">
        There are no players for this group/team.
    </div>

    <% } else { %>

    <div id="spreadsheetTableContainer">

        <table id="spreadsheetTable">

            <%-- Events in header: --%>
            <tr class="eventDescrRow">
                <th class="ball"></th>
                <%
                    relativeEventNumber = -1;

                    for (EventBean event : events)
                    {
                        relativeEventNumber++;

                        final String edgeCellClass = component.
                                getLeftOrRightEdgeCellClass(relativeEventNumber, lastRelativeEventNumber);

                        final String cellClass = "eventDescr eventHeader " + edgeCellClass;
                %>
                <th class="<%= cellClass %> col<%= relativeEventNumber%>">
                    <%= event.getDateFormattedForColumnHeader() %>
                    <br/>
                    <%= event.getDescription() %>
                </th>
                <% }   // for event %>
            </tr>


            <%  for (PlayerBean player : players) { %>
            <tr class="player_row <%= component.isOddRowNumber() ? "odd" : "even" %>" 
            	title="<%= player.getName() %>">
                <td class="playerName">
                    <a href="/GroupContacts.page?groupId=<%= component.getGroupId() %>&playerId=<%= player.getId() %>" 
                    	title="<%= "Mbr? " + player.getMbrTypeDescr() %>">
                    <%= player.getName() %>
                    </a>
                </td>

                <%
                    ArrayList<EventResponseJoinedDataBean> eventResponses = component.getEventResponses(player.getId()).getRecords();

                    relativeEventNumber = -1;

                    for (EventResponseJoinedDataBean eventResponse : eventResponses)
                    {
                        relativeEventNumber++;
                        final PlayerResponse currentResponse = eventResponse.getPlayerResponse();

                        final String edgeCellClass = component.
                                getLeftOrRightEdgeCellClass(relativeEventNumber, lastRelativeEventNumber);

                        final String cellClass = "response " + edgeCellClass;
                %>
                <td class="<%= cellClass %> col<%= relativeEventNumber%>"
                    id="resp_<%= player.getId() %>_<%= eventResponse.getEventId() %>">

                    <input type="hidden"
                           name="old_<%= player.getId() %>_<%= eventResponse.getEventId() %>"
                           value="<%= currentResponse.name() %>"/>

                    <select class="responseInput e<%= eventResponse.getEventId() %>"
                            name="new_<%= player.getId() %>_<%= eventResponse.getEventId() %>"
                            onchange="ssShowSaveOrCancelButtons()">
                        <% for (PlayerResponse playerResponse : PlayerResponse.values())
                        {
                            final boolean isSelected = playerResponse == currentResponse;
                            component.potentiallyIncrementYesCount(relativeEventNumber, playerResponse, isSelected);
                        %>
                        <option <%= isSelected ? "selected=selected" : "" %>
                                value="<%= playerResponse.name() %>"><%= playerResponse.getLabel() %></option>
                        <% }   // for playerResponse %>
                    </select>
                </td>
                <% }   // for event %>
            </tr>
            <% }   // for player %>


            <tr class="yescount">
                <td class="yescount_label">"Yes" count:</td>
                <%
                    relativeEventNumber = -1;

                    for (EventBean event : events)
                    {
                        relativeEventNumber++;

                        final String edgeCellClass = component.
                                getLeftOrRightEdgeCellClass(relativeEventNumber, lastRelativeEventNumber);

                        final String cellClass = "yescount_cell " + edgeCellClass;
                %>
                <td class="<%= cellClass %> col<%= relativeEventNumber%>">

							<div class="yesCountContent">

								<%-- I tried various alignment techniques, but a table structure worked best. --%>
								<table class="yesCountTable">

									<tr>

										<td><%=component.getYesCount(relativeEventNumber)%></td>

										<% if (hasAdminClearance) { %>
										<td><img class="commButton" src="images/megaphone_24x24.png"
											title="Display communications page"
											onclick="displayCommunicationsPage('?eventId=<%=event.getId()%>')" />
										</td>

										<td><img class="tooLateButton" src="images/clock_24x24.png"
											title="Mark all non-replies as too late" 
											onclick="markTooLate('<%=event.getId()%>', '<%= PlayerResponse.outlate.name() %>')" />
										</td>
										<% } %>

									</tr>

								</table>
							</div>

						</td>
                <%
                	} // for event
                %>
            </tr>


            <%-- Events in footer: --%>
            <tr class="eventDescrRow">
                <th class="racquet"></th>
                <%
                    relativeEventNumber = -1;

                    for (EventBean event : events)
                    {
                        relativeEventNumber++;

                        final String edgeCellClass = component.
                                getLeftOrRightEdgeCellClass(relativeEventNumber, lastRelativeEventNumber);

                        final String cellClass = "eventDescr eventFooter " + edgeCellClass;
                %>
                <th class="<%= cellClass %>  col<%= relativeEventNumber%>">
                    <%= event.getDateFormattedForColumnHeader() %>
                    <br/>
                    <%= event.getDescription() %>
                </th>
                <% }   // for event %>
            </tr>

        </table>

    </div>

    <div class="bottomSaveChangesContainer">
        <span class="saveOrCancelButtons">
            <input type="button" value="Save" onclick="document.spreadsheetForm.submit();"/> or
            <input type="button" value="Cancel" onclick="document.spreadsheetForm.reset();ssHideSaveOrCancelButtons()"/>
            change(s) when ready
        </span>
        <span class="savemsg">
        <%= wasJustSaved ? "Your changes were saved successfully." : "" %>
        </span>
    </div>


    <% } // no events %>

    <div id="creditsArea">
        Portions created by Tanner Swett.  Enhanced by Steve Swett.
    </div>

</form>


</div>

</body>

</html>

<% /* } */   // if (method.equals("POST")) %>