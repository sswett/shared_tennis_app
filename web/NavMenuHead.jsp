<%@ page import="other.SessionHelper" %>

<%
    final boolean isMobile = SessionHelper.isMobileBrowser(request);
	final int dropDownMenuWidth = isMobile ? 80 : 150;
	final boolean openOnClick = isMobile ? true : false;   // desktop: use false
	final boolean closeOnMouseOut = isMobile ? false : true;   // desktop: use true
%>


	<script src="js/jquery.mb.menu-2.9.7/inc/jquery.metadata.js"></script>
	<script src="js/jquery.mb.menu-2.9.7/inc/jquery.hoverIntent.js"></script>
	<script src="js/jquery.mb.menu-2.9.7/inc/mbMenu.js"></script>
	
	<% if (isMobile) { %>
	<link rel="stylesheet" type="text/css" href="js/jquery.mb.menu-2.9.7/css/menu_ss_mobile.css<%= SessionHelper.getVersionQueryString() %>" media="screen" />
	<% } else { %>
	<link rel="stylesheet" type="text/css" href="js/jquery.mb.menu-2.9.7/css/menu_ss_desktop.css<%= SessionHelper.getVersionQueryString() %>" media="screen" />
	<% } %>
	
    <script type="text/javascript">

    $(document).ready(function() {
        nmBuildMenu();
    })

    
	function nmBuildMenu()
	{
	    $(".nmMenuBar").buildMenu({
	        /* template:"yourMenuVoiceTemplate",
	        additionalData:"", */
	        menuSelector:".menuContainer",
	        menuWidth:<%= dropDownMenuWidth %>,   /* drop-down menu minimum width */
	        openOnRight:false,
	        /* containment:"window", */
	        iconPath:"ico/",
	        hasImages:false,
	        fadeInTime:0,
	        fadeOutTime:200,
	        menuTop:0,
	        menuLeft:0,
	        submenuTop:0,
	        submenuLeft:4,
	        opacity:1,
	        shadow:false,
	        shadowColor:"black",
	        shadowOpacity:.2,
	        openOnClick:<%= openOnClick %>,
	        closeOnMouseOut:<%= closeOnMouseOut %>,
	        closeAfter:100,
	        minZindex:"auto",
	        hoverIntent:250, //if you use jquery.hoverIntent.js set this to time in milliseconds; 0= false;
	        submenuHoverIntent:0, //if you use jquery.hoverIntent.js set this to time in milliseconds; 0= false;
	    });
	}
    
    </script>
